// To get all the contacts data
function getContacts(evt, type, load=false) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent_contact");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("nav-link");
  if (load == false) 
  {
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      evt.currentTarget.className += " active";
   }
  document.getElementById(type).style.display = "block";
   
   if (type == 'consultant') {
       allContacts('consultant');
   } else if (type == 'sme') {
       allContacts('smes');
   }
    else if (type == 'bench') {
       allContacts('bench');
   }
   else 
   {
       allContacts('all');
   }
  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(type).style.display = "block";
}


$('body').on('click', '#daily_work_track_edit', function (event) {
  event.preventDefault();
  var id = $(this).data('id');
  var url = `/daily_work_track/${id}/edit`;
  $.get(url, function (data) {
      var data_1 = JSON.parse((data));
      $("#edit_data").append(data_1);
      $('#daily-work-track-edit-modal').modal('show');
  })
});
$('#daily-work-track-edit-modal').on('hidden.bs.modal', function (e) {
  $("#edit_data").empty();
});

function allContacts(mode) {
    //alert(mode);
    $('.' + mode + '_datatable').DataTable().destroy();
    $('.' + mode + '_datatable').DataTable({
         dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
    "order": [[ 0, "desc" ]] ,
      processing: true,
      serverSide: true,
      ajax: "/get_contacts_by_type/" + mode,
      columns: [
          {data: 'id', name: 'id'},
          {data: 'firstname', name: 'firstname'},
          {data: 'email', name: 'email'},
          {data: 'mobile', name: 'mobile'},
          {data: 'consultantType', name: 'consultantType'},
          {data: 'contactType', name: 'contactType'},
          {data: 'technology', name: 'technology'},
          {data: 'workType', name: 'workType'},
          {data: 'skill', name: 'skill'},
          {data: 'status', name: 'status',
          render: function( data) {
            if (data=='1')
                      return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                    else
                      return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                    endif
                }
      },
          {data: 'action', name: 'action', orderable: false, searchable: false},
      ]
  });
}

function searchInterview()
{
    var contactId = $('#contactName').val();
    var SMEId = $('#SMEName').val();
    var startdate = $('#fromDate').val();
    var enddate = $('#toDate').val();
    var created_by = $('#created_by').val();
    $('.interviews_datatable').DataTable().destroy();
    $('.interviews_datatable').DataTable({
         dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
    "order": [[ 2, "desc" ]] ,
      processing: true,
      serverSide: true,
      ajax: {
        'headers': {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        'type': 'POST',
        'url': '/search_interview_list',
        'data': {
            SMEId: SMEId,
            startdate: startdate,
            enddate: enddate,
            contactId: contactId,
            created_by: created_by
        }
      },
      columns: [
          {data: 'id', name: 'id'},
          {data: 'consultantName', name: 'consultantName'},
          {data: 'created_at', name: 'Date',
             render: function( data) {
            var date = new Date(data);
            var month = date.getMonth() + 1;
            return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
          }},
          {data: 'SMEAgreeAmount', name: 'SMEAgreeAmount'},
          {data: 'technology', name: 'technology'},
          {data: 'SMEName', name: 'SMEName'},
          {data: 'Status', name: 'Status'},
          {data: 'paymentStatus', name: 'paymentStatus'},
          {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
}

function searchSME()
{
    var contactId = $('#contactName').val();
    var SMEId = $('#SMEName').val();
    var startdate = $('#fromDate').val();
    var enddate = $('#toDate').val();
    var created_by = $('#created_by').val();
    var work_type = $('#work_type').val();
    $('.smeind_datatable').DataTable().destroy();
    $('.smeind_datatable').DataTable({
         dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
    "order": [[ 2, "desc" ]] ,
      processing: true,
      serverSide: true,
      ajax: {
        'headers': {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        'type': 'POST',
        'url': '/search_sme_list',
        'data': {
            SMEId: SMEId,
            startdate: startdate,
            enddate: enddate,
            contactId: contactId,
            created_by: created_by,
            work_type: work_type
        }
      },
      columns: [
          {data: 'id', name: 'id'},
          {data: 'contactName', name: 'contactName'},
          {data: 'mobile', name: 'mobile'},
          {data: 'created_at', name: 'created_at'},
          {data: 'agreePayment', name: 'agreePayment'},
          {data: 'technology', name: 'technology'},
          {data: 'SMEName', name: 'SMEName'},
          {data: 'action', name: 'action', orderable: false, searchable: false},
      ]
    });
}
function searchSMPayments () {
   var contactId = $('#contactName').val();
    var SMEId = $('#SMEName').val();
    var startdate = $('#fromDate').val();
    var enddate = $('#toDate').val();
    var type = $('#type').val();
    var created_by = $('#created_by').val();
    var payment_status = $('#payment_status').val();
    $('.smepayments_datatable').DataTable().destroy();
    $('.smepayments_datatable').DataTable({
         dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
    "order": [[ 2, "desc" ]] ,
      processing: true,
      serverSide: true,
      ajax: {
        'headers': {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        'type': 'POST',
        'url': '/search_smpayments_list',
        'data': {
            SMEId: SMEId,
            startdate: startdate,
            enddate: enddate,
            contactId: contactId,
            type: type,
            created_by: created_by,
            payment_status: payment_status
        }
      },
      columns: [
        {data: 'id', name: 'id'},
        {data: 'contactId', name: 'contactId'},
        {data: 'date', name: 'date'},
        {data: 'agreeamount', name: 'agreeamount'},
        {data: 'status', name: 'status'},
        {data: 'type', name: 'type'},
        {data: 'employee', name: 'employee'},
        {data: 'SMENAME', name: 'SMENAME'},
                  {data: 'mobile', name: 'mobile'},
        {data: 'action', name: 'action', orderable: false, searchable: false},
    ]
    }); 
}

function resetFields(type) {
    $('#contactName').val('');
    $('#SMEName').val('');
    $('#fromDate').val('');
    $('#toDate').val('');
    $('#created_by').val('');
    if (type == 'sme') {
        searchSME();
    } else if (type == 'smpayments') {
        $('#type').val('');
         $('#payment_status').val('');
        searchSMPayments();
    } else {
       searchInterview(); 
    }
}


$(document).ready(function () {
    var table = $('.h1b_datatable').DataTable({
        dom: 'Bfrtip',
        columnDefs: [
            {
                targets: 0,
                className: 'noVis'
            }
        ],
        buttons: [
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ],
      "order": [[ 0, "desc" ]] ,
        processing: true,
        serverSide: true,
        ajax: "/h1b",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'first_name', name: 'first_name'},
            {data: 'email', name: 'email'},
            {data: 'case_worker', name: 'case_worker'},
            {data: 'years_exp', name: 'years_exp'},
            {data: 'uscis_cap', name: 'uscis_cap'},
            {data: 'status', name: 'status',
              render: function( data) {
                if (data=='1')
                          return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                        else
                          return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                        endif
                    }
          },
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
    table = $('.daily_work_track_datatable').DataTable({
        dom: 'Bfrtip',
        columnDefs: [
            {
                targets: 0,
                className: 'noVis'
            }
        ],
        buttons: [
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ],
      "order": [[ 0, "desc" ]] ,
        processing: true,
        serverSide: true,
        ajax: "/daily_work_track",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'calls_count', name: 'calls_count'},
            {data: 'employee', name: 'employee'},
            {data: 'track_file', name: 'track_file'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        rowCallback: function( row, data, index ) {
            if (data.track_file != '') {
                $('td:eq(3)', row).html( '<a href="daily_work_track/download_report/' + data.id + '" targer="_blank">Download</a>' );
            }
        }
    });
});