  $(document).ready(function () {
      tableins.draw();
    
   
    
    // Redraw the table based on the custom input
    $('.buttoninstallment').bind("click", function(){
        tableins.draw();
    });
    tabletransaction.draw();
    
    
    
    // Redraw the table based on the custom input
    $('.buttoninstallment1').bind("click", function(){
        tabletransaction.draw();
    });
    tpendingrentaltransactions.draw();
    
    // Redraw the table based on the custom input
    $('.buttoninstallment').bind("click", function(){
        tpendingrentaltransactions.draw();
    });
    
      $('body').on('click', '.editProduct', function (event) {
          
          var type = $(this).attr('videotype');
        //  alert(type);
          $.get("/paymentcriterialist/"+ type , function (data) {
            $('#modelHeading').html("PaymentCriteria");
            $('#ajaxModel').modal('show');
            console.log(data);
            for(var i=0;i<data.length;i++)
            {
                $(table).find('tbody').append("<tr><td>aaaa</td></tr>");
            }
        })
          
      });
        //customer edit
        $('body').on('click', '#customer_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/customer/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#customer-edit-modal').modal('show');
          })
        });
        $('#customer-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //customer view
        $('body').on('click', '#customer_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/customer/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#customer-view-modal').modal('show');
          })
        });
        $('#customer-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        //genre edit
        $('body').on('click', '#genre_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/genre/${id}/edit`;
          $.get(url, function (data) {
              $('#genre-edit-modal').modal('show');
              $('#edit_id').val(data.data.id);
              $('#edit_title').val(data.data.title);
              $('#edit_icon').val(data.data.icon);
              $('#edit_publication').val(data.data.publication);
              if (data.data.description) {
                $("#edit_description").val($("#edit_description").val() + data.data.description);
              }
              if (data.data.status==1) {
                $("#edit_status").prop("checked", true);
              }
          })
        });
        //genre view
        $('body').on('click', '#genre_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/genre/${id}/edit`;
          $.get(url, function (data) {
              $('#genre-view-modal').modal('show');
              $('#view_id').val(data.data.id);
              $('#view_title').val(data.data.title);
              $('#view_icon').val(data.data.icon);
              $('#view_publication').val(data.data.publication);
              if (data.data.description) {
                $("#view_description").val($("#view_description").val() + data.data.description);
              }
              if (data.data.status==1) {
                $("#view_status").prop("checked", true);
              }
          })
        });
        //artist edit
        $('body').on('click', '#artist_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/artist/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#artist-edit-modal').modal('show');
          })
        });
        $('#artist-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //artist view
        $('body').on('click', '#artist_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/artist/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#artist-view-modal').modal('show');
          })
        });
        $('#artist-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });

         //employee edit
         $('body').on('click', '#vemployee_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/vemployee/${id}/edit`;
          $.get(url, function (data) {
              $('#vemployee-edit-modal').modal('show');
              $('#edit_id').val(data.data.id);
              $('#admin_id').val(data.data.admin_id);
              $('#edit_first_name').val(data.data.first_name);
              $('#edit_middle_name').val(data.data.middle_name);
              $('#edit_last_name').val(data.data.last_name);
              $('#edit_contact').val(data.data.contact);
              $('#edit_email').val(data.data.email);
              $('#edit_adhar').val(data.data.adhar);
              $('#edit_voter').val(data.data.voter);
              $('#edit_account_number').val(data.data.account_number);
              $('#edit_ifsc').val(data.data.ifsc);
              $('#edit_password').val(data.data.password);
              if (data.data.status==1) {
                $("#edit_status").prop("checked", true);
              }
          })
        });
           //employee edit
           $('body').on('click', '#vemployee_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/vemployee/${id}/edit`;
            $.get(url, function (data) {
                $('#vemployee-view-modal').modal('show');
                $('#view_id').val(data.data.id);
                $('#view_first_name').val(data.data.first_name);
                $('#view_middle_name').val(data.data.middle_name);
                $('#view_last_name').val(data.data.last_name);
                $('#view_contact').val(data.data.contact);
                $('#view_email').val(data.data.email);
                $('#view_adhar').val(data.data.adhar);
                $('#view_voter').val(data.data.voter);
                $('#view_account_number').val(data.data.account_number);
                $('#view_ifsc').val(data.data.ifsc);
                $('#view_password').val(data.data.password);
                if (data.data.status==1) {
                  $("#view_status").prop("checked", true);
                }
            })
          });
          
           $('body').on('click', '#employee_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/employee/${id}/edit`;
          $.get(url, function (data) {
              $('#employee-edit-modal').modal('show');
              $('#edit_id').val(data.data.id);
              $('#admin_id').val(data.data.admin_id);
              $('#edit_first_name').val(data.data.first_name);
              $('#edit_middle_name').val(data.data.middle_name);
              $('#edit_last_name').val(data.data.last_name);
              $('#edit_contact').val(data.data.contact);
              $('#edit_email').val(data.data.email);
              $('#edit_adhar').val(data.data.adhar);
              $('#edit_voter').val(data.data.voter);
              $('#edit_account_number').val(data.data.account_number);
              $('#edit_ifsc').val(data.data.ifsc);
              $('#edit_password').val(data.data.password);
              if (data.data.status==1) {
                $("#edit_status").prop("checked", true);
              }
          })
        });
           //employee edit
           $('body').on('click', '#employee_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/employee/${id}/edit`;
            $.get(url, function (data) {
                $('#employee-view-modal').modal('show');
                $('#view_id').val(data.data.id);
                $('#view_first_name').val(data.data.first_name);
                $('#view_middle_name').val(data.data.middle_name);
                $('#view_last_name').val(data.data.last_name);
                $('#view_contact').val(data.data.contact);
                $('#view_email').val(data.data.email);
                $('#view_adhar').val(data.data.adhar);
                $('#view_voter').val(data.data.voter);
                $('#view_account_number').val(data.data.account_number);
                $('#view_ifsc').val(data.data.ifsc);
                $('#view_password').val(data.data.password);
                if (data.data.status==1) {
                  $("#view_status").prop("checked", true);
                }
            })
          });
          //contentapprover edit
         $('body').on('click', '#contentapprover_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/contentapprover/${id}/edit`;
          $.get(url, function (data) {
              $('#contentapprover-edit-modal').modal('show');
              $('#edit_id').val(data.data.id);
              $('#admin_id').val(data.data.admin_id);
              $('#edit_first_name').val(data.data.first_name);
              $('#edit_middle_name').val(data.data.middle_name);
              $('#edit_last_name').val(data.data.last_name);
              $('#edit_contact').val(data.data.contact);
              $('#edit_levels').val(data.data.levels);
              $('#edit_email').val(data.data.email);
              $('#edit_adhar').val(data.data.adhar);
              $('#edit_voter').val(data.data.voter);
              $('#edit_account_number').val(data.data.account_number);
              $('#edit_ifsc').val(data.data.ifsc);
              $('#edit_password').val(data.data.password);
              if (data.data.status==1) {
                $("#edit_status").prop("checked", true);
              }
          })
        });
           //contentapprover view
           $('body').on('click', '#contentapprover_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/contentapprover/${id}/edit`;
            $.get(url, function (data) {
                $('#contentapprover-view-modal').modal('show');
                $('#view_id').val(data.data.id);
                $('#view_first_name').val(data.data.first_name);
                $('#view_middle_name').val(data.data.middle_name);
                $('#view_last_name').val(data.data.last_name);
                $('#view_contact').val(data.data.contact);
                $('#view_email').val(data.data.email);
                $('#view_adhar').val(data.data.adhar);
                $('#view_voter').val(data.data.voter);
                $('#view_account_number').val(data.data.account_number);
                $('#view_ifsc').val(data.data.ifsc);
                $('#view_password').val(data.data.password);
                if (data.data.status==1) {
                  $("#view_status").prop("checked", true);
                }
            })
          });//financialteam edit
          $('body').on('click', '#financialteam_edit', function (event) {
           event.preventDefault();
           var id = $(this).data('id');
           var url = `/financialteam/${id}/edit`;
           $.get(url, function (data) {
               $('#financialteam-edit-modal').modal('show');
               $('#edit_id').val(data.data.id);
               $('#admin_id').val(data.data.admin_id);
               $('#edit_first_name').val(data.data.first_name);
               $('#edit_middle_name').val(data.data.middle_name);
               $('#edit_last_name').val(data.data.last_name);
               $('#edit_contact').val(data.data.contact);
               $('#edit_email').val(data.data.email);
               $('#edit_adhar').val(data.data.adhar);
               $('#edit_voter').val(data.data.voter);
               $('#edit_account_number').val(data.data.account_number);
               $('#edit_ifsc').val(data.data.ifsc);
               $('#edit_password').val(data.data.password);
               if (data.data.status==1) {
                 $("#edit_status").prop("checked", true);
               }
           })
         });
            //financialteam view
            $('body').on('click', '#financialteam_view', function (event) {
             event.preventDefault();
             var id = $(this).data('id');
             var url = `/financialteam/${id}/edit`;
             $.get(url, function (data) {
                 $('#financialteam-view-modal').modal('show');
                 $('#view_id').val(data.data.id);
                 $('#view_first_name').val(data.data.first_name);
                 $('#view_middle_name').val(data.data.middle_name);
                 $('#view_last_name').val(data.data.last_name);
                 $('#view_contact').val(data.data.contact);
                 $('#view_email').val(data.data.email);
                 $('#view_adhar').val(data.data.adhar);
                 $('#view_voter').val(data.data.voter);
                 $('#view_account_number').val(data.data.account_number);
                 $('#view_ifsc').val(data.data.ifsc);
                 $('#view_password').val(data.data.password);
                 if (data.data.status==1) {
                   $("#view_status").prop("checked", true);
                 }
             })
           });

           //studio edit
        $('body').on('click', '#studio_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/studio/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#studio-edit-modal').modal('show');
          })
        });
        $('#studio-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //studio view
        $('body').on('click', '#studio_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/studio/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#studio-view-modal').modal('show');
          })
        });
        $('#studio-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });


        //album edit
        $('body').on('click', '#Skills_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/skills/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#album-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        
         $('body').on('click', '#contacttype_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/contacttype/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#contacttype-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#contacttype-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
         $('body').on('click', '#h1bcomments_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/h1bcommunications/${id}/h1bcommentsedit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#h1bcomments-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#h1bcomments-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        $('body').on('click', '#interviewtools_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/interviewtools/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#interviewtools-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#interviewtools-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        
        
         $('body').on('click', '#worktype_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/worktype/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#worktype-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#worktype-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        $('body').on('click', '#sme_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/sme/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#sme-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#sme-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        $('body').on('click', '#interviews_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/interviews/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#interviews-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#interviews-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        
         $('body').on('click', '#installments_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var sme = $(this).data('ins');
         // alert(sme);
          var url = `/installment/${id}/edit/${sme}`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#installments-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#installments-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
       
        
        $('body').on('click', '#statustype_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/statustype/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#statustype-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#statustype-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        
         $('body').on('click', '#location_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/location/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#location-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#location-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        
         $('body').on('click', '#states_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/states/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#states-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#states-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
         
         $('body').on('click', '#consultanttype_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/consultanttype/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#consultanttype-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#consultanttype-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        
         $('body').on('click', '#payment_status_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/payment_status/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#payment_status-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#payment_status-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        $('body').on('click', '#caseworker_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/caseworker/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#caseworker-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#caseworker-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
         
         $('body').on('click', '#accounttype_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/accounttype/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#accounttype-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#accounttype-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
        
        $('body').on('click', '#transactions_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#transactions-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#transactions-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
         
         $('body').on('click', '#paymenttype_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/paymenttype/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#paymenttype-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#paymenttype-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
         
         $('body').on('click', '#technology_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/technology/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#technology-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        
        $('#technology-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        
       /* $('body').on('click', '#paymentcriteria_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/paymentcriteria/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#paymentcriteriaedit_data").append(data);
              $('#paymentcriteria-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });*/
        $('#paymentcriteria-edit-modal').on('hidden.bs.modal', function (e) {
          $("#paymentcriteriaedit_data").empty();
        });
        
        $('body').on('click', '#paymentcriteria_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/paymentcriteria/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#paymentcriteriaedit_data").append(data);
              $('#paymentcriteria-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#paymentcriteria-edit-modal').on('hidden.bs.modal', function (e) {
          $("#paymentcriteriaedit_data").empty();
        });
        
         $('body').on('click', '#payperviewbounce_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/payperviewbounce/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#payperviewbounceedit_data").append(data);
              $('#payperviewbounce-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#payperviewbounce-edit-modal').on('hidden.bs.modal', function (e) {
          $("#payperviewbounceedit_data").empty();
        });
        //album view
        $('body').on('click', '#Skills_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/skills/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#album-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#album-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
         $('body').on('click', '#contacttype_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/contacttype/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#contacttype-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#contacttype-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
        
        $('body').on('click', '#interviewtools_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/interviewtools/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#interviewtools-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#interviewtools-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
        
        $('body').on('click', '#worktype_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/worktype/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#worktype-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#worktype-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });

     $('body').on('click', '#statustype_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/statustype/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#statustype-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#statustype-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });

    $('body').on('click', '#location_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/location/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#location-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#location-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
         $('body').on('click', '#states_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/states/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#states-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#states-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
        
        
        $('body').on('click', '#consultanttype_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/consultanttype/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#consultanttype-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#consultanttype-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
        
        $('body').on('click', '#payment_status_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/payment_status/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#payment_status-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#payment_status-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
        $('body').on('click', '#caseworker_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/caseworker/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#caseworker-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#caseworker-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
        $('body').on('click', '#accounttype_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/accounttype/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#accounttype-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#accounttype-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        
        
         $('body').on('click', '#transactions_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#transactions-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#transactions-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
       
        
         $('body').on('click', '#paymenttype_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/paymenttype/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#paymenttype-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#paymenttype-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });


     $('body').on('click', '#technology_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/technology/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#technology-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#technology-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });



    $('body').on('click', '#paymentcriteria_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/paymentcriteria/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#paymentcriteriaview").append(data);
             $('#paymentcriteria-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#paymentcriteria-view-modal').on('hidden.bs.modal', function (e) {
          $("#paymentcriteriaview").empty();
        });
        
        
    $('body').on('click', '#payperviewbounce_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/payperviewbounce/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#payperviewbounceview").append(data);
              $('#payperviewbounce-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#payperviewbounce-view-modal').on('hidden.bs.modal', function (e) {
          $("#payperviewbounceview").empty();
        });
        //musicgenre edit
        $('body').on('click', '#musicgenre_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/musicgenre/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#musicgenre-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#musicgenre-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //musicgenre view
        $('body').on('click', '#musicgenre_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/musicgenre/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#musicgenre-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#musicgenre-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });

          //musicartist edit
          $('body').on('click', '#musicartist_edit', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/musicartist/${id}/edit`;
            $.get(url, function (data) {
                var data = JSON.parse((data));
                $("#edit_data").append(data);
                $('#musicartist-edit-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
            })
          });
          $('#musicartist-edit-modal').on('hidden.bs.modal', function (e) {
            $("#edit_data").empty();
          });
          //musicartist view
          $('body').on('click', '#musicartist_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/musicartist/${id}`;
            $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#view").append(data);
                $('#musicartist-view-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
            })
          });
          $('#musicartist-view-modal').on('hidden.bs.modal', function (e) {
            $("#view").empty();
          });

           //mp3 edit
           $('body').on('click', '#mp3_edit', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/mp3/${id}/edit`;
            $.get(url, function (data) {
                var data = JSON.parse((data));
                $("#edit_data").append(data);
                $('#mp3-edit-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
                new Choices('#album', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#genre', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#artist', {
                  removeItemButton: true,
                  placeholder:true
                });
            })
          });
          $('#mp3-edit-modal').on('hidden.bs.modal', function (e) {
            $("#edit_data").empty();
          });
          //mp3 view
          $('body').on('click', '#mp3_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/mp3/${id}`;
            $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#view").append(data);
                $('#mp3-view-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
                new Choices('#album', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#genre', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#artist', {
                  removeItemButton: true,
                  placeholder:true
                });
            })
          });
          $('#mp3-view-modal').on('hidden.bs.modal', function (e) {
            $("#view").empty();
          });

           //pending movie edit
         $('body').on('click', '#pendingmovie_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/pendingmovies/${id}/editcopy`;
          $.get(url, function (data) {
              $('#pendingmovie-edit-modal').modal('show');
              $('#id').val(data.data.id);
              $('#name').val(data.data.name);
              $('#amount_required').val(data.data.amount_required);
              $('#amount_given').val(data.data.amount_given);
              $('#studio').val(data.data.studio_name);
              $('#studioid').val(data.data.studio);
              $('#expiry_date').val(data.data.expiry_date);
              $('#delete').val(data.data.delete);
          })
        });
        
         $('body').on('click', '#movieapproval_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/pendingmovies/${id}/movieapproval`;
          $.get(url, function (data) {
              $('#pendingmovieapproval-edit-modal').modal('show');
              $('#id').val(data.data.id);
              $('#name').val(data.data.name);
              $('#studio').val(data.data.studioname);
              $('#expiry_date').val(data.data.expiryDate);
              $('#delete').val(data.data.deletestatus);
          })
        });
        
        $('body').on('click', '#movieapproval_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/pendingmovies/${id}/movieapproval`;
          $.get(url, function (data) {
              $('#pendingmovieapproval-view-modal').modal('show');
              $('#id').val(data.data.id);
              $('#view_name').val(data.data.name);
              $('#view_studio').val(data.data.studioname);
              $('#view_expiry_date').val(data.data.expiryDate);
              $('#view_status').val(data.data.status);
          })
        });
           //pendingmovie view
           $('body').on('click', '#pendingmovie_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/pendingmovies/${id}/edit`;
            $.get(url, function (data) {
              $('#pendingmovie-view-modal').modal('show');
               $('#view_name').val(data.data.name);
              $('#view_amount_required').val(data.data.amount_required);
              $('#view_amount_given').val(data.data.amount_given);
              $('#view_studio').val(data.data.studio_name);
              $('#view_expiry_date').val(data.data.expiry_date);
                $('#view_delete').val(data.data.delete);
            })
          })
           //transaction movie edit
         $('body').on('click', '#transaction_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#transactions-edit-modal').modal('show');

          })
        });
         $('body').on('click', '#termscondition_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = 'term/'+id;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#transactions-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });

          })
        });
        $('body').on('click', '#transaction_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#edit_data").append(data);
            $('#transactions-edit-modal').modal('show');
          })
        })
        $('#transactions-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });

    $('body').on('click', '#earrings_edit', function (event) {
      event.preventDefault();
      var id = $(this).data('id');
      var url = `/studios/${id}/editearring`;
      $.get(url, function (data) {
          var data = JSON.parse((data));
          $("#edit_data").append(data);
          $('#earringsdata-edit-modal').modal('show');

      })
    });
    $('#earringsdata-edit-modal').on('hidden.bs.modal', function (e) {
      $("#edit_data").empty();
    });
          $('body').on('click', '#installment_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}/editinstallment`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#installment-edit-modal').modal('show');

          })
        });
        $('body').on('click', '#installment_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}/viewinstallment`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#edit_data").append(data);
            $('#installment-edit-modal').modal('show');
          })
        })
        $('#installment-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });


        //common delete
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('body').on('click', '.delete-confirm', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        const redirect = $(this).data("url");
        const method = $(this).data("method");
        swal({
            title: 'Are you sure?',
            text: 'This record and it`s details will be permanantly deleted!',
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
          if (value) {
            $.ajax({
              url: url,
              type: method,
              success: function(result) {
                window.location.href = redirect;
              },
              error: function (jqXHR, textStatus, errorThrown) {
               console.log(errorThrown);
              }
          });
          }

        });
      });
      
      $('body').on('click', '.studiodelete-confirm', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        const redirect = $(this).data("url");
        swal({
            title: 'Are you sure?',
            text: 'This record and it`s details will be permanantly deleted!',
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
          if (value) {
            $.ajax({
              url: url,
              type: 'GET',
              success: function(result) {
                window.location.href = redirect;
              },
              error: function (jqXHR, textStatus, errorThrown) {
               console.log(errorThrown);
              }
          });
          }

        });
      });
      
      //remove modal data on hide
      $('.editmodal').on('hidden.bs.modal', function (e) {
        $(this)
          .find("input,textarea,select")
            .val('')
            .end()
          .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
        });
      //creating slug
      $('body').on('keyup change', '#title', function (event) {
      var Text = $(this).val();
      Text = Text.toLowerCase();
      Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
      $("#slug").val(Text);
      });
      $('body').on('keyup change', '#edit_title', function (event) {
      var Text = $(this).val();
      Text = Text.toLowerCase();
      Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
      $("#edit_slug").val(Text);
      });

      $(function () {
        //customers table
        var table = $('.customer_datatable').DataTable({
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/customer",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'status', name: 'status',
                  render: function( data) {
                    if (data=='1')
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                            else
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                            endif
                        }
              },
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        
        var table = $('.documents_datatable').DataTable({
             dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/documents/"+ $('#id').val(),
            columns: [
                {data: 'id', name: 'id'},
               // {data: 'document_id', name: 'document_id'},
                {data: 'document_id', name: 'document_id',
                    render: function( data) {
                    if (data=='1')
                              return 'Petitioner Document';
                    else if (data=='2')
                              return 'Benefecary Document';
                    else if (data=='3')
                              return 'Dependent Document';
                    else if (data=='4')
                              return 'Recipt';
                    endif
                    }
                },
                 {data: 'document', name: 'document',
                          
                           render: function( data, type, full, meta ) {
                        return '<a href=/'+data+'> Link</a>';
                      }
                      },
               // {data: 'document', name: 'document'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        
        var table = $('.communications_datatable').DataTable({
             dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/communications/"+ $('#id').val(),
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'comments', name: 'comments'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        
         var table = $('.installments_datatable').DataTable({
             dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/installments/"+ $('#contactId').val(),
            columns: [
                {data: 'id', name: 'id'},
                {data: 'consultantName', name: 'consultantName'},
                {data: 'date', name: 'date'},
                {data: 'amount', name: 'amount'},
                {data: 'paidStatus', name: 'paidStatus'},
                {data: 'technology', name: 'technology'},
                // {data: 'caseworker', name: 'caseworker'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        
        
         var table = $('.smedetailsview_datatable').DataTable({
             dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/smedetailsview/"+ $('#contactId').val(),
            columns: [
                {data: 'id', name: 'id'},
                {data: 'contactId', name: 'contactId'},
                {data: 'consultantName', name: 'consultantName'},
                {data: 'date', name: 'date'},
                {data: 'agreeamount', name: 'agreeamount'},
                 {data: 'status', name: 'status',
                  render: function( data) {
                    if (data=='1')
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Paid<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                            else
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">Un-Paid<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                            endif
                        }
                    
                 },
               {data: 'type', name: 'type'},
                {data: 'employee', name: 'employee'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        
        var table = $('.smepayments_datatable').DataTable({
            dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/smepayments",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'contactId', name: 'contactId'},
                {data: 'date', name: 'date'},
                {data: 'agreeamount', name: 'agreeamount'},
                 {data: 'status', name: 'status'},
                 {data: 'type', name: 'type'},
                {data: 'employee', name: 'employee'},
                 {data: 'SMENAME', name: 'SMENAME'},
                  {data: 'mobile', name: 'mobile'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
       
       
        var table = $('.copyrightmovie_datatable').DataTable({
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/approvalcopyrightindex",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'studioname', name: 'studioname'},
                {data: 'expiryDate', name: 'expiryDate'},
                {data: 'status', name: 'status',
                  render: function( data) {
                    if (data=='1')
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                            else
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                            endif
                        }
              },
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        var table = $('.contact_us_datatable').DataTable({
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/contactus",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                 {data: 'address', name: 'address'},
                /*{data: 'status', name: 'status',
                  render: function( data) {
                    if (data=='1')
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                            else
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                            endif
                        }
              },
                {data: 'action', name: 'action', orderable: false, searchable: false},*/
            ]
        });
         //genre table
         var table = $('.genre_datatable').DataTable({
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/genre",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
             /*   {data: 'icon', name: 'icon'},*/
                {data: 'status', name: 'status',
                  render: function( data) {
                    if (data=='1')
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                            else
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                            endif
                        }
              },
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
          //genre table
          var table = $('.artist_datatable').DataTable({
            "order": [[ 0, "desc" ]] ,
              processing: true,
              serverSide: true,
              ajax: "/artist",
              columns: [
                  {data: 'id', name: 'id'},
                  {data: 'image', name: 'image',
                    render: function (data) {
                      return '<img src="' + data + '" width="100" height="100">'; 
                    }
                  },
                  {data: 'title', name: 'title'},
                 /* {data: 'number', name: 'number'},*/
                  {data: 'type', name: 'type'},
                  {data: 'status', name: 'status',
                    render: function( data) {
                      if (data=='1')
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                              else
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                              endif
                          }
                },
                  {data: 'action', name: 'action', orderable: false, searchable: false},
              ]
          });
          //employee table
          var table = $('.employee_datatable').DataTable({
            "order": [[ 0, "desc" ]] ,
              processing: true,
              serverSide: true,
              ajax: "/employee",
              columns: [
                  {data: 'id', name: 'id'},
                  {data: 'first_name', name: 'first_name'},
                  // {data: 'last_name', name: 'last_name'},
                  {data: 'contact', name: 'contact'},
                  {data: 'email', name: 'email'},
                   {data: 'password', name: 'password'},
                  {data: 'status', name: 'status',
                    render: function( data) {
                      if (data=='1')
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                              else
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                              endif
                          }
                },
                  {data: 'action', name: 'action', orderable: false, searchable: false},
              ]
          });
          
          var table = $('.vemployee_datatable').DataTable({
            "order": [[ 0, "desc" ]] ,
              processing: true,
              serverSide: true,
              ajax: "/vemployee",
              columns: [
                  {data: 'id', name: 'id'},
                  {data: 'first_name', name: 'first_name'},
                  // {data: 'last_name', name: 'last_name'},
                  {data: 'contact', name: 'contact'},
                  {data: 'email', name: 'email'},
                   {data: 'password', name: 'password'},
                  {data: 'status', name: 'status',
                    render: function( data) {
                      if (data=='1')
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                              else
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                              endif
                          }
                },
                  {data: 'action', name: 'action', orderable: false, searchable: false},
              ]
          });
             //contentapprover table
             var table = $('.contentapprover_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/contentapprover",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'first_name', name: 'first_name'},
                    // {data: 'last_name', name: 'last_name'},
                    {data: 'contact', name: 'contact'},
                    {data: 'email', name: 'email'},
                       {data: 'password', name: 'password'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            //financialteam table
            var table = $('.financialteam_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/financialteam",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'first_name', name: 'first_name'},
                    // {data: 'last_name', name: 'last_name'},
                    {data: 'contact', name: 'contact'},
                    {data: 'email', name: 'email'},
                     {data: 'password', name: 'password'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
             //studio table
             var table = $('.studio_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/studio",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'studio_name', name: 'studio_name'},
                    // {data: 'last_name', name: 'last_name'},
                    {data: 'contact', name: 'contact'},
                  
                    {data: 'email', name: 'email'},
                       {data: 'password', name: 'password'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
              var table = $('.studio_payment_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/payment",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'studio_name', name: 'studio_name'},
                    // {data: 'last_name', name: 'last_name'},
                    {data: 'account_number', name: 'account_number'},
                    {data: 'ifsc', name: 'ifsc'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
              //movie table
              var table = $('.Analysis_movie_datatable').DataTable({
                "order": [[ 2, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/analysis/0/0/0/0",
                  columns: [
                      {data: 'poster', name: 'poster',
                          
                           render: function( data, type, full, meta ) {
                        return '<div class="tableimg"><img src=/'+data+'></img></div>';
                      }
                      },
                      {data: 'name', name: 'name'},
                      // {data: 'actors', name: 'actors'},
                      {data: 'userCount', name: 'userCount'},
                     {data: 'citizenView', name: 'citizenView'},
                     {data: 'noncitizenView', name: 'noncitizenView'},
                      {data: 'view', name: 'view'},
                      {data: 'watch_time', name: 'watch_time'},
                     // {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              var table = $('.studiologs_datatable').DataTable({
                 "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/studiologs",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'poster', name: 'poster',
                          
                           render: function( data, type, full, meta ) {
                        return '<div class="tableimg"><img src=/'+data+'></img></div>';
                      }
                      },
                      {data: 'moviename', name: 'moviename'},
                      // {data: 'actors', name: 'actors'},
                      {data: 'description', name: 'description'},
                      {data: 'studio_name', name: 'studio_name'},
                     // {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
               var table = $('.worklist_datatable').DataTable({
                 "order": [[3, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/worklistdata",
                  columns: [
                      {data: 'id', name: 'id'},
                   //  {data: 'name', name: 'name'},
                      {data: 'studioName', name: 'studioName'},
                      // {data: 'actors', name: 'actors'},
                      {data: 'paidamount', name: 'paidamount'},
                      {data: 'unpaidamount', name: 'unpaidamount'},
                       {data: 'approvedamount', name: 'approvedamount'},
                     // {data: 'action', name: 'action', orderable: false, searchable: false},
                     {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              var url = window.location.pathname;
              
              var table = $('.worklisttotal_datatable').DataTable({
                 "order": [[3, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/worklistdatatotal/" +  url.substring(url.lastIndexOf('/') + 1),
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                      {data: 'studioName', name: 'studioName'},
                      // {data: 'actors', name: 'actors'},
                      {data: 'paidamount', name: 'paidamount'},
                      {data: 'unpaidamount', name: 'unpaidamount'},
                       {data: 'approvedamount', name: 'approvedamount'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                    // {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
               var table = $('.reach_movie_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/reach/0/0",
                  columns: [
                      {data: 'poster', name: 'poster',
                          
                           render: function( data, type, full, meta ) {
                        return '<div class="tableimg"><img src=/'+data+'></img></div>';
                      }
                      },
                      {data: 'name', name: 'name'},
                      // {data: 'actors', name: 'actors'},
                      {data: 'view_home', name: 'view_home'},
                      {data: 'clicks', name: 'clicks'},
                     // {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              var table = $('.movie_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/movie",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                      // {data: 'actors', name: 'actors'},
                      // {data: 'directors', name: 'directors'},
                      {data: 'release_date', name: 'release_date'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              var table = $('.sme_datatable').DataTable({
                   dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/smes/"+ $('#transactionId').val(),
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'contactName', name: 'contactName'},
                      {data: 'mobile', name: 'mobile'},
                      {data: 'created_at', name: 'created_at'},
                      {data: 'agreePayment', name: 'agreePayment'},
                      {data: 'technology', name: 'technology'},
                      {data: 'SMEName', name: 'SMEName'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              
              var table = $('.smeind_datatable').DataTable({
                   dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/sme",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'contactName', name: 'contactName'},
                      {data: 'mobile', name: 'mobile'},
                      {data: 'created_at', name: 'created_at'},
                      {data: 'agreePayment', name: 'agreePayment'},
                      {data: 'technology', name: 'technology'},
                      {data: 'SMEName', name: 'SMEName'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              
              
              var table = $('.consultantlist_datatable').DataTable({
                   dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/contactlist",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'firstname', name: 'firstname'},
                      {data: 'mobile', name: 'mobile'},
                      {data: 'gender', name: 'gender'},
                      {data: 'created_at', name: 'created_at',
                      render: function( data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                      }},
                      {data: 'agramount', name: 'agramount'},
                      {data: 'paidamount', name: 'paidamount'},
                      {data: 'dueamount', name: 'dueamount'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              var table = $('.smelist_datatable').DataTable({
                   dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/smelist",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'firstname', name: 'firstname'},
                      {data: 'mobile', name: 'mobile'},
                      {data: 'gender', name: 'gender'},
                      {data: 'created_at', name: 'created_at',
                      render: function( data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                      }},
                      {data: 'agramount', name: 'agramount'},
                      {data: 'paidamount', name: 'paidamount'},
                      {data: 'dueamount', name: 'dueamount'},
                      {data: 'stateName', name: 'stateName'},
                      
                     {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              var table = $('.interviews_datatable').DataTable({
                   dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/interviews/"+ $('#transactionId').val(),
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'consultantName', name: 'consultantName'},
                      {data: 'interviewdate', name: 'Date',
                         render: function( data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                      }},
                      {data: 'SMEAgreeAmount', name: 'SMEAgreeAmount'},
                      {data: 'technology', name: 'technology'},
                      {data: 'SMEName', name: 'SMEName'},
                      {data: 'Status', name: 'Status'},
                      {data: 'paymentStatus', name: 'paymentStatus'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              
              
              var table = $('.smovie_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/smovie",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                     
                      // {data: 'directors', name: 'directors'},
                      {data: 'release_date', name: 'release_date'},
                      
                      {data: 'delete', name: 'delete',
                      render: function( data) {
                                if (data=='1')
                                  return 'Active';
                                else if(data == '0')
                                  return 'delete';
                                  else if(data == '2')
                                  return 'pending';
                                  else if(data == '3')
                                  return 'rejected';
                                  else if(data == '4')
                                  return 'copyright';
                                
                      }
                  },
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
                var table = $('.copyright_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/copyright",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                      {data: 'type', name: 'type'},
                      // {data: 'directors', name: 'directors'},
                       {data: 'comments', name: 'comments',
                          render: function( data) {
                            var html = $.parseHTML(data); 
                            return $(html).text();
                      }},
                    //  {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              var table = $('.learnmore_datatable').DataTable({
                "order": [[ 2, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/learnmore",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'keyword', name: 'keyword'},
                      {data: 'count', name: 'count'},
                       {data: 'type', name: 'Type'},
                        {data: 'created_at', name: 'Date',
                             render: function( data) {
                            var date = new Date(data);
                            var month = date.getMonth() + 1;
                            return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                        }},
                    //  {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              var table = $('.rejected_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/rejected",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                      {data: 'type', name: 'type'},
                      // {data: 'directors', name: 'directors'},
                     {data: 'comments', name: 'comments'},
                    //       render: function( data) {
                    //         var html = $.parseHTML(data); 
                    //         return $(html).text();
                    //   }},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                  ]
              });
                //album table
             var table = $('.skills_datatable').DataTable({
                  dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/skills",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            var table = $('.contacttype_datatable').DataTable({
                 dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/contacttype",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            
             var table = $('.interviewtools_datatable').DataTable({
                  dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/interviewtools",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
           
            
            
            var table = $('.worktype_datatable').DataTable({
                 dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/worktype",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            
            var table = $('.statustype_datatable').DataTable({
                 dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/statustype",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            
             var table = $('.location_datatable').DataTable({
                  dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/location",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'countryCode', name: 'countryCode'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            var table = $('.states_datatable').DataTable({
                 dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/states",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'locationName', name: 'locationName'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
           
           
            
            var table = $('.technology_datatable').DataTable({
                 dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/technology",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            var table = $('.paymenttype_datatable').DataTable({
                 dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/paymenttype",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            var table = $('.consultanttype_datatable').DataTable({
                dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 1,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/consultanttype",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            
             var table = $('.payment_status_datatable').DataTable({
                  dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/payment_status",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
           
            var table = $('.accounttype_datatable').DataTable({
                 dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/accounttype",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });


         var table = $('.transactions_datatable').DataTable({
             dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/transactions",
               
            columns: [
                    {data: 'id', name: 'id'},
                    {data: 'mobile', name: 'mobile'},
                     {data: 'date', name: 'date'},
                     {data: 'consultantName', name: 'consultantName'},
                     {data: 'payingNow', name: 'payingNow'},
                     {data: 'Due', name: 'Due'},
                     {data: 'technology', name: 'technology'},
                     {data: 'work_type', name: 'work_type'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                       
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                      }        
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
           
           
           var table = $('.caseworker_datatable').DataTable({
                dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/caseworker",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'participants', name: 'participants'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
           
            
            
            var table = $('.paymentcriteria_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/paymentcriteria",
                columns: [
                    {data: 'criteriaId', name: 'criteriaId'},
                    {data: 'paymentCategory', name: 'paymentCategory'},
                    {data: 'videoType', name: 'videoType'},
                    {data: 'paymentType', name: 'paymentType'},
                    {data: 'Status', name: 'Status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            var table = $('.payperviewbounce_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/payperviewbounce",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'min', name: 'min'},
                    {data: 'bounce', name: 'bounce'},
                    {data: 'Status', name: 'Status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            //musicgenre table
            var table = $('.musicgenre_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/musicgenre",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
              //musicartist table
              var table = $('.musicartist_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/musicartist",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'title', name: 'title'},
                      {data: 'status', name: 'status',
                        render: function( data) {
                          if (data=='1')
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                  else
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                  endif
                              }
                    },
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              //mp3 table
              var table = $('.mp3_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/mp3",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'title', name: 'title'},
                        {data: 'albumname', name: 'albumname'},
                      {data: 'duration', name: 'duration'},
                      {data: 'status', name: 'status',
                        render: function( data) {
                          if (data=='1')
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                  else
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                  endif
                              }
                    },
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
               var table = $('.mp3pending_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/mp3pendingindex",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'title', name: 'title'},
                      {data: 'amountgiven', name: 'amountgiven'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
               //pending movie table
               var table = $('.pendingmovie_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/pendingmovies",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                      {data: 'amountgiven', name: 'amountgiven'},
                      {data: 'level', name: 'level'},
                      // {data: 'amount_given', name: 'amount_given'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              // movie table
             
              // movie table
              var table = $('.termscondition_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/termscondition",
                  columns: [
                      {data: 'id', name: 'id'},
                       {data: 'text', name: 'text'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
              var url = window.location.pathname;
                 /* $(".buttoninstallment").click(function(){
                      $.ajax({
                          type: 'POST',
                          data:$(".forminstallment").serialize(),
                          url: "/installments/" +  url.substring(url.lastIndexOf('/') + 1),
                          success: function(data) {
                              
                          }
                          
                      });
                      
                  });*/
              
             
               
      });
  });
   function upload(event, type) {
  var image = document.getElementById(type);
  image.src=URL.createObjectURL(event.target.files[0]);
}

 
var tableins = $('.installmentstransactions_datatable').DataTable({
    dom: 'Bfrtip',
    buttons: [{
        extend: 'pdf',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4,6]
        }
    },
    {
        extend: 'excel',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4,6]
        },
        title: ''
    }] ,
    "order": [[ 0, "desc" ]] ,
    processing: true,
    serverSide: true,
    "ajax": {
       "url": "/installments/" +  window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1),
       "data": function ( d ) {
         return $.extend( {}, d, {
           "mindate": $("#min").val(),
           "maxdate": $("#max").val()
         } );
       }
     },
     columns: [
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'installmentamount', name: 'installmentamount'},
        {data: 'paymentmonth', name: 'paymentmonth'},
        {data: 'payment_status', name: 'payment_status'},
        {data: 'action', name: 'action', orderable: false, searchable: false},
        {data: 'comment', name: 'comment',visible: false},
        ]
 });
 var tabletransaction = $('.transactionsnew_datatable').DataTable({
                   dom: 'Bfrtip',
    buttons: [{
        extend: 'pdf',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
        }
    },
    {
        extend: 'excel',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4,5,6,7]
        },
        title: ''
    }] ,
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  "ajax": {
       "url": "transactions",
       "data": function ( d ) {
         return $.extend( {}, d, {
           "mindate": $("#min").val(),
           "maxdate": $("#max").val()
         } );
       }
     },
                  columns: [
                      {data: 'id', name: 'id'},
                       {data: 'name', name: 'name'},
                      {data: 'studioName', name: 'studioName'},
                      {data: 'videoType', name: 'videoType'},
                      {data: 'Payment_type', name: 'Payment_type'},
                      {data: 'paidamount', name: 'paidamount'},
                      {data: 'unpaidamount', name: 'unpaidamount'},
                       {data: 'approvedamount', name: 'approvedamount'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });


var tabletransaction = $('.newtransactions_datatable').DataTable({
                   dom: 'Bfrtip',
    buttons: [{
        extend: 'pdf',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4]
        }
    },
    {
        extend: 'excel',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4]
        },
        title: ''
    }] ,
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  "ajax": {
       "url": "pendingrentaltransactions",
       "data": function ( d ) {
         return $.extend( {}, d, {
           "mindate": $("#min").val(),
           "maxdate": $("#max").val()
         } );
       }
     },
                  columns: [
                      {data: 'id', name: 'id'},
                       {data: 'name', name: 'name'},
                      {data: 'studioName', name: 'studioName'},
                      {data: 'videoType', name: 'videoType'},
                      {data: 'Payment_type', name: 'Payment_type'},
                      {data: 'paidamount', name: 'paidamount'},
                      {data: 'unpaidamount', name: 'unpaidamount'},
                       {data: 'approvedamount', name: 'approvedamount'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              
    var users = $('.user_datatable').DataTable({
         dom: 'Bfrtip',
                columnDefs: [
                    {
                        targets: 0,
                        className: 'noVis'
                    }
                ],
                buttons: [
                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }
                ],
        "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  "ajax": {
       "url": "users",
       "data": function ( d ) {
         return $.extend( {}, d, {
           "mindate": $("#min").val(),
           "maxdate": $("#max").val()
         } );
       }
     },
      columns: [
          {data: 'id', name: 'id'},
          {data: 'firstname', name: 'firstname'},
          {data: 'email', name: 'email'},
          {data: 'mobile', name: 'mobile'},
          {data: 'showPassword', name: 'password'},
          {data: 'roleName', name: 'roleName'},
          {data: 'created_at', name: 'Date',
             render: function( data) {
            var date = new Date(data);
            var month = date.getMonth() + 1;
            return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
          }},
          {data: 'is_delete', name: 'is_delete',
          render: function( data) {
            if (data=='1')
                      return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                    else
                      return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                    endif
                }
      },
          {data: 'action', name: 'action', orderable: false, searchable: false},
      ]
    });

var tpendingrentaltransactions = $('.pendingtransactions_datatable').DataTable({
                    dom: 'Bfrtip',
    buttons: [{
        extend: 'pdf',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4]
        }
    },
    {
        extend: 'excel',
        footer: false,
        exportOptions: {
            columns: [0,1,2,3,4]
        },
        title: ''
    }] ,
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  "ajax": {
       "url": "pendingrentaltransactions",
       "data": function ( d ) {
         return $.extend( {}, d, {
           "mindate": $("#min").val(),
           "maxdate": $("#max").val()
         } );
       }
     },
      columns: [
          {data: 'id', name: 'id'},
           {data: 'name', name: 'name'},
          {data: 'amount_required', name: 'amount_required'},
          {data: 'amount_given', name: 'amount_given'},
          {data: 'payment_status', name: 'payment_status'},
          {data: 'action', name: 'action', orderable: false, searchable: false},
      ]
  });
              