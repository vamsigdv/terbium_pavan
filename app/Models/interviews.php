<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class interviews extends Model
{
    use HasFactory;
    protected $table = 'interviews';
	public $primaryKey = 'id';
    protected $fillable = [
        'transactionId',
        'Noofrounds',
        'contactId',
        'consultantName',
        'modeofinterview',
        'technologyId',
        'technology',
        'SMEAgreeAmount',
        'Status',
        'comment',
        'smeid',
        'SMEName',
        'paymentStatus',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'caseWorker',
        'caseWorkerId',
        'interviewdate',
        'resume',
        'recruter','recruterId'
    ];
}
