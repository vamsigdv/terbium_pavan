<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;
    protected $table = 'permission';
	public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'status'
    ];
    
    public function roles() {

       return $this->belongsToMany(Role::class,'roles_permissions');
           
    }
    
    public function adminiy() {
       return $this->belongsToMany(Adminiy::class,'adminiys_permissions');
    }
}

?>