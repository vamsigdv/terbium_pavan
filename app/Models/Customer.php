<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'email',
        'password',
        'epic',
        'phone',
        'photo',
        'status',
        'delete',
        'block',
        'created_at',
        'updated_at',
    ];
}
