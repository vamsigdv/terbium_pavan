<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class comments extends Model
{
    use HasFactory;
    protected $table = 'comments';
	public $primaryKey = 'id';
    protected $fillable = [
        'interviewId',
        'comments',
        'roundName',
        'round',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'statusTypeId',
        'statusType','noofrounds','interviewdate','contactId','consultantName','smeId','smeName','technologyId','technologyName'
    ];
}
