<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    protected $table = 'contact';
	public $primaryKey = 'id';
    protected $fillable = [
        'firstname',
        'lastname',
        'middlename',
        'gender',
        'email',
        'mobile',
        'countryCode',
        'altmobile',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'dob',
        'contactTypeId',
        'contactType',
        'workTypeId',
        'workType',
        'consultantTypeId',
        'consultantType',
        'locationId',
        'location',
        'technologyId',
        'technology',
        'skillId',
        'skill',
        'description',
        'paymentTypeId',
        'paymentType',
        'accountTypeId',
        'accountType',
        'bankname',
        'branch',
        'ifsc',
        'fileinput',
        'caseworkerId',
        'caseworker',
        'status',
        'RoutingNumber',
        'BankNameZille',
        'Contactname',
        'ZilleID',
        'profilePic',
        'coverPic',
        'stateId',
        'stateName'
    ];
    
    public function location_details()
    {
        return $this->hasOne('App\Models\location', 'id', 'locationId');
    }
}
