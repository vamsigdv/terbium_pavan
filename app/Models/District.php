<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;
    protected $table = 'district';
	public $primaryKey = 'id';
	public $timestamps = false;
    protected $fillable = [
        'district_name',
        'state_name',
        'count'
    ];
}
