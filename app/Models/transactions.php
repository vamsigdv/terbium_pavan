<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transactions extends Model
{
    use HasFactory;
    protected $table = 'transactions';
	public $primaryKey = 'id';
    protected $fillable = [
        'contactId',
        'consultantName',
        'technologyId',
        'technology',
        'date',
        'paymentTypeId',
        'paymentType',
        'transactionId',
        'payingNow',
        'Due',
        'nextcomitdate',
        'caseworkerId',
        'caseworker',
        'created_at',
        'updated_at',
        'status',
        'createdBy',
        'updatedBy',
        'total',
        'paid',
        'unpaid',
        'paymentStatus',
        'refund_amount',
        'mobile',
        'smepaid',
        'smeunpaid',
        'profit',
        'work_type'
    ];
    
    public function installments() {
        $this->hasMany('App\Models\installaments', 'id', 'transactionId');
    }
}
