<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin_session extends Model
{
    use HasFactory;
    protected $table = 'admin_session';
	public $primaryKey = 'id';
    protected $fillable = [
        'user_id','is_deleted','is_active','check_login','check_logout',
    ];
}
