<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Adminiy extends Model
{
    use HasFactory;
	protected $table = 'adminiy';
    protected $fillable = [
        'name', 'email', 'password','user_id','role','is_active','contact','image','user_kyc_bounce',
        'showPassword1','user_tax','quaterly_todate','quaterly_fromdate','cover_photo','email_otp'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
