<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class H1BDocument extends Model
{
    use HasFactory;
    
    protected $table = 'h1b_documents';
	public $primaryKey = 'id';
	
    protected $fillable = [
        'h1b_id',
        'document_id',
        'document',
        'created_at',
        'updated_at'
    ];
    
    public function h1b() {
        return $this->belongsTo(H1B::class);
    }
}

?>