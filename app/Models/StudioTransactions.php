<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudioTransactions extends Model
{
    use HasFactory;

    protected $primary_key = "id";
    protected $table = "studio_transaction";

    protected $fillable = [
        'studio_id',
        'order_id',
        'sname',
        'finance_id',
        'finance_name',
        'coins',
        'amount',
        'status',
        'transaction_id',
        'created_at',
        'updated_at',
        'excel_path',
    ];
    
    
}
