<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'name',
        'email',
        'password',
        'mobile',
        'is_delete',
        'status',
        'role',
        'roleName',
        'showPassword',
        'admin_id',
        'address',
        'accountno',
        'branch',
        'image',
        'cover_photo'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function quizResult() 
    {
        return $this->hasMany(QuizResult::class);
    }

    public function userEarnings() 
    {
        return $this->hasMany(UserEarning::class);
    }

    public function userFavourites() 
    {
        return $this->hasMany(UserFavourite::class);
    }

    public function userKyc() 
    {
        return $this->hasOne(UserKyc::class);
    }
}
