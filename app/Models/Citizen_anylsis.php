<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Citizen_anylsis extends Model
{
    use HasFactory;
    protected $table = 'citizen_anylses';
	public $primaryKey = 'id';
    protected $fillable = [
        'vid',
        'uid',
        'gender',
        'age',
        'location',
        'created_at',
        'updated_at',
        'citizenstatus'
    ];
}
