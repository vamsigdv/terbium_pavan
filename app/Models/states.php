<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class states extends Model
{
    use HasFactory;
    protected $table = 'states';
	public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'locationId',
        'locationName',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'status'
    ];
}
