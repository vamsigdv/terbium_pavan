<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    
    protected $primary_key = "id";
    protected $table = "city";
    
    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'state_id',
    ];
}
