<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class common_transactions extends Model
{
    use HasFactory;
    protected $primary_key = 'comTransId';
    protected $table = 'common_transactions';
    protected $fillable = 
    [
        'videoId',
        'name',
        'studioId',
        'studioName',
        'videoType',
        'payment_cretira',
        'Payment_type',
        'amountgiven',
        'perview',
        'Payperviewamount',
        'created_at',
        'updated_at',
        'Durataion',
        'level'
    ];
}
