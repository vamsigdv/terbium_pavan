<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transactioninstallments extends Model
{
    use HasFactory;
    protected $table = 'transactioninstallments';
	public $primaryKey = 'id';
	 public $timestamps = false;
    protected $fillable = [
        'VID',
        'UID',
        'orderId',
        'installmentamount',
        'transactionId',
        'date',
        'paymentmonth',
        'duration',
        'payment_status',
        'Payment_type',
        'sid',
        'created_at',
        'updated_at',
        'transaction_doc',
        'comment',
    ];
}
