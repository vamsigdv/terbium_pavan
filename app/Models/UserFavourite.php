<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFavourite extends Model
{
    use HasFactory;

    protected $primary_key = "id";
    protected $table = "user_fav";

    protected $fillable = [
        'user_id',
        'vid',
        'date',
        'title',
        'image',
        'status'
    ];

    public function user() {
        $this->belongsTo(User::class);
    }
}
