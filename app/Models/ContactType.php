<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
    use HasFactory;
    protected $table = 'contacttype';
	public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'status'
    ];
}
