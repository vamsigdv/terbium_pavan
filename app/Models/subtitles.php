<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subtitles extends Model
{
    use HasFactory;
    protected $fillable = [
       
        'movieId',
        'movieName',
        'title',
        'type',
        'file'
    ];
}
