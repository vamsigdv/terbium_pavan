<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Achievements extends Model
{
    use HasFactory;
    protected $table = 'achievements';
	public $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'description',
        'created_at',
        'updated_at',
    ];
}