<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transactiontechnology extends Model
{
    use HasFactory;
    protected $table = 'transactiontechnology';
	public $primaryKey = 'id';
	 public $timestamps = false;
    protected $fillable = [
        'contactId',
        'contactName',
        'technologyId',
        'technology',
        'smeid',
        'status',
        'createdBy',
        'updatedBy',
        'created_at',
        'updated_at',
        'reasonSME',
        'agreePayment',
        'comment',
        'NoofInterviews',
        'workTypeId',
        'workType',
        'caseWorkerId',
        'caseWorker',
        'SMEName',
        'batchId',
        'batchName'
    ];
}
