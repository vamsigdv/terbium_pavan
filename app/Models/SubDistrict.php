<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubDistrict extends Model
{
    protected $primary_key = "id";
    protected $table = "sub_district";
    
    public $timestamps = false;
}