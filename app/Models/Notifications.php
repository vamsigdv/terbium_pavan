<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    use HasFactory;

    protected $primary_key = "id";
    protected $table = "notifications";
    protected $fillable = [
        "type",
        "message",
        "transactionId",
        "created_by",
        "created_at",
        "updated_at",
        "contactId"
    ];
}
