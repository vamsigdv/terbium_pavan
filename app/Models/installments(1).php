<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transactiontechnology extends Model
{
    use HasFactory;
    protected $table = 'transactiontechnology';
	public $primaryKey = 'id';
    protected $fillable = [
        'contactId',
        'contactName',
        'technologyId',
        'technology',
        'smeid',
        'status',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy'
    ];
}
