<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
    use HasFactory;
    protected $table = 'technology';
	public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'status','minamount','maxamount'
    ];
}
