<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudioEarning extends Model
{
    use HasFactory;

    protected $primary_key = "id";
    protected $table = "studio_earning";

    protected $fillable = [
        'studio_id',
        'vid',
        'date',
        'coins',
        'orderid',
        'payid',
        'payment_cat',
        'paym_status',
        'exp_date',
        'views',
        'payment_title',
        'transaction_doc',
        'comment',
    ];
    
    public function studio()
    {
        return $this->belongsTo(Studio::class);
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }
}
