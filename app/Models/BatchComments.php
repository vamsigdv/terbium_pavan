<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatchComments extends Model
{
    use HasFactory;
    protected $table = 'batch_comments';
    public $primaryKey = 'id';
    protected $fillable = [
        'batch_id',
        'start_time',
        'end_time',
        'date',
        'created_at',
        'updated_at',
        'attachment',
        'sme_id',
        'description',
        'participant',
    ];
}