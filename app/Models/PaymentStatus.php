<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    use HasFactory;
    
    protected $table = 'payment_status';
	public $primaryKey = 'id';
	
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'status'
    ];
}