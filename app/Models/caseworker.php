<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class caseworker extends Model
{
    use HasFactory;
    protected $table = 'caseworker';
	public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'batch_start_date',
        'batch_end_date',
        'consultantName',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'status',
        'technologyId',
        'technology',
        'amount',
        'contactId',
        'employeeId',
        'employeeName',
        'participants',
        'assigned_to'
    ];
}
