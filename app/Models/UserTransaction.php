<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    use HasFactory;

    protected $primary_key = 'id';
    protected $table = 'user_transaction';

    protected $fillable = [
        'order_id',
        'user_id',
        'uname',
        'finance_id',
        'finance_name',
        'coins',
        'amount',
        'status',
        'transaction_id',
        'payment_status',
        'remarks'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
