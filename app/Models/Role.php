<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = 'role';
	public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'status'
    ];
    
    public function permissions() {

       return $this->belongsToMany(Permission::class, 'roles_permissions');
           
    }
    
    public function adminiy() {
       return $this->belongsToMany(Adminiy::class, 'adminiys_roles');
    }
}

?>