<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class reviews extends Model
{
    use HasFactory;
    protected $primary_key = 'id';
    protected $table = 'reviews';
    protected $fillable = [
        'MID',
        'SID',
        'UID',
        'RATING',
        'username',
        'mobile',
        'movieTitle',
        'created_at',
        'updated_at',
        'comment',
        'Status'
    ];
}
