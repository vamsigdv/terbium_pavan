<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class installments extends Model
{
    use HasFactory;
    protected $table = 'installments';
	public $primaryKey = 'id';
    protected $fillable = [
        'amountType',
        'contactId',
        'date',
        'amount',
        'paidStatus',
        'paymentType',
        'comments',
        'refund_amount',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'nextcommiteddate',
        'due'
    ];
    
    public function transaction() {
        return $this->hasOne('App\Models\transactions', 'id', 'transactionId');
    }
    
    public function contact() {
        return $this->hasOne('App\Models\Contact', 'id', 'contactId');
    }
    
    public function pay_status() {
        return $this->hasOne('App\Models\PaymentStatus', 'id', 'paidStatus');
    }
    
    public function payment_type() {
        return $this->hasOne('App\Models\PaymentType', 'id', 'paymentType');
    }
}
