<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class smecomments extends Model
{
    use HasFactory;
    protected $table = 'smecomments';
	public $primaryKey = 'id';
    protected $fillable = [
        'reason',
        'transactiontechnology',
        'newsmeId',
        'oldSmeId',
        'newSmeName',
        'oldSmeName',
        'changeDate',
        'paymenttosme',
        'paymentStatus',
        'created_at',
        'updated_at',
        'comments'
    ];
}