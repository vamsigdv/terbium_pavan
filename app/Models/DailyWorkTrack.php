<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyWorkTrack extends Model
{
    use HasFactory;
    protected $table = 'daily_work_track';
	public $primaryKey = 'id';
    protected $fillable = [
        'calls_count',
        'created_at',
        'updated_at',
        'track_file',
        'employee_id',
        'date'
    ];
}
