<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TermsCondition extends Model
{
    use HasFactory;
    protected $table = 'termscondition';
	public $primaryKey = 'id';
    protected $fillable = [
        'text',
    ];
    public function getTextAttribute($value){
        return htmlspecialchars_decode(stripslashes($value));
        
    }
}
