<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class accounttype extends Model
{
    use HasFactory;
    protected $table = 'accounttype';
	public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'status'
    ];
}
