<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class H1BCommunication extends Model
{
    use HasFactory;
    
    protected $table = 'h1b_communication';
	public $primaryKey = 'id';
	
    protected $fillable = [
        'h1b_id',
        'title',
        'comments',
        'created_at',
        'updated_at'
    ];
    
    public function h1b() {
        return $this->belongsTo(H1B::class);
    }
}

?>