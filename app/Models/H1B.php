<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class H1B extends Model
{
    use HasFactory;
    protected $table = 'h1b';
	public $primaryKey = 'id';
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'passport',
        'application_status',
        'case_worker',
        'years_exp',
        'gender',
        'altphone',
        'marital_status',
        'ref_by',
        'doc_verified',
        'co_affidevit',
        'uscis_status',
        'uscis_cap',
        'pre_recipt',
        'edu_evaluation',
        'lca',
        'ds_form',
        'rfe',
        'phone',
        'qualification',
        'technology',
        'skills',
        'country_loc',
        'dependents',
        'attorney_emp',
        'ref_contact',
        'ack_email',
        'service_center',
        'pre_status',
        'bio_details',
        'organization',
        'h1b_recp',
        'documentation',
        'ship_tracker',
        'visa_interview',
         'technolgy',
          'h1b_petition_doc',
          'vendor_doc',
          'Visa_int_slot',
          'wel_email',
          'visa_int_req_doc',
          'visa_slot_det',
          'pre_reg_recipt',
          'usics_status',
          'sup_doc',
        'rfe_status',
        'created_at',
        'updated_at',
        'status'
    ];
    
    public function h1b_docuemnts() {
        return $this->hasMany(H1BDocument::class);
    }
    
    public function h1b_communications() {
        return $this->hasMany(H1BCommunication::class);
    }
    public function cap()
    {
        
         return $this->belongsTo(Cap::class, 'uscis_cap');
    }
}

?>