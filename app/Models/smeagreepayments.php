<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class smeagreepayments extends Model
{
    use HasFactory;
    protected $table = 'smeagreepayments';
	public $primaryKey = 'id';
    protected $fillable = [
        'contactId',
        'interviewId',
        'agreeamount',
        'date',
        'created_at',
        'updated_at',
        'createdBy',
        'updatedBy',
        'employee',
        'type',
        'status',
        'transactionId',
        'smeId'
        
    ];
}
