<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employee';
	public $primaryKey = 'id';
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'contact',
        'email',
        'adhar',
        'voter',
        'password',
        'account_number',
        'ifsc',
        'status',
        'delete',
        'created_at',
        'updated_at',
        'admin_id',
        'dob',
        'photo'
    ];
}
