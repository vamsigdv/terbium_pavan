<?php

    namespace App\Listeners;

    use App\Notifications\AdminChannelServices;
    use App\Events\NewVendorRegistered;

    class SendNotifyRegistered
    {

        public function handle(NewVendorRegistered $event)
        {
            $user = $event->user;


            $data = [
                'id' =>  $user->id,
                'name' =>  $user->name,
                'avatar' =>  "",
                // 'link' => route('user.admin.upgrade'),
                'type' => 'user_register',
                'message' => '<b>'. $user->name . '</b> Create New  Account'
            ];

            $user->notify(new AdminChannelServices($data));
        }
    }
