<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class PusherNotificationAdminEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $avatar;
    public $link;
    public $idNotification;

    public function __construct($idNotification, $data, $user)
    {
        $this->avatar  = "test";
        $this->link  = "afasdf";
        $this->idNotification  = 1;
        $this->message  = strip_tags($data['message']);
    }

    // public function broadcastOn()
    // {
    //     return ['admin-channel'];
    // }


    public function broadcastOn()
    {
        return ['my-channel'];
    }

    // public function broadcastAs()
    // {
    //     return 'my-event';
    // }
}
