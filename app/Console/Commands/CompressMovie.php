<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\MovieQtyVideo;
use App\Models\Movie;
use File;

class CompressMovie extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'compress:movie {movieId}';
    protected $signature = 'compress:movie';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // 0 is   Pending
        // 1 is  Compress is Process
        // 2 is  Done
 

\Log::info("yes");

        $qtyVideo = [
            '720' => "ffmpeg -i :videoPath -s hd720 -c:v libx264 -crf 23 -c:a aac -strict -2 :destimationPath",
            '480' => "ffmpeg -i :videoPath -s hd480 -c:v libx264 -crf 23 -c:a aac -strict -2 :destimationPath",
            '360' => "ffmpeg -i :videoPath -vf scale=-1:360 -c:v libx264 -crf 18 -preset veryslow -c:a copy :destimationPath"
        ];
        
        //  $qtyVideo = [
        //     '720' => 'ffmpeg -i :videoPath -r 30 :destimationPath',
        //     '480' => 'ffmpeg -i :videoPath -vf "scale=854:480" -c:a copy :destimationPath',
        //     '360' => 'ffmpeg -i :videoPath -vf "scale=640:360" -c:a copy :destimationPath'
        // ];

        $processMovieCount =  MovieQtyVideo::selectRaw('movie_qty_videos.*')
                             ->where(['movie_qty_videos.isCompressDone' => 1])->count();

          if($processMovieCount > 0)
          {
            return false;
          }

        $getCompressProcessVideo =  MovieQtyVideo::selectRaw('movie_qty_videos.*')->where(['movie_qty_videos.isCompressDone' => 0])->first();

        if(!empty($getCompressProcessVideo))
        {
            $movieVideoPath = MovieQtyVideo::where(['movieId' => $getCompressProcessVideo->movieId,'qty' => '1080','isCompressDone' => 2,'type' => $getCompressProcessVideo->type])->first();
            
            if($movieVideoPath->qty == "1080" && $movieVideoPath->isCompressDone != 2)
            {
                $movieVideoPath->isCompressDone = 2;
                $movieVideoPath->save();
                
                  $movieVideoPath = MovieQtyVideo::where(['movieId' => $getCompressProcessVideo->movieId,'qty' => '1080','isCompressDone' => 2,'type' => $getCompressProcessVideo->type])->first();
            }

            $videoPath = base_path() .'/'. $movieVideoPath->fileName;
            $destimationPath = base_path() .'/'.$getCompressProcessVideo->fileName;

            // try {

                $getCompressProcessVideo->isCompressDone = 1;
                $getCompressProcessVideo->save();

                $getCompressCommand = $qtyVideo[$getCompressProcessVideo->qty];

                $path = str_replace(":videoPath",$videoPath,$getCompressCommand);
                $cmd = str_replace(":destimationPath",$destimationPath,$path);
                
             
                exec($cmd);

                $getCompressProcessVideo->isCompressDone = 2;

                try {
                    $size = File::size(base_path($getCompressProcessVideo->fileName));
                    $getCompressProcessVideo->movieBytes = $size;
                   }
                  catch(\Exception $e) {
                      \Log::info($e->getMessage());
                  }

                $getCompressProcessVideo->save();

            //  }
            //     catch (\Exception $e) {

            //         \Log::info($e->getMessage());

            //         $getCompressProcessVideo->isCompressDone = 0;
            //         $getCompressProcessVideo->save();
            //     return true;
            // }

        }

/*

        $movieQtyVideos =  MovieQtyVideo::selectRaw('movie_qty_videos.*')
                 ->where(['movie_qty_videos.isCompressDone' => 0]);

        $movieQtyVideos = $movieQtyVideos->chunkById(500, function ($movieQtyVideo)
        {

            foreach($movieQtyVideo as $value)
            {


            $movieVideoPath = MovieQtyVideo::where(['movieId' => $value->movieId,'qty' => '1080','isCompressDone' => 1,'type' => $value->type])->first();


            $videoPath = base_path() .'/'. $movieVideoPath->fileName;
            $destimationPath = base_path() .'/'.$value->fileName;

            try {

                $value->isCompressDone = 2;
                $value->save();

                        if($value->qty == 720)
                        {
                            $cmd =  "ffmpeg -i $videoPath -s hd720 -c:v libx264 -crf 23 -c:a aac -strict -2 $destimationPath";
                        }

                        if($value->qty == 480)
                        {
                            $cmd =  "ffmpeg -i $videoPath -s hd480 -c:v libx264 -crf 23 -c:a aac -strict -2 $destimationPath";
                        }

                        if($value->qty == 360)
                        {
                            // $cmd =  "ffmpeg -i $videoPath -s hd264 -c:v libx264 -crf 23 -c:a aac -strict -2 $destimationPath";

                            // ffmpeg -i input.mp4 -vf scale=-1:360 -c:v libx264 -crf 18 -preset veryslow -c:a copy output.mp4

                            $cmd =  "ffmpeg -i $videoPath -vf scale=-1:360 -c:v libx264 -crf 18 -preset veryslow -c:a copy $destimationPath";
                        }
                        exec($cmd);
                    $value->isCompressDone = 1;
                     $value->save();

             }
                catch (\Exception $e) {
                    \Log::info($e->getMessage());
                    $value->isCompressDone = 2;
                    $value->save();
                return true;
            }

            }


        }, 'id');*/




    }
}
