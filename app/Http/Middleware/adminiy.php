<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class adminiy
{
    // public function handle($request, Closure $next)
    // {
       
       
    // }
    // public function terminate($request, $response){
    	
    // }
    
     public function handle($request, Closure $next)
    {
        if(Auth::guard('adminiy')->check()){
            return $next($request);
        } else {
            return redirect()->route('login');
        }
    }
    public function terminate($request, $response){
    	
    }
}
