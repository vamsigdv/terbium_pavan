<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Studio;
use App\Models\UserKyc;
use App\Models\Citizen_anylsis;
use App\Models\UserTransaction;
use App\Models\UserEarning;
use DataTables;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redis;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\ContentApprover;
use App\Models\FinancialTeam;
use App\Models\Adminiy;
use App\Models\StudioTransactions;
use App\Models\Transactions;
use App\Models\Movie;
use App\Models\Genre;
use App\Models\Artist;
use App\Models\logs;
use App\Models\WatchVideoDurations;
use App\Models\Admin_session;
use App\Models\StudioDelete;
use App\Models\vemployee;
use Hash;
use App\Models\TermsCondition;
use PDF;
use App\Models\transactioninstallments;
use App\Models\StudioEarning;
use App\Models\City;
use App\Models\District;
use App\Models\States;
use App\Models\SubDistrict;
use App\Models\Villages;
use App\Models\Country;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
class UserController extends Controller
{
    /**
     * To get the users list
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::get();
            foreach ($users as $user) {
                $user->created_at = date('Y-m-d',strtotime($user->created_at));
               /* $user->kyc = false;
                if ($user->userKyc) {
                    $user->kyc = true;
                }*/
            }
            return DataTables::of($users)->addIndexColumn()
                ->addColumn('action', function($row){
                    $sbtn = '';
                    $btn = $sbtn. ' 
                   <a href="'. url('users/'.$row->id.'/edit').'" rel="noopener noreferrer">
                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    </a>

                    <a href="'.route('users.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('users.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users');
    }

    public function create()
    {
        return view('createuser');
    }

    public function store(Request $request)
    {   
        // \Log::error($request);
        $validated = $request->validate([
            'email' => 'required|unique:adminiy|email',
            'role'=>'required',
            'contact' => 'required|unique:adminiy|numeric|regex:/[0-9]/'
        ]);
        // print_r($request->role);exit;
        if($request->role == 2){
            $status='Accounts';
        }
        elseif($request->role == 3){
            $status='Employee';
        }
        elseif($request->role == 6){
            $status='immigration';
        }
         elseif($request->role == 5){
            $status='Admin';
        }
        elseif($request->role == 1){
            $status='Admin';
        }
        
        $actorphotofile=$request->file('photo');
        if (isset($actorphotofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorphotofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $profilePic = $up_location.$file_name;
            $actorphotofile->move($up_location,$file_name);
        } else {
                $profilePic='';
        }
        
        $actorcover_photofile=$request->file('cover_photo');
        if (isset($actorcover_photofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorcover_photofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $coverPic = $up_location.$file_name;
            $actorcover_photofile->move($up_location,$file_name);
        } else {
            $coverPic='';
        }
        
        $id = User::create([
              'firstname' => $request->firstname,
              'lastname' => $request->lastname,
              'email' => $request->email,
              'mobile' => $request->contact,
              'password' => bcrypt($request->password),
              'showPassword' => $request->password,
              'role' => $request->role,
              'roleName'=>$status,
              'branch' => $request->branch,
              'address' => $request->Address,
              'accountno' => $request->accountno,
              'image'=>$profilePic,
              'cover_photo'=>$coverPic,
              'status' => 1,
              'is_delete' => 1,
              'created_at'=>Carbon::now(),
          ])->id;
                     if ($id) 
                     {
                        $admin_id=Adminiy::create([
                            'name'=>$request->firstname,
                            'email'=>$request->email,
                            'contact'=>$request->contact,
                            'password'=> Hash::make($request->password),
                            'showPassword1'=> $request->password,
                            'image'=>$profilePic,
                            'cover_photo'=>$coverPic,
                            'role'=> $status,
                            'is_active'=> 1,
                            'user_id'=> $id,
                            'created_at'=>Carbon::now(),
                        ])->id;
                        $update = User::find($id)->update([
                            'admin_id'=> $admin_id,
                            'updated_at'=>Carbon::now(),
                        ]);
                     }
                        
            
        return Redirect()->route('users.index')->with('success','customer created successfully');

       // return Redirect()->back()->with('success','customer created successfully');
    }


    public function verifiedusers(Request $request)
    {
        if ($request->ajax()) {
            $data = [];
            if ($request->type == 'verified') {
                $users = UserKyc::where('kycdropdown', 'verified')->get();
            } elseif($request->type == 'unverified') {
                $users =  User::with('userKyc')->select('id','name','email','mobile','status','showPassword')->where('is_delete',1)->get();
            } 
            elseif($request->type == 'pending') {
                $users = UserKyc::where('kycdropdown', 'pending')->get();
            }
             elseif($request->type == 'rejected') {
                $users = UserKyc::where('kycdropdown', 'rejected')->get();
            } 
            else {
                $users = UserKyc::all();
            }
            if($request->type != 'unverified')
            {
            foreach ($users as $user) {
                $name = User::where('id', $user->user_id)->first();
                if(!empty($name))
                {
                    $user->name = $name->name;
                    $user->email = $name->email;
                    $user->mobile = $name->mobile;
                    $user->showPassword = $name->showPassword;
                    $user->kyc = true;
        
                        // $user->kyc = ($user->kycdropdown == 'verified') ? true : false;
                    
                    $user->delete = $name;
                    $user->id = $name->id;
                    if(!empty($user->user_id))
                    {
                        $data[] = $user;
                    }
                }
                
            }
            }
            else
            {
                foreach ($users as $user) 
                {
                    if (!$user->userKyc) {
                        $data[] = $user;
                    }
                }
            }
            return DataTables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $sbtn = '';
                    if ($row->kyc) {
                        $sbtn = '<a href="' . url('user/show_kyc_details'). '/' . $row->id . '" id="'.$row->id.'" class="btn p-0 ms-2" title="Show KYC"><i class="fas fa-eye" style="color:darkgreen"></i></a>';
                    }
                    $btn = $sbtn. ' 
                    <button class="btn p-0 ms-2" id="user_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <a href="'.route('users.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('users.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('verifiedusers');
    }
    
    
    


    public function edit($id)
    {
        $data = User::find($id);
        return view('edituser', compact('data'));

    }
    
  public function batchinfo()
    {
        
        return view('batchinfo');

    }
    

    /**
     * To update User data
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request) 
    {
        $id =$request->id;
        if($request->role == 2){
            $status='Accounts';
        }
        elseif($request->role == 3){
            $status='Employee';
        }
        elseif($request->role == 6){
            $status='immigration';
        }
         elseif($request->role == 5){
            $status='Admin';
        }
        if ($request->password != '') {
            $password = bcrypt($request->password);
            $showPassword = $request->password;
        }  else {
            $password = $request->old_password;
             $showPassword = $request->showPassword;
        }     
         $image = User::where('id',$request->id)->first();
        
        $actorphotofile=$request->file('photo');
        if (isset($actorphotofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorphotofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $profilePic = $up_location.$file_name;
            $actorphotofile->move($up_location,$file_name);
        } else {
                $profilePic=$image->image;
        }
        
        $actorcover_photofile=$request->file('cover_photo');
        if (isset($actorcover_photofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorcover_photofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $coverPic = $up_location.$file_name;
            $actorcover_photofile->move($up_location,$file_name);
        } else {
                $coverPic=$image->cover_photo;
        }
        
        
        $update = User::find($id)->update([
              'firstname' => $request->firstname,
              'lastname' => $request->lastname,
              'mobile' => $request->contact,
              'password' => bcrypt($request->password),
              'showPassword' => $showPassword,
              'role' => $request->role,
              'roleName'=>$status,
              'image'=>$profilePic,
              'cover_photo'=>$coverPic,
              'branch' => $request->branch,
              'address' => $request->Address,
              'accountno' => $request->accountno,
              'is_delete' => $request->is_delete,
              'updated_at' => Carbon::now(),
        ]);
        if ($update) {
            
            Adminiy::where('user_id',$id)->update([
            'role'=>$status,
            'image'=>$profilePic,
            'cover_photo'=>$coverPic,
            'is_active' => $request->is_delete,
            'password' => bcrypt($request->password),
            'showPassword1' => $showPassword,
            'updated_at'=>Carbon::now(),
        ]);
            return Redirect()->route('users.index')->with('success','customer updated successfully');
        }
        return Redirect()->route('users.index')->with('failed','please try again');
    }
    
   
    public function destroy($id)
    {
        $delete = User::find($id)->update([
            'is_delete'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        
        $deleteadmin = Adminiy::where('user_id',$id)->update([
            'is_active'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'User deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
   
   
    
   
    
    
    public function logoutalldevices(Request $request)
    {
        $user = Adminiy::find(Auth::guard('adminiy')->user()->id);
        if (Hash::check($request->password, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            if (Auth::guard('adminiy')->attempt([
                'email' => Auth::guard('adminiy')->user()->email,
                'password' => $request->password,
            ])) {
                
                return Redirect()->back()->with('success', 'Removed all devices exccept Current device');
            }
        }
        return Redirect()->back()->with('fail', 'Wrong Password');
    }
    
    public function deactivateaccount()
    {
       
          $user = Adminiy::find(Auth::guard('adminiy')->user()->id);
          $user->is_active = 0;
          $user->update();
          $user = Auth::guard('adminiy')->user();
          $admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $user->id)->orderBy('created_at', 'desc')->first();
           if($admin_session){
            $admin_session->check_login = 0;
            $admin_session->check_logout = 1;
            $admin_session->save();
          }
        
        Adminiy::find(Auth::guard('adminiy')->user()->id)->delete();
        Auth::guard('adminiy')->logout();
        return redirect()->route('login');
         // Auth::guard('adminiy')->logout();
       //   return redirect()->route('login');
    }
    public function generalsettings(){
        return view('generalsettings'); 
    }
    public function ProfileUpdate()
    {
          $user = User::where('id',Auth::guard('adminiy')->user()->user_id)->first();
        //  print_r($user);exit;
          return view('adminedit',compact('user'));   
    }
    
    public function adminupdate(Request $request) 
    {
        $id =$request->edit_id;
        if ($request->password != '') {
            $password = bcrypt($request->password);
            $showPassword = $request->password;
        }  else {
            $password = $request->old_password;
             $showPassword = $request->showPassword;
        }     
        if($request->role == 2){
            $status='Accounts';
        }
        elseif($request->role == 3){
            $status='Employee';
        }
        elseif($request->role == 6){
            $status='immigration';
        }
         elseif($request->role == 5){
            $status='Admin';
        }
        else
        {
            $status = 'Super Admin';
        }
        if($status != 'Super Admin')
        {  
            $image = User::find(Auth::guard('adminiy')->user()->user_id);
        }
        else
        {
            $image = Adminiy::where('id', Auth::guard('adminiy')->user()->id)->first();
        }
        
        $actorphotofile=$request->file('photo');
        if (isset($actorphotofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorphotofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $profilePic = $up_location.$file_name;
            $actorphotofile->move($up_location,$file_name);
        } else {
            $profilePic= ($image->image) ? $image->image : null ; 
        }
        
        $actorcover_photofile=$request->file('cover_photo');
        if (isset($actorcover_photofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorcover_photofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $coverPic = $up_location.$file_name;
            $actorcover_photofile->move($up_location,$file_name);
        } else {
                $coverPic=$image->cover_photo;
        }
        
        
        $update = User::find($id)->update([
              'firstname' => $request->firstname,
              'lastname' => $request->lastname,
              'mobile' => $request->contact,
              'password' => bcrypt($request->password),
              'showPassword' => $showPassword,
              'role' => $request->role,
              'roleName'=>$status,
              'image'=>$profilePic,
              'cover_photo'=>$coverPic,
              'branch' => $request->branch,
              'address' => $request->Address,
              'accountno' => $request->accountno,
              'is_delete' => 1,
              'updated_at' => Carbon::now(),
        ]);
        if ($update) {
            
            Adminiy::where('user_id',$id)->update([
            'role'=>$status,
            'image'=>$profilePic,
            'cover_photo'=>$coverPic,
            'is_active' => 1,
            'password' => bcrypt($request->password),
            'showPassword1' => $showPassword,
            'updated_at'=>Carbon::now(),
        ]);
       
            return Redirect()->back()->with('success','Profile updated successfully');
       }
        return Redirect()->back()->with('failed','please try again');
    }

}

    