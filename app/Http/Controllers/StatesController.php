<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\states;
use App\Models\location;
use DataTables;
use Session;
class StatesController extends Controller
{
    public function index(Request $request)
    {
        $location = location::where('status','1')->get();
        if ($request->ajax()) {
            $data = states::get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="states_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="states_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('states.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('states.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('states',compact('location'));
    }
    public function store(Request $request)
    {   
       
        $location = location::where('id',$request->locationName)->first();
        $request->locationName = $location->name;
        $request->locationId = $location->id;
        states::insert([
              'name'=>$request->name,
              'locationId'=> $request->locationId,
              'locationName'=> $request->locationName,
              'status'=> 1,
              'created_at'=>Carbon::now(),
              'createdBy'=> Auth::guard('adminiy')->user()->id,
              
          ]);
        return Redirect()->back()->with('success','states created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = states::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'states deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = states::find($id);
        $location = location::where('status','1')->get();
        $view=1;
        $html = view('modal.states_edit',compact('data','view','location'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = states::find($id);
        $view=0;
        $location = location::where('status','1')->get();
        $html = view('modal.states_edit',compact('data','view','location'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $location = location::where('id',$request->locationName)->first();
        $request->locationName = $location->name;
        $request->locationId = $location->id;
        $update = states::find($id)->update([
            'name'=>$request->name,
            'locationId'=> $request->locationId,
            'locationName'=> $request->locationName,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('states.index')->with('success','states updated successfully');
        }
        return Redirect()->route('states.index')->with('failed','please try again');
      }

}