<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Skills;
use DataTables;
use Session;
use App\Models\Technology;
use App\Models\Contact;
use App\Models\paymenttype;
use App\Models\caseworker;
use App\Models\transactiontechnology;
use App\Models\installments;
use App\Models\worktype;
use App\Models\transactions;
use App\Models\interviews;
use App\Models\interviewtools;
use App\Models\statustype;
use App\Models\User;
use App\Models\smeagreepayments;
use App\Models\smecomments;
class SMECommentsController extends Controller
{
    
    
    public function index(Request $request)
    {
        $newdata = [];
        $statustype = statustype::where('status',1)->get();
        if ($request->ajax()) {
           
            return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="transactions_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                     <a href="' . url('details', $row->id) . '" >
                    <button class="btn p-0 ms-2 " type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="View details"><i class="fas fa-eye" style="color:orange"></i></button>
                    </a>
                    <a href="'.route('transactions.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('transactions.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('comments',compact('statustype'));
    }
    
    
    public function store(Request $request)
    {   
        $id =$request->id;
        $data = transactiontechnology::find($id);
        $sme = Contact::where('id',$request->SMEName)->first();
        $request->SMEName = $sme->firstname;
        $request->smeid = $sme->id;
        $data->smeid = $request->smeid;
        $data->SMEName = $request->SMEName;
        $data->update();
        $newdata = transactions::where('id',$request->transactionId)->first();
        if($request->paymentStatus == 1)
        {
             $newdata->smepaid = $newdata->smepaid+$request->agreePayment;
             if(!empty($newdata->profit) || $newdata->profit != 0)
             {
                 $newdata->profit =  $newdata->profit-$newdata->smepaid;
             }
        }
        else
        {
            $newdata->smeunpaid = $newdata->smeunpaid +$request->agreePayment;
            if(!empty($newdata->profit) || $newdata->profit != 0)
            {
                 $newdata->profit =  $newdata->profit-$newdata->smeunpaid;
            }
        }
       // print_r($request->transactionId);exit;
        $newdata->update();
        $id =$request->id;
        $status=1;
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->firstname;
        $request->caseWorkerId = $caseworker->id;
        
        
        $contact = Contact::where('id',$request->contactId)->first();
        $request->contactName = $contact->firstname;
        $request->contactId = $contact->id;
        
        $worktype = worktype::where('id',$request->workType)->first();
        $request->workType = $worktype->name;
        $request->workTypeId = $worktype->id;
        
       
       
         $batch = caseworker::where('id',$request->batch)->first();
        if(!empty($batch))
        {
            $request->batchName = $batch->name;
            $request->batchId = $batch->id;
        }
        $value = $request->technology;
                $technology = Technology::where('id',$value)->first();
                 $request->technology = $technology->name;
                 $request->technologyId = $technology->id;
             transactiontechnology::insert([
                'transactionId'=>$request->transactionId,
               'contactId'=>$request->contactId,
                'status'=> $status,
                'technology'=>$request->technology,
                'technologyId'=>$request->technologyId,
                'contactName'=>$request->contactName,
                'smeid'=>$request->smeid,
                'reasonSME'=>$request->reasonSME,
                'agreePayment'=>$request->agreePayment,
                'comment'=>$request->comment,
                'NoofInterviews'=>$request->NoofInterviews,
                'smeid'=>$request->smeid,
                'workTypeId'=>$request->workTypeId,
                'workType'=>$request->workType,
                'caseWorkerId'=>$request->caseWorkerId,
                'caseWorker'=>$request->caseWorker,
                'SMEName'=>$request->SMEName,
                'batchName'=>$request->batchName,
                'batchId'=>$request->batchId,
                'created_at'=>Carbon::now(),
                'createdBy'=> Auth::guard('adminiy')->user()->id,
          ]);
            
    
         if(!empty($request->SMEAgreeAmount))
          {
                smeagreepayments::insert([
                    'transactionId'=>$id,
                    'contactId'=>$request->contactId,
                    'smeId'=>$request->smeid,
                    'interviewId'=>$id,
                    'type'=>$request->workType,
                    'agreeamount'=>$request->agreePayment,
                    'status'=>$request->paymentStatus,
                    'date'=>date('Y-m-d'),
                    'employee'=>$request->caseWorkerId,
                    'created_at'=>Carbon::now(),
                    'createdBy'=> Auth::guard('adminiy')->user()->id,
                    'updated_at'=>Carbon::now(),
                    'updatedBy'=> Auth::guard('adminiy')->user()->id,
                ]); 
          }
        $id =  smecomments::create([
                'transactiontechnology'=>$id,
                'reason'=>$request->reasonSME,
                'comments'=>$request->comment,
                'newsmeId'=>$request->smeid,
                'newSmeName'=>$request->SMEName,
                'oldSmeId'=>$data->smeid,
                'oldSmeName'=>$data->SMEName,
                'changeDate'=>date('Y-m-d'),
                'paymenttosme'=>$request->agreePayment,
                'paymentStatus'=>$request->paymentStatus,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
          ])->id;
         if ($request->statusTypeId == 3 || $request->statusTypeId == 6) {
            return Redirect()->route('interviews',$interview->transactionId)->with('success','Interviews updated successfully');
        }
        else
        {
            return Redirect()->back()->with('success','Comment Added successfully');
        }
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Skills::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Skills deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Skills::find($id);
        $view=1;
         $technology = Technology::where('status',1)->get();
        $html = view('modal.Skills_edit',compact('data','view','technology'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = interviews::find($id);
        $getTransactions =Transactions::where('id', $data->transactionId)
              ->first();
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $sme = Contact::where('status',1)->where('contactTypeId',1)->get();
        $worktype = worktype::where('status',1)->get();
        $statustype = statustype::where('status',1)->get();
        $interviewtools = interviewtools::where('status',1)->get();
        $caseworker = User::where('status',1)->get();
        $view=0;
        $html = view('modal.interviews_edit',compact('data','view','technology','getTransactions','contact','worktype','caseworker','sme','statustype','interviewtools'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        $data = interviews::find($id);
        
        $technology = Technology::where('id',$request->technology)->first();
        $request->technology = $technology->name;
        $request->technologyId = $technology->id;
        
       $caseworker = caseworker::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->name;
        $request->caseWorkerId = $caseworker->id;
        
        
        $contact = Contact::where('id',$request->contactName)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
        
        
        
        $sme = Contact::where('id',$request->SMEName)->first();
        $request->SMEName = $sme->firstname;
        $request->smeid = $sme->id;
        
        
        $update = interviews::find($id)->update([
            'transactionId'=>$data->transactionId,
                'Noofrounds'=>$request->Noofrounds,
                'contactId'=>$request->contactId,
                'consultantName'=>$request->consultantName,
                'modeofinterview'=>$request->modeofinterview,
                'technologyId'=>$request->technologyId,
                'technology'=>$request->technology,
                'SMEAgreeAmount'=>$request->SMEAgreeAmount,
                'Status'=>$request->Status,
                'comment'=>$request->comment    ,
                'smeid'=>$request->smeid,
                'SMEName'=>$request->SMEName,
                'paymentStatus'=>$request->paymentStatus,
                'caseWorkerId'=>$request->caseWorkerId,
                'caseWorker'=>$request->caseWorker,
                'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('interviews',$data->transactionId)->with('success','Interviews updated successfully');
        }
        return Redirect()->route('interviews',$data->transactionId)->with('failed','please try again');
      }

}