<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Movie;
use App\Models\Artist;
use App\Models\Studio;  
use App\Models\Genre;  
use App\Models\Transactions; 
use App\Models\logs;
use App\Models\transactioninstallments;
use App\Models\StudioTransactions;
use App\Models\UserTransaction;
use PDF;
use DateTime;
use App\Models\Adminiy;
use App\Models\TermsCondition;
class TermsConditionController extends Controller
{
    public function index(Request $request)    
    {     
        if ($request->ajax()) {
         
             if (Auth::guard('adminiy')->user()->role == "studio") {
                
                 $data =TermsCondition::get();
            }else{
             $data =TermsCondition::get();
            }
            
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    if (Auth::guard('adminiy')->user()->role != "studio") {
                $btn = '<button class="btn p-0 ms-2" id="termscondition_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
               ';
                    }
                    else{
                        
                         $btn = '
                <button class="btn p-0 ms-2" id="termscondition_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>';
                    }
                return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('termscondition');
    }
    public function edit($id){
       
      $data = TermsCondition::where('id',$id)->first();
      
        $view=0;
        $html = view('modal.termscondition_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
       
      $update = TermsCondition::find($request->id)->update([
            'text'=> trim($request->comments),
        ]);
        
        if ($update) {
                return Redirect()->route('termscondition')->with('success','Terms and condition updated successfully');
        }else{
            return Redirect()->route('termscondition')->with('failed','please try again');
        }
        return Redirect()->route('termscondition')->with('failed','please try again');
      
    }
    
    public function view(){
         $data =TermsCondition::first();
        
         
          return view('termsconditionview',compact('data'));
        
    }
    
    
    
}
