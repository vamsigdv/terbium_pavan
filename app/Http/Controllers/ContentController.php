<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\H1B;
use App\Models\Admin_session;
use Illuminate\Support\Facades\Validator;
use Redirect;
use App\Models\H1BDocument;
use App\Models\Skills;
use App\Models\location;
use App\Models\Cap;
use App\Models\User;
use App\Models\H1BCommunication;
use App\Models\Notifications;

class ContentController extends Controller
{
    public function H1BAppicationForm()
    {
        $skills = Skills::where('status', 1)->get();
        $locations = location::where('status', 1)->get();
        $cap = Cap::get();
        $caseworkers = User::where('status',1)->where('role','>','1')->get();
        return view('h1b.h1b_application_form', compact('skills', 'locations', 'caseworkers','cap')); 
    }
    
    public function H1BSave(Request $request)
    {
        \Log::error($request);
        $data = $request->all();
        $created = H1B::create([
             'wel_email'=>$request->wel_email,
          'Visa_int_slot'=>$request->Visa_int_slot,
              'vendor_doc'=>$request->vendor_doc,
              'h1b_petition_doc'=>$request->h1b_petition_doc,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'email' => $request->email_id,
            'passport' => $request->passport,
            'application_status' => $request->application_status,
            'case_worker' => $request->case_worker,
            'years_exp' => $request->experiance,
            'gender' => $request->gender,
            'marital_status' => $request->marital_status,
            'ref_by' => $request->ref_by,
            'doc_verified' => $request->doc_verified,
            'co_affidevit' => $request->co_affidevit,
            'uscis_status' => $request->uscis_status,
            'uscis_cap' => $request->uscis_cap,
            'pre_recipt' => $request->pre_reg_recipt,
            'edu_evaluation' => $request->education_evalution,
            'lca' => $request->lca,
            'ds_form' => $request->ds_160_form,
            'rfe' => $request->rfe,
            'phone' => $request->mobile,
            'qualification' => $request->qualification,
            'technology' => $request->technology, ##
          //  'skills' => implode(', ', $request->skills),
            'skills' => $request->skills,
            'country_loc' => $request->country,
            'dependents' => $request->dependents,
            'attorney_emp' => $request->att_case_worker,
            'ref_contact' => $request->ref_contact,
            'ack_email' => $request->email_sent,
            'service_center' => $request->service_center,
            'pre_status' => $request->pre_reg_status,
            'bio_details' => $request->bio_metric_detatils,
            'organization' => $request->organization,
            'h1b_recp' => $request->h1b_recipt,
            'documentation' => $request->documentation,
            'ship_tracker' => $request->ship_tracker,
            'visa_slot_det' => $request->visa_slot_det,
            'visa_int_req_doc' => $request->visa_int_req_doc,
            'visa_interview' => $request->visa_interview,
            'rfe_status' => $request->rfe_status,
            'status' => 1,
            'technolgy' => $request->technolgy,
            'created_at' => Carbon::now(),
            
        ])->id;
        $files = $request->file('uploaded_document');
        if (isset($data['uploaded_document']) && is_array($data['uploaded_document']) && count($data['uploaded_document']) > 1) {
   
            foreach ($data['uploaded_document'] as $key => $document) {
                $name_gen = hexdec(uniqid());
                $file_ext = strtolower($document['doc']->getClientOriginalExtension());
                $file_name = $name_gen.'.'. $file_ext;
                $up_location = 'uploads/h1b/'.$created. '/';
                $img_path = $up_location.$file_name;
                $document['doc']->move($up_location,$file_name);
                
                H1BDocument::create([
                    'h1b_id' => $created,
                    'document_id' => $document['name'],
                    'document' => $img_path,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
        Notifications::create([
            'message' => $request->first_name . ' ' . $request->last_name . ' created by ' . $request->case_worker,
            'type' => "H1B",
            'updated_at'=>Carbon::now(),
            'created_at'=>Carbon::now(),
        ]);
        if (isset($data['communication']))
        {
            foreach ($data['communication'] as $communication) {
                H1BCommunication::create([
                    'h1b_id' => $created,
                    'title' => $request->comm_title,
                    'comments' => $communication,
                    'created_at' => Carbon::now(),
                ]);
            }
        }
       
        return Redirect()->back()->with('success','Form Submitted!');
    }
}