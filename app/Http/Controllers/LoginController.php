<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Adminiy;
use App\Models\Admin_session;
use Hash;
use App\Rules\PasswordMatch;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminiyLoginRequest;
use App\Models\Studio;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use App\Models\H1B;
use App\Jobs\SendEmailJob as SEJ;
use Illuminate\Support\Facades\Bus;
use App\Helper\JsonApiResponse;

class LoginController extends Controller
{
    
    public function loginUsingFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function testEmail(){
        
        $details = [
            'title' => "Mail",
            'password'=>'password',
            'subject' => 'subject',
            'email' => 'vamsikrishnanarayana97@gmail.com',
            'mail_type' =>'dummyMaillableTemplate',
            'username'=> 'vamsi',
            'url'=>'oogle.com'
        ];
        Bus::dispatch(new SEJ($details));
       
       
    }
    public function callbackFromFacebook()
    {
     try {
          $user = Socialite::driver('facebook')->user();

          $saveUser = User::updateOrCreate([
              'facebook_id' => $user->getId(),
          ],[
              'name' => $user->getName(),
              'email' => $user->getEmail(),
              'password' => Hash::make($user->getName().'@'.$user->getId())
               ]);

          Auth::loginUsingId($saveUser->id);

          return redirect()->route('home');
          } catch (\Throwable $th) {
             throw $th;
          }
      }


    public function show()
    {
        
        if(Auth::guard('adminiy')->check()){
            return redirect()->route('adminiy.panel');
        }
        return view('login');
    }
    
    public function loginwithotp()
    {
      
   

    
      //  echo 'hi';exit;
        return view('loginwithotp');
    }
    public function OTPValidate()
    {
       // echo 'hi';exit;
        return view('otpvalidate');
    }
    public function validateotp(Request $request)
    {
         $request->validate(
           [
                'otp'     => 'required'
            ]
        );
       $user = Adminiy::where('contact', Session::get('contact'))->first();
       if($user)
       {
            if($user->otp == $request->otp)
            {
                 
                  $user->otp = "";
                  $user->update();
                  if(Auth::guard('adminiy')->attempt(['email'=> $user->email, 'password'=> $user->showPassword1], '1')){
            
                        \Log::info(Auth::guard('adminiy')->check());
                        $Admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $user->id)->orderBy('created_at', 'desc')->first();
                     
                        if($Admin_session){
                          
                        }elseif (Auth::guard('adminiy')) {
                           $Admin_session = Admin_session::create([
                            "user_id" => $user->id,
                            "check_login" => 1,
                            "check_logout" => 0,
                            ]);
                        }
                      return redirect()->route('adminiy.panel');
                    } 
                    else
                    {
                        echo 'hello';exit;
                    }
                  
            }
            else
            {
                return redirect()->route('otpvalidate')->withInput($request->input())->withError("Incorrect Otp");
            }
       }
       //return view('validateotp');
    }
    public function sendotp(Request $request){
        
        $request->validate(
           [
                'contact'     => 'required'
            ]
        );
         $user = Adminiy::where('contact',$request->contact)->first();
         if($user)
         {
             $mobile_number = trim($request->contact);
              $characters = '1234567890';
              $length_otp = 5;
              $charactersLength = strlen($characters);
              $randomString_otp = '';
              for ($io = 0; $io < $length_otp; $io++) {
                $randomString_otp .= $characters[rand(0, $charactersLength - 1)];
              }
              $username = "slinstamart@gmail.com";
              $hash = "Njg3MDU1NjI2ZTQ1MzczODM0NDczMzY4MzA2MjMxNDY=";
            	// Config variables. Consult http://api.textlocal.in/docs for more info.
              $test = "0";
              $sender = "SLDHDP"; // This is who the message appears to be from.
        	  $numbers = $mobile_number;
              $message = "$randomString_otp  is your verification code for login - SLDHDP";
              $apiKey = urlencode('NGU2NzcwMzkzOTVhNGEzOTMzNmM0ODcxNTU1NzM4MzM=');
              $message = rawurlencode($message);
              $numbers = $mobile_number;
              $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
                 
                	// Send the POST request with cURL
              $ch = curl_init('https://api.textlocal.in/send/');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $response = curl_exec($ch);
              curl_close($ch);
              $user->otp = $randomString_otp;
              $user->update();
             // Session::put('variableName', $value);
              Session::put("contact",$mobile_number);
              return redirect()->route('otpvalidate');
         }
         else
         {
             //return redirect('loginwithotp')->with('message', 'Welcome to ItSolutionStuff Tutorials!');
             return redirect()->route('loginwithotp')->withInput($request->input())->withError("Contact Not available");
         }
    }
    public function resendotp(){
        
         $user = Adminiy::where('contact',Session::get('contact'))->first();
         if($user)
         {
             $mobile_number = trim(Session::get('contact'));
              $characters = '1234567890';
              $length_otp = 5;
              $charactersLength = strlen($characters);
              $randomString_otp = '';
              for ($io = 0; $io < $length_otp; $io++) {
                $randomString_otp .= $characters[rand(0, $charactersLength - 1)];
              }
              $username = "slinstamart@gmail.com";
              $hash = "Njg3MDU1NjI2ZTQ1MzczODM0NDczMzY4MzA2MjMxNDY=";
            	// Config variables. Consult http://api.textlocal.in/docs for more info.
              $test = "0";
              $sender = "SLDHDP"; // This is who the message appears to be from.
        	  $numbers = $mobile_number;
              $message = "$randomString_otp  is your verification code for login - SLDHDP";
              $apiKey = urlencode('NGU2NzcwMzkzOTVhNGEzOTMzNmM0ODcxNTU1NzM4MzM=');
              $message = rawurlencode($message);
              $numbers = $mobile_number;
              $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
                 
                	// Send the POST request with cURL
              $ch = curl_init('https://api.textlocal.in/send/');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $response = curl_exec($ch);
              curl_close($ch);
              $user->otp = $randomString_otp;
              $user->update();
             // Session::put('variableName', $value);
              Session::put("contact",$mobile_number);
              return redirect()->route('otpvalidate');   
         }
         
    }


    public function loginEmailWithotp()
    {
        return view('email_otp.loginwithotp');
    }

    public function sendEmailotp(Request $request){
        
        $request->validate(
           [
                'email'     => 'required'
            ]
        );
        $password=$this->generateRandomPassword();
         $user = Adminiy::where('email',$request->email)->first();
         if($user)
         {
            $user->email_otp = $this->generateRandomPassword();
            $user->update();
            Session::put("email", trim($request->email));
            return redirect()->route('emailotpvalidate');
         }
         else
         {
             //return redirect('loginwithotp')->with('message', 'Welcome to ItSolutionStuff Tutorials!');
             return redirect()->route('email.loginwithotp')->withInput($request->input())->withError("Contact Not available");
         }
    }

    public function resendEmailotp(){
        
        $user = Adminiy::where('email',Session::get('email'))->first();
        if($user)
        {
            $otp=$this->generateRandomPassword();
            $user->email_otp = $otp;
            $user->update();
            Session::put("email", trim($user->email));
            $details = [
                'title' => "Terbium One Time Password",
                'password'=>$otp,
                'subject' => 'Terbium One Time Password',
                'email' => $user->email,
                'mail_type' =>'dummyMaillableTemplate',
                'username'=> 'vamsi',
                'url'=>route('emailotpvalidate')
            ];
            Bus::dispatch(new SEJ($details));
            return redirect()->route('emailotpvalidate');
      
            
        }
        
   }

    public function EmailValidateotp(Request $request)
    {
         $request->validate(
           [
                'otp'     => 'required'
            ]
        );
       // print_r($request->otp);exit;
       $user = Adminiy::where('email',Session::get('email'))->first();
       \Log::info("yes");
       if($user)
       {
            if($user->email_otp == $request->otp)
            {
                 
                
                //   if (Auth::guard('adminiy')->attempt(['email' => $user->email], true)) {
                    
                  if(Auth::guard('adminiy')->attempt(['email'=> $user->email, 'email_otp'=> $user->email_otp])){
            
                        $Admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $user->id)->orderBy('created_at', 'desc')->first();
                     
                        if($Admin_session){
                          
                        }elseif (Auth::guard('adminiy')) {
                           $Admin_session = Admin_session::create([
                            "user_id" => $user->id,
                            "check_login" => 1,
                            "check_logout" => 0,
                            ]);
                        }
                        $user->email_otp = "";
                        $user->update();
                      return redirect()->route('adminiy.panel');
                    } 
                    else
                    {
                        return redirect()
                        ->route('emailotpvalidate')
                        ->withInput($request->input())
                        ->withErrors(['otp' => 'some thing went wrong please contact support team']);
                    }
                  
            }
            else
            {

\Log::info("yes");
                return redirect()
                    ->route('emailotpvalidate')
                    ->withInput($request->input())
                    ->withErrors(['otp' => 'Incorrect OTP']);

                return redirect()->route('emailotpvalidate')->withInput($request->input())->withError("Incorrect Otp");
            }
       }
       //return view('validateotp');
    }

    public function EmailOTPValidate()
    {
       // echo 'hi';exit;
        return view('email_otp.otpvalidate');
    }

    public function slogin(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required|string|min:6',
            'identifier' => 'required|string', // Assuming the input field is named 'identifier'
        ]);

        $loginField = filter_var($validatedData['identifier'], FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';

        $credentials = [
            $loginField => $validatedData['identifier'],
            'password' => $validatedData['password'],
        ];

        if (auth()->attempt($credentials)) {
            return JsonApiResponse::success('Successfully logged in.');
        } else {
            return JsonApiResponse::error('Please enter correct credentials.', 401);
        }
    }
    public function login(Request $request)
    {
        return JsonApiResponse::error('Please enter correct credentials.', 401);
        $request->validate(
           [
                // 'email'     => 'required',
                'password'  => 'required'
            ]
        );
        if(Auth::guard('adminiy')->check()){
            return JsonApiResponse::error('failed.', 401);
            return redirect()->route('adminiy.panel');
        }
       
        // $user = Adminiy::where('email',$request->email)->first();
        
         $user = Adminiy::where('email',$request->email)->where('is_active',1)->orwhere('contact',$request->email)->first();

        if(empty($user))
        {
            return JsonApiResponse::error('User not found.', 401);
            return redirect()->route('login')->withInput($request->input())->withError("user not found");
        }
        
        // print_r($user);exit;
        $remember=0;
        if(isset($request->remember)){
            $remember=1;
        }
        
          if (Str::contains($request->email, '@')) {
            $requestType = "email";
        } else {
            $requestType = "contact";
        }
        
        // if(Auth::guard('adminiy')->attempt(['email'=> $request->email, 'password'=> $request->password], $remember)){
        
           if(Auth::guard('adminiy')->attempt([$requestType => $request->email, 'password'=> $request->password], $remember)){
            
            $Admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $user->id)->orderBy('created_at', 'desc')->first();
         
            if($Admin_session){
              
            }elseif (Auth::guard('adminiy')) {
               $Admin_session = Admin_session::create([
                "user_id" => $user->id,
                "check_login" => 1,
                "check_logout" => 0,
                ]);
            }
            if(Auth::guard('adminiy')->user()->role == 'content approver')
            {
                 return redirect()->route('audit');
            }
            elseif( Auth::guard('adminiy')->user()->role == 'financial team')
            {
                return redirect()->route('');
            }
            else
            {
               return redirect()->route('adminiy.panel');
            }
        } else {
            return redirect()->route('login')->withInput($request->input())->withError("Incorrect username and password");
        }
    }
    public function authenticate(Request $request)
    {
        $request->validate(
           [
                // 'email'     => 'required',
                'password'  => 'required'
            ]
        );
        if(Auth::guard('adminiy')->check()){
            return redirect()->route('adminiy.panel');
        }
       
        // $user = Adminiy::where('email',$request->email)->first();
        
         $user = Adminiy::where('email',$request->email)->where('is_active',1)->orwhere('contact',$request->email)->first();

        if(empty($user))
        {
            return redirect()->route('login')->withInput($request->input())->withError("user not found");
        }
        
        // print_r($user);exit;
        $remember=0;
        if(isset($request->remember)){
            $remember=1;
        }
        
          if (Str::contains($request->email, '@')) {
            $requestType = "email";
        } else {
            $requestType = "contact";
        }
        
        // if(Auth::guard('adminiy')->attempt(['email'=> $request->email, 'password'=> $request->password], $remember)){
        
           if(Auth::guard('adminiy')->attempt([$requestType => $request->email, 'password'=> $request->password], $remember)){
            
            $Admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $user->id)->orderBy('created_at', 'desc')->first();
         
            if($Admin_session){
              
            }elseif (Auth::guard('adminiy')) {
               $Admin_session = Admin_session::create([
                "user_id" => $user->id,
                "check_login" => 1,
                "check_logout" => 0,
                ]);
            }
            if(Auth::guard('adminiy')->user()->role == 'content approver')
            {
                 return redirect()->route('audit');
            }
            elseif( Auth::guard('adminiy')->user()->role == 'financial team')
            {
                return redirect()->route('');
            }
            else
            {
               return redirect()->route('adminiy.panel');
            }
        } else {
            return redirect()->route('login')->withInput($request->input())->withError("Incorrect username and password");
        }
    }

    public function authenticateEmailWithOtp(Request $request)
    {
        $request->validate(
           [
                // 'email'     => 'required',
                'password'  => 'required'
            ]
        );
        if(Auth::guard('adminiy')->check()){
            return redirect()->route('adminiy.panel');
        }
       
        // $user = Adminiy::where('email',$request->email)->first();
        // $user = Adminiy::where('email',$request->email)->where('password',Hash::make($request->password))->where('is_active',1)
         $user = Adminiy::where('email',$request->email)->where('is_active',1)
                // ->where('password',Hash::make($request->password))
                ->first();

        if(empty($user))
        {
            return redirect()->route('login')->withInput($request->input())->withError("Invalid Credentials");
        }
       
        if($user)
        {
            $otp=$this->generateRandomPassword();
            $user->email_otp = $otp;
            $user->update();
            Session::put("email", trim($user->email));
            $details = [
                'title' => "Terbium One Time Password",
                'password'=>$otp,
                'subject' => 'Terbium One Time Password',
                'email' => $user->email,
                'mail_type' =>'dummyMaillableTemplate',
                'username'=> 'vamsi',
                'url'=>route('emailotpvalidate')
            ];
            Bus::dispatch(new SEJ($details));
             return redirect()->route('emailotpvalidate');
        }
        else
        {
              //return redirect('loginwithotp')->with('message', 'Welcome to ItSolutionStuff Tutorials!');
              return redirect()->route('login')->withInput($request->input())->withError("Email not available");
        }
    }
    public function logout(){
        $user = Auth::guard('adminiy')->user();
        $admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $user->id)->orderBy('created_at', 'desc')->first();
       
        if($admin_session){
            $admin_session->check_login = 0;
            $admin_session->check_logout = 1;
            $admin_session->save();
        }
        
        Auth::guard('adminiy')->logout();
        
        return redirect()->route('login');
    }
    
    public function H1BAppicationForm()
    {
       return view('h1b.h1b_application_form'); 
    }
    
    public function H1BSave(Request $request)
    {
        $data = $request->all();
        H1B::create([]);
        return redirect()->back()->with('message', 'IT WORKS!');
    }

    public function generateRandomPassword($length = 10) {
        $lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
        $uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $numbers = '0123456789';
        $specialCharacters = '!@#$%^&*';
        
        $characterSets = [$lowercaseLetters, $uppercaseLetters, $numbers, $specialCharacters];
        $password = '';
        
        // Make sure there is at least one character from each character set
        foreach ($characterSets as $charSet) {
            $password .= $charSet[rand(0, strlen($charSet) - 1)];
        }
        
        // Fill the rest of the password with random characters
        for ($i = strlen($password); $i < $length; $i++) {
            $randomCharSet = $characterSets[rand(0, count($characterSets) - 1)];
            $password .= $randomCharSet[rand(0, strlen($randomCharSet) - 1)];
        }
        
        // Shuffle the password to randomize the character order
        $password = str_shuffle($password);
        
        // Ensure there are no more than 2 identical characters in a row
        $password = preg_replace('/(.)\\1{2,}/', '$1$1', $password);
        
        return $password;
    }
}
