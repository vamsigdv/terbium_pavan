<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Notifications;
use Illuminate\Support\Carbon;
use Session;
use App\Models\Movie;
use Illuminate\Support\Facades\Auth;
class NotificationsController extends Controller
{
    /**
     * To get the list of Live Stream
     * 
     * @param Request
     * @return array
     */
    public function index(Request $request)
    {
        $movies = Movie::where('status', 2)->where('delete', 1)->select('id', 'name')->get();
        if ($request->ajax()) {
            if (Auth::guard('adminiy')->user()->role != "studio") {
                $data = Notifications::where('is_delete', 1)->get();
            } else {
                $data = [];
            }
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="notification_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <a href="' . route('notifications.destroy',$row->id) . '" id="'.$row->id.'" data-method="DELETE" data-url="' . route('notifications.index') . '" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>
                    
                      <a href="' . route('notifications.send',$row->id) . '" data-url="' . route('notifications.send',$row->id) . '"  data-id="'.$row->id.'"  class="button btn btn-succes notification_send">
                             Send Notification
                    </a>
                    
                    ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('notifications', compact('movies'));
    }

    /**
     * To store the values from the form
     * 
     * @params  Request $reqeust
     * @return array
     */
    public function store(Request $request)
    {  
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('thumbnail_image');
        $name_stream = hexdec(uniqid());
        $file_ext = strtolower($file->getClientOriginalExtension());
        $file_name = $name_stream . '.' . $file_ext;
        $up_location = 'uploads/notifications/';
        $thumbnail =  $up_location . $file_name;
        $file->move($up_location, $file_name);
        $notificationInsert = Notifications::create([
              'image' => $thumbnail,
              'message' => $request->description,
              'external_link' => $request->external_link,
              'title' => $request->title,
              'type' => $request->type,
              'date_time' => $request->date_time,
              'status' => $status,
              'created_at' => Carbon::now(),
              'movie_id' => $request->movie_id
          ]);
          
            $details = Notifications::where('id',$notificationInsert->id)->first();

          Notifications::sendPushNotification([
              'title' => strip_tags($details->title)
          ]);
          
        return Redirect()->back()->with('success','Notification details created successfully');
    }
    
     public function notificationSend($id)
    {
       $details = Notifications::where('id',$id)->first();

        Notifications::sendPushNotification([
            'title' => strip_tags($details->title)
        ]);

        return Redirect()->back()->with('success','Notification Send successfully');

    }

    /**
     * delete stream details
     * 
     * @params  int $id
     * @return array
     */
    public function destroy($id)
    {   
        $delete = Notifications::find($id)->update([
            'is_delete' => 0,
            'updated_at' => Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Details deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }

    /**
     * To show Notification details
     * 
     * @params int $id
     * @return array
     */
    public function show($id) 
    {
        $data = Notifications::find($id);
        $view=1;
        $html = view('modal.notification_edit',compact('data','view'))->render();
        return json_encode($html);
    }

    /**
     * To view Notification details
     * 
     * @param int $id
     * @return array
     */
    public function edit($id)
    {
        $movies = Movie::where('status', 2)->where('delete', 1)->select('id', 'name')->get();
        $data = Notifications::find($id);
        $view=0;
        $html = view('modal.notification_edit',compact('data','view', 'movies'))->render();
        return json_encode($html);
    }

    /**
     * To update the details if exists
     * 
     * @param Request $request
     * @return array
     */
    public function update(Request $request)
    {
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $notifications = Notifications::find($id);
        $file=$request->file('image');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/notifications/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        } else {
            $img_path=$request->photo_old;
        }
        $update = $notifications->update([
            'image' => $img_path,
            'message' => $request->description,
            'external_link' => $request->external_link,
            'title' => $request->title,
            'type' => $request->type,
            'date_time' => $request->date_time,
            'movie_id' => $request->movie_id,
            'status' => $status,
            'updated_at' => Carbon::now()
        ]);
        if ($update) {
            return Redirect()->route('notifications.index')->with('success','notifications updated successfully');
        }
        return Redirect()->route('notifications.index')->with('failed','please try again');
    }
}
