<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Role;
use App\Models\Permission;
use App\Models\RolePermission;

class RolesController extends Controller
{
    public function index() 
    {
        $roles = Role::all();
        $permissions = Permission::all();
        $role_permission = DB::table('role_permission')->get();
        return view('userprivileges', compact('roles', 'permissions', 'role_permission'));
    }
    
    public function store(Request $request)
    {
        DB::table('role_permission')->delete();
        foreach ($request->screen as $permissions) {
          foreach ($permissions as $role_id => $permission) {
              DB::table('role_permission')->insert([
                  'role_id' => $role_id,
                  'permission_id' => $permission
                  ]);
          }
       }
       return Redirect()->back()->with('success','Role upadated successfully!');
    }
}
?>