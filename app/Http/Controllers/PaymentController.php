<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\PAYMENT_criteria;
class PaymentController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PAYMENT_criteria::get();
           // print_r($data);exit;
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="paymentcriteria_edit" type="button" data-id="'.$row->criteriaId.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="paymentcriteria_view" type="button" data-id="'.$row->criteriaId.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('paymentcriteria.destroy',$row->criteriaId).'" id="'.$row->criteriaId.'" data-method="DELETE" data-url="'.route('paymentcriteria.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('payments');
    }
    public function store(Request $request)
    {   
        
        PAYMENT_criteria::insert([
              'paymentCategory'=>$request->paymentCategory,
              'videoType'=>$request->videoType,
              'paymentType'=>$request->paymentType,
              'Description'=>$request->Description,
              'Status'=> $request->Status,
              'price'=> $request->price,
              'perview'=>$request->perview,
              'perviewamount'=> $request->perviewamount,
              'rentalamount'=> $request->rentalamount,
              'created_at'=>Carbon::now(),
              'updated_at'=>Carbon::now(),
          ]);
        return Redirect()->back()->with('success','Payment Criteria created successfully');
    }
    
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = PAYMENT_criteria::where('criteriaId',$id)->update([
            'Status'=>'In-active',
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'PAYMENT_criteria deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    
    public function edit($id){
        $data = PAYMENT_criteria::where('criteriaId',$id)->first();
       // print_r($data);exit;
        $view=0;
        $html = view('modal.paymentcriteria_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function show($id){
        $data = PAYMENT_criteria::where('criteriaId',$id)->first();
        $view=1;
        $html = view('modal.paymentcriteria_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
      //  print_r($id);exit;
        
        $update = PAYMENT_criteria::where('criteriaId',$id)->update([
              'paymentCategory'=>$request->paymentCategory,
              'videoType'=>$request->videoType,
              'paymentType'=>$request->paymentType,
              'Description'=>$request->Description,
              'Status'=> $request->Status,
                'price'=> $request->price,
              'perview'=>$request->perview,
              'perviewamount'=> $request->perviewamount,
              'rentalamount'=> $request->rentalamount,
              'updated_at'=>Carbon::now(),
        ]);
         return Redirect()->route('paymentcriteria.index')->with('success','PAYMENT Criteria updated successfully');
        // return Redirect()->route('paymentcriteria.index')->with('failed','please try again');
      }
    public function fetchPrice(Request $request)
    {
        $data = PAYMENT_criteria::where("criteriaId",$request->paymentcriteria)->first();
        return response()->json($data);
    }
    
    public function paymentcriterialist(Request $request)
    {

       $paymentcriteria = PAYMENT_criteria::where('Status','Active')->where('videoType',$request->type)->get();

        return response()->json($paymentcriteria);

    }
    
}