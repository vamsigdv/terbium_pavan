<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\location;
use DataTables;
use Session;
class LocationController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = location::get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="location_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="location_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('location.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('location.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('location');
    }
    public function store(Request $request)
    {   
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        location::insert([
              'countryCode'=>$request->countryCode,
              'name'=>$request->name,
              'status'=> $status,
              'created_at'=>Carbon::now(),
              'createdBy'=> Auth::guard('adminiy')->user()->id,
              
          ]);
        return Redirect()->back()->with('success','location created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = location::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'location deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = location::find($id);
        $view=1;
        $html = view('modal.location_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    
    public function fetchlocation(Request $request){
        $id =$request->location;
        $data = location::find($id);
       
        return json_encode($data);
    }
    public function edit($id){
        $data = location::find($id);
        $view=0;
        $html = view('modal.location_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $update = location::find($id)->update([
            'name'=>$request->name,
             'countryCode'=>$request->countryCode,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('location.index')->with('success','location updated successfully');
        }
        return Redirect()->route('location.index')->with('failed','please try again');
      }

}