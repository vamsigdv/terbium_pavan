<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\H1B;
use App\Models\Adminiy;
use App\Models\Admin_session;
use Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\location;
use App\Models\H1BCommunication;
use App\Models\H1BDocument;
use App\Models\Notifications;
use App\Models\Skills;
use App\Models\Cap;

class H1BController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = H1B::where('status', 1)->with('cap')->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    /*<a href="'. url('h1b/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>*/
                    $btn = ' 
                   <a href="'. url('h1bcommunications/'.$row->id).'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-eye" style="color:darkgreen"></i></button> </a>
                     <a href="'.route('h1b.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('h1b.index').'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>
                    ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $locations = location::where('status', 1)->get();
        $cap = Cap::get();
        $caseworkers = User::where('status',1)->where('role','>','1')->get();
        return view('h1b.h1b', compact('locations', 'caseworkers','cap')); 
 
    }
    
    public function searchH1B(Request $request) 
    {
        $h_data = new H1B(); 
         
        if ($request->country_loc != '') {
           $h_data = $h_data->where('country_loc', $request->country_loc);
        }
        if ($request->uscis_cap != '') {
            $h_data = $h_data->where('uscis_cap', $request->uscis_cap);
        }
        if ($request->application_status != '') {
            $h_data = $h_data->where('application_status', $request->application_status);
        }
        if ($request->case_worker != '') {
            $h_data = $h_data->where('case_worker', $request->case_worker);
        }
        //   if ($request->h1b_to_date != '') {
        //     $h_data = $h_data->where('created_at', $request->h1b_to_date);
        // }
        // if ($request->h1b_from_date != '') {
        //     $h_data = $h_data->where('created_at', $request->h1b_from_date);
        // }

        if (($request->h1b_from_date != '' && $request->h1b_to_date != '')) {
            $h_data =  $h_data->whereDate('created_at', '>=', $request->h1b_from_date)
                             ->whereDate('created_at', '<=', $request->h1b_to_date);
         } 
         elseif ($request->h1b_from_date != '') {
            $h_data =  $h_data->whereDate('created_at', '>=', $request->h1b_from_date);
                             
         }
         elseif ($request->h1b_to_date != '') {
            $h_data = $h_data->whereDate('created_at', '<=', $request->h1b_to_date);
         }

        $data = $h_data->where('status', 1)->get();
        return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    /*<a href="'. url('h1b/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>*/
                    $btn = ' 
                   <a href="'. url('h1bcommunications/'.$row->id).'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-eye" style="color:darkgreen"></i></button> </a>
                     <a href="'.route('h1b.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('h1b.index').'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>
                    ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        // return view('h1b.h1b')
    }
    public function documents($id,Request $request)
    {
        if ($request->ajax()) {
            $data = H1BDocument::where('h1b_id', $id)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('documentsdestroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="/h1b/'.$row->h1b_id.'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
       
    }
    
    public function documentsdestroy($id)
    {
       
        //print_r($id);exit;
        $document = H1BDocument::find($id);
       // print_r($document);exit;
        $delete = H1BDocument::where('id',$id)->delete();
        if ($delete) 
        {
           Session::flash('success', 'Document deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    
     public function commentsdestroy($id)
    {
       
        //print_r($id);exit;
        $document = H1BCommunication::find($id);
       // print_r($document);exit;
        $delete = H1BCommunication::where('id',$id)->delete();
        if ($delete) 
        {
           Session::flash('success', 'Commet deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    
    public function h1bcommunications($id,Request $request)
    {
        if ($request->ajax()) {
            $data = H1BCommunication::where('h1b_id', $id)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                     $btn = '
                     <button class="btn p-0 ms-2" id="h1bcomments_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                     <a href="'.route('commentsdestroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="/h1bcommunications/'.$row->h1b_id.'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
       
    }
     public function h1bcommentsedit($id){
        $data = H1BCommunication::find($id);
        $view=0;
        $html = view('modal.h1bcomments_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    
    public function store(Request $request)
    {   
        \Log::info($request);
         $logmessage = Auth::guard('adminiy')->user()->name.' '.Auth::guard('adminiy')->user()->role.' has aproved the movie '.$request->name;
      
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $employee_id=H1B::create([
                'first_name'=>$request->first_name,
                'middle_name'=>$request->middle_name,
                'last_name'=>$request->last_name,
                'email'=>$request->email,
                'passport'=>$request->passport,
                'application_status'=>$request->application_status,
                'case_worker'=>$request->case_worker,
                'years_exp'=>$request->years_exp,
                'gender'=>$request->gender,
                'marital_status'=>$request->marital_status,
                'ref_by'=>$request->ref_by,
                'doc_verified'=>$request->doc_verified,
                'co_affidevit'=>$request->co_affidevit,
                'uscis_status'=>$request->uscis_status,
                'uscis_cap'=>$request->uscis_cap,
                'pre_recipt'=>$request->pre_recipt,
                'edu_evaluation'=>$request->edu_evaluation,
                'lca'=>$request->lca,
                'ds_form'=>$request->ds_form,
                'rfe'=>$request->rfe,
                
                'phone'=>$request->phone,
                'qualification'=>$request->qualification,
                'technology'=>$request->technology,
                'skills'=>$request->skills,
                'country_loc'=>$request->country_loc,
                'dependents'=>$request->dependents,
                'attorney_emp'=>$request->attorney_emp,
                'ref_contact'=>$request->ref_contact,
                'ack_email'=>$request->ack_email,
                'service_center'=>$request->service_center,
                'pre_status'=>$request->pre_status,
                'bio_details'=>$request->bio_details,
                'organization'=>$request->organization,
                'h1b_recp'=>$request->h1b_recp,
                'documentation'=>$request->documentation,
                'ship_tracker'=>$request->ship_tracker,
                'visa_interview'=>$request->visa_interview,
                'rfe_status'=>$request->rfe_status,
                 'technolgy'=>$request-> technolgy,
  'h1b_petition_doc'=>$request->h1b_petition_doc,
  'vendor_doc'=>$request->vendor_doc,
  'Visa_int_slot'=>$request->Visa_int_slot,
  'wel_email'=>$request->wel_email,
  'visa_int_req_doc'=>$request->visa_int_req_doc,
  'visa_slot_det'=>$request->visa_slot_det,
  'pre_reg_recipt'=>$request->pre_reg_recipt,
  'usics_status'=>$request->usics_status,
  'sup_doc'=>$request->sup_doc,
                'status'=> $status,
                'created_at'=>Carbon::now(),
          ])->id;
          if ($employee_id) {
            $admin_id=Adminiy::create([
                'name'=>$request->first_name,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                 'showPassword1'=> $request->password,
                'role'=> 'employee',
                'is_active'=> $status,
                'user_id'=> $employee_id,
                'created_at'=>Carbon::now(),
            ])->id;
            $update = Employee::find($employee_id)->update([
                'admin_id'=> $admin_id,
                'updated_at'=>Carbon::now(),
            ]);
          }
        return Redirect()->back()->with('success','employee created successfully');

        }
    public function empregister()
    {
        return view('empregister');
    }
    public function empregisterstore(Request $request)
    {   
         $validated = $request->validate([
            'username' => 'required',
            'first_name' => 'required',
            'email' => 'required|unique:adminiy',
            'contact' => 'required|unique:studio|numeric',
            'last_name' => 'required',
           // 'account_number' => 'required',
        //    'ifsc' => 'required',
            'photo' => 'required|max:2000',
            'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
        ],[
             'password.regex' => 'Password Min 8 characters with special characters (@ # ! ?etc.) and alphanumeric characters with one uppercase letter!',
             'contact.max'=>'Phone number "Min 10 characters"'
          ]
        );
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
         $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/studio/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        } else {
            $img_path='';
        }
        $employee_id=H1B::create([
                   'first_name'=>$request->first_name,
'middle_name'=>$request->middle_name,
'last_name'=>$request->last_name,
'email'=>$request->email,
'passport'=>$request->passport,
'application_status'=>$request->application_status,
'case_worker'=>$request->case_worker,
'years_exp'=>$request->years_exp,
'gender'=>$request->gender,
'marital_status'=>$request->marital_status,
'ref_by'=>$request->ref_by,
'doc_verified'=>$request->doc_verified,
'co_affidevit'=>$request->co_affidevit,
'uscis_status'=>$request->uscis_status,
'uscis_cap'=>$request->uscis_cap,
'pre_recipt'=>$request->pre_recipt,
'edu_evaluation'=>$request->edu_evaluation,
'lca'=>$request->lca,
'ds_form'=>$request->ds_form,
'rfe'=>$request->rfe,

'phone'=>$request->phone,
'qualification'=>$request->qualification,
'technology'=>$request->technology,
'skills'=>$request->skills,
'country_loc'=>$request->country_loc,
'dependents'=>$request->dependents,
'attorney_emp'=>$request->attorney_emp,
'ref_contact'=>$request->ref_contact,
'ack_email'=>$request->ack_email,
'service_center'=>$request->service_center,
'pre_status'=>$request->pre_status,
'bio_details'=>$request->bio_details,
'organization'=>$request->organization,
'h1b_recp'=>$request->h1b_recp,
 'technolgy'=>$request-> technolgy,
  'h1b_petition_doc'=>$request->h1b_petition_doc,
  'vendor_doc'=>$request->vendor_doc,
  'Visa_int_slot'=>$request->Visa_int_slot,
  'wel_email'=>$request->wel_email,
  'visa_int_req_doc'=>$request->visa_int_req_doc,
  'visa_slot_det'=>$request->visa_slot_det,
  'pre_reg_recipt'=>$request->pre_reg_recipt,
  'usics_status'=>$request->usics_status,
  'sup_doc'=>$request->sup_doc,

'documentation'=>$request->documentation,
'ship_tracker'=>$request->ship_tracker,
'visa_interview'=>$request->visa_interview,
'rfe_status'=>$request->rfe_status,
              'status'=> $status,
              'created_at'=>Carbon::now(),
          ])->id;
          if ($employee_id) {
            $admin_id=Adminiy::create([
                'name'=>$request->username,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                 'showPassword1'=> $request->password,
                'role'=> 'employee',
                'is_active'=> $status,
                'user_id'=> $employee_id,
                'created_at'=>Carbon::now(),
            ])->id;
            $update = H1B::find($employee_id)->update([
                'admin_id'=> $admin_id,
                'updated_at'=>Carbon::now(),
            ]);
          }
       if (Str::contains($request->email, '@')) {
            $requestType = "email";
        } else {
            $requestType = "contact";
        }
        
        $remember = 1;
         if(Auth::guard('adminiy')->attempt([$requestType => $request->email, 'password'=> $request->password], $remember)){
            
            $Admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $admin_id)->orderBy('created_at', 'desc')->first();
         
            if($Admin_session){
              
            }elseif (Auth::guard('adminiy')) {
               $Admin_session = Admin_session::create([
                "user_id" => $admin_id,
                "check_login" => 1,
                "check_logout" => 0,
                ]);
            }
            return redirect()->route('adminiy.panel');
        }

        }
    public function destroy($id)
    {
        $employee = H1B::find($id);
        $delete = H1B::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
          //  $admin_id=Adminiy::find($employee->admin_id)->delete();
           Session::flash('success', 'employee deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    
    public function show($id){
        $h1b = H1B::find($id);
        return view('viewh1b',compact('h1b'));
    }
    
    public function communications($id){
       // print_r($id);exit;
        $h1b = H1B::with('cap')->find($id);
        if($h1b->uscis_cap)
        {
            $cap =Cap::where('id',$h1b->uscis_cap)->first();
        } else {
            $cap ='';
        }
        
        $h1b['locations'] = location::where('status', 1)->where('id',$h1b->country_loc)->first('name');
        return view('h1bcommunications',compact('h1b','cap'));
    }
    public function edit($id){
        $h1b = H1B::find($id);
        $locations = location::where('status', 1)->get();
        $caseworkers = User::where('status',1)->where('role','>','1')->get();
        $communication = H1BCommunication::where('h1b_id',$id)->get();
        $documents = H1BDocument::where('h1b_id',$id)->get();
        $cap = Cap::get();
        return view('edith1b',compact('h1b','locations','caseworkers','documents','communication','cap'));
    }
    public function update(Request $request){
        \Log::error($request->all());
        $data = $request->all();
        $id =$request->edit_id;
        $admin_id =$request->admin_id;
        if($request->edit_status){
            $status=1;
        }else{
            $status=1;
        }
        $update = H1B::find($id)->update([
'first_name'=>$request->first_name,
'middle_name'=>$request->middle_name,
'last_name'=>$request->last_name,
'email'=>$request->email,
'passport'=>$request->passport,
'altphone'=>$request->altphone,
'application_status'=>$request->application_status,
'case_worker'=>$request->case_worker,
'years_exp'=>$request->years_exp,
'gender'=>$request->gender,
'marital_status'=>$request->marital_status,
'ref_by'=>$request->ref_by,
'doc_verified'=>$request->doc_verified,
'co_affidevit'=>$request->co_affidevit,
'uscis_status'=>$request->uscis_status,
'uscis_cap'=>$request->uscis_cap,
'pre_recipt'=>$request->pre_recipt,
'edu_evaluation'=>$request->edu_evaluation,
'lca'=>$request->lca,
'ds_form'=>$request->ds_form,
'rfe'=>$request->rfe,

'phone'=>$request->phone,
'qualification'=>$request->qualification,
//'technology'=>$request->technology,
'skills'=>$request->skills,
'country_loc'=>$request->country_loc,
'dependents'=>$request->dependents,
'attorney_emp'=>$request->attorney_emp,
'ref_contact'=>$request->ref_contact,
'ack_email'=>$request->ack_email,
//'service_center'=>$request->service_center,
'pre_status'=>$request->pre_status,
'bio_details'=>$request->bio_details,
'organization'=>$request->organization,
'h1b_recp'=>$request->h1b_recp,
 'technolgy'=>$request-> technolgy,
  'h1b_petition_doc'=>$request->h1b_petition_doc,
  'vendor_doc'=>$request->vendor_doc,
  'Visa_int_slot'=>$request->Visa_int_slot,
  'wel_email'=>$request->wel_email,
  'visa_int_req_doc'=>$request->visa_int_req_doc,
  'visa_slot_det'=>$request->visa_slot_det,
  'pre_reg_recipt'=>$request->pre_reg_recipt,
  'usics_status'=>$request->usics_status,
  'sup_doc'=>$request->sup_doc,
'documentation'=>$request->documentation,
'ship_tracker'=>$request->ship_tracker,
'visa_interview'=>$request->visa_interview,
'rfe_status'=>$request->rfe_status,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('h1b_documents')->where('h1b_id',$id)->delete();
        DB::table('h1b_communication')->where('h1b_id',$id)->delete();
        $files = $request->file('uploaded_document');
      // print_r($data['uploaded_document']);exit;
        
        if (isset($data['uploaded_document']) && is_array($data['uploaded_document']) && count($data['uploaded_document']) > 1) {
   
            if($data['uploaded_document'][1]['name'] != ""){
                foreach ($data['uploaded_document'] as $key => $document) {
                    $name_gen = hexdec(uniqid());
                    if(!is_string($document['doc'])){
                    $file_ext = strtolower($document['doc']->getClientOriginalExtension());
                    $file_name = $name_gen.'.'. $file_ext;
                    $up_location = 'uploads/h1b/'.$id. '/';
                    $img_path = $up_location.$file_name;
                    $document['doc']->move($up_location,$file_name);
                    }
                    else{
                        $img_path = $document['doc'];
                    }
                    
                    
                    H1BDocument::create([
                        'h1b_id' => $id,
                        'document_id' => $document['name'],
                        'document' => $img_path,
                        'created_at' => Carbon::now(),
                    ]);
                }
            }
        }
        
         if(isset($data['communication']) != []){
        foreach ($data['communication'] as $communication) {
            H1BCommunication::create([
                'h1b_id' => $id,
                'title' => $request->comm_title,
                'comments' => $communication,
                'created_at' => Carbon::now(),
            ]);
        }
         }
       /* if ($update) {
            $admin_id=Adminiy::find($admin_id)->update([
                'name'=>$request->edit_first_name,
                'email'=>$request->edit_email,
                'is_active'=>$status,
                'password'=> Hash::make($request->edit_password),
                 'showPassword1'=> $request->edit_password,
                'updated_at'=>Carbon::now(),
            ]);
            //  if(str_replace("http://prabhu.yoursoftwaredemo.com/","",redirect()->back()->getTargetUrl()) != 'profileupdate')
             if(str_replace(env('APP_URL'),"",redirect()->back()->getTargetUrl()) != 'profileupdate')
            {
            return Redirect()->route('employee.index')->with('success','employee updated successfully');
            }
            else{
                return Redirect()->back()->with('success','Profile updated successfully');
            }
        }
        return Redirect()->route('employee.index')->with('failed','please try again');*/
        Notifications::create([
            'message' => $request->first_name . ' ' . $request->last_name . ' created by ' . $request->case_worker,
            'type' => "H1B",
            'updated_at'=>Carbon::now(),
            'created_at'=>Carbon::now(),
        ]);
         if ($update) {
            return Redirect()->route('h1b.index')->with('success','H1B updated successfully');
        }
        return Redirect()->route('h1b.index')->with('failed','please try again');
      }
      
      
      public function h1bcommunicationsupdate(Request $request){
        $data = $request->all();
        $id =$request->edit_id;
        $h1b = H1BCommunication::where('id',$id)->first()->h1b_id;
        $update = H1BCommunication::find($id)->update([
            'title' => $request->comm_title,
            'comments' => $data['communication'][0],
            'updated_at'=>Carbon::now(),
        ]);
        
        if ($update) {
            return Redirect()->route('communications',$h1b)->with('success','H1B updated successfully');
        }
        return Redirect()->route('communications',$h1b)->with('failed','please try again');
      }
    
      
      public function addh1bdocuments(Request $request){
        $data = $request->all();
        $id =$request->edit_id;
       // print_r($data);exit;
        $files = $request->file('uploaded_document');
       
        if($data['document'] != ""){
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($data['document']->getClientOriginalExtension());
            $file_name = $name_gen.'.'. $file_ext;
            $up_location = 'uploads/h1b/'.$id. '/';
            $img_path = $up_location.$file_name;
            $data['document']->move($up_location,$file_name);
            H1BDocument::create([
                'h1b_id' => $id,
                'document_id' => $request->document_type,
                'document' => $img_path,
                'created_at' => Carbon::now(),
            ]);
            }
        if ($data['document'] != "") 
        {
            return redirect()->back()->with('success', "Documents updated successfully");
        }
        return redirect()->back()->with('failed', "please try again");
      }
      
       public function addh1bcomments(Request $request){
        $data = $request->all();
        $id =$request->edit_id;
        
        if(isset($data['communication']) != []){
        foreach ($data['communication'] as $communication) {
            H1BCommunication::create([
                'h1b_id' => $id,
                'title' => $request->comm_title,
                'comments' => $communication,
                'created_at' => Carbon::now(),
            ]);
        }
         }
        if (isset($data['communication']) != []) 
        {
            return redirect()->back()->with('success', "Com updated successfully");
        }
        return redirect()->back()->with('failed', "please try again");
      }
}
