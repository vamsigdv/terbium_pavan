<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Cap;
use DataTables;
use Session;
use App\Models\Technology;
class CapController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Cap::get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="cap_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <a href="'.route('cap.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('cap.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
               
        }
        return view('cap');

    }
    public function store(Request $request)
    {   
        $validatedData = $request->validate([
            'year' => [
                'required',
                'string',
               
            ],
           
            
        ]);
        Cap::insert([
            'year'=>$validatedData['year'],
            'created_by'=> Auth::guard('adminiy')->user()->id,
            'created_at'=>Carbon::now(),
              
          ]);
        return Redirect()->back()->with('success','Cap created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role !== "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        
        $recordToDelete = Cap::find($id);
        
        if ($recordToDelete) {
            if ($recordToDelete->delete()) {
                Session::flash('success', 'Record deleted successfully');
                return true;
            }
        }
        
        Session::flash('failed', 'Failed to delete record. Please try again.');
        return false;
        
    }
    public function show($id){
        $data = Cap::find($id);
        
        $html = view('modal.cap_edit',compact('data'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = Cap::find($id);
       
        $html = view('modal.cap_edit',compact('data'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;

        $update = Cap::find($id)->update([
            'year'=>$request->year,
           
            'updated_at'=>Carbon::now(),
            'updated_by'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('cap.index')->with('success','Cap updated successfully');
        }
        return Redirect()->route('cap.index')->with('failed','please try again');
      }

}