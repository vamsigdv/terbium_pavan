<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
class CustomerController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Customer::select('id','name','email','phone','status')->where('delete',1)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="customer_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="customer_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('customer.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('customer.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('customers');
    }
    public function store(Request $request)
    {   
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/customers/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $img_path='';
        }
        Customer::insert([
              'name'=>$request->name,
              'email'=>$request->email,
              'photo'=>$request->photo,
              'epic'=>$request->epic,
              'phone'=>$request->phone,
              'password'=>$request->password,
              'status'=> $status,
              'photo'=> $img_path,
              'created_at'=>Carbon::now(),
          ]);
        return Redirect()->back()->with('success','customer created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Customer::find($id)->update([
            'delete'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'customer deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Customer::find($id);
        $view=1;
        $html = view('modal.customer_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = Customer::find($id);
        $view=0;
        $html = view('modal.customer_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/customers/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $img_path=$request->photo_old;
        }
        $update = Customer::find($id)->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'photo'=>$request->photo,
            'epic'=>$request->epic,
            'phone'=>$request->phone,
            'password'=>$request->password,
            'status'=> $status,
            'photo'=> $img_path,
            'updated_at'=>Carbon::now(),
        ]);
        if ($update) {
            return Redirect()->route('customer.index')->with('success','customer updated successfully');
        }
        return Redirect()->route('customer.index')->with('failed','please try again');
      }
}
