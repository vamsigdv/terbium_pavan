<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Contact;
use App\Models\ContactType;
use App\Models\worktype;
use App\Models\consultanttype;
use App\Models\location;
use App\Models\states;
use App\Models\Technology;
use App\Models\Skills;
use App\Models\paymenttype;
use App\Models\accounttype;
use App\Models\caseworker;
use DataTables;
use Session;
use App\Models\User;
use App\Models\transactions;
use App\Models\smeagreepayments;
use App\Models\Notifications;

class ContactController extends Controller
{
     public function Notifications()
    {
        $notificationsnew = Notifications::get();
        $notifications = Notifications::orderBy('id', 'DESC')->skip(($_GET['page'] - 1) * $_GET['limit'])
                            ->take($_GET['limit'])->get();
        $count = count($notificationsnew);
        $total_pages = ceil($count / $_GET['limit']);
        return view('notifications',compact('notifications','total_pages'));
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Contact::where('contactTypeId',2)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $row->
                    $trans = transactions::where('contactId',$row->id)->first();
                    if (empty($trans)) {
                        $btn = ' <a href="'. url('contact/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>
                                <a href="'.route('contact.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('contact.index').'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>';
                    } else {
                        $btn = ' <a href="'. url('contact/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>
                                <a href="'. url('details/'.$row->id).'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-eye" style="color:darkgreen"></i></button> </a>
                                <a href="'.route('contact.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('contact.index').'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('contact');
    }
    
    public function smelist(Request $request)
    {
        if ($request->ajax()) {
            $data = Contact::where('contactTypeId',1)->get();
            $newdata = [];
            if($data != [])
            {
                foreach($data as $key=>$value)
                {
                    $agramount = 0;
                    $paidamount = 0;
                    $dueamount = 0;
                    $paidamount = smeagreepayments::where('smeId',$value->id)->where('status',1)->sum('agreeamount');    
                    $dueamount = smeagreepayments::where('smeId',$value->id)->where('status',2)->sum('agreeamount');    
                    $agramount = $paidamount+$dueamount;
                   // $value->created_at = $date;
                    $value->dueamount =$dueamount;
                    $value->paidamount =$paidamount;
                    $value->agramount =$agramount;
                    $newdata[] =$value;
                }
            }
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                     $btn = '<a href="'. url('smedetails/'.$row->id).'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-eye" style="color:darkgreen"></i></button> </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('smelist');
    }
    
    public function contactlist(Request $request)
    {
        if ($request->ajax()) {
            $data = Contact::where('contactTypeId',2)->get();
            $newdata = [];
            if($data != [])
            {
                foreach($data as $key=>$value)
                {
                    $agramount = 0;
                    $paidamount = 0;
                    $dueamount = 0;
                    $trans = transactions::where('contactId',$value->id)->first();    
                    if($trans)
                    {
                        $agramount = $trans->total;
                        $paidamount = $trans->paid;
                        $dueamount = $trans->unpaid;
                    }
                    $date = date('Y-m-d',strtotime($value->created_at));
                    $value->created_at = $date;
                    $value->dueamount =$dueamount;
                    $value->paidamount =$paidamount;
                    $value->agramount =$agramount;
                    $newdata[] =$value;
                }
            }
            return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                  
$btn = '<a href="'. url('details/'.$row->id).'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-eye" style="color:darkgreen"></i></button> </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('contactlist');
    }
    
    public function getContactsByType(Request $request, $type) 
    {
       if ($request->ajax()) {
            if ($type == 'consultant') {
                $contactType = [2];
            }
            elseif ($type == 'smes') {
                $contactType = [1];
            }
            elseif ($type == 'bench') {
                $contactType = [3];
            }
            elseif ($type == 'all') {
                $contactType = [1, 2,3];
            }
            $data = Contact::whereIn('contactTypeId', $contactType)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $trans = '';
                    if($row->contactTypeId == 2)
                    {
                        $trans = transactions::where('contactId',$row->id)->first();
                    }
                    if($row->contactTypeId == 1)
                    {
                        $trans = "smes"; 
                    }
                    if($row->contactTypeId == 3)
                    {
                        $trans = transactions::where('contactId',$row->id)->first();
                    }
                    //print_r(($trans));
                    if (empty($trans)) {
                        $btn = ' <a href="'. url('contact/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>
                                <a href="'.route('contact.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('contact.index').'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>';
                    }
                    elseif($trans == "smes")
                    {
                        $btn = ' <a href="'. url('contact/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>
                                <a href="'. url('smedetails/'.$row->id).'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-eye" style="color:darkgreen"></i></button> </a>
                                <a href="'.route('contact.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('contact.index').'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>';
                    }
                    else 
                    {
                        $btn = ' <a href="'. url('contact/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>
                                <a href="'. url('details/'.$row->id).'"  rel="noopener noreferrer">
                                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-eye" style="color:darkgreen"></i></button> </a>
                                <a href="'.route('contact.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('contact.index').'" class="button delete-confirm">
                                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                                </a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('contact'); 
    }
    
    public function create()
    {
        $contacttype = ContactType::where('status',1)->get();
        $worktype = worktype::where('status',1)->get();
        $consultanttype = consultanttype::where('status',1)->get();
        $location = location::where('status',1)->get();
        $states = states::where('status',1)->get();
        $technology = Technology::where('status',1)->get();
        $skills = Skills::where('status',1)->get();
        $paymenttype = paymenttype::where('status',1)->get();
        //print_r(Auth::guard('adminiy')->user()->role);exit;
        if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $accounttype = accounttype::where('status',1)->get();
        return view('createcontact',compact('contacttype','worktype','location','consultanttype','technology','skills','paymenttype','caseworker','accounttype','states'));
    }
    
    public function store(Request $request)    { 
   
   
     $validated = $request->validate([
            'email' => 'required|unique:contact|email',
            'mobile' => 'required|unique:contact|regex:/[0-9]/'
        ]);
        $contacttype = ContactType::where('id',$request->contactType)->first();
        $request->contactType = $contacttype->name;
        $request->contactTypeId = $contacttype->id;
        
        $worktype = worktype::where('id',$request->workType)->first();
        $request->workType = $worktype->name;
        $request->workTypeId = $worktype->id;
        
         
        $consultanttype = consultanttype::where('id',$request->consultantType)->first();
        $request->consultantType = $consultanttype->name;
        $request->consultantTypeId = $consultanttype->id;
        
        $location = location::where('id',$request->location)->first();
        $request->location = $location->name;
        $request->locationId = $location->id;
        
        $states = states::where('id',$request->stateName)->first();
        $request->stateName = $states->name;
        $request->stateId = $states->id;
        
        if($request->skill != [])
        {
            foreach($request->skill as $key=>$value)
            {
                $skill = Skills::where('id',$value)->first();
                $skills[] = $skill->name;
                $skillIds[] = $skill->id;
            }
        }
        if($request->technology != [])
        {
            foreach($request->technology as $tkey=>$tvalue)
            {
                $technology = Technology::where('id',$tvalue)->first();
                $technologys[] = $technology->name;
                $technologyIds[] = $technology->id;
            }
        }
        
        $paymenttype = paymenttype::where('id',$request->paymentType)->first();
        $request->paymentType = $paymenttype->name;
        $request->paymentTypeId = $paymenttype->id;
        
        if(!empty($request->accountType))
        {
            $accountype = accounttype::where('id',$request->accountType)->first();
            $request->accountType = $accountype->name;
            $request->accountTypeId = $accountype->id;
        }
        else
        {
            $request->accountType = "";
            $request->accountTypeId = 0;
        }
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseworker = $caseworker->firstname;
        $request->caseworkerId = $caseworker->id;
        
        $actorimagefile=$request->file('fileinput');
        if (isset($actorimagefile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorimagefile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $img_path = $up_location.$file_name;
            $actorimagefile->move($up_location,$file_name);
        } else {
                $img_path='';
        }
        
        $actorphotofile=$request->file('photo');
        if (isset($actorphotofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorphotofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $profilePic = $up_location.$file_name;
            $actorphotofile->move($up_location,$file_name);
        } else {
                $profilePic='';
        }
        
        $actorcover_photofile=$request->file('cover_photo');
        if (isset($actorcover_photofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorcover_photofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $coverPic = $up_location.$file_name;
            $actorcover_photofile->move($up_location,$file_name);
        } else {
            $coverPic='';
        }
        
       $id=  Contact::create([
              'firstname'=>$request->firstname,
              'lastname'=>$request->lastname,
              'middlename'=>$request->middlename,
              'stateName'=>$request->stateName,
              'stateId'=>$request->stateId,
              'gender'=>$request->gender,
              'email'=>$request->email,
              'mobile'=>$request->mobile,
              'altmobile'=>$request->altmobile,
              'dob'=>$request->dob,
              'contactType'=>$request->contactType,
              'contactTypeId'=>$request->contactTypeId,
              'workType'=>$request->workType,
              'workTypeId'=>$request->workTypeId,
              'consultantType'=>$request->consultantType,
              'consultantTypeId'=>$request->consultantTypeId,
              'location'=>$request->location,
              'locationId'=>$request->locationId,
             'technology'=>implode(',',$technologys),
              'technologyId'=>implode(',',$technologyIds),
              'skill'=>implode(',',$skills),
              'skillId'=>implode(',',$skillIds),
              'description'=>$request->description,
              'paymentType'=>$request->paymentType,
              'paymentTypeId'=>$request->paymentTypeId,
              'accountType'=>$request->accountType,
              'accountTypeId'=>$request->accountTypeId,
              'bankname'=>$request->bankname,
              'branch'=>$request->branch,
              'ifsc'=>$request->ifsc,
              'caseworker'=>$request->caseworker,
              'caseworkerId'=>$request->caseworkerId,
              'RoutingNumber'=>$request->RoutingNumber,
              'BankNameZille'=>$request->BankNameZille,
              'Contactname'=>$request->Contactname,
              'ZilleID'=>$request->ZilleID,
              'profilePic'=>$profilePic,
              'coverPic'=>$coverPic,
              'fileinput'=>$img_path,
              'status'=>1,
              'created_at'=>Carbon::now(),
              'createdBy'=> Auth::guard('adminiy')->user()->id,
          ])->id;
            $logmessage = $request->firstname.' '.$request->contactType.' Created By '.Auth::guard('adminiy')->user()->name;
            $logresult = Notifications::create([
            'message' => $logmessage,
            'type' => "Contact",
            'transactionId' => $id,
            'updated_at'=>Carbon::now(),
            'created_at'=>Carbon::now(),
        ]);
          
        return Redirect()->route('contact.index')->with('success','Contact created successfully');
        
    }
    
    public function destroy($id)    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Contact::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Contact deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    
    public function show($id){
        $contact = Contact::find($id);
        return view('editcontact',compact('contact'));
    }
    
    public function edit($id){
        $contacttype = ContactType::where('status',1)->get();
        $worktype = worktype::where('status',1)->get();
        $consultanttype = consultanttype::where('status',1)->get();
        $location = location::where('status',1)->get();
        $technology = Technology::where('status',1)->get();
        $skills = Skills::where('status',1)->get();
        $paymenttype = paymenttype::where('status',1)->get();
        $contact = Contact::find($id);
        $caseworker = User::where('status',1)->get();
        $accounttype = accounttype::where('status',1)->get();
        $states = states::where('status',1)->get();
        return view('editcontact',compact('contacttype','worktype','location','consultanttype','technology','skills','paymenttype','contact','caseworker','accounttype','states'));
       
    }
    
    public function update(Request $request){
        $id = $request->id;
        if ($request->status) {
            $status=1;
        } else {
            $status=0;
        }
        
        $contacttype = ContactType::where('id',$request->contactType)->first();
        $request->contactType = $contacttype->name;
        $request->contactTypeId = $contacttype->id;
        
        $worktype = worktype::where('id',$request->workType)->first();
        $request->workType = $worktype->name;
        $request->workTypeId = $worktype->id;
        
         
        $consultanttype = consultanttype::where('id',$request->consultantType)->first();
        $request->consultantType = $consultanttype->name;
        $request->consultantTypeId = $consultanttype->id;
        
        $location = location::where('id',$request->location)->first();
        $request->location = $location->name;
        $request->locationId = $location->id;
        
        $states = states::where('id',$request->stateName)->first();
        $request->stateName = $states->name;
        $request->stateId = $states->id;
        
        if ($request->skill != []) {
            foreach ($request->skill as $key=>$value) {
                $skill = Skills::where('id',$value)->first();
                $skills[] = $skill->name;
                $skillIds[] = $skill->id;
            }
        }
        if ($request->technology != []) {
            foreach($request->technology as $tkey=>$tvalue)
            {
                $technology = Technology::where('id',$tvalue)->first();
                $technologys[] = $technology->name;
                $technologyIds[] = $technology->id;
            }
        }
        $paymenttype = paymenttype::where('id',$request->paymentType)->first();
        $request->paymentType = $paymenttype->name;
        $request->paymentTypeId = $paymenttype->id;
        
        if(!empty($request->accountType))
        {
            $accountype = accounttype::where('id',$request->accountType)->first();
            $request->accountType = $accountype->name;
            $request->accountTypeId = $accountype->id;
        }
        else
        {
            $request->accountType = "";
            $request->accountTypeId = 0;
        }
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseworker = $caseworker->firstname;
        $request->caseworkerId = $caseworker->id;
        
        $image = Contact::where('id',$request->id)->first();
        
        $actorimagefile=$request->file('fileinput');
        if (isset($actorimagefile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorimagefile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $img_path = $up_location.$file_name;
            $actorimagefile->move($up_location,$file_name);
        } else {
                $img_path=$image->fileinput;
        }
        
        
        $actorphotofile=$request->file('photo');
        if (isset($actorphotofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorphotofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $profilePic = $up_location.$file_name;
            $actorphotofile->move($up_location,$file_name);
        } else {
                $profilePic=$image->profilePic;
        }
        
        $actorcover_photofile=$request->file('cover_photo');
        if (isset($actorcover_photofile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorcover_photofile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $coverPic = $up_location.$file_name;
            $actorcover_photofile->move($up_location,$file_name);
        } else {
                $coverPic=$image->coverPic;
        }
        
        $update = Contact::find($id)->update([
              'firstname'=>$request->firstname,
              'lastname'=>$request->lastname,
              'middlename'=>$request->middlename,
               'stateName'=>$request->stateName,
              'stateId'=>$request->stateId,
              'gender'=>$request->gender,
              'email'=>$request->email,
              'mobile'=>$request->mobile,
              'altmobile'=>$request->altmobile,
              'dob'=>$request->dob,
              'contactType'=>$request->contactType,
              'contactTypeId'=>$request->contactTypeId,
              'workType'=>$request->workType,
              'workTypeId'=>$request->workTypeId,
              'consultantType'=>$request->consultantType,
              'consultantTypeId'=>$request->consultantTypeId,
              'location'=>$request->location,
              'locationId'=>$request->locationId,
              'technology'=>implode(',',$technologys),
              'technologyId'=>implode(',',$technologyIds),
              'skill'=>implode(',',$skills),
              'skillId'=>implode(',',$skillIds),
              'description'=>$request->description,
              'paymentType'=>$request->paymentType,
              'paymentTypeId'=>$request->paymentTypeId,
              'accountType'=>$request->accountType,
              'accountTypeId'=>$request->accountTypeId,
              'bankname'=>$request->bankname,
              'branch'=>$request->branch,
              'ifsc'=>$request->ifsc,
              'caseworker'=>$request->caseworker,
              'caseworkerId'=>$request->caseworkerId,
              'fileinput'=>$img_path,
              'status'=> $status,
              'RoutingNumber'=>$request->RoutingNumber,
              'BankNameZille'=>$request->BankNameZille,
              'Contactname'=>$request->Contactname,
              'ZilleID'=>$request->ZilleID,
              'profilePic'=>$profilePic,
              'coverPic'=>$coverPic,
              'updated_at'=>Carbon::now(),
              'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('contact.index')->with('success','Contact updated successfully');
        }
        return Redirect()->route('contact.index')->with('failed','please try again');
      }

}