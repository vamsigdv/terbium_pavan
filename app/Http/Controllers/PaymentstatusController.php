<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\PaymentStatus;
use DataTables;
use Session;
class PaymentstatusController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PaymentStatus::get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="payment_status_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="payment_status_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('payment_status.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('payment_status.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('payment_status');
    }
    public function store(Request $request)
    {   
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        PaymentStatus::insert([
              'name'=>$request->name,
              'status'=> $status,
              'created_at'=>Carbon::now(),
              'createdBy'=> Auth::guard('adminiy')->user()->id,
              
          ]);
        return Redirect()->back()->with('success','PaymentStatus created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = PaymentStatus::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'PaymentStatus deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = PaymentStatus::find($id);
        $view=1;
        $html = view('modal.payment_status_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = PaymentStatus::find($id);
        $view=0;
        $html = view('modal.payment_status_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $update = PaymentStatus::find($id)->update([
            'name'=>$request->name,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('payment_status.index')->with('success','PaymentStatus updated successfully');
        }
        return Redirect()->route('payment_status.index')->with('failed','please try again');
      }

}