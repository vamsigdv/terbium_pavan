<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Skills;
use DataTables;
use Session;
use App\Models\Technology;
use App\Models\Contact;
use App\Models\paymenttype;
use App\Models\caseworker;
use App\Models\transactiontechnology;
use App\Models\installments;
use App\Models\worktype;
use App\Models\transactions;
use App\Models\User;
use App\Models\smeagreepayments;
use App\Models\PaymentStatus;
use App\Models\smecomments;
use App\Models\statustype;
use App\Models\interviews;

class SMEController extends Controller
{
    
    public function index(Request $request)
    {
        $contacts = Contact::where('status', 1)->get();
        $smes = $contacts->filter(function ($contact) {
        if ($contact->contactTypeId == 1) {
                return true;
            }
        });
        
        $consultants = $contacts->filter(function ($contact) {
            if ($contact->contactTypeId == 2) {
                return true;
            }
        });
        
        $work_types = worktype::where('status', 1)->get();
        
        $created_users = Adminiy::where('is_active', 1)->get();
        
        if ($request->ajax()) {
            $newdata = [];
            $data = transactiontechnology::get();
            if(!empty($data))
            {
                foreach($data as $key=>$value)
                {
                     $sme = Contact::where('id',$value->smeid)->first();
                     if(!empty($sme))
                     {
                        $value->mobile = $sme->mobile;
                     }
                     else
                     {
                         $value->mobile = 0;
                     }
                     $newdata[] = $value;
                }
        
            }
            return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                   
                        $btn = ' <button class="btn p-0 ms-2" id="sme_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button><a href="' . url('/smecomment/' . $row->id) . '" class="btn p-0 ms-2" type="button" title="View"><i class="fas fa-eye" style="color:orange"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('sme', compact('consultants', 'smes', 'created_users', 'work_types'));
    }
    
    public function smecomments(Request $request,$id)
    {
        $interview = transactiontechnology::where('id',$id)->first();
       // print_r($interview->smeid);exit;
        $contact = Contact::where('status',1)->where('contactTypeId',2)->get();
        $sme = Contact::where('status',1)->where('contactTypeId',1)->get();
        $technology = Technology::where('status',1)->get();
        $logs =smecomments::where('transactiontechnology',$id)->get();
        $statustype = statustype::where('status',1)->get();
        $data = transactiontechnology::find($id);
        $getTransactions =Transactions::where('id', $data->transactionId)
              ->first();
        $worktype = worktype::where('status',1)->get();
         if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
         $batch = caseworker::where('status',1)->get();
        $payment_statuses  = PaymentStatus ::where('status', 1)->get();
       
        if ($request->ajax()) {
           
            return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="transactions_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                     <a href="' . url('details', $row->id) . '" >
                    <button class="btn p-0 ms-2 " type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="View details"><i class="fas fa-eye" style="color:orange"></i></button>
                    </a>
                    <a href="'.route('transactions.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('transactions.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('smecomments',compact('statustype','id','logs','interview','contact','sme','technology','data','getTransactions','caseworker','worktype','batch','payment_statuses'));
    }
    
    public function store(Request $request)
    {   
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
       
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->firstname;
        $request->caseWorkerId = $caseworker->id;
        
        
        $contact = Contact::where('id',$request->contactId)->first();
        $request->contactName = $contact->firstname;
        $request->contactId = $contact->id;
        
        $worktype = worktype::where('id',$request->workType)->first();
        $request->workType = $worktype->name;
        $request->workTypeId = $worktype->id;
        
        
        $sme = Contact::where('id',$request->SMEName)->first();
        $request->SMEName = $sme->firstname;
        $request->smeid = $sme->id;
        
        
        $batch = caseworker::where('id',$request->batch)->first();
        if(!empty($batch))
        {
            $request->batchName = $batch->name;
            $request->batchId = $batch->id;
        }
        $newdata = transactions::where('id', $id)->first();
       
        if($request->paymentStatus == 1)
        {
             $newdata->smepaid = $newdata->smepaid+$request->SMEAgreeAmount;
             if(!empty($newdata->profit) || $newdata->profit != 0)
             {
                 $newdata->profit =  $newdata->profit-$newdata->smepaid;
             }
        }
        else
        {
            $newdata->smeunpaid = $newdata->smeunpaid +$request->SMEAgreeAmount;
            if(!empty($newdata->profit) || $newdata->profit != 0)
            {
                 $newdata->profit =  $newdata->profit-$newdata->smeunpaid;
            }
        }
         //print_r($newdata);exit;
        $newdata->update();
        
        // print_r(count($request->technology));exit;
            foreach($request->technology as $key=>$value)
            {
                $technology = Technology::where('id',$value)->first();
                 $request->technology = $technology->name;
                 $request->technologyId = $technology->id;
             transactiontechnology::insert([
                'transactionId'=>$id,
               'contactId'=>$request->contactId,
                'status'=> $status,
                'technology'=>$request->technology,
                'technologyId'=>$request->technologyId,
                'contactName'=>$request->contactName,
                'smeid'=>$request->smeid,
                'reasonSME'=>$request->reasonSME,
                'agreePayment'=>$request->SMEAgreeAmount,
                'comment'=>$request->comment,
                'NoofInterviews'=>$request->NoofInterviews,
                'smeid'=>$request->smeid,
                'workTypeId'=>$request->workTypeId,
                'workType'=>$request->workType,
                'caseWorkerId'=>$request->caseWorkerId,
                'caseWorker'=>$request->caseWorker,
                'SMEName'=>$request->SMEName,
                'batchName'=>$request->batchName,
                'batchId'=>$request->batchId,
                'created_at'=>Carbon::now(),
                'createdBy'=> Auth::guard('adminiy')->user()->id,
          ]);
            }
    
         if(!empty($request->SMEAgreeAmount))
          {
                smeagreepayments::insert([
                    'transactionId'=>$id,
                    'contactId'=>$request->contactId,
                    'smeId'=>$request->smeid,
                    'interviewId'=>$id,
                    'type'=>$request->workType,
                    'agreeamount'=>$request->SMEAgreeAmount,
                    'status'=>$request->paymentStatus,
                    'date'=>date('Y-m-d'),
                    'employee'=>$request->caseWorkerId,
                    'created_at'=>Carbon::now(),
                    'createdBy'=> Auth::guard('adminiy')->user()->id,
                    'updated_at'=>Carbon::now(),
                    'updatedBy'=> Auth::guard('adminiy')->user()->id,
                ]); 
          }
        return Redirect()->back()->with('success','transactiontechnology created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Skills::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Skills deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Skills::find($id);
        $view=1;
         $technology = Technology::where('status',1)->get();
        $html = view('modal.Skills_edit',compact('data','view','technology'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = transactiontechnology::find($id);
        $getTransactions =Transactions::where('id', $data->transactionId)
              ->first();
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $sme = Contact::where('status',1)->where('contactTypeId',1)->get();
        $worktype = worktype::where('status',1)->get();
         if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $batch = caseworker::where('status',1)->get();
        $view=0;
        $payment_statuses = PaymentStatus::all();
        $interviews = interviews::where('transactionId',$data->transactionId)->count();

        $html = view('modal.sme_edit',compact('data','view','technology','getTransactions','contact','worktype','caseworker','sme','batch','payment_statuses','interviews'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        $data = transactiontechnology::find($id);
        print_r($data);exit;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $technology = Technology::where('id',$request->technology)->first();
        $request->technology = $technology->name;
        $request->technologyId = $technology->id;
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->firstname;
        $request->caseWorkerId = $caseworker->id;
        
        
        $contact = Contact::where('id',$request->contactName)->first();
        $request->contactName = $contact->firstname;
        $request->contactId = $contact->id;
        
        $worktype = worktype::where('id',$request->workType)->first();
        $request->workType = $worktype->name;
        $request->workTypeId = $worktype->id;
        
        
        $sme = Contact::where('id',$request->SMEName)->first();
        $request->SMEName = $sme->firstname;
        $request->smeid = $sme->id;
        
        
        $batch = caseworker::where('id',$request->batch)->first();
       
        if(!empty($batch))
        {
            $request->batchName = $batch->name;
            $request->batchId = $batch->id;
        }
       
        
        $update = transactiontechnology::find($id)->update([
            'contactId'=>$request->contactId,
            'status'=> $status,
            'technology'=>$request->technology,
            'technologyId'=>$request->technologyId,
            'contactName'=>$request->contactName,
            'smeid'=>$request->smeid,
            'reasonSME'=>$request->reasonSME,
            'agreePayment'=>$request->agreePayment,
            'comment'=>$request->comment,
            'NoofInterviews'=>$request->NoofInterviews,
            'smeid'=>$request->smeid,
            'workTypeId'=>$request->workTypeId,
            'workType'=>$request->workType,
            'caseWorkerId'=>$request->caseWorkerId,
            'caseWorker'=>$request->caseWorker,
            'SMEName'=>$request->SMEName,
            'updated_at'=>Carbon::now(),
            'batchName'=>$request->batchName,
            'batchId'=>$request->batchId,
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        
        /*if(!empty($request->agreePayment))
          {
                smeagreepayments::insert([
                    'transactionId'=>$id,
                    'contactId'=>$request->contactId,
                    'smeId'=>$request->smeid,
                    'interviewId'=>$id,
                    'type'=>$request->workType,
                    'agreeamount'=>$request->agreePayment,
                    'date'=>date('Y-m-d'),
                    'employee'=>$request->caseWorkerId,
                    'created_at'=>Carbon::now(),
                    'createdBy'=> Auth::guard('adminiy')->user()->id,
                    'updated_at'=>Carbon::now(),
                    'updatedBy'=> Auth::guard('adminiy')->user()->id,
                ]); 
          }*/
       // print_r(str_replace(env('APP_URL'),"",redirect()->back()->getTargetUrl()));exit;
        if(str_replace(env('APP_URL'),"",redirect()->back()->getTargetUrl()) == '/sme') {
            return Redirect()->route('sme.index')->with('success','SME updated successfully');
        }
        else
        {
             return Redirect()->route('smes',$data->transactionId)->with('success','SME updated successfully');
        }
        return Redirect()->route('smes',$data->transactionId)->with('failed','please try again');
        
      }
    public function smedetails($id)
    {
        $data = Contact::where('id', $id)
                    ->first();
        $unpaidamount = smeagreepayments::where('smeId', $id)->where('status',0)->sum('agreeamount');
        $paidamount = smeagreepayments::where('smeId', $id)->where('status',1)->sum('agreeamount');
        $total = $paidamount + $unpaidamount;
         
        $movie_amounts = [
            'total_amount' => $total, 
            'total_paid_amount' => $paidamount, 
            'total_unpaid_amount' => $unpaidamount
        ];
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $img = Contact::where('id',$id)->first();
        $paymenttype = paymenttype::where('status',1)->get();
        $caseworker = User::where('status',1)->get();
        return view('smedetails',compact('data','movie_amounts','img'));
    }
    
    public function smedetailsview($id) 
    {
      $getTransactions =smeagreepayments::where('smeid', $id)
              ->get();
        if(!empty($getTransactions)){
           foreach($getTransactions as $key=>$value)
          {
            
                $tras = User::where('id',$value->employee)->first();
                if(!empty($tras)){
                $value->employee = $tras->firstname;
                }
               // print_r($tras->firstname);exit;
                $data = Contact::where('id',$id)->first();
                $consltantname = Contact::where('id',$value->contactId)->first();
                $value->consultantName = $consltantname->firstname;
                $value->contactId = $data->firstname;
               // $value->technology = implode(',',$names);
          }
        }
             return Datatables::of($getTransactions)->addIndexColumn()
                    ->addColumn('action', function($row)
                    {
                   $btn ='';
                   if($row->status != '1' && $row->transactionId != ''){
                    $btn .= ' <button class="btn p-0 ms-2" id="installments_edit" type="button" data-ins="sme"  data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>';
                   }
                   $btn .=' <a target="_blank" href="' . url('invoice/' . $row->id . '/2') .'"><i class="fa fa-file-invoice"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }
    
    
    public function smepaymentsdata(Request $request) 
    {
        $contacts = Contact::where('status', 1)->get();
        $consultants = $contacts->filter(function ($contact) {
            if ($contact->contactTypeId == 2) {
                return true;
            }
        });
        $employees = User::where('status', 1)->get();
        $created_users = Adminiy::where('is_active', 1)->get();
        $payment_statuses = PaymentStatus::where('status', 1)->get();
         if ($request->ajax()) {
             $newdata = [];
        $getTransactions = smeagreepayments::get();
        
        if (!empty($getTransactions)) {
           foreach ($getTransactions as $key=>$value) {
                $tras = User::where('id', $value->employee)->first();
                $value->employee = $tras->firstname;
                $data = Contact::where('id', $value->contactId)->first();
                $value->contactId = $data->firstname;
                $sme = Contact::where('id',$value->smeId)->first();
                if (!empty($sme)) {
                    $value->mobile = $sme->mobile;
                    $value->SMENAME =  $sme->firstname;
                } else {
                    $value->mobile = 0;
                    $value->SMENAME =  "";
                }
                $payment_status = PaymentStatus::find($value->status);
                $value->status = $payment_status->name;
                $newdata[] = $value;
                  
            }
        }
             return Datatables::of($newdata)->addIndexColumn()
                    ->addColumn('action', function($row)
                    {
                   
                     $btn ='';
                   if($row->status != 'Paid'){
                    $btn .= ' <button class="btn p-0 ms-2" id="installments_edit" type="button" data-ins="sme"  data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>';
                   }
                  
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('smepayments', compact('employees', 'consultants', 'created_users', 'payment_statuses'));       
    }
    
    public function searchSMEList(Request $request) 
    {
        $smeData = new transactiontechnology(); 
           
        if (($request->startdate != '' && $request->enddate != '')) {
           $smeData = $smeData->where('created_at', '>=', $request->startdate)
                            ->where('created_at', '<=', $request->enddate);
        } 
        elseif ($request->startdate != '') {
           $smeData = $smeData->where('created_at', '=', $request->startdate);
        }
        elseif ($request->enddate != '') {
           $smeData = $smeData->where('created_at', '=', $request->enddate);
        }
        if ($request->SMEId != '') {
           $smeData = $smeData->where('smeid', $request->SMEId);
        }
        if ($request->contactId != '') {
           $smeData = $smeData->where('contactId', $request->contactId);
        }
        if ($request->created_by != '') {
           $smeData = $smeData->where('createdBy', $request->created_by);
        }
        if ($request->work_type != '') {
           $smeData = $smeData->where('workType', $request->work_type);
        }
        $newdata = [];
        $data = $smeData->get();
        if (!empty($data)) {
            foreach ($data as $key=>$value) {
                $sme = Contact::where('id',$value->smeid)->first();
                if (!empty($sme)) {
                    $value->mobile = $sme->mobile;
                } else {
                     $value->mobile = 0;
                }
                $newdata[] = $value;
            }
        }
       
        return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                   $btn = ' <button class="btn p-0 ms-2" id="interviews_edit" type="button" data-id="' . $row->id . '"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> <a href="' . url('/comments/' . $row->id) . '" class="btn p-0 ms-2" type="button" title="View"><i class="fas fa-eye" style="color:orange"></i></a>';
                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }
    
    public function searchSmpaymentsList(Request $request) 
    {
        
        $smeData = smeagreepayments::join('contact', 'contact.id', '=', 'smeagreepayments.contactId')
                    ->join('users', 'users.id', '=', 'smeagreepayments.employee');
        if (($request->startdate != '' && $request->enddate != '')) {
           $smeData = $smeData->whereDate('smeagreepayments.date', '>=', $request->startdate)
                            ->whereDate('smeagreepayments.date', '<=', $request->enddate);
        } 
        elseif ($request->startdate != '') {
           $smeData = $smeData->whereDate('smeagreepayments.date', '=', $request->startdate);
        }
        elseif ($request->enddate != '') {
           $smeData = $smeData->whereDate('smeagreepayments.date', '=', $request->enddate);
        }
        if ($request->SMEId != '') {
           $smeData = $smeData->where('employee', $request->SMEId);
        }
        if ($request->contactId != '') {
           $smeData = $smeData->where('contactId', $request->contactId);
        }
        if ($request->type != '') {
           $smeData = $smeData->where('smeagreepayments.type', $request->type);
        }
        if ($request->created_by != '') {
           $smeData = $smeData->where('smeagreepayments.createdBy', $request->created_by);
        }
        if ($request->payment_status != '') {
           $smeData = $smeData->where('smeagreepayments.status', $request->payment_status);
        }
        
        $newdata = [];
        $data = $smeData->select(
            'smeagreepayments.id', 
            'contact.firstname as contactId',
            'smeagreepayments.date',
            'agreeamount',
            'smeagreepayments.status',
            'type',
            'smeId',
            'users.firstname as employee')->get();
             if (!empty($data)) {
                foreach ($data as $key=>$value) {
                     $sme = Contact::where('id',$value->smeId)->first();
                     if (!empty($sme)) {
                        $value->mobile = $sme->mobile;
                        $value->SMENAME =  $sme->firstname;
                     } else {
                         $value->mobile = 0;
                         $value->SMENAME =  "";
                     }
                     $payment_status = PaymentStatus::find($value->status);
                     $value->status = $payment_status->name;
                     $newdata[] = $value;
                    
                }
            }
                     
        return Datatables::of($newdata)->addIndexColumn()
            ->addColumn('action', function($row)
            {
           
                $btn = ' <button class="btn p-0 ms-2" id="installments_edit" type="button" data-id="'. $row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>';
                return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    

}