<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Skills;
use DataTables;
use Session;
use App\Models\Technology;
use App\Models\Contact;
use App\Models\paymenttype;
use App\Models\caseworker;
use App\Models\transactiontechnology;
use App\Models\installments;
use App\Models\worktype;
use App\Models\transactions;
use App\Models\interviews;
use App\Models\interviewtools;
use App\Models\statustype;
use App\Models\User;
use App\Models\smeagreepayments;
use App\Models\comments;
use App\Models\PaymentStatus;

class InterviewsController extends Controller
{   
    
    public function index(Request $request)
    {
        $contacts = Contact::where('status', 1)->get();
        $smes = $contacts->filter(function ($contact) {
        if ($contact->contactTypeId == 1) {
                return true;
            }
        });
        $consultants = $contacts->filter(function ($contact) {
            if ($contact->contactTypeId == 2) {
                return true;
            }
        });
        $created_users = Adminiy::where('is_active', 1)->get();
        if ($request->ajax()) {
            $newdata = [];
            $data = interviews::get();
            if (!empty($data)) {
                foreach($data as $key => $value) {
                     $statuses = PaymentStatus::where('id',$value->paymentStatus)->first();
                    // print_r($value->paymentStatus);
                     $statustype = statustype::where('id',$value->Status)->first();
                     $value->paymentStatus = $statuses->name;
                     $value->Status = $statustype->name;
                      $newdata [] = $value;
                }
            }
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                   $btn = ' <button class="btn p-0 ms-2" id="interviews_edit" type="button" data-id="' . $row->id . '"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> <a href="' . url('/comments/' . $row->id) . '" class="btn p-0 ms-2" type="button" title="View"><i class="fas fa-eye" style="color:orange"></i></a>';
                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('interviews', compact('consultants', 'smes', 'created_users'));
    }
    
    public function comments(Request $request,$id)
    {
        $interview = interviews::where('id',$id)->first();
        $interviews = comments::where('interviewId',$interview->id)->where('round','!=','NULL')->count();
        $contacts = Contact::where('status',1)->where('contactTypeId',2)->get();
        $smes = Contact::where('status',1)->where('contactTypeId',1)->get();
        $technology = Technology::where('status',1)->get();
        $logs =comments::where('interviewId',$id)->get();
        $statustype = statustype::where('status',1)->get();
       
        if ($request->ajax()) {
           
            return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="transactions_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                     <a href="' . url('details', $row->id) . '" >
                    <button class="btn p-0 ms-2 " type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="View details"><i class="fas fa-eye" style="color:orange"></i></button>
                    </a>
                    <a href="'.route('transactions.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('transactions.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('comments',compact('statustype','id','logs','interview','contacts','smes','technology','interviews'));
    }
    
    
    public function store(Request $request)
    {   
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
       
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->firstname;
        $request->caseWorkerId = $caseworker->id;
        
        if(!empty($request->recruter))
        {
            $recruter = User::where('id',$request->recruter)->first();
            $request->recruter = $recruter->firstname;
            $request->recruterId = $recruter->id;
        }
        else
        {
            $request->recruter = "";
            $request->recruterId =0;
        }
        
        
        $contact = Contact::where('id',$request->contactId)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
        
        
        
        $sme = Contact::where('id',$request->SMEName)->first();
        $request->SMEName = $sme->firstname;
        $request->smeid = $sme->id;
        
        
        if($request->technology != [])
        {
           
            foreach($request->technology as $key=>$value)
            {
                 $technology = Technology::where('id',$value)->first();
                 $technologys[] = $technology->name;
                 $technologyIds[] = $technology->id;
            }
        }
         $data = transactions::where('id', $id)->first();
         if($request->paymentStatus == 1)
         {
            $data->smepaid = $data->smepaid+$request->SMEAgreeAmount;
            if(!empty($data->profit) || $data->profit != 0)
            {
                 $data->profit =  $data->profit-$data->smepaid;
            }
         }
         else
         {
             $data->smeunpaid = $data->smeunpaid +$request->SMEAgreeAmount;
            if(!empty($data->profit) || $data->profit != 0)
            {
                 $data->profit =  $data->profit-$data->smeunpaid;
            }
         }
         $data->update();
         $interview =  interviews::where('transactionId', $id)
              ->first();
        $actorimagefile=$request->file('fileinput');
        if (isset($actorimagefile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorimagefile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $img_path = $up_location.$file_name;
            $actorimagefile->move($up_location,$file_name);
        } else {
                $img_path='';
        }
            $id =  interviews::create([
                'transactionId'=>$id,
                'Noofrounds'=>$request->Noofrounds,
                'contactId'=>$request->contactId,
                'consultantName'=>$request->consultantName,
                'modeofinterview'=>$request->modeofinterview,
                'technologyId'=>implode(',',$technologyIds),
                'technology'=>implode(',',$technologys),
                'SMEAgreeAmount'=>$request->SMEAgreeAmount,
                'Status'=>$request->Status,
                'comment'=>$request->comment    ,
                'smeid'=>$request->smeid,
                'SMEName'=>$request->SMEName,
                'resume'=>$img_path,
                'paymentStatus'=>$request->paymentStatus,
                'caseWorkerId'=>$request->caseWorkerId,
                'caseWorker'=>$request->caseWorker,
                'recruterId'=>$request->recruterId,
                'recruter'=>$request->recruter,
                'interviewdate'=>$request->interviewdate,
                'created_at'=>Carbon::now(),
                'createdBy'=> Auth::guard('adminiy')->user()->id,
          ])->id;
      
          if(!empty($request->SMEAgreeAmount))
          {
                smeagreepayments::insert([
                    'contactId'=>$request->contactId,
                    'interviewId'=>$id,
                    'smeId'=>$request->smeid,
                    'type'=>'Interview',
                    'status'=>$request->paymentStatus,
                    'agreeamount'=>$request->SMEAgreeAmount,
                    'date'=>date('Y-m-d'),
                    'employee'=>$request->caseWorkerId,
                    'created_at'=>Carbon::now(),
                    'createdBy'=> Auth::guard('adminiy')->user()->id,
                    'updated_at'=>Carbon::now(),
                    'updatedBy'=> Auth::guard('adminiy')->user()->id,
                ]); 
          }
        return Redirect()->back()->with('success','Interview created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Skills::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Skills deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Skills::find($id);
        $view=1;
         $technology = Technology::where('status',1)->get();
        $html = view('modal.Skills_edit',compact('data','view','technology'))->render();
        return json_encode($html);
    }
    
    public function edit($id){
        $data = interviews::find($id);
        $getTransactions =Transactions::where('id', $data->transactionId)
              ->first();
        $technology = Technology::where('status', 1)->get();
        $contact = Contact::where('status', 1)->get();
        $sme = Contact::where('status', 1)->where('contactTypeId',1)->get();
        $worktype = worktype::where('status', 1)->get();
        $statustype = statustype::where('status', 1)->get();
        $interviewtools = interviewtools::where('status', 1)->get();
        if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $payment_statuses = PaymentStatus::all();
        $view=0;
        $html = view('modal.interviews_edit',
                compact('data',
                    'view',
                    'technology',
                    'getTransactions',
                    'contact','worktype',
                    'caseworker',
                    'sme',
                    'statustype',
                    'interviewtools', 
                    'payment_statuses')
                    )->render();
        return json_encode($html);
    }
    
    public function update(Request $request){
        $id =$request->id;
        $data = interviews::find($id);
        
        $technology = Technology::where('id',$request->technology)->first();
        $request->technology = $technology->name;
        $request->technologyId = $technology->id;
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->name;
        $request->caseWorkerId = $caseworker->id;
         if(!empty($request->recruter))
        {
            $recruter = User::where('id',$request->recruter)->first();
            $request->recruter = $recruter->firstname;
            $request->recruterId = $recruter->id;
        }
        else
        {
            $request->recruter = "";
            $request->recruterId =0;
        }
        
       // print_r($request->contactId);exit;
        
        $contact = Contact::where('id',$request->contactId)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
        
        $sme = Contact::where('id',$request->SMEName)->first();
        $request->SMEName = $sme->firstname;
        $request->smeid = $sme->id;
        
         $data = transactions::where('id', $data->transactionId)->first();
         if($request->paymentStatus == 1)
         {
             $data->smepaid = $data->smepaid+$request->SMEAgreeAmount;
             $data->profit =  $data->profit-$data->smepaid;
         }
         else
         {
             $data->smeunpaid = $data->smeunpaid +$request->SMEAgreeAmount;
             $data->profit =  $data->profit-$data->smeunpaid;
         }
         $data->update();
         
         
        $image = interviews::where('id',$request->id)->first();
        
        $actorimagefile=$request->file('fileinput');
        if (isset($actorimagefile)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($actorimagefile->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/contacts/';
            $img_path = $up_location.$file_name;
            $actorimagefile->move($up_location,$file_name);
        } else {
                $img_path=$image->resume;
        }
         
         $update = interviews::find($id)->update([
            'transactionId'=>$data->transactionId,
                'Noofrounds'=>$request->Noofrounds,
               'contactId'=>$request->contactId,
               'consultantName'=>$request->consultantName,
                'modeofinterview'=>$request->modeofinterview,
                'technologyId'=>$request->technologyId,
                'technology'=>$request->technology,
                'SMEAgreeAmount'=>$request->SMEAgreeAmount,
                'Status'=>$request->Status,
                'comment'=>$request->comment    ,
                'smeid'=>$request->smeid,
                'resume'=>$img_path,
                'SMEName'=>$request->SMEName,
                'paymentStatus'=>$request->paymentStatus,
                'caseWorkerId'=>$request->caseWorkerId,
                'caseWorker'=>$request->caseWorker,
                'recruterId'=>$request->recruterId,
                'recruter'=>$request->recruter,
                'interviewdate'=>$request->interviewdate,
                'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
          ]);
        /* if(!empty($request->SMEAgreeAmount))
          {
                smeagreepayments::insert([
                    'contactId'=>$request->contactId,
                    'interviewId'=>$id,
                    'smeId'=>$request->smeid,
                    'type'=>'Interview',
                    'agreeamount'=>$request->SMEAgreeAmount,
                    'date'=>date('Y-m-d'),
                    'employee'=>$request->caseWorkerId,
                    'created_at'=>Carbon::now(),
                    'createdBy'=> Auth::guard('adminiy')->user()->id,
                    'updated_at'=>Carbon::now(),
                    'updatedBy'=> Auth::guard('adminiy')->user()->id,
                ]); 
          }*/
        if(str_replace(env('APP_URL'),"",redirect()->back()->getTargetUrl()) == '/interviews') 
        {
            return Redirect()->route('interviews.index')->with('success','Interviews updated successfully');
        }
        else
        {
            return Redirect()->route('interviews',$data->transactionId)->with('success','Interviews updated successfully');
        }
        return Redirect()->route('interviews',$data->transactionId)->with('failed','please try again');
    }
      
    public function searchInterviewList(Request $request) 
    {
       $interviewsData = interviews::whereIn('Status', [1,2,3,4]); 
       
       if (($request->startdate != '' && $request->enddate != '')) {
           $interviewsData = $interviewsData->whereDate('created_at', '>=', $request->startdate)
                            ->whereDate('created_at', '<=', $request->enddate);
                           
       } 
       elseif ($request->startdate != '') {
           $interviewsData = $interviewsData->whereDate('created_at', '=', $request->startdate);
       }
       elseif ($request->enddate != '') {
           $interviewsData = $interviewsData->whereDate('created_at', '=', $request->enddate);
       }
       if ($request->SMEId != '') {
           $interviewsData = $interviewsData->where('smeid', $request->SMEId);
       }
       if ($request->contactId != '') {
           $interviewsData = $interviewsData->where('contactId', $request->contactId);
       }
       if ($request->created_by != '') {
           $interviewsData = $interviewsData->where('createdBy', $request->created_by);
       }
       $interviewsData = $interviewsData->get();
       $newdata = [];
       if (!empty($interviewsData)) {
                foreach($interviewsData as $key => $value) {
                      $statuses = PaymentStatus::where('id',$value->paymentStatus)->first();
                      //print_r($value->Status);exit;
                      $statustype = statustype::where('id',$value->Status)->first();
                      $value->paymentStatus = $statuses->name;
                      $value->Status = $statustype->name;
                      $newdata [] = $value;
                }
            }
       return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                   $btn = ' <button class="btn p-0 ms-2" id="interviews_edit" type="button" data-id="' . $row->id . '"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> <a href="' . url('/comments/' . $row->id) . '" class="btn p-0 ms-2" type="button" title="View"><i class="fas fa-eye" style="color:orange"></i></a>';
                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

}