<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\transactions;
use DataTables;
use Session;
use App\Models\Technology;
use App\Models\Contact;
use App\Models\paymenttype;
use App\Models\caseworker;
use App\Models\transactiontechnology;
use App\Models\installments;
use App\Models\worktype;
use App\Models\interviews;
use App\Models\User;
use App\Models\interviewtools;
use App\Models\statustype;
use App\Models\PaymentStatus;
use App\Models\comments;
use App\Models\logs;
use PDF;
use App\Models\transactioninstallments;
use App\Models\Setting;
use App\Models\Notifications;
class TransactionsController extends Controller
{
    public function index(Request $request)
    {
        $technology = Technology::where('status',1)->get();
        $newcontact = Contact::where('status', 1)->where('contactTypeId', '!=', 1)->get();
        $work_types = WorkType::where('status', 1)->get();
        $contact = [];
        $date = date('mm/dd/yyyy');
        $payment_statuses = PaymentStatus::all();
        if ($newcontact != []) {
            foreach($newcontact as $key => $value)
            {
                 $trans = transactions::where('contactId', $value->id)->first(); 
                 if(empty($trans)){
                     $contact[] = $value;
                 }
            }
        }
        $paymenttype = paymenttype::where('status',1)->get();
        if (Auth::guard('adminiy')->user()->role == 'Super Admin') {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        } else {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $statustype = statustype::where('status',1)->get();
        $interviewtools = interviewtools::where('status',1)->get();
        $newdata = [];
        if ($request->ajax()) {
            $data = transactions::get();
            if ($data !=[]) {
                foreach($data as $key=>$value)
                {
                   $techs = [];
                   $technology = explode(',',$value->technology);
                   foreach($technology as $k=>$v){
                       $tech= Technology::where('id', $v)->first();
                       $techs[] = $tech->name;
                   }
                  
                   $value->technology = implode(',',$techs);
                   $w_t = $work_types->where('id', $value->work_type)->first();
                   $value->work_type = ($w_t) ? $w_t->name : '';
                   $newdata[] = $value;
                }
            }
            return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="transactions_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                     <a href="' . url('details', $row->contactId) . '" >
                    <button class="btn p-0 ms-2 " type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="View details"><i class="fas fa-eye" style="color:orange"></i></button>
                    </a>
                    <a href="'.route('transactions.destroy', $row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('transactions.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('transactions', compact(
            'technology',
            'contact',
            'paymenttype',
            'caseworker',
            'interviewtools',
            'statustype',
            'date',
            'payment_statuses', 
            'work_types'
        ));
    }
    
    public function store(Request $request)
    {   
        if($request->status) {
            $status=1;
        } else {
            $status=0;
        }
        
        $paymenttype = paymenttype::where('id',$request->paymentType)->first();
        $request->paymentType = $paymenttype->name;
        $request->paymentTypeId = $paymenttype->id;
        
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseworker = $caseworker->firstname;
        $request->caseworkerId = $caseworker->id;
        
        
        $contact = Contact::where('id',$request->consultantName)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
        $request->mobile = $contact->mobile;
        $total = 0;
        $unpaid = 0;
        $paid = 0;
        if (!empty($request->payingNow)) {
            $paid = $request->payingNow;
        }
        
        if (!empty($request->Due)) {
            $unpaid = $request->Due;
        }
        $total = $paid + $unpaid;
        $id =  transactions::create([
              'consultantName'=> $request->consultantName,
              'contactId'=> $request->contactId,
              'total'=>$total,
              'unpaid'=> $unpaid,
              'paid'=> $paid,
              'profit'=>$total,
              'technology'=>implode(',', $request->technology),
              'date'=> date('Y-m-d',strtotime($request->date)),
              'paymentType'=>$request->paymentType,
              'paymentTypeId'=>$request->paymentTypeId,
              'transactionId'=> $request->transactionId,
              'payingNow'=> $request->payingNow,
              'mobile'=>$request->mobile,
              'Due'=>$request->Due,
              'caseworkerId'=> $request->caseworkerId,
              'nextcomitdate'=> date('Y-m-d',strtotime($request->nextcomitdate)),
              'caseworker'=>$request->caseworker,
              'paymentStatus'=>$request->paymentStatus,
              'status'=> $request->paymentStatus,
              'work_type' => $request->work_type,
              'created_at'=>Carbon::now(),
              'createdBy'=> Auth::guard('adminiy')->user()->id,
          ])->id;
          
         /*if ($request->technology != []) {
            foreach ($request->technology as $key=>$value) {
                 $technology = Technology::where('id',$value)->first();
                 $request->technology = $technology->name;
                 $request->technologyId = $technology->id;
                 transactiontechnology::insert([
                  'contactName'=> $request->consultantName,
                  'contactId'=> $request->contactId,
                  'transactionId'=>$id,
                  'technology'=>$request->technology,
                  'technologyId'=> $request->technologyId,
                  'created_at'=>Carbon::now(),
                  'createdBy'=> Auth::guard('adminiy')->user()->id,
                  'updated_at'=>Carbon::now(),
                  'updatedBy'=> Auth::guard('adminiy')->user()->id,
              ]);
            }
        }*/
        if (!empty($request->payingNow)) {
             installments::insert([
                  'amountType'=> 'PayingNow',
                  'date'=> $request->date,
                  'transactionId'=>$id,
                  'contactId'=> $request->contactId,
                  'amount'=>$request->payingNow,
                  'paidStatus'=> $request->paymentStatus,
                  'paymentType'=>$request->paymentType,
                  'created_at'=>Carbon::now(),
                  'createdBy'=> Auth::guard('adminiy')->user()->id,
                  'updated_at'=>Carbon::now(),
                  'updatedBy'=> Auth::guard('adminiy')->user()->id,
              ]);
        }
        if (!empty($request->Due) && $request->Due != 0) {
             installments::insert([
                  'amountType'=> 'Due',
                  'transactionId'=>$id,
                  'date'=> date('Y-m-d',strtotime($request->nextcomitdate)),
                  'contactId'=> $request->contactId,
                  'amount'=>$request->Due,
                  'paidStatus'=> 3,
                  'paymentType'=>$request->paymentType,
                  'created_at'=>Carbon::now(),
                  'createdBy'=> Auth::guard('adminiy')->user()->id,
                  'updated_at'=>Carbon::now(),
                  'updatedBy'=> Auth::guard('adminiy')->user()->id,
              ]);
        }
        $logmessage = $request->consultantName.' Transaction  Created By '.Auth::guard('adminiy')->user()->name;
            $logresult = Notifications::create([
            'message' => $logmessage,
            'type' => "Transaction",
            'transactionId' => $id,
            'updated_at'=>Carbon::now(),
            'created_at'=>Carbon::now(),
        ]);
        return Redirect()->back()->with('success','transactions created successfully');
    }
    
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = transactions::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'transactions deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = transactions::find($id);
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $paymenttype = paymenttype::where('status',1)->get();
        $caseworker = User::where('status',1)->where('role','>','1')->get();
        $view=1;
        $html = view('modal.transactions_edit',compact('data','view','technology','contact','paymenttype','caseworker'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = transactions::find($id);
        $view=0;
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $paymenttype = paymenttype::where('status',1)->get();
        if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $payment_statuses = PaymentStatus::all();
        $html = view('modal.transactions_edit',compact('data','view','technology','contact','paymenttype','caseworker','payment_statuses'))->render();
        return json_encode($html);
    }
    
    public function update(Request $request)
    {
        $id =$request->id;
        if ($request->status){
            $status=1;
        } else {
            $status=0;
        }
        
        $paymenttype = paymenttype::where('id',$request->paymentType)->first();
        $request->paymentType = $paymenttype->name;
        $request->paymentTypeId = $paymenttype->id;
        
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseworker = $caseworker->firstname;
        $request->caseworkerId = $caseworker->id;
        
        
      /*  $contact = Contact::where('id',$request->consultantName)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;*/
        $trans =transactions::find($id)->first();
        $contact = Contact::where('id',$trans->contactId)->first();
        $request->mobile = $contact->mobile;
        $update = transactions::find($id)->update([
             //'consultantName'=> $request->consultantName,
              //'contactId'=> $request->contactId,
              'date'=> date('Y-m-d',strtotime($request->date)),
              'paymentType'=>$request->paymentType,
              'paymentTypeId'=>$request->paymentTypeId,
              'transactionId'=> $request->transactionId,
              'technology'=>implode(',', $request->technology),
              'payingNow'=> $request->payingNow,
              'Due'=>$request->Due,
              'mobile' => $request->mobile,
              'caseworkerId'=> $request->caseworkerId,
              'nextcomitdate'=> date('Y-m-d',strtotime($request->nextcomitdate)),
              'caseworker'=>$request->caseworker,
              'status'=> $request->paymentStatus,
              'paymentStatus'=>$request->paymentStatus,
              'updated_at'=>Carbon::now(),
              'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
       /* transactiontechnology::where('contactId',$request->contactId)->Delete();
        if($request->technology != [])
        {
           // print_r(count($request->technology));exit;
            foreach($request->technology as $key=>$value)
            {
                 $technology = Technology::where('id',$value)->first();
                 $request->technology = $technology->name;
                 $request->technologyId = $technology->id;
                 transactiontechnology::insert([
                  'contactName'=> $request->consultantName,
                  'contactId'=> $request->contactId,
                  'technology'=>$request->technology,
                  'technologyId'=> $request->technologyId,
                  'created_at'=>Carbon::now(),
                  'createdBy'=> Auth::guard('adminiy')->user()->id,
                  'updated_at'=>Carbon::now(),
                  'updatedBy'=> Auth::guard('adminiy')->user()->id,
              ]);
            }
        }*/
        
        if ($update) {
            return Redirect()->route('transactions.index')->with('success','transactions updated successfully');
        }
        return Redirect()->route('transactions.index')->with('failed','please try again');
    }
      
    public function details($id)
    {
        $data = transactions::where('contactId', $id)->first();
        $log = logs::where('contactID',$data->contactId)->count();
       
        $amount = Transactions::where('contactId', $data->contactId)->groupby('contactId')->get(
            array(
            DB::raw('SUM(total) as total'),
            DB::raw('SUM(paid) as paid'),
            DB::raw('SUM(refund_amount) as refund_amount'),
            DB::raw('SUM(smepaid) as smepaid'),
            DB::raw('SUM(smeunpaid) as smeunpaid'),
            DB::raw('SUM(profit) as profit'),
            DB::raw('SUM(unpaid) as unpaid'),
          ));
        $movie_amounts = [
            'total_amount' => $amount[0]->total, 
            'total_paid_amount' => $amount[0]->paid,
            'total_refund_amount' => $amount[0]->refund_amount,
            'total_unpaid_amount' => $amount[0]->unpaid,
            'total_smepaid' => $amount[0]->smepaid,
            'total_smeunpaid' => $amount[0]->smeunpaid,
            'total_profit' => $amount[0]->profit
        ];
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $img = Contact::where('id',$data->contactId)->first();
        $paymenttype = paymenttype::where('status',1)->get();
        $payment_status = PaymentStatus::all(); 
        if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        return view('details', compact('data','movie_amounts','technology','contact','paymenttype','caseworker','img', 'payment_status','log'));
    }
    
    public function transactiondetails($id) 
    {
     $getTransactions = installments::where('contactId', $id)->get();
     if(!empty($getTransactions)){
         foreach($getTransactions as $key=>$value)
         {
            // print_r($value->transactionId);
            $tras = transactions::where('id',$value->transactionId)->first();
            $names = [];
            $tech = explode(',',$tras->technology);
            foreach($tech as $key=>$kvalue)
            {
                $technology = Technology::where('id',$kvalue)->first();
                $names[] = $technology->name;
            }
            $value->consultantName = $tras->consultantName;
            $value->caseworker = $tras->caseworker;
        
            $payment_status = PaymentStatus::find($value->paidStatus);
            $value->technology = implode(',',$names);
            $value->paidStatus = $payment_status->name;
         }
     }
     return Datatables::of($getTransactions)->addIndexColumn()
                    ->addColumn('action', function($row)
                    {
                    $btn = '';
                    if ($row->paidStatus != 'Paid' && $row->paidStatus != 'Refund') {
                       $btn .= ' <button class="btn p-0 ms-2" id="installments_edit" type="button" data-id=" ' . $row->id . ' "  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                        '; 
                    }
                     $btn .= '<a target="_blank" href="' . url('invoice/' . $row->id . '/1') .'"><i class="fa fa-file-invoice"></i></a>'; 
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }
    
    public function smes($id,Request $request) 
    {
        $getTransactions =Transactions::where('id', $id)
              ->first();
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $sme = Contact::where('status',1)->where('contactTypeId',1)->get();
        $worktype = worktype::where('status',1)->get();
        $batch = caseworker::where('status',1)->get();
         if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $payment_statuses = PaymentStatus::all();
        $interviews = interviews::where('transactionId',$id)->count();

         $statustype = statustype::where('status', 1)->get();
         if ($request->ajax()) {
            $getTransactions =transactiontechnology::where('transactionId', $id)
              ->get();
            if(!empty($getTransactions)){
            foreach($getTransactions as $key=>$value)
            {
                 $transactionId = Transactions::where('id',$value->transactionId)->first();
                 $value->transactionId = $transactionId->transactionId;
                 $mobile = Contact::where('id',$value->contactId)->first();
                 $value->mobile= $mobile->mobile;
            }
        
       }
       return Datatables::of($getTransactions)->addIndexColumn()
                    ->addColumn('action', function($row)
                    {
                    
                    //print_r($row->technology);exit;    
                    $url = '/studios/earningsdata/' . $row->id;
                    $btn = ' <button class="btn p-0 ms-2" id="sme_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button></i></button> <a href="' . url('/smecomment/' . $row->id) . '" class="btn p-0 ms-2" type="button" title="View"><i class="fas fa-eye" style="color:orange"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
         }
       return view('smes',compact('technology','contact','worktype','caseworker','id','getTransactions','sme','caseworker','batch','statustype','payment_statuses','interviews'));

    }
    
    public function interviews($id, Request $request) 
    {
        $getTransactions =Transactions::where('id', $id)->first();
        $technology = Technology::where('status', 1)->get();
        $contact = Contact::where('status', 1)->get();
        $sme = Contact::where('status', 1)->where('contactTypeId',1)->get();
        $worktype = worktype::where('status', 1)->get();
        $statustype = statustype::where('status', 1)->get();
        $interviewtools = interviewtools::where('status', 1)->get();
         if(Auth::guard('adminiy')->user()->role == 'Super Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $payment_statuses = PaymentStatus::all();
        $interview =  interviews::where('transactionId', $id)
              ->first();
       
        if(!empty($interview))
        {
            $comments = comments::where('interviewId',$interview->id)->count();
            // /print_r($comments);exit;
        }
        else
        {
            $comments = 0;
        }
         
        if ($request->ajax()) {
            $getTransactions =interviews::where('transactionId', $id)
              ->get();
            if (!empty($getTransactions)) {
                foreach($getTransactions as $key => $value) {
                     $transactionId = Transactions::where('id',$value->transactionId)->first();
                     $value->transactionId = $transactionId->transactionId;
                     $statusKey = $statustype->find($value->Status);
                     $statuses = PaymentStatus::where('id',$value->paymentStatus)->first();
                     $statustype = statustype::where('id',$value->Status)->first();
                     $value->Status = $statusKey->name;
                     $value->paymentStatus = $statuses->name;
                     $value->Status = $statustype->name;
                }
            }
           return Datatables::of($getTransactions)->addIndexColumn()
                        ->addColumn('action', function($row)
                        {
                        $url = '/studios/earningsdata/' . $row->id;
                        $btn = ' <button class="btn p-0 ms-2" id="interviews_edit" type="button" data-id="' . $row->id . '"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> <a href="' . url('/comments/' . $row->id) . '" class="btn p-0 ms-2" type="button" title="View"><i class="fas fa-eye" style="color:orange"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
         }
       return view('interview', compact('technology','contact','worktype','caseworker','id','getTransactions','sme','statustype','interviewtools','payment_statuses','comments'));
    }
    
    public function invoice($id, $type)
    {
        $companyDetails = Setting::find(1);
        $getTransaction = installments::with('transaction', 'contact.location_details')->find($id);
        if(empty($getTransaction->due))
        {
            $getTransaction->due = 0;
        }
        //echo "<pre>";print_r($getTransaction);die;
        $technologies = Technology::whereIN('id', [$getTransaction->transaction['technology']])->get();
        $techs = [];
        if (!empty($technologies))
        {
            foreach($technologies as $tech)
            {
                $techs[]= $tech->name;
            }
        }
       // print_r($type);exit;
        if ($type == 1) 
        {
            return view('invoice.invoice1', compact('companyDetails', 'getTransaction', 'techs'));
        }
        else
        {
            return view('invoice.invoice1', compact('companyDetails', 'getTransaction', 'techs'));
        }
        
    }
    
    // public function invoice2($id)
    // {
    //     $companyDetails = Setting::find(1);
        
    // //   $data =transactioninstallments::select('transactioninstallments.id','transactioninstallments.installmentamount','transactioninstallments.duration','transactioninstallments.paymentmonth','movies.name','transactioninstallments.payment_status','transactioninstallments.orderId','transactioninstallments.created_at','movies.type','movies.thumbnail','transactions.Payment_type','studio.studio_name','studio.contact','studio.email','studio.address','studio.first_name','studio.last_name','studio.middle_name','studio.contact')
    // //               ->leftJoin('movies', 'movies.id', '=', 'transactioninstallments.VID')
    // //               ->leftJoin('transactions', 'transactions.movie_id', '=', 'transactioninstallments.VID')
    // //               ->leftJoin('studio', 'studio.id', '=', 'movies.studio')
    // //               ->where('transactioninstallments.id', $id)
    // //               ->first();
                  
    //     // $user_tax = Adminiy::where('id',1)->first('user_tax');
         
    //       //$termscondition = TermsCondition::where('id',1)->first('text');
          
    //     //  return view('pdfinvoice',compact('data','user_tax','termscondition'));
    //     //   $pdf = PDF::loadView('pdfinvoice',compact('data','user_tax','termscondition'));
    //     return view('invoice.invoice2', compact('companyDetails'));
    // }
    
}