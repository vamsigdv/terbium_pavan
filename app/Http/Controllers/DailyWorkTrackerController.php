<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use DataTables;
use Session;
use App\Models\DailyWorkTrack;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Response;

class DailyWorkTrackerController extends Controller
{
    protected $path;
    
    public function __construct() {
        $this->path = 'uploads/work_track/';
        if(!Storage::exists($this->path)){
            Storage::makeDirectory($this->path);
        }
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = DailyWorkTrack::join('users', 'users.id', '=', 'daily_work_track.employee_id')->select('daily_work_track.id', 'calls_count', DB::raw("CONCAT(users.firstname, ' ', users.lastname) AS employee"), 'track_file','date')->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="daily_work_track_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <a href="'.route('daily_work_track.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('daily_work_track.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('work_track.daily_work_track');
    }
    
    public function store(Request $request)
    {
        $file = $request->file('track_file');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = 'DailyWorkDataTracker_' . $name_gen . '.' . $file_ext;
            $up_location = $this->path;
            $file_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $file_path='';
        }
        DailyWorkTrack::create([
            'calls_count' => $request->calls_count,
            'track_file' => $file_path,
            'employee_id' =>  Auth::guard('adminiy')->user()->id,
            'date' => $request->created_date,
            'created_at' => Carbon::now(),
        ]); 
        
        return Redirect()->back()->with('success','Daily Work data created successfully');
    }
    
    public function destroy($id)
    {
        $work_tracker = DailyWorkTrack::find($id);
        $delete = DailyWorkTrack::where('id', $id)->delete();
        if ($delete) {
            $path = substr(public_path(),0, -7) . "/" . $work_tracker->track_file;
            unlink($path);  
            Session::flash('success', 'Daily work data tracker deleted successfully');
            return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    
    public function download_report($id) {
        $work_tracker = DailyWorkTrack::find($id);
        return Response::download($work_tracker->track_file);
    }
    
    public function edit($id) {
        $track = DailyWorkTrack::find($id); 
        $html = view('modal.work_track.daily_work_track_edit',compact('track'))->render();
        return json_encode($html);
    }
    
    public function update(Request $request) {
        $id =$request->id;
        $update = DailyWorkTrack::find($id);
        $file=$request->file('track_file');
        if (isset($file)) {
            $path = substr(public_path(),0, -7) . "/" . $update->track_file;
            if (unlink($path)) {
                $name_gen = hexdec(uniqid());
                $file_ext = strtolower($file->getClientOriginalExtension());
                $file_name = 'DailyWorkDataTracker_' . $name_gen.'.'.$file_ext;
                $up_location = $this->path;
                $file_path = $up_location.$file_name;
                $file->move($up_location,$file_name);
            } else {
                $file_path = $request->old_file;
            }
        } else {
            $file_path = $request->old_file;
        }
        $update->calls_count = $request->calls_count;
        $update->track_file = $file_path;
        $update->date = $request->created_date;
        $update->updated_at = Carbon::now();
        if ($update->save()) {
            return Redirect()->route('daily_work_track.index')->with('success','Daily Work Data updated successfully');
        }
        return Redirect()->route('daily_work_track.index')->with('failed','please try again');
    }
}