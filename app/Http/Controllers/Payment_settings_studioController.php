<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Studio;
use App\Models\Adminiy;
use App\Models\Admin_session;
use Hash;
use App\Models\StudioEarning;
use App\Models\StudioTransactions;

class StudioController extends Controller
{
    
    /**
     * To not check authntication for the store method
     */
    public function __construct()
    {
        $this->middleware('adminiy', ['except' => [
            'register', 'registerStudio'
            ]
        ]);
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Studio::select('id','studio_name','contact','email','status','password')->where('delete',1)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="studio_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="studio_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('studio.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('studio.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('studio');
    }
    public function store(Request $request)
    {   
      
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/studio/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $img_path='';
        }
        $id=Studio::create([
              'studio_name'=>$request->studio_name,
              'contact'=>$request->contact,
              'email'=>$request->email,
              'first_name'=>$request->first_name,
              'last_name'=>$request->last_name,
              'middle_name'=>$request->middle_name,
              'district'=>$request->district,
              'village'=>$request->village,
              'vouchar'=>$request->vouchar,
              'voter'=>$request->voter,
              'address'=>$request->address,
              'account_number'=>$request->account_number,
              'ifsc'=>$request->ifsc,
              'password'=>$request->password,
              'photo'=>$img_path,
              'status'=> $status,
              'created_at'=>Carbon::now(),
        ])->id;
        if ($id) {
            $admin_id=Adminiy::create([
                'name'=>$request->first_name,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                'role'=> 'studio',
                'is_active'=> $status,
                'user_id'=> $id,
                'created_at'=>Carbon::now(),
            ])->id;
            $update = Studio::find($id)->update([
                'admin_id'=> $admin_id,
                'updated_at'=>Carbon::now(),
            ]);
        }
        return Redirect()->back()->with('success','studio created successfully');
    }
    public function destroy($id)
    {
        $studio = Studio::find($id);
        if (Auth::guard('adminiy')->user()->role != "admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Studio::find($id)->update([
            'delete'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
            $admin_id=Adminiy::find($studio->admin_id)->update([
                'is_active'=>0,
                'updated_at'=>Carbon::now(),
            ]);
           Session::flash('success', 'studio deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Studio::find($id);
        $view=1;
        $html = view('modal.studio_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = Studio::find($id);
        $view=0;
        $html = view('modal.studio_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $admin_id =$request->admin_id;
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/customers/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $img_path=$request->photo_old;
        }
        $update = Studio::find($id)->update([
            'studio_name'=>$request->studio_name,
            'contact'=>$request->contact,
            'email'=>$request->email,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'middle_name'=>$request->middle_name,
            'district'=>$request->district,
            'village'=>$request->village,
            'vouchar'=>$request->vouchar,
            'voter'=>$request->voter,
            'address'=>$request->address,
            'account_number'=>$request->account_number,
            'ifsc'=>$request->ifsc,
            'password'=>$request->password,
            'photo'=>$img_path,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
        ]);
        if ($update) {
            $admin_id=Adminiy::find($admin_id)->update([
                'name'=>$request->first_name,
                'email'=>$request->email,
                'is_active'=>$status,
                'password'=> Hash::make($request->password),
                'updated_at'=>Carbon::now(),
            ]);
            return Redirect()->route('studio.index')->with('success','studio updated successfully');
        }
        return Redirect()->route('studio.index')->with('failed','please try again');
      }
      
      public function register()
    {
        return view('register');
    }

    /**
     * To Register studio details
     *
     * @param Request $request
     * @return void
     */
    public function registerStudio(Request $request)
    {
        $validated = $request->validate([
            'studio_name' => 'required|unique:studio',
            'first_name' => 'required',
            'email' => 'required|unique:studio',
            'contact' => 'required|unique:studio',
        ]);
        if ($request->status) {
            $status=1;
        } else {
            $status=0;
        }
        $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/studio/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        } else {
            $img_path='';
        }
        $id=Studio::create([
              'studio_name'=>$request->studio_name,
              'contact'=>$request->contact,
              'email'=>$request->email,
              'first_name'=>$request->first_name,
              'last_name'=>$request->last_name,
              'middle_name'=>$request->middle_name,
              'district'=>$request->district,
              'village'=>$request->village,
              'vouchar'=>$request->vouchar,
              'voter'=>$request->voter,
              'address'=>$request->address,
              'account_number'=>$request->account_number,
              'ifsc'=>$request->ifsc,
              'password'=>$request->password,
              'photo'=>$img_path,
              'status'=> $status,
              'created_at'=>Carbon::now(),
        ])->id;
        if ($id) {
            $admin_id=Adminiy::create([
                'name'=>$request->first_name,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                'role'=> 'studio',
                'is_active'=> 1,
                'user_id'=> $id,
                'created_at'=>Carbon::now(),
            ])->id;
            $update = Studio::find($id)->update([
                'admin_id'=> $admin_id,
                'updated_at'=>Carbon::now(),
            ]);
        }
        return Redirect()->back()->with('success','studio created successfully');
    }
    
    /**
     * To display the use coins
     *
     * @param Request $request
     * @return void
     */
    public function getEarnings(Request $request)
    {
        
        if ($request->ajax()) {
            $earnings = StudioEarning::with('movie', 'studio')->get();
            $earnings->map(function ($earning) {
                $earning->sname = $earning->studio['studio_name'];
                return $earning;
            });
            return DataTables::of($earnings)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' 
                    <button class="btn p-0 ms-2" id="studio_earnings_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <a href="' . url('studios/delete_earning') . '/'. $row->id . '" id="'.$row->id.'" data-method="POST" data-url="' . url('user/get_earnings') . '" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('studio_earnings');
    }

    /**
     * To edit earnings
     *
     * @param  int $id
     * @return void
     */
    public function getEarning($id)
    {
        $earning = StudioEarning::with('studio')->find($id);
        $html = view('modal.studio_earning_edit', compact('earning'))->render();
        return json_encode($html);
    }

    /**
     * To update earning
     *
     * @param  int $id
     * @return void
     */
    public function updateEarning($id, Request $request)
    {
        $earning = StudioEarning::with('studio')->find($id);
        $earning->updated_at = Carbon::now();
        $earning->coins = $request->coins;
        $earning->save();
        if ($earning) {
            return Redirect()->back()->with('success','updated');        
        }
        return Redirect()->back()->with('success','Nothing to update');     
    }

   /**
     * Delete Earning
     *
     * @param Request $request
     * @return void
     */
    public function deleteEarning($id)
    {
        $delete = StudioEarning::find($id)->delete();
        if ($delete) {
           Session::flash('success', 'Earning deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function withdrawEarnings($id)
	{
		$transId = uniqid();
        $studio = Studio::find($id);
        $studioTransaction = StudioTransactions::create([
			'order_id' => $transId,
			'studio_id' => $id,
			'sname' => $studio->studio_name,
			'finance_id' => '1',
			'finance_name' => 'demo',
			'amount' => $studio->wallet,
			'coins' => $studio->wallet,
		]);
		if ($studioTransaction) {
			$studio=Studio::find($id)->update([
				'wallet' => 0,
				'updated_at' => Carbon::now()
			]);
            if($studio){
                return Redirect()->route('studiotransactions')->with('success','studio transaction created successfully');
            }
           
		}		
	}
    public function getTransactions(Request $request)
    {
        if ($request->ajax()) {
            if (Auth::guard('adminiy')->user()->role == "studio") {
                $transactions = StudioTransactions::select('id','order_id','sname','finance_name','amount','status')->where('studio_id',Auth::guard('adminiy')->user()->user_id)->get();
            }else{
                $transactions = StudioTransactions::select('id','order_id','sname','finance_name','amount','status')->get();
            }
            return DataTables::of($transactions)
                ->addIndexColumn()
            //    ->addColumn('action', function($row){
            //         $btn = ' ';
            //         return $btn;
            //     })
            //     ->rawColumns(['action'])
                ->make(true);
        }
        return view('studio_transactions');
    }
    public function settings(){
        $data = Studio::find(Auth::guard('adminiy')->user()->user_id);
        return view('studiosettings',compact('data'));
    }
    public function update_settings(Request $request){
        $admin_id =$request->admin_id;
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/customers/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $img_path=$request->photo_old;
        }
        $update = Studio::find($id)->update([
            'studio_name'=>$request->studio_name,
            'contact'=>$request->contact,
            'email'=>$request->email,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'middle_name'=>$request->middle_name,
            'district'=>$request->district,
            'village'=>$request->village,
            'vouchar'=>$request->vouchar,
            'voter'=>$request->voter,
            'address'=>$request->address,
            'account_number'=>$request->account_number,
            'ifsc'=>$request->ifsc,
            'password'=>$request->password,
            'photo'=>$img_path,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
        ]);
        if ($update) {
            $admin_id=Adminiy::find($admin_id)->update([
                'name'=>$request->first_name,
                'email'=>$request->email,
                'is_active'=>$status,
                'password'=> Hash::make($request->password),
                'updated_at'=>Carbon::now(),
            ]);
            return Redirect()->route('studiosettings')->with('success','studio updated successfully');
        }
        return Redirect()->route('studiosettings')->with('failed','please try again');
      }
}
