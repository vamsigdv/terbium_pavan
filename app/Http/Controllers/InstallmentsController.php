<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Skills;
use DataTables;
use Session;
use App\Models\Technology;
use App\Models\Contact;
use App\Models\paymenttype;
use App\Models\caseworker;
use App\Models\transactiontechnology;
use App\Models\installments;
use App\Models\worktype;
use App\Models\transactions;
use App\Models\interviews;
use App\Models\interviewtools;
use App\Models\statustype;
use App\Models\User;
use App\Models\PaymentStatus;
use App\Models\smeagreepayments;
use App\Models\logs;
class InstallmentsController extends Controller
{
    
    public function store(Request $request)
    {   
        $trans = Transactions::where('id',$request->transactionId)->first();
        $trans->total = $trans->total + $request->amount;
       
        if ($request->status == 1) 
        {
            $trans->paid = $trans->paid + $request->amount;
            $trans->unpaid = 0;
            $trans->due = 0;
            $trans->update();
        } else {
            $trans->unpaid = $trans->unpaid + $request->amount;
            $trans->due = $trans->due + $request->amount;
            $trans->update();
        }
        $nextcommiteddate =date('Y-m-d');
        $due ='';
        
        if ($request->status == 4) 
        {
            $nextcommiteddate = $request->nextcommiteddate;
            $due = $request->due;
           // $logmessage = date('Y-m-d').' '.$trans->consultantName . ' Paid ' . $request->amount . ' Still Due AMount ' . $due. 'is pending  next commited_date'.$nextcommiteddate.' Payment Status: Partially Paid';
             $logmessage = $trans->caseworker . ' commented: On ' . date('Y-m-d'). ' '.$trans->consultantName. 'paid ' .$request->amount - $due. ' still due amount '. $due.' is pending  next commited_date'.$nextcommiteddate.' Payment Status: Partially Paid';
           
            $logresult = logs::create([
            'log' => $logmessage,
            'contactID' => $trans->contactId,
            'transactionId' => $trans->transactionId,
            'updated_at'=>Carbon::now(),
            'created_at'=>Carbon::now(),
        ]);
        }
        if(!empty($request->refund_amount)){
            //$trans->refund_amount =  $request->refund_amount;
            if($request->status == 5)
            {
                if(!empty($trans->profit) || $trans->profit != 0)
                {
                    $trans->profit = $trans->profit-$request->refund_amount;
                }
                $trans->refund_amount = $trans->refund_amount+$request->refund_amount;
                $logmessage = $trans->consultantName . ' Got Refund amount ' . $request->refund_amount . ' to ' . $trans->caseworker. 'on '.date('Y-m-d').' Payment Status: Refund';
                $logresult = logs::create([
                'log' => $logmessage,
                'contactID' => $trans->contactId,
                'transactionId' => $trans->transactionId,
                'updated_at'=>Carbon::now(),
                'created_at'=>Carbon::now(),
            ]);
            }
            elseif($request->status == 6)
            {
                $trans->profit = $trans->profit+$request->refund_amount;
                if(!empty($trans->refund_amount) || $trans->refund_amount != 0)
                {
                    $trans->refund_amount = $trans->refund_amount-$request->refund_amount;
                }
                $logmessage = $trans->consultantName . ' Adjust amount ' . $request->refund_amount . ' to ' . $trans->caseworker. 'on '.date('Y-m-d').' Payment Status: Adjustment';
                $logresult = logs::create([
                'log' => $logmessage,
                'contactID' => $trans->contactId,
                'transactionId' => $trans->transactionId,
                'updated_at'=>Carbon::now(),
                'created_at'=>Carbon::now(),
            ]);
            }
            
            $request->amount = $request->refund_amount;
        }
        else
        {
             $trans->profit =  $trans->total;
        }
        $trans->update();
          installments::insert([
                'transactionId'=>$request->transactionId,
                'contactId'=>$request->contactId,
                'created_at'=>Carbon::now(),
                'amountType'=>'New',
                'due'=>$due,
                'nextcommiteddate'=>$nextcommiteddate,
                'date'=>$request->date,
                'amount'=>$request->amount,
                'paymentType'=>$request->paymentType,
                'paidStatus'=> $request->status,
                'comments' => $request->comments,
                'refund_amount' => $request->refund_amount,
                'created_at'=>Carbon::now(),
                'createdBy'=> Auth::guard('adminiy')->user()->id,
          ]);
            
        return Redirect()->back()->with('success','Interview created successfully');
    }
   
    public function edit($id,$type){
        if($type == 'sme')
        {
            $data = smeagreepayments::find($id);
            if($data->type != 'Interview' && $data->type !='Batch')
            {
                $typenew = worktype::where('name',$data->type)->first();
                $data->type = $typenew->id;
            }
            $data->paidStatus = $data->status;
            $data->amount = $data->agreeamount;
        }
        else
        {
            $data = installments::find($id);
        }
       // print_r($type);exit;
        $tras = transactions::where('id',$data->transactionId)->first();
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $paymenttype = paymenttype::where('status',1)->get();
        $caseworker = User::where('status',1)->get();
        $payment_status = PaymentStatus::all();
        $worktype = worktype::where('status',1)->get();
        $view=0;
        $html = view('modal.installments_edit',compact('data','view','technology','contact','paymenttype','caseworker','tras', 'payment_status','worktype','type'))->render();
        return json_encode($html);
    }
    
    public function update(Request $request){
        $id =$request->id;
        //print_r($request->workType);exit;
        if($request->workType == '')
        {
            $data = installments::find($id);
            
            $trans = Transactions::where('id', $data->transactionId)->first();
            if($request->status == 1)
            {
                
                $trans->paid = $trans->paid + $trans->unpaid;
                $trans->unpaid = 0;
                $trans->due = 0;
                $trans->update();
            }
        }
        else
        {
            $data = smeagreepayments::find($id);
            $trans = Transactions::where('id', $data->transactionId)->first();
            if($request->paymentStatus == 1)
            {
                 $trans->smepaid = $trans->smepaid+$request->SMEAgreeAmount;
            }
            else
            {
                 $trans->smeunpaid =  $trans->smeunpaid - $request->SMEAgreeAmount;
            }
             $trans->update();
        }
        
        

        if(!empty($request->refund_amount)){
            $request->status = 5;
            $request->amount = $request->refund_amount;
            $trans->refund_amount =  $request->refund_amount;
            $trans->update();
        }
        $nextcommiteddate =date('Y-m-d');
        $due ='';
        if ($request->status == 4) 
        {
            $nextcommiteddate = $request->nextcommiteddate;
            $due = $request->due;
            //print_r($trans);exit;
            $logmessage = $trans->caseworker . ' commented: On ' . date('Y-m-d'). ' '.$trans->consultantName. 'paid ' .$request->amount - $due. ' still due amount '. $due.' is pending  next commited_date'.$nextcommiteddate.' Payment Status: Partially Paid';
            $logresult = logs::create([
            'log' => $logmessage,
            'contactID' => $data->contactId,
            'transactionId' => $data->transactionId,
            'updated_at'=>Carbon::now(),
            'created_at'=>Carbon::now(),
        ]);

        }
        if($request->workType == '')
        {
        $update = installments::find($id)->update([
            'paidStatus' => $request->status,
            'comments' => $request->comments,
            'amount'=>$request->amount,
            'due'=>$due,
            'nextcommiteddate'=>$nextcommiteddate,
            'refund_amount' => $request->refund_amount,
            'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        }
        else
        {
             $update = smeagreepayments::find($id)->update([
            'status' => $request->status,
            'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        }
       // print_r($data->transactionId);exit;
       
         if(str_replace(env('APP_URL'),"",redirect()->back()->getTargetUrl()) == '/smepayments') 
         {
              return Redirect()->route('smepaymentsdata')->with('success','Installment updated successfully');
         }
        elseif ($request->workType == '') {
            return Redirect()->route('details',$data->contactId)->with('success','Installment updated successfully');
        }
        elseif ($request->workType) {
            return Redirect()->route('smedetails',$data->smeId)->with('success','Installment updated successfully');
        }
        else
        {
            return Redirect()->route('details',$data->contactId)->with('failed','please try again');
        }
      }

}