<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Skills;
use DataTables;
use Session;
use App\Models\Technology;
class SkillsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Skills::get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="Skills_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="Skills_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('skills.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('skills.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $technology = Technology::where('status',1)->get();
        return view('skills',compact('technology'));
    }
    public function store(Request $request)
    {   
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $technology = Technology::where('id',$request->technology)->first();
        $request->technology = $technology->name;
        $request->technologyId = $technology->id;
        Skills::insert([
              'name'=>$request->name,
              'rolesres'=>$request->rolesres,
              'status'=> $status,
              'technology'=>$request->technology,
              'technologyId'=>$request->technologyId,
              'created_at'=>Carbon::now(),
              'createdBy'=> Auth::guard('adminiy')->user()->id,
              
          ]);
        return Redirect()->back()->with('success','Skills created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Skills::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Skills deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Skills::find($id);
        $view=1;
         $technology = Technology::where('status',1)->get();
        $html = view('modal.Skills_edit',compact('data','view','technology'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = Skills::find($id);
        $technology = Technology::where('status',1)->get();
        $view=0;
        $html = view('modal.Skills_edit',compact('data','view','technology'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
         $technology = Technology::where('id',$request->technology)->first();
        $request->technology = $technology->name;
        $request->technologyId = $technology->id;
        $update = Skills::find($id)->update([
            'name'=>$request->name,
             'rolesres'=>$request->rolesres,
            'status'=> $status,
            'technology'=>$request->technology,
              'technologyId'=>$request->technologyId,
            'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('skills.index')->with('success','Skills updated successfully');
        }
        return Redirect()->route('skills.index')->with('failed','please try again');
      }

}