<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Slider;
use Illuminate\Support\Carbon;
use Session;
use App\Models\SliderType;
use App\Models\Movie;

class SliderController extends Controller
{
    /**
     * To get the list of Sliders
     * 
     * @param Request
     * @return array
     */
    public function index(Request $request)
    {
        $slider_types = SliderType::get();
        $movies = Movie::where('delete', 1)->select('id', 'name')->get();
        if ($request->ajax()) {
            $data = Slider::select('id', 'slider_image as image', 'title', 'status')->where('is_delete', 1)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="slider_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <a href="'.route('slider.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('slider.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('slider', compact('slider_types', 'movies'));
    }

    /**
     * To store the values from the form
     * 
     * @params  Request $reqeust
     * @return array
     */
    public function store(Request $request)
    {   
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('slider_image');
        $name_stream = hexdec(uniqid());
        $file_ext = strtolower($file->getClientOriginalExtension());
        $file_name = $name_stream . '.' . $file_ext;
        $up_location = 'uploads/slider/';
        $thumbnail =  $up_location . $file_name;
        $file->move($up_location, $file_name);
        Slider::insert([
              'slider_image' => $thumbnail,
              'title' => $request->title,
              'type' => $request->type,
              'movie_id' => $request->movie_id,
              'status' => $status,
              'created_at' => Carbon::now(),
          ]);
        return Redirect()->back()->with('success','Details created successfully');
    }

    /**
     * delete stream details
     * 
     * @params  int $id
     * @return array
     */
    public function destroy($id)
    {   
        $delete = Slider::find($id)->update([
            'is_delete' => 0,
            'updated_at' => Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Details deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }

    /**
     * To view Slider details
     * 
     * @param int $id
     * @return array
     */
    public function edit($id){
        $data = Slider::find($id);
        $slider_types = SliderType::get();
        $movies = Movie::where('delete', 1)->select('id', 'name')->get();
        $html = view('modal.slider_edit',compact('data', 'slider_types', 'movies'))->render();
        return json_encode($html);
    }

    /**
     * To update the details if exists
     * 
     * @param Request $request
     * @return array
     */
    public function update(Request $request)
    {
        $id = $request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $slider = Slider::find($id);
        $file=$request->file('slider_image');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen . '.' . $file_ext;
            $up_location = 'uploads/slider/';
            $img_path = $up_location . $file_name;
            $file->move($up_location, $file_name);
           // print_r(public_path());exit;
            //unlink(public_path() . '/' . $slider->image);
        }else{
            $img_path=$request->photo_old;
        }
        $update = $slider->update([
            'slider_image' => $img_path,
            'title' => $request->title,
            'type' => $request->type,
            'movie_id' => $request->movie_id,
            'status' => $status,
            'updated_at' => Carbon::now()
        ]);
        if ($update) {
            return Redirect()->route('slider.index')->with('success','slider updated successfully');
        }
        return Redirect()->route('slider.index')->with('failed','please try again');
    }
}
