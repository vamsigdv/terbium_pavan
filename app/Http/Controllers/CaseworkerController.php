<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\caseworker;
use DataTables;
use Session;
use App\Models\Contact;
use App\Models\Technology;
use App\Models\smeagreepayments;
use App\Models\User;
use App\Models\BatchComments;
use Response;

class CaseworkerController extends Controller
{
    public function index(Request $request)
    {
        $contact = Contact::where('status',1)->where('contactTypeId','1')->get();
        $contacts = Contact::where('status', 1)->get();
        $technology = Technology::where('status',1)->get();
       // if(Auth::guard('adminiy')->user()->role == 'Admin') {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        //} else {
         //   $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        //}
        if ($request->ajax()) {
            $data = caseworker::get();
            foreach ($data as $_data) {
                $_p = [];
                if ($_data->participants != '') {
                    $p = explode(',', $_data->participants);
                    foreach ($p as $_pp) {
                        $c = $contacts->where('id', $_pp)->first();
                        $_p[] = ($c->firstname) ? $c->firstname : '';
                    }
                    $_data->participants = implode(', ', $_p);
                }
                
            }
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="caseworker_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <a class="btn p-0 ms-2" href="' . url('case_worker/batch_comments/' . $row->id) . '"><i class="fas fa-eye" style="color:aqua"></i></a>
                    <a href="'.route('caseworker.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('caseworker.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('caseworker',compact('contact','technology','caseworker', 'contacts'));
    }
    
    
    public function store(Request $request)
    {   
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $contact = Contact::where('id', $request->consultantName)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
        
        if($request->technology != [])
        {
            foreach($request->technology as $tkey=>$tvalue)
            {
                $technology = Technology::where('id',$tvalue)->first();
                $technologys[] = $technology->name;
                $technologyIds[] = $technology->id;
            }
        }
        
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->firstname;
        $request->caseWorkerId = $caseworker->id;
        
        caseworker::insert([
              'name'=>$request->name,
              'batch_start_date'=>$request->batch_start_date,
              'batch_end_date'=>$request->batch_end_date,
              'contactId'=> $request->contactId,
              'consultantName'=> $request->consultantName,
              'employeeId'=> $request->caseWorkerId,
              'employeeName'=> $request->caseWorker,
              'technology'=>implode(',',$technologys),
              'technologyId'=>implode(',',$technologyIds),
              'participants' => implode(',', $request->participants),
              'amount'=> $request->amount,
              'status'=> $status,
              'created_at'=>Carbon::now(),
              'createdBy'=> Auth::guard('adminiy')->user()->id,
              'assigned_to' => $request->assigned_to,
              
          ]);
          if(!empty($request->amount))
          {
                smeagreepayments::insert([
                   
                    'contactId'=>$request->contactId,
                    'smeId'=>$request->contactId,
                   // 'interviewId'=>$id,
                    'type'=>"Batch",
                    'agreeamount'=>$request->amount,
                    'date'=>date('Y-m-d'),
                    'status'=>1,
                    'employee'=>$request->contactId,
                    'created_at'=>Carbon::now(),
                    'createdBy'=> Auth::guard('adminiy')->user()->id,
                    'updated_at'=>Carbon::now(),
                    'updatedBy'=> Auth::guard('adminiy')->user()->id,
                    
                ]); 
          }
        return Redirect()->back()->with('success','caseworker created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = caseworker::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'caseworker deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = caseworker::find($id);
        $technology = Technology::where('status',1)->get();
          if(Auth::guard('adminiy')->user()->role == 'Admin')
        {
            $caseworker = User::where('status',1)->where('role','>','1')->get();
        }
        else
        {
            $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        }
        $contact = Contact::where('status',1)->get();
        $view=1;
         $contact = Contact::where('status',1)->where('contactTypeId','1')->get();
        $html = view('modal.caseworker_edit',compact('data','view','contact','technology','caseworker'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = caseworker::find($id);
        $view=0;
        //  if(Auth::guard('adminiy')->user()->role == 'Admin')
        //{
            $caseworker = User::where('status',1)->where('role','>','1')->get();
       // }
       // else
        //{
        //    $caseworker = User::where('id',Auth::guard('adminiy')->user()->user_id)->get();
        //}
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->where('contactTypeId','1')->get();
        $contacts = Contact::where('status',1)->get();
        $html = view('modal.caseworker_edit',compact('data','view','contact','technology','contacts','caseworker'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $contact = Contact::where('id',$request->consultantName)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
   
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        
        if($request->technology != [])
        {
            foreach($request->technology as $tkey=>$tvalue)
            {
                $technology = Technology::where('id',$tvalue)->first();
                $technologys[] = $technology->name;
                $technologyIds[] = $technology->id;
            }
        }
        $caseworker = User::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->firstname;
        $request->caseWorkerId = $caseworker->id;
        $update = caseworker::find($id)->update([
            'name'=>$request->name,
            'batch_start_date'=>$request->batch_start_date,
            'batch_end_date'=>$request->batch_end_date,
            'contactId'=> $request->contactId,
            'consultantName'=> $request->consultantName,
            'employeeId'=> $request->caseWorkerId,
              'employeeName'=> $request->caseWorker,
            'status'=> $status,
            'technology'=>implode(',',$technologys),
              'technologyId'=>implode(',',$technologyIds),
              'amount'=> $request->amount,
            'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
            'assigned_to' => $request->assigned_to,
        ]);
        if ($update) {
            return Redirect()->route('caseworker.index')->with('success','Batch Details updated successfully');
        }
        return Redirect()->route('caseworker.index')->with('failed','please try again');
      }
      
      public function batchComments($batch_id)
      {
        $batch_comments = BatchComments::where('batch_id', $batch_id)->get(); 
        if ($batch_comments) {
            foreach ($batch_comments as $batch_comment) {
                $k = [];
                if ($batch_comment->participants != '') {
                   $participants = explode(',', $batch_comment->participants); 
                   foreach ($participants as $participant) {
                       $_p = Contact::where('id', $participant)->first();
                       $k[] = ($_p->firstname) ? $_p->firstname : '';
                   }
                   $batch_comment->participants = implode(',', $k);
                }
            }
        }
        $contacts = Contact::get();
        return view('batch_comments', compact('batch_comments', 'contacts', 'batch_id'));  
      }
      
      public function saveBatchComments(Request $request) 
      {
        if (isset($request->attachment)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($request->attachment->getClientOriginalExtension());
            $file_name = $name_gen . '.' . $file_ext;
            $up_location = 'uploads/batch_comments/';
            $attachment = $up_location.$file_name;
            $request->attachment->move($up_location,$file_name);
        }
        BatchComments::insert([
         'batch_id' => $request->id,
         'sme_id' => $request->sme_id,
         'attachment' => $attachment,
         'date' => $request->date,
         'start_time' => $request->start_time,
         'end_time' => $request->end_time,
         'description' => $request->description,
         'participants' => implode(",", $request->participants),
         'created_at' => Carbon::now()
        ]);
        return Redirect()->back()->with('success','Comment Added!');
    }
    
    public function downloadAttachment($comment_id) 
    {
        $batch_comments = BatchComments::find($comment_id); 
        //PDF file is stored under project/public/download/info.pdf
        $file= $batch_comments->attachment;
    
        // $headers = array(
        //           'Content-Type: application/pdf',
        //         );
    
        return Response::download($file); //, 'filename.pdf', $headers
    }

}