<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\ContactUs;

class LegaltermsController extends Controller
{
    public function index()
    {
         return view('privacyterms');
    }
     public function test()
    {
         return view('privacyterms');
    }
    
    public function opensourcelicence()
    {
         return view('opensourcelicence');
    }
    
    public function cookiespreference()
    {
         return view('cookiespreference');
    }
    
    public function termsofuse()
    {
         return view('termsofuse');
    }
    
    public function webopensourcelicence()
    {
         return view('webopensourcelicence');
    }
    
    public function webcookiespreference()
    {
         return view('webcookiespreference');
    }
    
    public function webtermsofuse()
    {
         return view('webtermsofuse');
    }
    
    public function webprivacy()
    {
         return view('webprivacy');
    }
    
    public function helptab()
    {
         return view('helptab');
    }
    
    public function contactus(Request $request)
    {
        if ($request->ajax()) {
            if (Auth::guard('adminiy')->user()->role != "studio") {
                $data = ContactUs::all();
            }
            return Datatables::of($data)->addIndexColumn()
            // <button class="btn p-0 ms-2" id="movie_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                             ->addColumn('action', function($row){
                    $commonArray = ['type' => 'movie','id' => $row->id];
                    $btn = '  <a href="'. url('movie/'.$row->id.'/edit').'"  rel="noopener noreferrer">
                    <button class="btn p-0 ms-2" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button> </a>

                    <a href="'.route('movie.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('movie.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>

                    <a href="'.route('movie.trailer',$commonArray).'" >
                    <button class="btn p-0 ms-2 " type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit Trailer">Edit  Movie</button>
                    </a>

                    ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
            
        }
         return view('contactus');
    }
    
    public function contactcreate()
    {
         return view('contactcreate');
    }
    
     public function contactstore(Request $request)
    {
         $contact = ContactUs::create([
              'name'=>$request->edit_first_name,
              'email'=>$request->edit_email,
              'phone'=>$request->contact,
              'address'=>$request->address,
               'created_at'=>Carbon::now(),
        ]);
        if(Auth::guard('adminiy')->user()->role != "studio"){
         return Redirect()->route('contactus')->with('success','COntact created successfully');
        }
        else
        {
            return Redirect()->route('contactcreate')->with('success','COntact created successfully');   
        }

    }
}
