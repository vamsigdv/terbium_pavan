<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use App\Models\Adminiy;
use App\Lib\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Skills;
use DataTables;
use Session;
use App\Models\Technology;
use App\Models\Contact;
use App\Models\paymenttype;
use App\Models\caseworker;
use App\Models\transactiontechnology;
use App\Models\installments;
use App\Models\worktype;
use App\Models\transactions;
use App\Models\interviews;
use App\Models\interviewtools;
use App\Models\statustype;
use App\Models\User;
use App\Models\smeagreepayments;
use App\Models\comments;
class CommentsController extends Controller
{
    
    
    public function index(Request $request)
    {
        $newdata = [];
        $statustype = statustype::where('status',1)->get();
        if ($request->ajax()) {
           
            return Datatables::of($newdata)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="transactions_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                     <a href="' . url('details', $row->id) . '" >
                    <button class="btn p-0 ms-2 " type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="View details"><i class="fas fa-eye" style="color:orange"></i></button>
                    </a>
                    <a href="'.route('transactions.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('transactions.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('comments',compact('statustype'));
    }
    
    
    public function store(Request $request)
    {   
        $id =$request->id;
        
        $interview = interviews::where('id',$id)->first();
        if($request->statusType == 6)
        {
            $interview->Status = $request->statusType;
        } 
        elseif($request->statusType == 3)
        {
            $interview->Status = $request->statusType;
            $interview->interviewdate = $request->interviewdate;
        }
       // $request->statusType = $request->statusTyper;
        $interview->Noofrounds = $request->Noofrounds;
        $type = statustype::where('id',$request->statusType)->first();
       
        $request->statusType = $type->name;
        $request->statusTypeId = $type->id;   
       // print_r($request->statusType);exit;
        $interview->update();
        $id =  comments::create([
                'interviewId'=>$id,
                'comments'=>$request->comments,
                'roundName'=>$request->roundName,
                'round'=>$request->round,
                'noofrounds'=>$request->Noofrounds,
                'interviewdate'=>$interview->interviewdate,
                'contactId'=>$interview->contactId,
                'consultantName'=>$interview->consultantName,
                 'smeId'=>$interview->smeid,
                'smeName'=>$interview->SMEName,
                 'technologyId'=>$interview->technologyId,
                'technologyName'=>$interview->technology,
                'statusTypeId'=>$request->statusTypeId,
                'statusType'=>$request->statusType,
                'created_at'=>Carbon::now(),
                'createdBy'=> Auth::guard('adminiy')->user()->id,
                'updated_at'=>Carbon::now(),
                'updatedBy'=> Auth::guard('adminiy')->user()->id,
          ])->id;
         if ($request->statusTypeId == 3 || $request->statusTypeId == 6) {
            return Redirect()->route('interviews',$interview->transactionId)->with('success','Interviews updated successfully');
        }else{
        return Redirect()->back()->with('success','Comment Added successfully');
        }
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Skills::find($id)->update([
            'status'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'Skills deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Skills::find($id);
        $view=1;
         $technology = Technology::where('status',1)->get();
        $html = view('modal.Skills_edit',compact('data','view','technology'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = interviews::find($id);
        $getTransactions =Transactions::where('id', $data->transactionId)
              ->first();
        $technology = Technology::where('status',1)->get();
        $contact = Contact::where('status',1)->get();
        $sme = Contact::where('status',1)->where('contactTypeId',1)->get();
        $worktype = worktype::where('status',1)->get();
        $statustype = statustype::where('status',1)->get();
        $interviewtools = interviewtools::where('status',1)->get();
        $caseworker = User::where('status',1)->get();
        $view=0;
        $html = view('modal.interviews_edit',compact('data','view','technology','getTransactions','contact','worktype','caseworker','sme','statustype','interviewtools'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        $data = interviews::find($id);
        
        $technology = Technology::where('id',$request->technology)->first();
        $request->technology = $technology->name;
        $request->technologyId = $technology->id;
        
       $caseworker = caseworker::where('id',$request->caseworker)->first();
        $request->caseWorker = $caseworker->name;
        $request->caseWorkerId = $caseworker->id;
        
        
        $contact = Contact::where('id',$request->contactName)->first();
        $request->consultantName = $contact->firstname;
        $request->contactId = $contact->id;
        
        
        
        $sme = Contact::where('id',$request->SMEName)->first();
        $request->SMEName = $sme->firstname;
        $request->smeid = $sme->id;
        
        
        $update = interviews::find($id)->update([
            'transactionId'=>$data->transactionId,
                'Noofrounds'=>$request->Noofrounds,
                'contactId'=>$request->contactId,
                'consultantName'=>$request->consultantName,
                'modeofinterview'=>$request->modeofinterview,
                'technologyId'=>$request->technologyId,
                'technology'=>$request->technology,
                'SMEAgreeAmount'=>$request->SMEAgreeAmount,
                'Status'=>$request->Status,
                'comment'=>$request->comment    ,
                'smeid'=>$request->smeid,
                'SMEName'=>$request->SMEName,
                'paymentStatus'=>$request->paymentStatus,
                'caseWorkerId'=>$request->caseWorkerId,
                'caseWorker'=>$request->caseWorker,
                'updated_at'=>Carbon::now(),
            'updatedBy'=> Auth::guard('adminiy')->user()->id,
        ]);
        if ($update) {
            return Redirect()->route('interviews',$data->transactionId)->with('success','Interviews updated successfully');
        }
        return Redirect()->route('interviews',$data->transactionId)->with('failed','please try again');
      }

}