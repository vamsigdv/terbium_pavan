<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Adminiy;
use App\Models\Admin_session;
use Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;

use Illuminate\Support\Str;


class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Employee::select('id','first_name','last_name','contact','email','status','password')->where('delete',1)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="employee_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="employee_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('employee.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('employee.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('employee');
    }
    public function store(Request $request)
    {   
         $logmessage = Auth::guard('adminiy')->user()->name.' '.Auth::guard('adminiy')->user()->role.' has aproved the movie '.$request->name;
      
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $employee_id=Employee::create([
              'first_name'=>$request->first_name,
              'middle_name'=>$request->middle_name,
              'last_name'=>$request->last_name,
              'contact'=>$request->contact,
              'email'=>$request->email,
              'adhar'=>$request->adhar,
              'account_number'=>$request->account_number,
              'ifsc'=>$request->ifsc,
              'password'=>$request->password,
              'voter'=>$request->voter,
              'status'=> $status,
              'created_at'=>Carbon::now(),
          ])->id;
          if ($employee_id) {
            $admin_id=Adminiy::create([
                'name'=>$request->first_name,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                 'showPassword1'=> $request->password,
                'role'=> 'employee',
                'is_active'=> $status,
                'user_id'=> $employee_id,
                'created_at'=>Carbon::now(),
            ])->id;
            $update = Employee::find($employee_id)->update([
                'admin_id'=> $admin_id,
                'updated_at'=>Carbon::now(),
            ]);
          }
        return Redirect()->back()->with('success','employee created successfully');

        }
    public function empregister()
    {
        return view('empregister');
    }
    public function empregisterstore(Request $request)
    {   
         $validated = $request->validate([
            'username' => 'required',
            'first_name' => 'required',
            'email' => 'required|unique:adminiy',
            'contact' => 'required|unique:studio|numeric',
            'last_name' => 'required',
           // 'account_number' => 'required',
        //    'ifsc' => 'required',
            'photo' => 'required|max:2000',
            'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
        ],[
             'password.regex' => 'Password Min 8 characters with special characters (@ # ! ?etc.) and alphanumeric characters with one uppercase letter!',
             'contact.max'=>'Phone number "Min 10 characters"'
          ]
        );
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
         $file=$request->file('photo');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/studio/';
            $img_path = $up_location.$file_name;
            $file->move($up_location,$file_name);
        } else {
            $img_path='';
        }
        $employee_id=Employee::create([
              'first_name'=>$request->first_name,
              'middle_name'=>$request->middle_name,
              'last_name'=>$request->last_name,
              'contact'=>$request->contact,
              'email'=>$request->email,
              'adhar'=>$request->adhar,
              'account_number'=>$request->account_number,
              'ifsc'=>$request->ifsc,
              'password'=>$request->password,
              'voter'=>$request->voter,
              'status'=> $status,
              'photo'=>$img_path,
              'dob'=>$request->dob,
              'created_at'=>Carbon::now(),
          ])->id;
          if ($employee_id) {
            $admin_id=Adminiy::create([
                'name'=>$request->username,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                 'showPassword1'=> $request->password,
                'role'=> 'employee',
                'is_active'=> $status,
                'user_id'=> $employee_id,
                'created_at'=>Carbon::now(),
            ])->id;
            $update = Employee::find($employee_id)->update([
                'admin_id'=> $admin_id,
                'updated_at'=>Carbon::now(),
            ]);
          }
       if (Str::contains($request->email, '@')) {
            $requestType = "email";
        } else {
            $requestType = "contact";
        }
        
        $remember = 1;
         if(Auth::guard('adminiy')->attempt([$requestType => $request->email, 'password'=> $request->password], $remember)){
            
            $Admin_session = Admin_session::where("check_login" , 1)->where("check_logout" , 0)->where("user_id" , $admin_id)->orderBy('created_at', 'desc')->first();
         
            if($Admin_session){
              
            }elseif (Auth::guard('adminiy')) {
               $Admin_session = Admin_session::create([
                "user_id" => $admin_id,
                "check_login" => 1,
                "check_logout" => 0,
                ]);
            }
            return redirect()->route('adminiy.panel');
        }

        }
    public function destroy($id)
    {
        $employee = Employee::find($id);
        if (Auth::guard('adminiy')->user()->role != "Admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Employee::find($id)->update([
            'delete'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
            $admin_id=Adminiy::find($employee->admin_id)->delete();
           Session::flash('success', 'employee deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $employee = Employee::find($id);
        return response()->json([
            'data' => $employee
          ]);
    }
    public function edit($id){
        $employee = Employee::find($id);
        return response()->json([
            'data' => $employee
          ]);
    }
    public function update(Request $request){
        $id =$request->edit_id;
        $admin_id =$request->admin_id;
        if($request->edit_status){
            $status=1;
        }else{
            $status=0;
        }
        $update = Employee::find($id)->update([
            'first_name'=>$request->edit_first_name,
            'middle_name'=>$request->edit_middle_name,
            'last_name'=>$request->edit_last_name,
            'contact'=>$request->edit_contact,
            'email'=>$request->edit_email,
            'adhar'=>$request->edit_adhar,
            'voter'=>$request->edit_voter,
            'account_number'=>$request->edit_account_number,
            'ifsc'=>$request->edit_ifsc,
            'password'=>$request->edit_password,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
        ]);
        if ($update) {
            $admin_id=Adminiy::find($admin_id)->update([
                'name'=>$request->edit_first_name,
                'email'=>$request->edit_email,
                'is_active'=>$status,
                'password'=> Hash::make($request->edit_password),
                 'showPassword1'=> $request->edit_password,
                'updated_at'=>Carbon::now(),
            ]);
            //  if(str_replace("http://prabhu.yoursoftwaredemo.com/","",redirect()->back()->getTargetUrl()) != 'profileupdate')
             if(str_replace(env('APP_URL'),"",redirect()->back()->getTargetUrl()) != 'profileupdate')
            {
            return Redirect()->route('employee.index')->with('success','employee updated successfully');
            }
            else{
                return Redirect()->back()->with('success','Profile updated successfully');
            }
        }
        return Redirect()->route('employee.index')->with('failed','please try again');
      }
}
