<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use DataTables;
use Session;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Album;
class AlbumController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Album::select('id','title','status')->where('delete',1)->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = ' <button class="btn p-0 ms-2" id="album_edit" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="fas fa-edit" style="color:darkgreen"></i></button>
                    <button class="btn p-0 ms-2" id="album_view" type="button" data-id="'.$row->id.'"  data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="fas fa-eye" style="color:aqua"></i></button>
                    <a href="'.route('album.destroy',$row->id).'" id="'.$row->id.'" data-method="DELETE" data-url="'.route('album.index').'" class="button delete-confirm">
                    <button class="btn p-0 ms-2 delete" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fas fa-trash" style="color:red"></i></button>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('albums');
    }
    public function store(Request $request)
    {   
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('thumbnail_image');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/albums/';
            $thumbnail_image = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $thumbnail_image='';
        }
        $file=$request->file('cover_image');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/albums/';
            $cover_image = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $cover_image='';
        }
        Album::insert([
              'title'=>$request->title,
              'thumbnail_image'=>$thumbnail_image,
              'cover_image'=>$cover_image,
              'description'=>$request->description,
              'status'=> $status,
              'created_at'=>Carbon::now(),
          ]);
        return Redirect()->back()->with('success','album created successfully');
    }
    public function destroy($id)
    {
        if (Auth::guard('adminiy')->user()->role != "admin") {
            Session::flash('failed', "You don't have access to delete");
            return false;
        }
        $delete = Album::find($id)->update([
            'delete'=>0,
            'updated_at'=>Carbon::now(),
        ]);
        if ($delete) {
           Session::flash('success', 'album deleted successfully');
           return true;
        }
        Session::flash('failed', 'please try again');
        return false;
    }
    public function show($id){
        $data = Album::find($id);
        $view=1;
        $html = view('modal.album_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function edit($id){
        $data = Album::find($id);
        $view=0;
        $html = view('modal.album_edit',compact('data','view'))->render();
        return json_encode($html);
    }
    public function update(Request $request){
        $id =$request->id;
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $file=$request->file('thumbnail_image');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/albums/';
            $thumbnail_image = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $thumbnail_image=$request->thumbnail_image_old;
        }
        $file=$request->file('cover_image');
        if (isset($file)) {
            $name_gen = hexdec(uniqid());
            $file_ext = strtolower($file->getClientOriginalExtension());
            $file_name = $name_gen.'.'.$file_ext;
            $up_location = 'uploads/albums/';
            $cover_image = $up_location.$file_name;
            $file->move($up_location,$file_name);
        }else{
            $cover_image=$request->cover_image_old;
        }
        $update = Album::find($id)->update([
            'title'=>$request->title,
            'thumbnail_image'=>$thumbnail_image,
            'cover_image'=>$cover_image,
            'description'=>$request->description,
            'status'=> $status,
            'updated_at'=>Carbon::now(),
        ]);
        if ($update) {
            return Redirect()->route('album.index')->with('success','album updated successfully');
        }
        return Redirect()->route('album.index')->with('failed','please try again');
      }
}
