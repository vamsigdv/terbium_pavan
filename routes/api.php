<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\REST\UserController;
use App\Http\Controllers\REST\RegisterController;
use App\Http\Controllers\REST\LoginController;
use App\Http\Controllers\REST\LiveStreamController;
use App\Http\Controllers\REST\QuizController;
use App\Http\Controllers\REST\ContentController;
use App\Http\Controllers\REST\MovieController;
use App\Http\Controllers\REST\HomeController;

use App\Http\Controllers\REST\NotificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
try{
    
} catch (\Exeption $e) {
    return $e->getMessage();
}
