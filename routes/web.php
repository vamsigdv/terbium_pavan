<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SkillsController;
use App\Http\Controllers\ContactTypeController;
use App\Http\Controllers\TechnologyController;
use App\Http\Controllers\PaymenttypeController;
use App\Http\Controllers\ConsultanttypeController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\StatustypeController;
use App\Http\Controllers\WorktypeController;
use App\Http\Controllers\InterviewtoolsController;
use App\Http\Controllers\CaseworkerController;
use App\Http\Controllers\AccountTypeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\TransactionsController;
use App\Http\Controllers\SMEController;
use App\Http\Controllers\InterviewsController;
use App\Http\Controllers\InstallmentsController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\SMECommentsController;
use App\Http\Controllers\PaymentstatusController;
use App\Http\Controllers\H1BController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\CapController;
use App\Http\Controllers\DailyWorkTrackerController;

use App\Models\logs;

Route::get('test-email', [LoginController::class, 'testEmail']);
Route::get('/login',[LoginController::class, 'show'])->name('login')->middleware('guest');
Route::post('/authenticate',[LoginController::class, 'authenticate'])->name('adminiy.authenticate')->middleware('throttle:4,1');
Route::post('/login',[LoginController::class, 'login'])->name('adminiy.login')->middleware('throttle:4,1');

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/loginwithotp',[LoginController::class, 'loginwithotp'])->name('loginwithotp');
Route::get('/otpvalidate',[LoginController::class, 'OTPValidate'])->name('otpvalidate');
Route::post('/validateotp',[LoginController::class, 'validateotp'])->name('validateotp');
Route::post('/sendotp',[LoginController::class, 'sendotp']);
 Route::get('/resendotp',[LoginController::class, 'resendotp']);

Route::post('/authenticate_email_with_otp',[LoginController::class, 'authenticateEmailWithOtp'])->name('adminiy.authenticate_email_with_otp');
// Route::get('/login_email_with_otp',[LoginController::class, 'loginEmailWithotp'])->name('loginemailwithotp');
Route::post('/send_email_otp',[LoginController::class, 'sendEmailotp']);
Route::get('/email_otp_validate',[LoginController::class, 'EmailOTPValidate'])->name('emailotpvalidate');
Route::post('/email_validate_otp',[LoginController::class, 'EmailValidateotp'])->name('emailvalidateotp');
Route::get('/resend_email_otp',[LoginController::class, 'resendEmailotp']);

Route::get('/h1b_form',[ContentController::class, 'H1BAppicationForm'])->name('H1BAppicationForm');

Route::post('h1b_save', [ContentController::class, 'H1BSave'])->name('H1BSave');

 //Route::group(['middleware' => ['adminiy', 'permission']], function() {
  Route::middleware('adminiy')->group(function () {
  Route::get('/', function () {return view('index');})->name('adminiy.panel');
  Route::resource('users', UserController::class);
  Route::get('profileupdate', [UserController::class, 'ProfileUpdate'])->name('ProfileUpdate');
  Route::PATCH('adminupdate', [UserController::class, 'adminupdate'])->name('adminupdate');
  Route::get('generalsettings', [UserController::class, 'generalsettings'])->name('generalsettings');
  Route::get('deactivateaccount', [UserController::class, 'deactivateaccount'])->name('deactivateaccount');
  Route::post('logoutalldevices', [UserController::class, 'logoutalldevices'])->name('logoutalldevices');
  Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
  Route::resource('transactions', TransactionsController::class);
  Route::get('details/{id}', [TransactionsController::class, 'details'])->name('details');
  Route::get('installments/{id}', [TransactionsController::class, 'transactiondetails'])->name('transactiondetails');
  Route::get('smes/{id}', [TransactionsController::class, 'smes'])->name('smes');
  Route::get('interviews/{id}', [TransactionsController::class, 'interviews'])->name('interviews');
 
  Route::get('invoice/{id}/{type}', [TransactionsController::class, 'invoice'])->name('invoice');
  Route::resource('comment', CommentsController::class);
  Route::resource('smecomments', SMECommentsController::class);
  Route::resource('contact', ContactController::class);
  Route::get('get_contacts_by_type/{type}', [ContactController::class, 'getContactsByType'])->name('getContactsByType');
  Route::get('smelist', [ContactController::class, 'smelist'])->name('smelist');
  Route::get('contactlist', [ContactController::class, 'contactlist'])->name('contactlist');
  Route::get('notifications', [ContactController::class, 'notifications'])->name('notifications');
  Route::resource('skills', SkillsController::class);
  Route::resource('cap', CapController::class);
  Route::resource('contacttype', ContactTypeController::class);
  Route::resource('sme', SMEController::class);
  Route::post('/search_sme_list', [SMEController::class, 'searchSMEList'])->name('searchSMEList');
   Route::resource('daily_work_track', DailyWorkTrackerController::class);
   Route::group(['prefix'=>'daily_work_track','as'=>'daily_work_track'], function(){
        Route::get('/download_report/{id}', [DailyWorkTrackerController::class, 'download_report'])->name('download_report');
    });
  Route::get('smedetails/{id}', [SMEController::class,'smedetails'])->name('smedetails');
  Route::get('smedetailsview/{id}', [SMEController::class,'smedetailsview'])->name('smedetailsview');
  Route::get('smepayments', [SMEController::class,'smepaymentsdata'])->name('smepaymentsdata');
  Route::post('search_smpayments_list', [SMEController::class,'searchSmpaymentsList'])->name('searchSmpaymentsList');
  Route::resource('interviews', InterviewsController::class);
  Route::post('/search_interview_list', [InterviewsController::class, 'searchInterviewList'])->name('searchInterviewList');
  Route::post('fetchlocation', [LocationController::class, 'fetchlocation'])->name('fetchlocation');
  Route::resource('technology',TechnologyController::class);
  Route::resource('payment_status',PaymentstatusController::class);
  Route::resource('paymenttype',PaymenttypeController::class);
  Route::resource('consultanttype',ConsultanttypeController::class);
  Route::resource('location',LocationController::class);
  Route::resource('states',StatesController::class);
  Route::resource('statustype',StatustypeController::class);
  Route::resource('worktype',WorktypeController::class);
  Route::resource('accounttype',AccountTypeController::class);
  Route::resource('caseworker',CaseworkerController::class);
  Route::resource('interviewtools',InterviewtoolsController::class);
  Route::resource('installment', InstallmentsController::class);
  Route::get('installment/{id}/edit/{type}', [InstallmentsController::class,'edit'])->name('edit');
  Route::get('comments/{id}', [InterviewsController::class, 'comments'])->name('comments');
  Route::get('smecomment/{id}', [SMEController::class, 'smecomments'])->name('smecomments');
  Route::get('dashboard', [UserController::class, 'dashboard'])->name('dashboard');
  Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
  Route::resource('h1b', H1BController::class);
  Route::post('/search_h1b_list', [H1BController::class, 'searchH1B'])->name('searchH1B');
  Route::get('h1bcommunications/{id}',[H1BController::class, 'communications'])->name('communications');
  Route::get('h1bcommunications/{id}/h1bcommentsedit',[H1BController::class, 'h1bcommentsedit'])->name('h1bcommentsedit');
  Route::PATCH('h1bcommunicationsupdate/{id}',[H1BController::class, 'h1bcommunicationsupdate'])->name('h1bcommunicationsupdate');
  Route::post('addh1bdocuments/{id}',[H1BController::class, 'addh1bdocuments'])->name('addh1bdocuments');
  Route::post('addh1bcomments/{id}',[H1BController::class, 'addh1bcomments'])->name('addh1bcomments');
  Route::get('documents/{id}',[H1BController::class, 'documents'])->name('documents');
  Route::get('communications/{id}',[H1BController::class, 'h1bcommunications'])->name('h1bcommunications');
  Route::delete('documentsdestroy/{id}',[H1BController::class, 'documentsdestroy'])->name('documentsdestroy');
  Route::delete('commentsdestroy/{id}',[H1BController::class, 'commentsdestroy'])->name('commentsdestroy');
    #Route::get('userprieviliazes', [UserController::class, 'userprieviliazes'])->name('userprieviliazes');
    Route::get('batchinfo', [UserController::class, 'batchinfo'])->name('batchinfo');
    #Route::get('userprieviliazes', [RolesController::class, 'getRoles'])->name('userprieviliazes');
    Route::resource('roles', RolesController::class);
    
     Route::get('/adminnotifications', function () {

         $countlogs = logs::orderBy('id', 'DESC')->where('contactID',$_GET['id'])->get();
         $logs = logs::orderBy('id', 'DESC')->where('contactID',$_GET['id'])->skip(($_GET['page'] - 1) * $_GET['limit'])
                            ->take($_GET['limit'])->get();
         $count = count($countlogs);
         $total_pages = ceil($count / $_GET['limit']);
         return view('admin_notifications',compact('logs','count','total_pages'));
     });
    /* Batch comments in case worker */
    Route::group(['prefix'=>'case_worker','as'=>'case_worker'], function() {
        Route::group(['prefix'=>'batch_comments','as'=>'batch_comments'], function() {
            Route::get('{batch_id}', [CaseWorkerController::class, 'batchComments'])->name('batchComments');
            Route::post('save', [CaseWorkerController::class, 'saveBatchComments'])->name('saveBatchComments');
            Route::get('download_attachment/{comment_id}', [CaseWorkerController::class, 'downloadAttachment'])->name('downloadAttachment'); 
        });
    });
    
   
 
});
Route::get('/route-cache', function() {
    Artisan::call('route:cache');
    return 'Routes cache has been cleared';
});

?>