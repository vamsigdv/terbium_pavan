--
-- Table structure for table `contnue_watch`
--

CREATE TABLE `continue_watch` (
  `id` int NOT NULL,
  `movie_id` int NOT NULL,
  `user_id` int NOT NULL,
  `run_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int NOT NULL,
  `slider_image` varchar(255) DEFAULT NULL,
  `movie_id` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `is_delete` tinyint NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slider_image`, `movie_id`, `title`, `type`, `status`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'uploads/slider/1752083500056778.jpg', 1, 'new Sloder', 2, 1, 0, '2022-12-13 07:37:14', '2022-12-13 07:39:48'),
(2, 'uploads/slider/1752095892991620.jpg', 1, 'new Slider New e', 2, 1, 0, '2022-12-13 07:40:06', '2022-12-13 10:54:21');

-- --------------------------------------------------------

--
-- Table structure for table `slider_types`
--

CREATE TABLE `slider_types` (
  `id` int NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `slider_types`
--

INSERT INTO `slider_types` (`id`, `name`) VALUES
(1, 'Movie'),
(2, 'Short film'),
(3, 'Scheme'),
(4, 'Influncer'),
(7, 'Story'),
(8, 'Music Album');

-- --------------------------------------------------------



--
-- Table structure for table `user_earning`
--

CREATE TABLE `user_earning` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `coins` int NOT NULL,
  `vid` int NOT NULL,
  `date` datetime NOT NULL,
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_fav`
--

CREATE TABLE `user_fav` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `vid` int NOT NULL,
  `date` date NOT NULL,
  `title` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_kyc`
--

CREATE TABLE `user_kyc` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `bank_account_number` varchar(20) NOT NULL,
  `ifsc_code` varchar(10) NOT NULL,
  `bank_statement` varchar(100) NOT NULL,
  `aadhaar_number` varchar(16) NOT NULL,
  `voter_id` varchar(25) NOT NULL,
  `pan_number` varchar(25) NOT NULL,
  `status` tinyint NOT NULL,
  `is_delete` tinyint NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contnue_watch`
--
ALTER TABLE `continue_watch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_types`
--
ALTER TABLE `slider_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_earning`
--
ALTER TABLE `user_earning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_fav`
--
ALTER TABLE `user_fav`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_kyc`
--
ALTER TABLE `user_kyc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contnue_watch`
--
ALTER TABLE `continue_watch`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider_types`
--
ALTER TABLE `slider_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_earning`
--
ALTER TABLE `user_earning`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_fav`
--
ALTER TABLE `user_fav`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_kyc`
--
ALTER TABLE `user_kyc`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

ALTER TABLE `movies` ADD `view` VARCHAR(20) NOT NULL DEFAULT 0 AFTER `delete`, ADD `fav` VARCHAR(20) NOT NULL DEFAULT 0 AFTER `view`, ADD `download` VARCHAR(20) NOT NULL DEFAULT 0 AFTER `fav`;