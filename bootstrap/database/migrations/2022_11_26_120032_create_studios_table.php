<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studio', function (Blueprint $table) {
            $table->id();
            $table->string('studio_name');
            $table->string('contact');
            $table->string('email');
            $table->text('address')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();
            $table->string('district')->nullable();
            $table->string('village')->nullable();
            $table->string('vouchar')->nullable();
            $table->string('voter')->nullable();
            $table->text('password')->nullable();
            $table->text('account_number')->nullable();
            $table->text('ifsc')->nullable();
            $table->text('photo')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->tinyInteger('block')->default('1');
            $table->tinyInteger('delete')->default('1');
            $table->integer('admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studio');
    }
}
