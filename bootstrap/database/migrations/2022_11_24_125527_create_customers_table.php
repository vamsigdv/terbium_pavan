<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('name');
            $table->string('epic')->nullable();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->string('photo')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->tinyInteger('delete')->default('1');
            $table->tinyInteger('block')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
