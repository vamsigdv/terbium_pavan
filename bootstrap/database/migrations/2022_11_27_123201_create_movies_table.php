<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->text('actors')->nullable();
            $table->text('directors')->nullable();
            $table->text('writters')->nullable();
            $table->text('studio')->nullable();
            $table->date('release_date')->nullable();
            $table->date('expiry_date')->nullable();
            $table->string('gener')->nullable();
            $table->date('run_time')->nullable();
            $table->string('trailer')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('poster')->nullable();
            $table->string('video')->nullable();
            $table->string('language')->nullable();
            $table->string('type')->nullable();
            $table->tinyInteger('publish')->default('1');
            $table->tinyInteger('recommended')->default('1');
            $table->tinyInteger('hot')->default('1');
            $table->tinyInteger('pickup')->default('1');
            $table->integer('amount_required')->nullable();
            $table->integer('amount_given')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1=amount_required , 2=amount_given');;
            $table->tinyInteger('delete')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
