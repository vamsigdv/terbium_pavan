<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('movie_id');
            $table->string('amount_required')->nullable();
            $table->text('amount_given')->nullable();
            $table->string('studio_id')->nullable();
            $table->string('financial_id')->nullable();
            $table->string('financial_name')->nullable();
            $table->string('content_approval_id')->nullable();
            $table->text('transaction_details')->nullable();
            $table->tinyInteger('payment_status')->default('1')->comment('1=paid , 2=unpaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
