<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMp3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mp3', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->string('duration')->nullable();
            $table->string('album')->nullable();
            $table->string('genre')->nullable();
            $table->string('artist')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('file')->nullable();
            $table->text('lyrics')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1=amount requested , 2=Amount Given');;
            $table->tinyInteger('delete')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mp3');
    }
}
