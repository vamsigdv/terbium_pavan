<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();
            $table->string('contact');
            $table->string('email');
            $table->string('adhar')->nullable();
            $table->string('voter')->nullable();
            $table->text('password')->nullable();
            $table->text('account_number')->nullable();
            $table->text('ifsc')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->tinyInteger('delete')->default('1');
            $table->integer('admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
