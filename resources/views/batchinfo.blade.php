@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
 
<style>
        table {
            border-collapse: collapse;
            width: 60%;
            margin: 20px;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        td:first-child {
            /* Style for the first column */
            font-weight: bold;
        }
    </style>
<body>
<?php 

$tech = DB::table('technology')->where('status', 1)->get();


?>
    <table>
        <tr>
            <td rowspan="2">Technology</td>
            <td colspan="3">Female</td>
            <td colspan="3">Male</td>
        </tr>
        <tr>
            <td>No.of SME</td>
            <td>Max Price</td>
            <td>Min Price</td>
            <td>No.of SME</td>
            <td>Max Price($)</td>
            <td>Min Price($)</td>
        </tr>
        <?php  if($tech != []){
        foreach($tech as $key=>$value){
            $smefemale = DB::table('transactiontechnology')->where('transactiontechnology.technologyId',$value->id)->leftJoin('contact', 'contact.id', '=', 'transactiontechnology.smeid')->where('contact.contactTypeId',1)->where('contact.gender','Female')->count();
            $smemale = DB::table('transactiontechnology')->where('transactiontechnology.technologyId',$value->id)->leftJoin('contact', 'contact.id', '=', 'transactiontechnology.smeid')->where('contact.contactTypeId',1)->where('contact.gender','Male')->count();
            
        ?>
        <tr>
            <td>{{$value->name}}</td>
            <td>{{$smefemale}}</td>
            <td>500</td>
            <td>300</td>
            <td>{{$smemale}}</td>
            <td>300</td>
            <td>300</td>
        </tr>
         <?php } }?>
        <!-- Add more rows for other technologies as needed -->
    </table>

</body>
@endsection