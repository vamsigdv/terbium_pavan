@extends('dashboard_layouts.adminmaster')
 @section('content') 
 
 <style>
     .topimgwrap .d-block{ height: 120px; background: #f8f8f8;}
     .topimgwrap .d-block img{ height: 120px; object-fit:contain; width:100%}
 </style>
<div>&nbsp;</div>
  <div class="card mb-3">
            <div class="card-body">
              <div class="row flex-between-center">
                <div class="col-sm-auto mb-2 mb-sm-0">
                  <h6 class="mb-0">Showing 1-{{$_GET['limit']}} of {{$count}} entries</h6>
                </div>
                <div class="col-sm-auto">
                  <!--<div class="row gx-2 align-items-center">
                    <div class="col-auto">
                      <form class="row gx-2">
                        <div class="col-auto"><small>Sort by:</small></div>
                        <div class="col-auto">
                          <select class="form-select form-select-sm" aria-label="Bulk actions">
                            <option selected="">Best Match</option>
                            <option value="Refund">Newest</option>
                            <option value="Delete">Price</option>
                          </select>
                        </div>
                      </form>
                    </div>
                    <div class="col-auto pe-0"> <a class="text-600 px-1" href="../../../app/e-commerce/product/product-list.html" data-bs-toggle="tooltip" data-bs-placement="top" title="Product List"><span class="fas fa-list-ul"></span></a></div>
                  </div>
                </div>-->
              </div>
            </div>
          </div>
        </div>
  <div class="card mb-3">
            <div class="card-body">
              <div class="row">
                  <?php if(!empty($movies)){
                      $type = '';
                            foreach ($movies as $key => $value) { ?>
                <div class="mb-4 col-md-6 col-lg-4">
                  <div class="border rounded-1 h-100 d-flex flex-column justify-content-between pb-3">
                    <div class="overflow-hidden">
                        <?php 
                            if($value->type == 'Music Video')
                            {
                                $type = 'musicvideo';
                            }
                            if($value->type == 'Movies')
                            {
                                $type = 'movie';
                            }
                            if($value->type == 'Schemes')
                            {
                                $type = 'schemes';
                            }
                             if($value->type == 'Stories')
                            {
                                $type = 'stories';
                            }
                            if($value->type == 'Short film')
                            {
                                $type = 'shortfilms';
                            }
                            if($value->type == 'Influncer')
                            {
                                $type = 'influncer';
                            }
                            
                              if ($value->delete=='1'){
                                   $delete = 'Active';
                              }
                              elseif ($value->delete=='0'){
                                 $delete = 'delete';
                              }
                               elseif ($value->delete=='2'){
                                 $delete = 'pending';
                              }
                              elseif ($value->delete=='3'){
                                 $delete = 'rejected';
                              }
                              elseif ($value->delete=='4'){
                                 $delete = 'copyright';
                              }
                                 
                                $rating=DB::table('reviews')->where('MID',$value->id)->average('RATING');
                                
                      
                        ?>
                      <div class="position-relative rounded-top overflow-hidden topimgwrap"><a class="d-block"><img class="img-fluid rounded-top" src="{{($value->poster) }}" alt="" /></a>
                      </div>
                      <div class="p-3">
                        <h5 class="fs-0"><a class="text-dark" >{{($value->name) }}</a></h5>
                        <p class="fs--1 mb-3"><a class="text-500" ></a></p>
                         <?php for ($i = 0; $i < 5; ++$i) {
                                        if($i < $rating ){
                                         ?>
                                         <svg class="svg-inline--fa fa-star fa-w-18 text-warning fs--1" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                         <?php
                                       }else{
                                       echo '<i class="fa fa-star"></i>';
                                       }} ?>
                        <p class="fs--1 mb-1">Status: <strong class="text-success">{{$delete}}</strong>
                        <p class="fs--1 mb-1">Views: <strong class="text-success">{{$value->view}}</strong>
                        </p>
                      </div>
                    </div>
                    <div class="d-flex flex-between-center px-3">
                     
                      <div><a class="btn btn-sm btn-falcon-default me-2" href="/video/<?php echo $type;?>/{{$value->id}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Video">Video</a>
                      <button class="btn btn-sm btn-falcon-default" id="pendingmovie_edit" type="button" data-id="{{$value->id}}"  data-bs-toggle="tooltip" data-bs-placement="top" title="Copy Right">Copy Right</button>
               
                     </div>
                    </div>
                  </div>
                </div>
                 <?php    }} ?>
              </div>
            </div>
            <div class="card-footer bg-light d-flex justify-content-center">
              <div>
                   @if($_GET['page'] > '1') 
                <a class="btn btn-falcon-default btn-sm me-2" href="/search?page={{$_GET['page']-1}}&limit=12&data={{$_GET['data']}}"><span class="fas fa-chevron-left"></span></a>
                @endif
                 @if($total_pages != 0)
                   @for($i = 1; $i <= $total_pages; $i++)
                    @if($_GET['page'] == $i)
                    <a class="btn btn-sm btn-falcon-default text-primary me-2 active" href="/search?page={{$i}}&limit=12&data={{$_GET['data']}}">{{$i}}</a>
                    @else
                        <a class="btn btn-sm btn-falcon-default text-primary me-2" href="/search?page={{$i}}&limit=12&data={{$_GET['data']}}">{{$i}}</a>
                    @endif
                    @endfor
                 @endif
                 @if($_GET['page'] < $total_pages) 
                 <a class="btn btn-falcon-default btn-sm me-2" href="/search?page={{$_GET['page']+1}}&limit=12&data={{$_GET['data']}}"><span class="fas fa-chevron-right"></span></a>
                @endif
              </div>
            </div>
          </div>
          
    <div class="modal editmodal fade bd-modal-lg" id="pendingmovie-edit-modal" tabindex="-1" role="dialog" aria-labelledby="pendingmovie-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Copyright Request</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('updatecopyrightindex')}}" method="post" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            @csrf
            <div class="mb-3"><label class="form-label" >Movie Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="name" name="name"  required  readonly/>
              <input class="form-control" type="hidden" id="id" name="id"  required />
              <input class="form-control" type="hidden" id="studioid" name="sid"  required />
            </div>
            <div class="mb-3"><label class="form-label" >Creator Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="studio" name="studioname"    readonly/>
            </div>
            <div class="mb-3"><label class="form-label" >Video Valid Up To</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="expiry_date" name="expiry_date"    readonly/>
            </div>
         
           <!-- <div class="mb-3">
              <label class="form-label" >Amount Required </label><span style="color:red"> *</span>
              <input class="form-control" type="number" id="amount_required" name="amount_required" required readonly />
            </div>
            <div class="mb-3"><label class="form-label" >Amount Given</label><span style="color:red"> *</span>
              <input class="form-control" type="number" id="amount_given" name="amount_given" required />
            </div>-->
               <div class="mb-3"><label class="form-label" >Write Your Concern</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="comments" name="comments" required/>
            </div>
             <div class="mb-3"><label class="form-label" >Requsting For</label><span style="color:red"> *</span>
               <select name="approval" id="delete" class="form-control">
                
                      <option value="4" selected>copyright</option>
                </select>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script>
     /* $('#search').on('click',function(){
          var data = document.getElementById("input").value;
          if(data != ''){
               window.location = "search?page=1&limit=12&data="+data;
          }
          else
          {
            alert("Please Enter Search Data");    
          }
         
      });*/
  </script>
  
@endsection