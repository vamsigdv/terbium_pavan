
@include('dashboard_layouts.head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
.pwdicon{ position:absolute; right:10px; top:5px}
.pwdicon input{ position: absolute; opacity:0; }
.pwdicon .fa-eye{display:none; }
.pwdicon input:checked + label .fa-eye-slash{ display:none; }
.pwdicon input:checked + label .fa-eye{ display:inline-block; }
.mb-3{ position:relative;}
.note{ clear: both; font-size: 12px; margin-top:10px}
.note span{ font-size:11px;}
.passwordfiled{ position:relative; }
.checklabel{font-size: 12px;
    line-height: 1.3;
    font-weight: 400 !important;}
</style>

<div class="container" data-layout="container">
    <script>
      var isFluid = JSON.parse(localStorage.getItem('isFluid'));
      if (isFluid) {
        var container = document.querySelector('[data-layout]');
        container.classList.remove('container');
        container.classList.add('container-fluid');
      }
    </script>
    
        <div class="row flex-center min-vh-100 py-6">
                <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 col-xxl-4"><a class="d-flex flex-center mb-4" href="#"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="153" /></a>
                    <div class="card">
                        <div class="card-body bg-light">
                  <div class="row">
                    <div class="col-12">
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                       
                         <li class="nav-item">
                          <a class="nav-link " id="profile-tab" data-toggle="tab" href="/register" role="tab" aria-controls="tab-profile" aria-selected="false">Studio Registration</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link active" id="home-tab" data-toggle="tab" href="/empregister" role="tab" aria-controls="tab-home" aria-selected="true">Admin Registration</a>
                        </li>
                       
                      </ul>
                    </div>
                  </div>
</div>
                        <div class="card-body p-4p-sm-5">
                            <div class="row flex-between-center mb-2">
                            <div class="col-auto">
                                <h5>Admin Registration</h5>
                            </div> 
                            @if(session('success'))
                            <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
                            <!--<div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>-->
                            <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                         
                            @endif                       
                            </div>
                            <form action="{{ url('empregisterstore') }}" method="post" enctype="multipart/form-data" novalidate>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @csrf
                                <div class="mb-3 "><label class="form-label" >User Name</label><span style="color:red"> *</span>
                                <input class="form-control" type="text" id="username" name="username" required />
                                </div>
                                <div class="row gx-2">
                                <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
                                <input class="form-control" type="text" id="first_name" name="first_name" value="{{ old('first_name') }}" required />
                                </div>
                                <div class="mb-3 col-sm-4">
                                <label class="form-label" >Middle Name</label>
                                <input class="form-control" type="text" id="middle_name" name="middle_name" value="{{ old('middle_name') }}" />
                                </div>
                                <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
                                <input class="form-control" type="text" id="last_name" name="last_name" value="{{ old('last_name') }}" required />
                                </div>
                            </div>
                            <div class="row gx-2">
                            <div class="mb-3 col-sm-6"><label class="form-label" >Mobile Number</label><span style="color:red"> *</span>
                                <input class="form-control" type="number" id="contact" name="contact" maxlength="11" value="{{ old('contact') }}" required />
                            </div>
                            <div class="mb-3 col-sm-6"><label class="form-label" >Email ID</label><span style="color:red"> *</span>
                                <input class="form-control" type="email" id="email" name="email" value="{{ old('email') }}" required />
                            </div>
                            </div>
                            <div class="row gx-2">
                            <div class="mb-3 col-sm-6"><label class="form-label" >Aadhaar number</label>
                                <input class="form-control" type="text" id="Aadhaar" name="Aadhaar"  value="{{ old('vouchar') }}"/>
                            </div>
                            <div class="mb-3 col-sm-6"><label class="form-label" >Voter ID</label>
                                <input class="form-control" type="text" id="voter" name="voter"  value="{{ old('voter') }}" required/>
                            </div>
                            </div>
                            <div class="row gx-2">
                            <div class="mb-3 col-sm-6"><label class="form-label" >Account Number</label><span style="color:red"> *</span>
                                <input class="form-control" type="number" id="account_number" name="account_number"  value="{{ old('account_number') }}" required />
                            </div> 
                            <div class="mb-3 col-sm-6"><label class="form-label" >IFSC Code</label><span style="color:red"> *</span>
                                <input class="form-control" type="text" id="ifsc" name="ifsc"  value="{{ old('ifsc') }}"  required/>
                            </div>
                            </div>
                            <div class="row gx-12">
                            <div class="mb-3 col-sm-12"><label class="form-label" >DOB</label><span style="color:red"> *</span>
                                <input class="form-control" type="date" accept="image/*" id="dob" name="dob"  value="{{ old('dob') }}" />
                            </div>
                            <div class="row gx-12">
                            <div class="mb-3 col-sm-12"><label class="form-label" >Logo/Photo (size less than 2 MB)</label><span style="color:red"> *</span>
                                <input class="form-control" type="file" accept="image/*" id="photo" name="photo"  value="{{ old('photo') }}" />
                            </div>
                            <div class="mb-3 "><label class="form-label" >Password</label>
                            <div class='passwordfiled'>
                                <input class="form-control" type="password" id="password" name="password"  value="{{ old('password') }}" required/>
                                
                                  <div class="pwdicon"> <input class="form-check-input" name="spassword" type="checkbox"   id="showPass" />
                                    <label class="form-check-label mb-0" for="showPass"> <span class="fa fa-eye-slash"></span> 
                                    <span class="fa fa-eye"></span>
                                    </label></div>
                                    </div>
                                  <div class="note"><span class="form-label" style="color:red"> * (Min 8 characters with special characters (@ # ! ?etc.) and alphanumeric characters with one uppercase letter)</span></div>
                            </div>
                            </div>
                            <div class="row flex-between-center">
                                <div class="col-auto">
                              
                                
                                
                                
                               
                                
                                
                                
                                
                                 <div class="form-check mb-0">
                                    <input class="form-check-input" name="spassword" type="checkbox"   id="condition" />
                                    <label class="form-check-label mb-0 checklabel" for="basic-checkbox">I hereby declare that the details furnished above are true and correct to the best of my knowledge)</label>
                                </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <input type="hidden" value="0" name="status" />
                                <button class="btn btn-primary d-block w-100 mt-3 mb-3" type="submit" name="submit">Submit</button>
                                    <div class="mb-3 mt-3"> Already have an account?  <a href="{{ url('login') }}" class="">Login</a></div>
                                    
                                   
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
        </div>
</div>
    </main>
</body>
</html>
<script>
    $(document).ready(function(){
   $('#showPass').on('click', function(){
      var passInput=$("#password");
      if(passInput.attr('type')==='password')
        {
          passInput.attr('type','text');
      }else{
         passInput.attr('type','password');
      }
  })
})
</script>