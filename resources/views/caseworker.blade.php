@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Batch  Management</h5>
          
        </div>
        <div class="col-auto ms-auto">
            <div>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></div>
            </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered caseworker_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >ID</th>
                    <th >Name</th>
                    <th >Participants</th>
                    <th >Is Active</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Create New Batch</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('caseworker.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3"><label class="form-label" >Batch Title </label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="name" name="name"  required />
            </div>
             <div class="row gx-2">
                    <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Batch Start Date</label><span style="color:red"> *</span>
                    <input class="form-control" type="date" id="batch_start_date" name="batch_start_date"  required />
                    </div>
           
                    <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Batch End Date</label><span style="color:red"> *</span>
                    <input class="form-control" type="date" id="batch_end_date" name="batch_end_date"  required />
                    </div>
           
        </div>
        
        
        <div class="row gx-2">
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">SME Name</label><span style="color:red"> *</span>
                     <select class="form-select" aria-label="Default select example" name="consultantName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}">{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                 <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class=" form-select" id="select2insidemodal" style="width:100%" name="technology[]"  data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple" >
                         <option value="">Select Option</option>
                         <?php if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}">{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
           </div>
           <div class="row gx-2">
            <div class="mb-3 col-md-6"><label class="form-label" >Amount </label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="amount" name="amount"  required />
            </div>
            <div class="mb-3 col-md-6"><label class="form-label" >Participents </label><span style="color:red"> *</span>
              <select class="form-select" name="participants[]" id="select2participents" style="width:100%" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple">
                  <option value="0">Select participants</option>
                  <?php if ($contacts) { foreach ($contacts as $_c) { ?>
                  <option value="{{ $_c->id }}">{{ $_c->firstname }}</option> 
                  <?php }} ?>
              </select>
            </div>
            </div>
            <div class="row gx-2">
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Created By</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" required {{ (Auth::guard('adminiy')->user()->user_id != 5)? 'disabled':'' }}>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{ (Auth::guard('adminiy')->user()->user_id == $cwvalue->id)?'selected':'' }}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                    <?php  if(Auth::guard('adminiy')->user()->user_id != 5){?>
                    <input class="form-control" type="hidden"  name="caseworker" value="{{Auth::guard('adminiy')->user()->user_id}}">
                    <?php } ?>
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Assigned To</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="assigned_to" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{ (Auth::guard('adminiy')->user()->user_id == $cwvalue->id)?'selected':'' }}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                    <?php  if(Auth::guard('adminiy')->user()->user_id != 5){?>
                    <input class="form-control" type="hidden"  name="assigned_to" value="{{Auth::guard('adminiy')->user()->user_id}}">
                    <?php } ?>
                </div>
            </div>
                
            <div class="form-check"><input class="form-check-input" type="checkbox" checked name="status" />
              <label class="form-label" >Status</label>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="caseworker-edit-modal" tabindex="-1" role="dialog" aria-labelledby="album-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Batch Details Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="caseworker-view-modal" tabindex="-1" role="dialog" aria-labelledby="album-view-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Batch View </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
        
          <div id="view"></div>
        </div>
      </div>
    </div>
  </div>
  
   <script>

$(document).ready(function() {
   
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
  
  $("#select2participents").select2({
    dropdownParent: $("#album-modal")
  });
  
  
});

</script>
@endsection