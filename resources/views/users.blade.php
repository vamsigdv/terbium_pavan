@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">User Management</h5>
          
        </div>
       
        <div class="col-auto ms-auto">
            <div>
                 <a href="{{ url('users/create/') }}" class="btn btn-falcon-success btn-sm" ><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></a></div>
            
            </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered user_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >ID</th>
                    <th >Name</th>
                    <th >Email</th>
                    <th >Mobile Number</th>
                    <th >Password</th>
                    <th >Role</th>
                    <th >Reg Date</th>
                    <th >Is Active</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-modal-lg" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="customer-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">User Add </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('users.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3"><label class="form-label" >Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="name" name="name"  required />
            </div>
            <div class="mb-3">
              <label class="form-label" >Email</label>
              <input class="form-control" type="email" id="email" name="email" />
            </div>
            <div class="mb-3"><label class="form-label" >Mobile Number</label><span style="color:red"> *</span>
              <input class="form-control" type="number" id="phone" name="phone" required />
            </div>
            <div class="mb-3"><label class="form-label" >Password</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="password" name="password" required />
            </div>
            <div class="form-check"><input class="form-check-input" type="checkbox" checked name="status" />
              <label class="form-label" >Status</label>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="user-edit-modal" tabindex="-1" role="dialog" aria-labelledby="customer-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">User Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>
@endsection