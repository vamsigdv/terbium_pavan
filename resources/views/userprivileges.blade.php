@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
 
<style>
    table {
        border-collapse: collapse;
        width: 60%;
        margin: 20px;
    }

    th,
    td {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    td:first-child {
        /* Style for the first column */
        font-weight: bold;
    }
</style>
<div class="card mb-3">
    <div class="card-header">
        <div class="row flex-between-end">
            <div class="col-auto align-self-center">
                <h5 class="mb-0" data-anchor="data-anchor">User Privilleges</h5>
            </div>
        </div>
    </div>
    <div class="card-body pt-0">
        <div class="tab-content">
             <div class="tab-pane preview-tab-pane active" role="tabpanel">
                @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
                <form method="post" action="{{ route('roles.store') }}" enctype="multipart/form-data">
                    @csrf
                     <table>
                        <tr>
                            <th>SL.No</th>
                            <th>Screen</th>
                            <?php foreach($roles as $role) {?>
                                <th>{{ $role->name }}</th>
                            <?php } ?>
                        </tr>
                        <?php $i = 1; $checked = [];foreach ($permissions as $key => $permission) { 
                            ?>
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $permission->name }}</td>
                            <?php 
                                foreach ($roles as $role) { 
                                    $role_perm = DB::table('role_permission')->where('role_id', $role->id)->where('permission_id', $permission->id)->first(); ?>
                                <td><input type="checkbox" id="vehicle1" name="screen[{{ $permission->id }}][{{ $role->id }}]" value="{{ $permission->id }}" <?php echo ($role_perm) ? 'checked' : ''; ?>>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        <!-- Add more rows for other technologies as needed -->
                    </table>
                    <button type="submit" name="Submit" class="btn btn-success">Submit</button>
                </form>
             </div>
        </div>
    </div>
</div>
@endsection