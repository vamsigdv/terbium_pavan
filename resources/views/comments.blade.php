@extends('dashboard_layouts.adminmaster')
 @section('content')  
<div class="card overflow-hidden mb-3">
            <div class="card-header bg-light">
              <div class="row flex-between-center">
                     
                <div class="col-sm-auto">
                  <h5 class="mb-1 mb-md-0">Comments</h5>
                </div>
               
               <div class="col-auto ms-auto">
            <div>
                <a href="/interviews" class="btn btn-falcon-success btn-sm">Home</a>
                <a href="/interviews/{{$interview->transactionId}}" class="btn btn-falcon-success btn-sm">Back</a>
                <?php  if($interview->Status != 6 ){?>
                <button class="btn btn-falcon-success btn-sm addReshedule" data-bs-toggle="modal" data-bs-target="#reshedule-modal"  type="button" ><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1" >Reschedule</span></button>
                <button class="btn btn-falcon-success btn-sm addCleared" data-bs-toggle="modal" data-bs-target="#cleared-modal" type="button"  ><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Interview cleared</span></button>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#failed-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Interview Failed</span></button>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>
                 </div>
                <?php }?>
            </div>
               
              
            </div>
           
          </div> 
          </div>
          <div class="card-body fs--1 p-0">
               <div class="notification-body">
                   
               <p class="mb-1">Consultant Name:{{$interview->consultantName}} </p>
               <p class="mb-1">Interview date:{{$interview->interviewdate}} </p>
               <p class="mb-1">No.of Rounds:{{$interviews}} </p>
               <?php if(!empty($interview->resume)){?>
               <p class="mb-1"><a href="{{ url($interview->resume) }}" target="_blank" rel="noopener noreferrer" > Download resume<!--<img src="/assets/img/team/file.png" height="50" width="50">--> </a> </p>
               <?php } ?>
               </div>
              <?php foreach($logs as $key => $log) {
              $interview=DB::table('interviews')->where('id', $log->interviewId)->first();
              $employee = DB::table('users')->where('id', $interview->caseWorkerId)->first();
              ?>
                 
              <a class="border-bottom-0 notification rounded-0 border-x-0 border-300" href="{{$log->link}}">
                <div class="notification-avatar">
                  <div class="avatar avatar-xl me-3">
                     
                      <img class="rounded-circle" src="{{ ($employee->image) != '' ? url($employee->image) : asset('assets/img/team/3-thumb.jpg')}}" alt="" />
                   
                  </div>
                </div>
                <div class="notification-body">
                    <p class="mb-1">Employee Name:{{$interview->caseWorker}} </p>
                  <p class="mb-1">No.of Rounds:{{$interviews}} </p>
                  <p class="mb-1">Round:{{$log->round}} </p>
                  <p class="mb-1">Round Name:{{$log->roundName}} </p>
                 
                 
                  <p class="mb-1">SME Name:{{$log->smeName}} </p>
                  <p class="mb-1">Technology:{{$log->technologyName}} </p>
                  <p class="mb-1">Status:{{$log->statusType}} </p>
                  <br> 
                     <h6>Comments:</h6>
                  <p class="mb-1">{{strip_tags(str_replace("&nbsp;", "", $log->comments))}} </p>
                  <br> 
                  <h6>Commented Date:</h6>
                <p class="mb-1">{{$log->created_at}}</p>
                </div>
              </a>
              <?php } ?>
             </div>
          <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Comment Add </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('comment.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
             <div class="mb-3 col-md-6"><label class="form-label" >No Of Rounds</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="Noofrounds" name="Noofrounds" value="{{$interview->Noofrounds+1}}" readOnly />
            </div>
            
             <div class="mb-3 col-md-6"><label class="form-label" >Round No</label><span style="color:red"> *</span>
             <input class="form-control" type="hidden" id="id" name="id"   value={{$id}} required />
              <input class="form-control" type="text" id="round" name="round"  required />
            </div>
            
            </div>
            <div class="row">
             <div class="mb-3 col-md-6"><label class="form-label" >Round Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="roundName" name="roundName"  required />
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comments</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="comments" name="comments"  required />
            </div>
            </div>
            
            <div class="row">
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select" id="select2insidemodal" style="width:100%" name="technology[]"  data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple"  required>
                         <option value="">Select Option</option>
                         
                         <?php 
                         $technologies=explode(",", $interview->technologyId);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">SME</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="SMEName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($smes)){
                            foreach ($smes as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}"  {{($interview->smeid==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
          
            </div>
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Name Of Consultant</label><span style="color:red"> *</span>
              
               <select class="form-select" aria-label="Default select example" name="contactName" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($contacts)){
                            foreach ($contacts as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($interview->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                </select>
            </div>
           
             <div class="mb-3 col-md-6"><label class="form-label" >Interview Date</label><span style="color:red"> *</span>
              <input class="form-control" type="date" id="interviewdate" name="interviewdate" value={{$interview->interviewdate}}  required />
            </div>
             </div>
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Status Type</label><span style="color:red"> *</span>
                    <select class="form-select statusType" aria-label="Default select example" id="status" name="statusType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($statustype)){
                            foreach ($statustype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}">{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                
            
            <div class="form-check"><input class="form-check-input" type="checkbox" checked name="status" />
              <label class="form-label" >Status</label>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade bd-modal-lg" id="reshedule-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Reshedule Interview </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('comment.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            
            
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comments</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id"   value={{$id}} required />
              <input class="form-control" type="text" id="comments" name="comments"  required />
            </div>
          
            
             <div class="row">
            
           
             <div class="mb-3 col-md-6"><label class="form-label" >Interview Date</label><span style="color:red"> *</span>
              <input class="form-control" type="date" id="interviewdate" name="interviewdate" value={{$interview->interviewdate}}  required />
            </div>
             </div>
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Status Type</label><span style="color:red"> *</span>
                    <select class="form-select statusType" aria-label="Default select example" id="status" name="statusType" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($statustype)){
                            foreach ($statustype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{ ($pvalue->id == 3)?'selected':'' }}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                    <input class="form-control" type="hidden" id="statusType" name="statusType" value=3 />
                </div>
                
            
           
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
   <div class="modal fade bd-modal-lg" id="cleared-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Interview Cleared</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('comment.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            
            
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comments</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id"   value={{$id}} required />
              <input class="form-control" type="text" id="comments" name="comments"  required />
            </div>
          
            
            
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Status Type</label><span style="color:red"> *</span>
                    <select class="form-select statusType" aria-label="Default select example" id="status" name="statusType" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($statustype)){
                            foreach ($statustype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{ ($pvalue->id == 6)?'selected':'' }}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                    <input class="form-control" type="hidden" id="statusType" name="statusType" value=6 />
                </div>
                
            
           
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade bd-modal-lg" id="failed-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Interview Failed</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('comment.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            
            
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comments</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id"   value={{$id}} required />
              <input class="form-control" type="text" id="comments" name="comments"  required />
            </div>
          
            
            
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Status Type</label><span style="color:red"> *</span>
                    <select class="form-select statusType" aria-label="Default select example" id="status" name="statusType" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($statustype)){
                            foreach ($statustype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{ ($pvalue->id == 7)?'selected':'' }}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                    <input class="form-control" type="hidden" id="statusType" name="statusType" value=7 />
                </div>
                
            
           
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  </div>
 
 <script>

$(document).ready(function() {
   
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
  
  
});


/*function disableFields(type) {
   //   alert(type);
    if (type === 'reshdule') {
       $('#status').attr("disabled", true);
       $('#status').val(3);
       $('#statusType').val(3);
    } else if (type == 'cleared') {
        $('#status').attr("disabled", true);
        $('#status').val(6);
        $('#statusType').val(6);
    }
}*/
</script>

@endsection
