
<html><head>
   <meta http-equiv="Content-Type" content="charset=utf-8"/>
  
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" crossorigin="anonymous">
    
    </head><body>
    <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 padding">
        <div class="card">
        <div class="card-header p-4">
        <a class="pt-2 d-inline-block" href="" data-abc="true"><img style="height: 50px;width: 150px;" src="{{url('images/logo.png')}}">
        </a>
        <div class="float-right"> <h3 class="mb-0">Invoice #</h3>
        Date: {{date("d-m-Y")}} </div>
        </div>
        <div class="card-body">
        <div class="row mb-4">
        <div class="col-sm-6">
        <h5 class="mb-3">From:</h5>
        <h3 class="text-dark mb-1">Meghalayan Age Limited</h3>
        <div>Address: Lower, Hopkinson Rd, Lachumiere</div>
        <div>Shillong, Meghalaya 793001</div>
        <div>Email: care@themeghalayanage.com</div>
        <div>Phone: +919999679875</div>
        </div>
        <div class="col-sm-6 ">
        <h5 class="mb-3">To:</h5>
        <h3 class="text-dark mb-1">{{$studio->first_name}} {{$studio->last_name}}</h3>
        <div>{{$studio->address}}</div>
        <div>Email: {{$studio->email}}  </div>
        <div>Phone: {{$studio->contact}} </div>
        </div>
        </div>
        <div class="table-responsive-sm">
        <table class="table table-striped">
        <thead>
        <tr>
        <th class="center">ORDER ID</th>
        <th class="right">Thumbnail</th>
        <th>MOVIE NAME</th>
        <th>PAYMENT TYPE</th>
        <th class="center">Video TYPE</th>
        <th class="right">Total</th>
        </tr>
        </thead>
        <tbody>
            @php
            $subtotal = 0;
            @endphp
            @foreach($data as $key => $da)
             <tr>
        <td class="center">{{$key + 1}}</td>
        <td><img src="{{url($da->thumbnail)}}" alt="" width="120" height="180"></td>
          <td class="left">{{$da->name}}</td>
        <td class="right">{{$da->Payment_type}}</td>
        <td class="center">{{$da->type}}</td>
         <td class="right">{{$da->installmentamount}}</td>
        </tr>
        @php
        $subtotal = $subtotal + $da->installmentamount;
        @endphp
            @endforeach
       
        </tbody>
        </table>
        </div>
        <div class="row">
        <div class="col-lg-4 col-sm-5">
        </div>
        <div class="col-lg-4 col-sm-5 ml-auto">
        <table class="table table-clear">
        <tbody>
        <tr>
        <td class="left">
        <strong class="text-dark">Subtotal</strong> 
        </td>
        <td class="right">{{$subtotal}}</td>
        </tr>
     
        <tr>
        <td class="left">
        <strong class="text-dark">TAX ({{$user_tax->user_tax}}%)</strong>
        @php
        $tax = ($subtotal * $user_tax->user_tax)/100;
        @endphp
        </td>
        <td class="right">{{$tax}}</td>
        </tr>
        <tr>
        <td class="left">
        <strong class="text-dark">Total</strong>
         </td>
        <td class="right">
        <strong class="text-dark">{{($subtotal + $tax)}}</strong>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        </div>
        </div>
        <div class="card-footer bg-white">
          <h1 class="text-dark mb-1" style="font-size:14px;"> Payment Terms  &amp; Condations</h1>
  <div class="col-sm-12 ">
        <?php echo htmlspecialchars_decode(stripslashes($termscondition->text)) ?>
        
        
        </div>

        </div>
        </div>
        </div>
    
</body></html>
