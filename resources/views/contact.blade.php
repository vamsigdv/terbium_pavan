@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Contact Management</h5>
          
        </div>
        
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" onclick="return getContacts(event, 'all');" id="all_load" data-toggle="tab" href="javascript:void(0);" role="tab" aria-controls="tab-profile" aria-selected="false">All</a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" onclick="return getContacts(event, 'consultant');"  data-toggle="tab" href="javascript:void(0);" role="tab" aria-controls="tab-profile" aria-selected="false">Consultant</a>
            </li>
          
            <li class="nav-item">
              <a class="nav-link" onclick="return getContacts(event, 'sme');"  data-toggle="tab" href="javascript:void(0);" aria-controls="tab-profile" aria-selected="false">SME</a>
            </li>
            
             <li class="nav-item">
              <a class="nav-link" onclick="return getContacts(event, 'bench');"  data-toggle="tab" href="javascript:void(0);" aria-controls="tab-profile" aria-selected="false">Bench</a>
            </li>
        </ul>
        <div class="col-auto ms-auto">
            <div>
                <a href="{{ route('contact.create')}}" rel="noopener noreferrer"><button class="btn btn-falcon-success btn-sm"  type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></a>
            </div>
        </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      @if(session('success'))
      <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
      <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
      <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
      @if(session('failed'))
      <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
      <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
      <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
        <div id="consultant" class="tabcontent_contact">
          <table class="table table-bordered consultant_datatable table-striped fs--1 mb-0" style="width:100%">
            <thead class="bg-200 text-900">
              <tr>
                <th >#</th>
                <th >First Name</th>
                <th >Email</th>
                <th >Mobile Number</th>
                <th >Consultant Type</th>
                 <th >Contact Type</th>
                <th >Technology</th>
                <th >Work Type</th>
                <th >Skills</th>
                <th >Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
        
        <div id="sme" class="tabcontent_contact">
          <table class="table table-bordered smes_datatable table-striped fs--1 mb-0" style="width:100%">
           <thead class="bg-200 text-900">
              <tr>
                <th >#</th>
                <th >First Name</th>
                <th >Email</th>
                <th >Mobile Number</th>
                <th >Consultant Type</th>
                 <th >Contact Type</th>
                <th >Technology</th>
                <th >Work Type</th>
                <th >Skills</th>
                <th >Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
            <tbody></tbody>
          </table>
        </div>
        
        <div id="bench" class="tabcontent_contact">
          <table class="table table-bordered bench_datatable table-striped fs--1 mb-0" style="width:100%">
           <thead class="bg-200 text-900">
              <tr>
                <th >#</th>
                <th >First Name</th>
                <th >Email</th>
                <th >Mobile Number</th>
                <th >Consultant Type</th>
                 <th >Contact Type</th>
                <th >Technology</th>
                <th >Work Type</th>
                <th >Skills</th>
                <th >Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
            <tbody></tbody>
          </table>
        </div>
        
        <div id="all" class="tabcontent_contact">
          <table class="table table-bordered all_datatable table-striped fs--1 mb-0" style="width:100%;">
            <thead class="bg-200 text-900">
              <tr>
                <th >#</th>
                <th >First Name</th>
                <th >Email</th>
                <th >Mobile Number</th>
                <th >Consultant Type</th>
                 <th >Contact Type</th>
                <th >Technology</th>
                <th >Work Type</th>
                <th >Skills</th>
                <th >Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
    </div>
  </div>
   <script>
        window.onload = function () {
            getContacts($('#all_load'), 'all', true);
        };
    </script>
@endsection