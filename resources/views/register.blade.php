@include('dashboard_layouts.head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .pwdicon {
        position: absolute;
        right: 10px;
        top: 5px
    }

    .pwdicon input {
        position: absolute;
        opacity: 0;
    }

    .pwdicon .fa-eye {
        display: none;
    }

    .pwdicon input:checked+label .fa-eye-slash {
        display: none;
    }

    .pwdicon input:checked+label .fa-eye {
        display: inline-block;
    }

    .mb-3 {
        position: relative;
    }

    .note {
        clear: both;
        font-size: 12px;
        margin-top: 10px
    }

    .note span {
        font-size: 11px;
    }

    .passwordfiled {
        position: relative;
    }

    .checklabel {
        font-size: 12px;
        line-height: 1.3;
        font-weight: 400 !important;
    }
</style>
<div class="container" data-layout="container">
    <script>
        var isFluid = JSON.parse(localStorage.getItem('isFluid'));
        if (isFluid) {
            var container = document.querySelector('[data-layout]');
            container.classList.remove('container');
            container.classList.add('container-fluid');
        }
    </script>
    <div class="row flex-center min-vh-100 py-6">
        <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 col-xxl-4"> <a class="d-flex flex-center mb-4" href="#"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="153" /></a>
            <div class="card">
                <div class="card-body bg-light">
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" id="profile-tab" data-toggle="tab" href="/register" role="tab" aria-controls="tab-profile" aria-selected="false">Studio Registration</a> </li>
                                <li class="nav-item"> <a class="nav-link" id="home-tab" data-toggle="tab" href="/login" role="tab" aria-controls="tab-home" aria-selected="true">Login</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body p-4p-sm-5">
                    <div class="row flex-between-center mb-2">
                        <div class="col-auto">
                            <h5>Register</h5>
                        </div> @if(session('success')) <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
                            <!--<div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>-->
                            <p class="mb-0 flex-1">{{ session('success')}}</p> <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div> @endif
                    </div>
                    <form action="{{ url('register_studio') }}" method="post" enctype="multipart/form-data" novalidate> @if ($errors->any()) <div class="alert alert-danger">
                            <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul>
                        </div> @endif @csrf <ul id='account'> </ul>
                        <div class="mb-3 "><label class="form-label">Studio Name / Channel Name / User Name</label><span style="color:red"> *</span> <input class="form-control" type="text" id="studio_name" name="studio_name" value="{{ old('studio_name') }}" required /> </div>
                        <div class="row gx-2">
                            <div class="mb-3 col-sm-4"><label class="form-label">First Name</label><span style="color:red"> *</span> <input class="form-control" type="text" id="first_name" name="first_name" value="{{ old('first_name') }}" required /> </div>
                            <div class="mb-3 col-sm-4"> <label class="form-label">Middle Name</label> <input class="form-control" type="text" id="middle_name" name="middle_name" value="{{ old('middle_name') }}" /> </div>
                            <div class="mb-3 col-sm-4"><label class="form-label">Last Name</label><span style="color:red"> *</span> <input class="form-control" type="text" id="last_name" name="last_name" value="{{ old('last_name') }}" required /> </div>
                        </div>
                        <div class="row gx-2">
                            <div class="mb-3 col-sm-6"><label class="form-label">Mobile Number</label><span style="color:red"> *</span> <input class="form-control" type="number" id="contact" name="contact" max="1000000000" value="{{ old('contact') }}" required /> </div>
                            <div class="mb-3 col-sm-6"><label class="form-label">Email ID</label><span style="color:red"> *</span> <input class="form-control" type="email" id="email" name="email" value="{{ old('email') }}" required /> </div>
                        </div>
                        <div class="mb-3"><label class="form-label">Address</label> <textarea class="form-control" id="address" rows="1" name="address">{{ old('address') }}</textarea> </div>
                        <div class="row gx-2">
                            <div class="mb-3 col-sm-6"><label class="form-label">City</label> <input class="form-control" type="text" id="district" name="district" value="{{ old('district') }}" required /> </div>
                            <div class="mb-3 col-sm-6"><label class="form-label">Pin code</label><span style="color:red"> *</span> <input class="form-control" type="number" id="village" name="village" max="100000" value="{{ old('village') }}" required /> </div>
                        </div>
                        <div class="row gx-2">
                            <div class="mb-3 col-sm-6"><label class="form-label">Aadhaar number</label> <input class="form-control" type="number" id="Aadhaar" name="Aadhaar" value="{{ old('vouchar') }}" /> </div>
                            <div class="mb-3 col-sm-6"><label class="form-label">Voter ID</label> <input class="form-control" type="text" id="voter" name="voter" value="{{ old('voter') }}" required /> </div>
                        </div>
                        <div class="row gx-2">
                            <div class="mb-3 col-sm-6"><label class="form-label">Bank Account Number</label><span style="color:red"> *</span> <input class="form-control" type="password" id="account_number" name="account_number" value="{{ old('account_number') }}" required /> </div>
                            <div class="mb-3 col-sm-6"><label class="form-label">Re-Enter Account Num</label><span style="color:red"> *</span> <input class="form-control" type="number" id="reaccount_number" name="reaccount_number" value="{{ old('reaccount_number') }}" required /> </div>
                            <div class="mb-3 col-sm-6"><label class="form-label">IFSC Code</label><span style="color:red"> *</span> <input class="form-control" type="text" id="ifsc" name="ifsc" value="{{ old('ifsc') }}" required /> </div>
                        </div>
                        <div class="row gx-12">
                            <div class="mb-3 col-sm-12"><label class="form-label">Bank Details/ Bank Statement/ Passbook(Scanned Copy)</label><span style="color:red"> *</span> <input class="form-control" type="file" id="bankstatement" name="bankstatement" value="{{ old('bankstatement') }}" /> </div>
                            <div class="col-sm-12">
                                <div class="mb-3 col-sm-12"><label class="form-label">Date of Birth</label><span style="color:red"> *</span> <input class="form-control" type="date" id="dob" name="dob" value="{{ old('dob') }}" onkeydown="return false"  max="<?php echo date("Y-m-d"); ?>" /> </div>
                                <div class="row gx-12">
                                    <div class="mb-3 col-sm-12"><label class="form-label">Logo/Photo (size less than 2 MB)</label><span style="color:red"> *</span> <input class="form-control" type="file" accept="image/*" id="photo" name="photo" value="{{ old('photo') }}" /> </div>
                                    <div class="mb-3 "> <label class="form-label">Password</label><span style="color:red"> *</span>
                                        <div class='passwordfiled'> <input class="form-control" type="password" id="password" name="password" value="{{ old('password') }}" required />
                                            <div class="pwdicon"> <input class="form-check-input" name="spassword" type="checkbox" id="showPass" /> <label class="form-check-label mb-0" for="showPass"> <span class="fa fa-eye-slash"></span> <span class="fa fa-eye"></span> </label> </div>
                                        </div>
                                        <div class="note"><span class="form-label" style="color:red"> * (Min 8 characters with special characters (@ # ! ?etc.) and alphanumeric characters with one uppercase letter)</span></div>
                                    </div>
                                </div>
                                <div class="row flex-between-center">
                                    <div class="col-auto">
                                        <div class="form-check mb-0"> <input class="form-check-input" name="condition" type="checkbox" required id="condition" /> <label class="form-check-label mb-0 checklabel" for="basic-checkbox">I hereby declare that the details furnished above are true and correct to the best of my knowledge and accepting the <a href="http://support.microsoft.com/kb/278835"> Terms and Conditions</a> </label> </div>
                                    </div>
                                </div>
                                <div class="mb-3"> <input type="hidden" value="0" name="status"/> <button class="btn btn-primary d-block w-100 mt-3 mb-3" type="submit" name="submit" id="submit"  onclick="return confirm('Do you want to submit?')">Submit</button>
                                    <div class="mb-3 mt-3"> Already have an account? <a href="{{ url('login') }}">Login</a></div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
</body>

</html>
<script>
    $(document).ready(function() {
        $('#submit').prop('disabled', true);
       $('input[type=number][max]:not([max=""])').on('input', function(ev) {
            var $this = $(this);
            var maxlength = $this.attr('max').length;
            var value = $this.val();
            if (value && value.length >= maxlength) {
              $this.val(value.substr(0, maxlength));
            }
          });
           $('input[type=text][max]:not([max=""])').on('input', function(ev) {
            var $this = $(this);
            var maxlength = $this.attr('max').length;
            var value = $this.val();
            if (value && value.length >= maxlength) {
              $this.val(value.substr(0, maxlength));
            }
          });
        $('#condition').on('click',function(){
            var checkbox = $('#condition').prop( "checked" ) ;
           // alert(checkbox);
            if(checkbox == true){
                 $('#submit').prop('disabled', false);
            }
            else
            {
                 $('#submit').prop('disabled', true);
            }
        })
        
       // $("input[type='submit']").prop('disabled', true);
        $('#showPass').on('click', function() {
           
            var passInput = $("#password");
            if (passInput.attr('type') === 'password') {
                passInput.attr('type', 'text');
            } else {
                passInput.attr('type', 'password');
            }
        }); 
        $('#reaccount_number').on('change', function() {
            var reaccount_number = $(this).val();
            var account_number = $('#account_number').val();
            if (reaccount_number != account_number) {
                alert("Account number and reaccount_number should be same")
            }
        });
      $(function() {
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if (month < 10) month = '0' + month.toString();
        if (day < 10) day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        $('#dob').attr('max', maxDate);
    });
    });
</script>