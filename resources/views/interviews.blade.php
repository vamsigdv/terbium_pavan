@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Active Interviews</h5>
         
        </div>
        <div class="col-auto ms-auto">
            <div>
              {{-- target="_blank" --}}
                <div>
                <!--<button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>--></div>
            </div>
        </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
            <div class='row'>
                <div class="mb-3 col-md-4">
                    <label class="form-label" >Interview from Date</label>
                    <input class="form-control" type="date" id="fromDate" name="nextcomitdate" onchange="return searchInterview();">
                </div>
                <div class="mb-3 col-md-4">
                    <label class="form-label" >Interview to Date</label>
                    <input class="form-control" type="date" id="toDate" name="nextcomitdate" onchange="return searchInterview();">
                </div>
                <div class="mb-3 col-md-4">
                    <label class="form-label">Consultant</label>
                    <select class="form-select" aria-label="Default select example" name="contactName" id="contactName" onchange="return searchInterview();">
                         <option value="">Select Option</option>
                         <?php if(!empty($consultants)){
                            foreach ($consultants as $ckey => $cvalue) { ?>
                                <option value="{{ $cvalue->id }}">{{ $cvalue->firstname }}</option>
                        <?php   }} ?>
                    </select>
                </div>
            </div>
            <div class='row'>
                <div class="mb-3 col-md-5">
                    <label class="form-label">SME</label>
                    <select class="form-select" aria-label="Default select example" name="SMEName" id="SMEName" onchange="return searchInterview();">
                         <option value="">Select Option</option>
                         <?php if(!empty($smes)){
                            foreach ($smes as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" >{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-5">
                    <label class="form-label">Created By</label>
                    <select class="form-select" aria-label="Default select example" name="created_by" id="created_by" onchange="return searchInterview();">
                         <option value="">Select Option</option>
                         <?php if(!empty($created_users)){
                            foreach ($created_users as $ckey => $created_user) { ?>
                                <option value="{{ $created_user->id }}" >{{ $created_user->name }}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-2">
                    <label class="form-label"></label>
                    <button type="button" class="btn btn-warning form-control" name="Reset" onClick="return resetFields('interviews');">Reset</button>
                </div>
            </div>
          @if(session('success'))
          <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
          <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
          <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif
          @if(session('failed'))
          <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
          <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
          <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif
          <table class="table table-bordered interviews_datatable table-striped fs--1 mb-0" style="width:100%">
            <thead class="bg-200 text-900">
              <tr>
                <th >ID</th>
                <th >Name Of The Consultant</th>
                <th >Interview Date</th>
                <th >Amount Paid($)</th>
                <th >Technology</th>
                <th >SME</th>
                <th >Status</th>
                <th>Payment Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
    
    <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add Interview </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('interviews.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >TID</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id" value="" required />
            <input class="form-control" type="hidden" id="id" name="contactId" value="" required />
              <input class="form-control" type="text" id="transactionId" name="transactionId" value="" readOnly />
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >No Of Rounds</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="Noofrounds" name="Noofrounds" value="0" readOnly />
            </div>
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Name Of Consultant</label><span style="color:red"> *</span>
              
               <select class="form-select" aria-label="Default select example" name="contactName" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($getTransactions->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
              <div class="mb-3 col-md-6"><label class="form-label" >Mode Of Interview</label><span style="color:red"> *</span>
              
              
               <select class="form-select" aria-label="Default select example" name="modeofinterview" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($interviewtools)){
                            foreach ($interviewtools as $ikey => $ivalue) { ?>
                                <option value="{{$ivalue->id}}" >{{$ivalue->name}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
            
            </div>
            
            <div class="row">
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select" id="select2insidemodal" style="width:100%" name="technology[]"  data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple"  required>
                         <option value="">Select Option</option>
                         
                         <?php 
                        
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" >{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            <div class="mb-3 col-md-6"><label class="form-label" >SME Agreed Payment</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="SMEAgreeAmount" name="SMEAgreeAmount"  required />
            </div>
            
          
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Status Of Interview</label><span style="color:red"> *</span>
              
              
               <select class="form-select" aria-label="Default select example" name="Status" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($statustype)){
                            foreach ($statustype as $skey => $svalue) { ?>
                                <option value="{{$svalue->id}}" >{{$svalue->name}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comment</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="comment" name="comment"  required />
            </div>
            
           
           
            </div>
            
            
            <div class="row">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">SME</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="SMEName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($sme)){
                            foreach ($sme as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" >{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Payment Status</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="paymentStatus" name="paymentStatus"  required />
            </div>
            
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}">{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Recruter</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="recruter" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}">{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
            
            <div class="form-check"><input class="form-check-input" type="checkbox" checked name="status" />
              <label class="form-label" >Status</label>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
    <div class="modal editmodal fade bd-modal-lg" id="interviews-edit-modal" tabindex="-1" role="dialog" aria-labelledby="album-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Edit SME</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>
  </div>
  
@endsection