@component('mail::message')

        <h2>Welcome to Our CRM Platform!</h2>
        <p>Dear Team,</p>
        <p>One Time Password (OTP) for your Account Email: </strong> {{ $details['email'] }}</p> to unlock Account (OTP) is  :</p>
        <p><strong>{{ $details['password'] }}</strong></p>
        <p>This password is valid only for one transaction or for 30 mins whichever is earlier. Do not share it with anyone.</p>
        <p>Thank you for joining our platform. If you have any questions or need assistance, feel free to contact us.</p>
        <p>Warm regards,<br>Terbium</p>
        @component('mail::button', ['url' => $details['url']])
            Click here to login
        @endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

