@include('dashboard_layouts.head')
<div class="container" data-layout="container">
    <script>
      var isFluid = JSON.parse(localStorage.getItem('isFluid'));
      if (isFluid) {
        var container = document.querySelector('[data-layout]');
        container.classList.remove('container');
        container.classList.add('container-fluid');
      }
    </script>
        <div class="row flex-center min-vh-100 py-6">
                <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 col-xxl-4"><a class="d-flex flex-center mb-4" href="#"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="153" /></a>
                    <div class="card">
                        <div class="card-body p-4 p-sm-5">
                            <div class="row flex-between-center mb-2">
                            <div class="col-auto">
                                <h5>Validate Otp</h5>
                            </div>
                        
                            </div>
                            <form method="POST" action="{{ url('email_validate_otp') }}" novalidate>
                                @csrf
                            <!-- <div class="mb-3">
                                <input  class="form-control" name="otp" type="otp" value="{{ old('otp') }}" placeholder="otp" />
                                <div class="invalid-feedback"  @error('otp') style="display:block" @enderror>Please Enter otp</div>
                            </div> -->
                            <div class="mb-3">
                                <input class="form-control" name="otp" type="otp" value="{{ old('otp') }}" placeholder="otp" />
                                @error('otp')
                                    <div class="invalid-feedback" style="display:block">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row flex-between-center">
                                <div class="col-auto">
                                <div class="form-check mb-0">
                                    <input class="form-check-input" name="remember" type="checkbox"  checked {{ old('remember') ? 'checked' : '' }}  />
                                    <label class="form-check-label mb-0" for="basic-checkbox">
                                        <!--<a href="http://prabhu.yoursoftwaredemo.com/privacy" target="_blank">accepting Terms and Conditions</a>-->
                                        
                                         <a href="{{ env('APP_URL').'privacy' }}" target="_blank">accepting Terms and Conditions</a>
                                         
                                        </label>
                                </div>
                                </div>
                                
                            </div>
                            <div class="mb-3"><button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Validate Otp</button></div>
                            <div class="mb-3"><a href="{{ url('resend_email_otp') }}" class="btn btn-primary d-block w-100 mt-3">Resend Otp</a></div>
                            </form>
                            
                        </div>
                    </div>
                </div>
        </div>
</div>
    </main>
</body>
</html>