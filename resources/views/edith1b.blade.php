@extends('dashboard_layouts.adminmaster')
 @section('content')   
 <div class="card mb-3">
    <div class="card-header">
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0">Edit H1b Details</h5>
        </div>
      </div>
      
    </div>
    <div class="card-body">
      <div class="tab-content">
        <form id="create_movie" action="{{ route('h1b.update', $h1b->id)}}" method="POST" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            @csrf
            <div class="col-auto align-self-center">
          <h5 class="mb-0">Edit Basic Details</h5>
        </div>
        
         <br>
     
            <div class="row gx-2">
                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input class="form-control" type="hidden" id="id" name="edit_id" value="{{$h1b->id}}"  required />
                                        <label for="email">First Name<span class="text-danger">*</span>:</label>
                                     
                                        <input type="text" class="form-control" id="email" name="first_name" value="{{ $h1b->first_name }}">

                                    </div>
                                </div>
                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Middle Name:</label>
                                        <input type="text" class="form-control" id="username" name="middle_name" value="{{$h1b->middle_name}}">
                                    </div>
                                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                                        <label for="pass">Last Name<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="pass" name="last_name" value="{{$h1b->last_name}}">
                    </div>
                </div>
                
                
                
                
                
                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="mobile">Phone<span class="text-danger">*</span>:</label>
                                        <input type="number" class="form-control" id="email" name="phone" value={{$h1b->phone}}>
                                    </div>
                                </div>
                            
            </div>
            
            
                <div class="row gx-2">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Email<span class="text-danger">*</span>:</label>
                                        <input type="email" class="form-control" id="username" name="email"  value={{$h1b->email}}>
                                    </div>
                                      </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Qualification<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="qualification" value={{$h1b->qualification}}>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Marital Status<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="marital_status">
                                            <option value="">Select Marital Status</option>
                                            <option value="1" {{ ($h1b->marital_status==1)?'selected':'' }}>Married</option>
                                            <option value="2" {{ ($h1b->marital_status==1)?'selected':'' }}>Unmarried</option>
                                            <option value="3" {{ ($h1b->marital_status==1)?'selected':'' }}>Divorced</option>
                                        </select>
                                    </div>
                                </div>
                                     <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Years of Experiance<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="years_exp" value={{$h1b->years_exp}}>
                                    </div>
                                </div>
                            
                                
                                 </div>
                                   
                                
                                
                          
                            
            
            
            <div class="row gx-2">
                          
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">H1B USCIS CAP:</label>
                                        <select class="form-control" name="uscis_cap">
                                            <option value="">Select Cap</option>
                                            <?php foreach ($cap as $cap) { ?>
                                                <option value="{{ $cap->id }}" {{ ($h1b->uscis_cap==$cap->id)?'selected':'' }}>{{ $cap->year }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                             
                     
                                
                                           <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Technology<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="technolgy" value={{$h1b->technolgy}}>
                                    </div>
                                </div>
                       
                             
                                    <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Reference By<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="ref_by" value={{$h1b->ref_by}}>
                                    </div>
                                </div>
                             
                             
                             
                             
                             
                                      
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Country Located<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="country_loc">
                                            <option value="">Select Option</option>
                                            <?php foreach($locations as $location) { ?>
                                                <option value="{{ $location->id }}" {{ ($h1b->country_loc==$location->id)?'selected':'' }}>{{ $location->name }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                   <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Pre-Reg Recipt #<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="pre_recipt" value={{$h1b->pre_recipt}}>
                                    </div>
                                </div>
                             
                             
                                            <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Gender<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="gender" >
                                            <option value="">Select Gender</option>
                                            <option value="1" {{ ($h1b->gender==1)?'selected':'' }}>Male</option>
                                            <option value="2" {{ ($h1b->gender==2)?'selected':'' }}>Female</option>
                                            <option value="3" {{ ($h1b->gender==3)?'selected':'' }}>Others</option>
                                        </select>
                                    </div>
                                </div>
                            
                             
                                      
                                   <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Passport Details<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="passport" value={{$h1b->passport}}>
                                    </div>
                                </div>
                                
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Technical Skills<span class="text-danger">*</span>:</label>
                                         <input type="text" class="form-control" id="skills" name="skills" value={{$h1b->skills}}>
                                      
                                    </div>
                                </div>
                               
                            </div>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                             <br>
                            <div class="col-auto align-self-center">
          <h5 class="mb-0">Post Selection Details</h5>
        </div>
        <br>
        
        
        
            <div class="row gx-2">
                
                    <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email"># of Dependents<span class="text-danger">*</span>:</label>
                                        <input type="number" class="form-control" id="email" name="dependents" value={{$h1b->dependents}}>
                                    </div>
                                </div>
                    <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Case Worker<span class="text-danger">*</span>:</label>
                                        <select class="form-control" name="case_worker">
                                            <option value="">Select Case Worker</option>
                                            <?php foreach ($caseworkers as $caseworker) { ?>
                                                <option value="{{ $caseworker->firstname }}" {{ ($h1b->case_worker==$caseworker->firstname)?'selected':'' }}>{{ $caseworker->firstname }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                             
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">RFE Response Status<span class="text-danger">*</span>:</label>
                                        
                                          <select class="form-select" name="rfe_status">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->rfe_status==1)?'selected':'' }}>Preparing the Documents</option>
                                            <option value="2" {{ ($h1b->rfe_status==2)?'selected':'' }}>Submitted</option>
                                        </select>
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                </div>
                            
                            
                            <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">H1B Receipt #<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="h1b_recp" value={{$h1b->h1b_recp}}>
                                    </div>
                                </div>
                           
                                
                       
                                
                                
                            </div>
                            
                            
                             <div class="row gx-2">
                
                
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Petition Acknowledgement Email<span class="text-danger">*</span>:</label>
                                       
                                         <select class="form-select" name="doc_verified">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->doc_verified==1)?'selected':'' }}>Yes</option>
                                            <option value="2" {{ ($h1b->doc_verified==2)?'selected':'' }}>No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">H1b Petition Document Verfication <span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="h1b_petition_doc">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->h1b_petition_doc==1)?'selected':'' }}>Yes</option>
                                            <option value="2" {{ ($h1b->h1b_petition_doc==2)?'selected':'' }}>No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">RFE<span class="text-danger">*</span>:</label>
                                        
                                         <select class="form-select" name="rfe">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->rfe==1)?'selected':'' }}>Received</option>
                                            <option value="2" {{ ($h1b->rfe==2)?'selected':'' }}>Not Received</option>
                                        </select>
                                        
                                        
                                        
                                    </div>
                                </div>
                             <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email"> H1B Documentation Status: <span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="documentation">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->documentation==1)?'selected':'' }}>Complete</option>
                                            <option value="2" {{ ($h1b->documentation==2)?'selected':'' }}>Pending</option>
                                            <option value="3" {{ ($h1b->documentation==3)?'selected':'' }}>Not Needed</option>
                                        </select>
                                    </div>
                                </div>
                            
                            
                                           
                             
                            </div>
                            
                            
                            
                            
                            
                            
                                       <div class="row gx-2">
                
                       <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">H1B Application Status<span class="text-danger">*</span>:</label>
                                      
                                          <select class="form-select" name="application_status">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->application_status==1)?'selected':'' }}>Complete</option>
                                            <option value="2" {{ ($h1b->application_status==2)?'selected':'' }}>Pending</option>
                        
                                        </select>
                                        
                                        
                                    </div>
                                </div>
                   
                            
                                  <div class="col-sm-3">
                                         <div class="form-group">
                                        <label for="email">Shipping Tracker<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="ship_tracker" value={{$h1b->ship_tracker}}>
                                    </div>
                                 
                                </div>
                            
                            
                                                                     <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Co-Worker Affidevit<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="co_affidevit">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->co_affidevit==1)?'selected':'' }}>Affidavit Not Needed</option>
                                            <option value="2" {{ ($h1b->co_affidevit==2)?'selected':'' }}>Affidavit Needed</option>
                                             <option value="3" {{ ($h1b->co_affidevit==2)?'selected':'' }}>Affidavit Completed</option>
                                          
                                        </select>
                                    </div>
                                </div>
                            
                            
                            
                                       
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Attorney Case Worker<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="username" name="attorney_emp" value={{$h1b->attorney_emp}}>
                                    </div>
                                </div>
                         
                            </div>
            <div class="row gx-2">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Refefrence Contact<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="username" name="ref_contact" value={{$h1b->ref_contact}}>
                                    </div>
                                </div>
                      
                             <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Service Center<span class="text-danger">*</span>:</label>
                                       
                                          <select class="form-select" name="service_center">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->service_center==1)?'selected':'' }}> California</option>
                                            <option value="2" {{ ($h1b->service_center==2)?'selected':'' }}>Texas </option>
                                             <option value="3" {{ ($h1b->service_center==3)?'selected':'' }}>Vermont </option>
                                              <option value="4" {{ ($h1b->service_center==4)?'selected':'' }}>Nebraska </option>
                                              
                        
                                        </select>
                                    </div>
                                </div>
                                
                                
                                      
                       
                                              <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="mobile">Alternative Number<span class="text-danger">*</span>:</label>
                                        <input type="number" class="form-control" id="email" name="altphone" value={{$h1b->altphone}}>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                                                         <br>
                            <div class="col-auto align-self-center">
          <h5 class="mb-0">Post Approval of H1B Petition</h5>
        </div>
        <br>
        
                            
                                             
            <div class="row gx-2">
                           
                  
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">DS-160 Form<span class="text-danger">*</span>:</label>
                                      
                                         <select class="form-select" name="ds_form">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->ds_form==1)?'selected':'' }}>Submitted</option>
                                            <option value="2" {{ ($h1b->ds_form==2)?'selected':'' }}>Not Submitted </option>
                                            
                                          
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Visa Interview<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="visa_interview" value={{$h1b->visa_interview}}>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Vendor Letters<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="rfe" value={{$h1b->vendor_doc}}>
                                    </div>
                                </div>
                              <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Visa  Slot Details <span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="visa_slot_det" value={{$h1b->visa_slot_det}}>
                                    </div>
                                </div>
                            
                                       
                           
                           
                           
                            
                   </div>         
                            
                            
                                                                  
            <div class="row gx-2">
                           
                         <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Bio Metric Details<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="bio_details" value={{$h1b->bio_details}}>
                                    </div>
                                </div>
                                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Supporting Doc Verified<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="doc_verified">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->doc_verified==1)?'selected':'' }}>Yes</option>
                                            <option value="2" {{ ($h1b->doc_verified==2)?'selected':'' }}>No</option>
                                        </select>
                                    </div>
                                </div>
              
                           
                                
                                       <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Pre-Registration Status:<span class="text-danger">*</span>:</label>
                                        
                                           <select class="form-select" name="pre_status">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->pre_status==1)?'selected':'' }}>Picked</option>
                                            <option value="2" {{ ($h1b->pre_status==2)?'selected':'' }}>Not Picked</option>
                                        </select>
                                        
                                        
                                    </div>
                                </div>
                                 
                          
                             <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">   Visa Interview Required Docs : <span class="text-danger">*</span>:</label>
                                         <select class="form-select" name="visa_int_req_doc">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->visa_int_req_doc==1)?'selected':'' }}>Sent</option>
                                            <option value="2" {{ ($h1b->visa_int_req_doc==2)?'selected':'' }}>Not Yet Sent </option>
                                            
                                          
                                        </select>
                                    </div>
                                </div>
                   </div>      
          
                            

       
                            <div class="row gx-2">
                                 
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">USCIS Status<span class="text-danger">*</span>:</label>
                                       
                                         <select class="form-select" name="uscis_status">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->uscis_status==1)?'selected':'' }}>Approved</option>
                                            <option value="2" {{ ($h1b->uscis_status==2)?'selected':'' }}>RFE </option>
                                            <option value="3" {{ ($h1b->uscis_status==3)?'selected':'' }}>Declined</option>
                                            
                                          
                                        </select>
                                    </div>
                                </div>
                 
                         
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Organization<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="organization" value={{$h1b->organization}}>
                                    </div>
                                </div>
                           
                               <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Educational Evaluation<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="education_evalution">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->edu_evaluation==1)?'selected':'' }}>Complete</option>
                                            <option value="2" {{ ($h1b->edu_evaluation==2)?'selected':'' }}>Not Needed</option>
                                            <option value="3" {{ ($h1b->edu_evaluation==3)?'selected':'' }}>Pending</option>
                                        </select>
                                    </div>
                                </div>
               
                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">LCA<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="lca">
                                            <option value="">Select Option</option>
                                            <option value="1" {{ ($h1b->lca==1)?'selected':'' }}>Certified</option>
                                            <option value="2" {{ ($h1b->lca==2)?'selected':'' }}>Pending</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
            <div class="row gx-2">
             
          
            </div>
            <div class="row gx-2">
                
                
                        <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="email">Welcome Email<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="wel_email" value={{$h1b->wel_email}}>
                                    </div>
                                </div>
                           
                           
                     
                      
                            
                            </div>
                            
                            
                            
                            <div class="row gx-2 mt-1">
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                           
                    </form>
     <div class="mb-3"></div>
     <div class="card mb-3">
         <div class="card-header">
            <div class="row flex-between-end">
                <div class="col-auto align-self-center">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active"  data-bs-toggle="tab" href="#documents-tab" role="tab" aria-controls="tab-profile" aria-selected="true">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  data-bs-toggle="tab" href="#communication-tab" role="tab" aria-controls="tab-profile" aria-selected="false">Communications</a>
                        </li>
                    </ul>
                </div>
        </div>
        <div class="card-body pt-0">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="documents-tab" role="tabpanel" aria-labelledby="tab-documents-tab">
                <div class="col-auto ms-auto" style="float:right;margin-bottom:5px;">
                    <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal-1" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>
                </div>
                <table  class="table documents_datatable table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Document Name</th>
                            <th>Document Link</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="communication-tab" role="tabpanel" aria-labelledby="tab-communication-tab">
              <div class="col-auto ms-auto" style="float:right;margin-bottom:5px;">
                    <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>
                </div>  
            <table  class="table table-bordered communications_datatable table-striped fs--1 mb-0" style="width:100%">
            <thead class="bg-200 text-900">
              <tr>
                <th >ID</th>
                <th >Title</th>
                <th >Comments</th>
                <!--<th>Action</th>-->
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
         </table>
              
            </div>
        </div>
    </div>
    <div class="modal fade bd-modal-lg" id="album-modal-1" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content border-0">
            <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
              <div class="position-relative z-index-1 light">
                <h4 class="mb-0 text-white" id="authentication-modal-label">Add Documents</h4>
              </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body py-4 px-5">
              <form action="{{ route('addh1bdocuments', $h1b->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row gx-2">
                     <input class="form-control" type="hidden" id="id" name="edit_id" value="{{$h1b->id}}"  required />
                      <div class="col-sm-10">
                          <div class="form-group">
                                <label for="email">Document Type<span class="text-danger">*</span>:</label>
                                <select class="form-select" name="document_type" required>
                                    <option value="">Select Option</option>
                                    <option value="1">Petitioner Document</option>
                                    <option value="2">Benefecary Document</option>
                                    <option value="3">Dependent Document</option>
                                    <option value="4">Recipt</option>
                                    <option value="5">Passport</option>
                                    <option value="6">Educational Details</option>
                                    <option value="7">Employment History</option>
                                    <option value="8">Educational Documents</option>
                                     <option value="9">Updated Resume</option>
                                     <option value="10">Experince Letters</option>
                                     <option value="11">Individual Profile</option>
                                     <option value="12">Job  duties mapping to the course work</option>
                                     <option value="13">Employment Offer Letter</option>
                                    <option value="14">MSA</option>
                                      <option value="15">G28</option>
                                        <option value="16">I129</option>
                                          <option value="17">LOS</option>
                                            <option value="18">LCA</option>
                                </select>
                            </div>
                      </div>
                        <div class="row gx-2 remove_comments">
                          <div class="col-sm-10">
                               <div class="form-group">
                                  <label for="email">File<span class="text-danger">*</span>:</label>
                                  <input type="file" name="document" class="form-control">
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="comments_wrapper"></div>
                <div class="mb-3">
                  <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
                </div>
              </form>
             
           
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content border-0">
            <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
              <div class="position-relative z-index-1 light">
                <h4 class="mb-0 text-white" id="authentication-modal-label">Add Comments</h4>
              </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body py-4 px-5">
              <form action="{{ route('addh1bcomments', $h1b->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row gx-2">
                     <input class="form-control" type="hidden" id="id" name="edit_id" value="{{$h1b->id}}"  required />
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                            <label for="email">Title<span class="text-danger">*</span>:</label>
                                            <input type="text" name="comm_title" class="form-control">
                                        </div>
                                  </div>
                                  <div class="row gx-2 remove_comments"><div class="col-sm-10"><div class="form-group"><label for="email">Comment<span class="text-danger">*</span>:</label><textarea class="form-control" name="communication[]"></textarea></div></div><div class="col-sm-2 mt-4"><div class="form-group"></div></div></div>
                              </div>
                              <div class="comments_wrapper"></div>
                <div class="mb-3">
                  <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
                </div>
              </form>
             
           
          </div>
        </div>
      </div>
    </div>
</div>
</div>
</div>
</div></div>
@endsection