@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Payment Criteria Management</h5>
          
        </div>
        <div class="col-auto ms-auto">
            <div>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#paymentcriteria-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></div>
            </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered paymentcriteria_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >Id</th>
                    <th >Payment Category</th>
                    <th >Video Type</th>
                    <th >Payment Type</th>
                    <th >Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-modal-lg" id="paymentcriteria-modal" tabindex="-1" role="dialog" aria-labelledby="paymentcriteria-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add Payment Criteria </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('paymentcriteria.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3"><label class="form-label" >Payment Category</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="paymentCategory" name="paymentCategory"  required />
            </div>
            <div class="mb-3"><label class="form-label" >Payment Type</label><span style="color:red"> *</span>
            <select class="form-select" aria-label="Default select example" name="paymentType" onchange="return paymentCriteriaHide(this.value, 'add')" required>
                <option value="Rental Payment" >Rental Payment</option>
                <option value="Pay Per View">Pay Per View</option>
                <option value="Hybrid Life Time">Hybrid Life Time</option>
                <option value="Hybrid Rental">Hybrid Rental</option>
                <option value="Life Time Payment">Life Time Payment</option>
            </select>
            </div>
            <div class="mb-3"><label class="form-label" >Video Type</label><span style="color:red"> *</span>
              <select class="form-select" aria-label="Default select example" name="videoType" required>
                        
                         <option value="Movies">Movies</option>
                        <option value="Stories">Stories</option>
                        <option value="Short film">Short film</option>
                        <option value="Schemes">Schemes</option>
                        <!--<option value="Pñar">Pñar</option>-->
                        <option value="Music Video">Music Video</option>
                         <option value="Influncer">Influncer</option>
                          <option value="Mp3">Mp3</option>
                    </select>
            </div>
            <div class="mb-3 ">
              <label class="form-label" for="">Description</label>
              <textarea class="form-control description" name="Description" id="" rows="1"></textarea>
            </div>
            <div class="mb-3 ">
              <label class="form-label" for="">Year</label>
              <select class="form-select" onchange="hideUnhide(this.value, 'add')">
                  <option value="0">Select Option</option>
                  <option value="1">Before</option></option>
                  <option value="2">After</option>
                  <option value="3">In Between</option>
              </select>
            </div>
            <div class="mb-3 " id="before_add" style="display: none;">
                <label class="form-label" for="">Before Year</label>
                <input class="form-control" type="text" name="before_year" placeholder="Before Year">
            </div>
            <div class="mb-3 " id="after_add" style="display: none;">
                <label class="form-label" for="">After Year</label>
                <input class="form-control" type="text" name="after_year" placeholder="After Year">
            </div>
             <div class="mb-3" id="per_view_add" style="display: none;">
                <label class="form-label" >Number of Views</label><span style="color:red"> *</span>
                <input class="form-control" type="text" id="perview" name="perview" />
            </div>
             <div class="mb-3" id="per_view_amount_add" style="display: none;">
                <label class="form-label" >Per View Amount</label><span style="color:red"> *</span>
               <input class="form-control" type="text" id="perviewamount" name="perviewamount" />
            </div>
             <div class="mb-3" id="rental_amount_add">
                <label class="form-label" >Milestone Payment</label><span style="color:red"> *</span>
                <input class="form-control" type="text" id="rentalamount" name="rentalamount" />
            </div>
             <div class="mb-3" id="price_add" style="display: none;">
                <label class="form-label" >Intial Payment</label><span style="color:red"> *</span>
                <input class="form-control" type="text" id="price" name="price" />
            </div>
            <div class="mb-3"><label class="form-label" >Status</label><span style="color:red"> *</span>
              <select class="form-select" aria-label="Default select example" name="Status">
                        
                         <option value="Active">Active</option>
                        <option value="In-active">In-active</option>
                    </select>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="paymentcriteria-edit-modal" tabindex="-1" role="dialog" aria-labelledby="paymentcriteria-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Payment Criteria Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="paymentcriteriaedit_data"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="paymentcriteria-view-modal" tabindex="-1" role="dialog" aria-labelledby="paymentcriteria-view-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Payment Criteria View </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
        
          <div id="paymentcriteriaview"></div>
        </div>
      </div>
    </div>
  </div>
@endsection