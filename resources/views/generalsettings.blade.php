@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="col-auto ms-auto">
             <div class="col-12">
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                          
                      <li class="nav-item">
                          <a class="nav-link" id="profile-tab" data-toggle="tab" href="/profileupdate" role="tab" aria-controls="tab-profile" aria-selected="false">  Account Settings</a>
                        </li>
                      
                       <li class="nav-item">
                          <a class="nav-link active" id="profile-tab" data-toggle="tab" href="/generalsettings" role="tab" aria-controls="tab-profile" aria-selected="false">General Settings</a>
                        </li>
                       
                      </ul>
                     
                    </div>
            
            </div>
 <div class="card">
      @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('fail'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('fail')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
  <div class="card-header">
    <h5 class="mb-0">Logout from All Devices</h5>
  </div>
  <div class="card-body bg-light">
    
    <p class="fs--1">This will log you out from all the existing login devices except current device</p>
    <a class="btn btn-falcon-danger d-block align-items-center mb-2" id="upload"  href="#" data-toggle="modal" data-target="#uploadcontent">Logout From All Devices</a>
    <!-- <a class="btn btn-falcon-warning d-block" href="/logoutalldevices"> -->
    <div class="border-bottom border-dashed my-4"></div>
    <h5 class="fs-0">Deactivate this account</h5>
    <p class="fs--1">Once you delete a account, there is no going back. Please be certain.</p><a class="btn btn-falcon-danger d-block" onclick="return confirm('Are you sure Do you want to Deactivate Yes / No?')" href="/deactivateaccount">Deactivate Account</a>
    <div class="border-bottom border-dashed my-4"></div>
    <h5 class="fs-0">Signout this account</h5>
    <p class="fs--1">Logout from current device.</p><a class="btn btn-falcon-danger d-block" href="/logout">Signout Account</a>
  </div>
</div>

<div class="modal fade" id="uploadcontent" tabindex="-1" role="dialog" aria-labelledby="applicantModal">
                <div class="modal-dialog">
                    
                    <div class="modal-body modal-content border-0">
                        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Confirm</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
            <div class="card-body fs--1">
              <div class="row">
                  <form action="/logoutalldevices" method="post">
                      {{ csrf_field() }}
                      <div class="md-12">
                          <label class="form-label" for="">Password</label>
                          <input type="password" name="password" class="form-control" required>
                        </div>
                      <div class="md-12">
                          <button class="btn btn-falcon-warning d-block mb15" type="submit">Submit</button>
                      </div>
                  </form>
               </div>
            </div>
            </div>
                </div>
           </div>
<script>
$('#upload').on('click',function(){
   $('#uploadcontent').modal('show');
})  
</script>
 
 @endsection