@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Active Interviews</h5>
          <input type="hidden" value="{{$id }}" id="transactionId">
        </div>
        <div class="col-auto ms-auto">
            <div>
              {{-- target="_blank" --}}
                <div>
                 <a href="/details/{{$getTransactions->contactId}}" class="btn btn-falcon-success btn-sm">Consultant Profile</a>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></div>
            </div>
        </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered interviews_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >ID</th>
                    <th >Name Of The Consultant</th>
                    <th >Interview Date</th>
                    <th >Amount Paid($)</th>
                    <th >Technology</th>
                    <th >SME</th>
                     <th >Interview Status</th>
                    <th >Payment Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
    
    <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add Interview </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('interviews.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >TID</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id" value="{{$getTransactions->id}}" required />
            <input class="form-control" type="hidden" id="id" name="contactId" value="{{$getTransactions->contactId}}" required />
              <input class="form-control" type="text" id="transactionId" name="transactionId" value="{{$getTransactions->transactionId}}" readOnly />
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >No Of Rounds</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="Noofrounds" name="Noofrounds" value="{{$comments}}"  readOnly />
              <input class="form-control" type="hidden" id="Noofrounds" name="Noofrounds" value="{{$comments}}"   />
            </div>
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Name Of Consultant</label><span style="color:red"> *</span>
              
               <select class="form-select" aria-label="Default select example" name="contactName" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($getTransactions->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
              <div class="mb-3 col-md-6"><label class="form-label" >Mode Of Interview</label><span style="color:red"> *</span>
              
              
               <select class="form-select" aria-label="Default select example" name="modeofinterview" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($interviewtools)){
                            foreach ($interviewtools as $ikey => $ivalue) { ?>
                                <option value="{{$ivalue->id}}" >{{$ivalue->name}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
            
            </div>
            
            <div class="row">
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select" id="select2insidemodal" style="width:100%" name="technology[]"  data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple"  required>
                         <option value="">Select Option</option>
                         
                         <?php 
                         $technologies=explode(",", $getTransactions->technology);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            <div class="mb-3 col-md-6"><label class="form-label" >SME Agreed Payment</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="SMEAgreeAmount" name="SMEAgreeAmount"  required />
            </div>
            
          
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Status Of Interview</label><span style="color:red"> *</span>
              
              
               <select class="form-select" aria-label="Default select example" name="Status" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($statustype)){
                            foreach ($statustype as $skey => $svalue) { ?>
                                <option value="{{$svalue->id}}" >{{$svalue->name}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comment</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="comment" name="comment"  required />
            </div>
            
           
           
            </div>
            
            
            <div class="row">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">SME</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="SMEName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($sme)){
                            foreach ($sme as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" >{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Payment Status</label><span style="color:red"> *</span>
              <select class="form-select" aria-label="Default select example" name="paymentStatus" required>
                    <option value="0">Select payment status</option>
                    <?php if ($payment_statuses) { 
                        foreach ($payment_statuses as $payment) {
                    ?>
                    <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                    <?php } 
                    }
                    ?>
                    </select>
            </div>
            
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" {{ (Auth::guard('adminiy')->user()->user_id != 8)? 'disabled':'' }}>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{ (Auth::guard('adminiy')->user()->user_id == $cwvalue->id)?'selected':'' }}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                     <?php  if(Auth::guard('adminiy')->user()->user_id != 5){?>
                    <input class="form-control" type="hidden"  name="caseworker" value="{{Auth::guard('adminiy')->user()->user_id}}">
                    <?php } ?>
                </div>
                
                 <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Recruter</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="recruter">
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" >{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                   
                </div>
                
                 <div class="mb-3 col-md-6"><label class="form-label" >Interview Date</label><span style="color:red"> *</span>
              <input class="form-control" type="date" id="interviewdate" name="interviewdate"  required />
            </div>
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Resume</label><span style="color:red"> *</span>
                    <input class="form-control" type="file" accept="application/pdf" name="fileinput" onchange="upload(event, 'movie_poster')" required>
                </div>
            </div>
            
            <div class="form-check"><input class="form-check-input" type="checkbox" checked name="status" />
              <label class="form-label" >Status</label>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
    <div class="modal editmodal fade bd-modal-lg" id="interviews-edit-modal" tabindex="-1" role="dialog" aria-labelledby="album-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Edit SME</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>
  </div>
   <script>

$(document).ready(function() {
   
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
  
  
});

</script>
@endsection