@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">H1B Management</h5>
          
        </div>
        <div class="col-auto ms-auto">
        <div>
                <a href="/h1b_form" rel="noopener noreferrer"><button class="btn btn-falcon-success btn-sm"  type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></a>
            </div>
            </div>
      </div>
  
    </div>
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class='row'>
            <div class="mb-3 col-md-4">
                <label class="form-label" >From Date</label>
                <input class="form-control" type="date" id="h1b_fromDate" name="nextcomitdate" onchange="return searchH1B();">
            </div>
            <div class="mb-3 col-md-4">
                <label class="form-label" >To Date</label>
                <input class="form-control" type="date" id="h1b_toDate" name="nextcomitdate" onchange="return searchH1B();">
            </div>
       
          <div class="mb-3 col-md-3">
              
              <label class="form-label">Country Located:</label>
              <select class="form-select" aria-label="Default select example" name="h1b_country_located" id="h1b_country_located" onchange="return searchH1B();" >
                  <option value="">Select Country Located</option>
                    <?php foreach($locations as $location) { ?>
                        <option value="{{ $location->id }}">{{ $location->name }}</option>
                    <?php } ?>
              </select>
              
          </div>
          <div class="mb-3 col-md-3">
              
              <label class="form-label">Cap:</label>
              <select class="form-select" aria-label="Default select example" name="h1b_cap" id="h1b_cap" onchange="return searchH1B();" >
                  <option value="">Select Cap</option>
                    <?php foreach($cap as $location) { ?>
                        <option value="{{ $location->year }}">{{ $location->year }}</option>
                    <?php } ?>
              </select>
              
          </div>
          <div class="mb-3 col-md-3">
              
              <label for="email">Application Status:</label>
              <select class="form-select" aria-label="Default select example" name="h1b_application_status" id="h1b_application_status" onchange="return searchH1B();" >
                  <option value="">Select Option</option>
                  <option value="1">Completed</option>
                  <option value="2">Pending</option>
              </select>
              
          </div>
          <div class="mb-3 col-md-3">
              
              <label class="form-label">Case Worker:</label>
              <select class="form-select" aria-label="Default select example" name="h1b_case_worker" id="h1b_case_worker" onchange="return searchH1B();" >
                  <option value="">Select Case Worker</option>
                  <?php foreach ($caseworkers as $caseworker) { ?>
                      <option value="{{ $caseworker->firstname }}">{{ $caseworker->firstname }}</option>
                  <?php } ?>
              </select>
              
          </div>
          
        </div>
        <div class='row'>
            
            
           
            <div class="mb-3 col-md-3">
                <label class="form-label"></label>
                <button type="button" class="btn btn-warning form-control" name="Reset" onClick="return resetFields('h1b');">Reset</button>
            </div>
        </div>
        
      </div>
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered h1b_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Case Worker</th>
                    <th>Years Of Exp</th>
                    <th>CAP</th>
                    <th>Is Active</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
@endsection