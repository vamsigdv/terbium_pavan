@include('dashboard_layouts.head')

<div class="container" data-layout="container">
    <div class="row flex-center min-vh-100 py-6">
        <div class="col-sm-10"><a class="d-flex flex-center mb-4" href="#"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="153" /></a>
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('h1b_save') }}" enctype="multipart/form-data" novalidate>
                        @csrf
                        @if(session('success'))
                          <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
                              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
                              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                          </div>
                         @endif
                         @if(session('failed'))
                          <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
                              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
                              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                          </div>
                         @endif
                          <a href="/h1b" class="btn btn-falcon-success btn-sm" style="float:right">Home</a>
                        <div class="form-step">
                            <h2>Personal Details</h2>
                            <p><strong>Fields marked with asterisk (<span class="text-danger">*</span>) are mandatory.</strong></p>
                            <div class="row gx-2">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="email">First Name<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="first_name" maxlength="40" pattern="[A-Za-z]" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="username">Middle Name:</label>
                                        <input type="text" class="form-control" id="username" name="middle_name" maxlength="40" pattern="[A-Za-z]">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="pass">Last Name<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="pass" name="last_name" maxlength="40" pattern="[A-Za-z]" required>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <div class="row gx-2">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="email">Phone Number<span class="text-danger">*</span>:</label>
                                        <input type="number" class="form-control" id="email" name="mobile" onKeyPress="if(this.value.length==15) return false;" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="username">Email ID<span class="text-danger">*</span>:</label>
                                        <input type="email" class="form-control" id="username" name="email_id" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="pass">Gender<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="gender" required>
                                            <option value="">Select Gender</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Others</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                                     <div class="row gx-2">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="technolgy"> Technology <span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="technolgy" name="technolgy">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                     <div class="form-group">
                                        <label for="username">H1B USCIS CAP:</label>
                                        <select class="form-control" name="uscis_cap">
                                            <option value="">Select Cap</option>
                                            <?php foreach ($cap as $cap) { ?>
                                                <option value="{{ $cap->id }}">{{ $cap->year }}</option>
                                            <?php } ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                
                                
                                      <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Reference By:</label>
                                        <input type="text" class="form-control" id="email" name="ref_by">
                                    </div>
                                </div>
                                    <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Refefrence Contact:</label>
                                        <input type="number" class="form-control" id="username" name="ref_contact" onKeyPress="if(this.value.length==15) return false;"  >
                                    </div>
                                </div>

                            </div>
                            
     
                            
                            
                            <div class="row gx-2">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Qualification<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="qualification" maxlength="40" pattern="[A-Za-z]" required>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Marital Status<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="marital_status" required>
                                            <option value="">Select Marital Status</option>
                                            <option value="1">Married</option>
                                            <option value="2">Unmarried</option>
                                            <option value="3">Divorced</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Passport<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="passport" required>
                                    </div>
                                </div>
                                
                                     <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Pre-Registration Status:</label>
                                      
                                         <select class="form-select" name="pre_reg_status" >
                                            <option value="">Select Option</option>
                                            <option value="1">Picked</option>
                                            <option value="2">Not Picked</option>
                                        </select>
                                
                                    </div>
                                </div>
                            </div>
                            <div class="row gx-2">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Years of Experiance<span class="text-danger">*</span>:</label>
                                        <input type="text" class="form-control" id="email" name="experiance" maxlength="15">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Technical Skills<span class="text-danger">*</span>:</label>
                                         <input type="text" class="form-control" id="skills" name="skills">
                                       <!-- <select class="form-select" id="organizerMultiple_writers" name="skills[]" multiple="multiple" data-options='{"removeItemButton":true,"placeholder":true}'>
                                            <option value="">Select Option</option>
                                            <?php foreach($skills as $skill) { ?>
                                                <option value="{{ $skill->id }}">{{ $skill->technology }}</option>
                                            <?php } ?>
                                        </select>-->
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Country Located<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="country" required>
                                            <option value="">Select Option</option>
                                            <?php foreach($locations as $location) { ?>
                                                <option value="{{ $location->id }}">{{ $location->name }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                    
                                   <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Pre Registration Receipt # :</label>
                                        <input type="text" class="form-control" id="email" name="pre_reg_recipt">
                                    </div>
                                </div>
                                

                                
                            </div>
                            </br>
                            
                            <div class="row gx-2 mt-1">
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary next-step">Next</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!-- Step 2 -->
                        <div class="form-step d-none">
                            <h3>Post selection details</h3>
                            <p><strong>Fields marked with asterisk (<span class="text-danger">*</span>) are mandatory.</strong></p>
                           
                           
                                          
                            <div class="row gx-2">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email"># of Dependents:</label>
                                        <input type="number" class="form-control" id="email" name="dependents" onKeyPress="if(this.value.length==1) return false;">
                                    </div>
                                </div>
                                     <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Attorney Case Worker:</label>
                                        <input type="text" class="form-control" id="username" name="att_case_worker" maxlength="40" >
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Case Worker<span class="text-danger">*</span>:</label>
                                        <select class="form-control" name="case_worker">
                                            <option value="">Select Case Worker</option>
                                            <?php foreach ($caseworkers as $caseworker) { ?>
                                                <option value="{{ $caseworker->firstname }}">{{ $caseworker->firstname }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">RFE Status</label>
                                        <select class="form-select" name="rfe_status">
                                            <option value="">Select  Status</option>
                                            <option value="1">Preparing the Documents</option>
                                            <option value="2">Submitted</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                           

                            
                            <div class="row gx-2">
                            
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Petition Acknowledgement Email</label>
                                            <select class="form-select" name="email_sent">
                                            <option value="">Select  Status</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                        
                                        </select>
                                        
                                        
                                    </div>
                                </div>
                                
                                
                                     <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">H1B Petition Documents Verfication</label>
                                        
                                                 <select class="form-select" name="h1b_petition_doc">
                                            <option value="">Select  Status</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                        
                                        </select>
                                    </div>
                                </div>
                                
                                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Co-Worker Affidavit:</label>
                                        <select class="form-select" name="co_affidevit" >
                                            <option value="">Select Option</option>
                                            <option value="1">Affidavit Not Needed</option>
                                            <option value="2">Affidavit Needed</option>
                                              <option value="3">Affidavit Completed</option>
                                        </select>
                                    </div>
                                </div>
                                    <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">RFE</label>
                                         <select class="form-select" name="rfe">
                                            <option value="">Select  Status</option>
                                            <option value="1">Received</option>
                                            <option value="2">Not Received</option>
                                        </select>
                                    </div>
                                </div>
                             
                            </div>
                            
                                     
                            <div class="row gx-2">
                            
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">H1B Receipt #:</label>
                                        <input type="text" class="form-control" id="email" name="h1b_recipt">
                                    </div>
                                </div>
                                
                                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">H1B Documentation Status:</label>
                                        <select class="form-select" name="documentation">
                                            <option value="">Select Option</option>
                                            <option value="1">Completed</option>
                                            <option value="2">Pending</option>
                                            <option value="3">Not Needed</option>
                                        </select>
                                    </div>
                                </div>
                                
                                    <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">H1B Application Status:</label>
                                         <select class="form-select" name="application_status" >
                                            <option value="">Select Option</option>
                                            <option value="1">Completed</option>
                                            <option value="2">Pending</option>
                                        </select>
                                    </div>
                                </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                             
                                                <label for="email">Service Center:</label>
                                         <select class="form-select" name="service_center" >
                                            <option value="">Select Option</option>
                                            <option value="1">California</option>
                                            <option value="2">Texas </option>
                                              <option value="3">Vermont</option>
                                              <option value="4">Nebraska</option>
                                        </select>
                                    </div>
                                </div>
                             
                            
                             <div class="row gx-2">
                                  
                                     <div class="col-sm-3">
                                    <div class="form-group">
                                    
                                         <label for="pass">Shipping Tracker:</label>
                                         <input type="text" class="form-control" id="email" name="ship_tracker">
                                   
                                    </div>
                                </div>
                              <div class="col-sm-3">
                                       <div class="form-group">
                                        <label for="pass">Alternative Number:</label>
                                         <input type="text" class="form-control" id="email" name="altphone">
                                   </div>
                                    </div>
                             
                             </div>
                             
                            </div>
                            
             
                                
                                </br>
                                
                                 <h3>Post Approval of H1B Petition</h3> 
                                
                                
                                
                                    </br>
                                
                                           <div class="row gx-2">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">DS-160 Form:</label>
                                       
                                         <select class="form-select" name="ds_160_form" >
                                            <option value="">Select Option</option>
                                            <option value="1">Submitted</option>
                                            <option value="2">Not Submitted</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Visa Interview:</label>
                                        <input type="text" class="form-control" id="email" name="visa_interview">
                                    </div>
                                </div>
                                   <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Vendor Letters</label>
                                        <input type="text" class="form-control" id="email" name="vendor_doc">
                                    </div>
                                </div>
                            
                         <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Visa Interview Slot:</label>
                                         <select class="form-select" name="Visa_int_slot" >
                                            <option value="">Select Option</option>
                                            <option value="1">Values</option>
                                            <option value="2">Booked</option>
                                        </select>
                                    </div>
                                </div>
                         
                            </div>
                                
                

                            <div class="row gx-2">
                           
                            
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Supporting Doc Verified:</label>
                                        <select class="form-select" name="doc_verified" >
                                            <option value="">Select Option</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                        </select>
                                    </div>
                                </div>
                            
                                       <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">USCIS Status:</label>
                                       
                                          <select class="form-select" name="uscis_status" >
                                            <option value="">Select Option</option>
                                            <option value="1">Approved</option>
                                            <option value="2">RFE </option>
                                              <option value="3">Declined</option>
                                        </select>
                                    </div>
                                </div>
                           
                    
                           
                         
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Organization:</label>
                                        <input type="text" class="form-control" id="email" name="organization">
                                    </div>
                                </div>
                                       
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Visa Slot Details</label>
                                       <input type="text" class="form-control" id="username" name="visa_slot_det" maxlength="40" >
                                    </div>
                                </div>
                         
                                
                                
                            </div>
                          
                            <div class="row gx-2">
                             
                             
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="pass">Educational Evaluation:</label>
                                        <select class="form-select" name="education_evalution">
                                            <option value="">Select Option</option>
                                            <option value="1">Complete</option>
                                            <option value="2">Not Needed</option>
                                            <option value="3">Pending</option>
                                        </select>
                                    </div>
                                </div>
                         
                           
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">LCA:</label>
                                        <select class="form-select" name="lca">
                                            <option value="">Select Option</option>
                                            <option value="1">Certified</option>
                                            <option value="2">Pending</option>
                                        </select>
                                    </div>
                                </div>
                          
                          
                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="username">Visa Interview Required docs</label>
                                        <select class="form-select" name="visa_int_req_doc">
                                            <option value="">Select Option</option>
                                            <option value="1">Sent</option>
                                            <option value="2">Not Yet Sent</option>
                                        </select>
                                    </div>
                                </div>
                               <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="email">Bio Metric Details</label>
                                        <input type="text" class="form-control" id="email" name="bio_metric_detatils">
                                    </div>
                                </div>
                            </div>
                 
                               
                                
                                                       <div class="row gx-2">
                             
                             <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="username">Welcome Email</label>
                                        <input type="text" class="form-control" id="email" name="wel_email">
                                    </div>
                                </div>
                            </div>
                            
                            
                                </br>
                            
                            <div class="row gx-2 mt-1">
                              <div class="col-sm-4">
                                  <button type="button" class="btn btn-secondary prev-step mr-2">Previous</button>
                                  <button type="button" class="btn btn-primary next-step">Next</button>
                              </div>
                            </div>
                        </div>
                        <div class="form-step d-none">
                          <h2>Documents</h2>
                          <p><strong>Fields marked with asterisk (<span class="text-danger">*</span>) are mandatory.</strong></p>
                          <div class="row gx-2">
                              <div class="col-sm-5">
                                  <div class="form-group">
                                        <label for="email">Document Type<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="uploaded_document[1][name]">
                                            <option value="">Select Option</option>
                                            <option value="1">Petitioner Document</option>
                                            <option value="2">Benefecary Document</option>
                                            <option value="3">Dependent Document</option>
                                            <option value="4">Recipt</option>
                                            <option value="5">Passport</option>
                                            <option value="6">Educational Details</option>
                                            <option value="7">Employment History</option>
                                            <option value="8">Educational Documents</option>
                                             <option value="9">Updated Resume</option>
                                             <option value="10">Experince Letters</option>
                                             <option value="11">Individual Profile</option>
                                             <option value="12">Job  duties mapping to the course work</option>
                                             <option value="13">Employment Offer Letter</option>
                                            <option value="14">MSA</option>
                                              <option value="15">G28</option>
                                                <option value="16">I129</option>
                                                  <option value="17">LOS</option>
                                                    <option value="18">LCA</option>
                                        </select>
                                    </div>
                              </div>
                              <div class="col-sm-5">
                                  <div class="form-group">
                                        <label for="email">Attach Document<span class="text-danger">*</span>:</label>
                                        <input type="file" name="uploaded_document[1][doc]" class="form-control">
                                    </div>
                              </div>
                              <div class="col-sm-2">
                                  <div class="form-group">
                                      <label>Action</label>
                                      <button class="btn btn-falcon-success btn-sm" type="button" id="add_more_docs">Add More Docs
                                      </button>
                                 </div>
                              </div>
                          </div>
                          <div class="field_wrapper"></div>
                          <div class="row gx-2">
                              <div class="col-sm-10">
                                  <div class="form-group">
                                        <!-- <label for="email">Title:</label>
                                        <input type="text" name="comm_title" class="form-control"> -->
                                    </div>
                              </div>
                              <div class="col-sm-2">
                                  <div class="form-group">
                                      <label>Action</label>
                                      <button class="btn btn-falcon-default btn-sm" type="button" id="add_comments">Add Comments
                                      </button>
                                 </div>
                              </div>
                          </div>
                          <div class="comments_wrapper"></div>
                          <div class="row gx-2 mt-1">
                              <div class="col-sm-4">
                                  <button type="button" class="btn btn-secondary prev-step mr-2">Previous</button>
                                  <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
      $('#organizerMultiple_writers').select2({
          selectOnClose: false
        });
    $(".next-step").click(function() {
      $(this).closest(".form-step").addClass('d-none').next().removeClass('d-none');
    });

    $(".prev-step").click(function() {
      $(this).closest(".form-step").addClass('d-none').prev().removeClass('d-none');
    });
    
    var maxField = 4; //Input fields increment limitation
    var addButton = $('#add_more_docs'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    
    var x = 1; //Initial field counter is 1
    
    // Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++;
            var fieldHTML = '<div class="row gx-2 remove_element"><div class="col-sm-5"><div class="form-group"><label for="email">Document Type<span class="text-danger">*</span>:</label><select class="form-select" name="uploaded_document[' + x + '][name]"> <option value="">Select Option</option><option value="1">Petitioner Document</option> <option value="2">Benefecary Document</option><option value="3">Dependent Document</option><option value="4">Recipt</option><option value="5">Passport</option> <option value="6">Educational Details</option><option value="7">Employment History</option><option value="8">Educational Documents</option><option value="9">Updated Resume</option><option value="10">Experince Letters</option><option value="11">Individual Profile</option><option value="12">Job  duties mapping to the course work</option><option value="13">Employment Offer Letter</option><option value="14">MSA</option> <option value="15">G28</option> <option value="16">I129</option><option value="17">LOS</option><option value="18">LCA</option></select></div></div>'; 
            fieldHTML +='<div class="col-sm-5"><div class="form-group"><label for="email">Attch Document<span class="text-danger">*</span>:</label><input type="file" name="uploaded_document[' + x + '][doc]" class="form-control"></div></div>';
            fieldHTML +='<div class="col-sm-2"><div class="form-group mt-4"><button class="btn btn-danger remove_button">Delete</button></div></div>';
             //Increase field counter
            $(wrapper).append(fieldHTML); //Add field html
        }else{
            alert('A maximum of ' + maxField + ' fields are allowed to be added. ');
        }
    });
    
    // Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).closest('.remove_element').remove(); //Remove field html
        x--; //Decrease field counter
    });
    
    
    var maxcomments = 10;
    var addComment = $('#add_comments');
    var comments_wrapper = $('.comments_wrapper'); //Input field wrapper
    var body = '<div class="row gx-2 remove_comments">  <div class="col-sm-10"><div class="form-group"><label for="email">Title:</label><input type="text" name="comm_title" class="form-control"></div></div><div class="col-sm-10"><div class="form-group"><label for="email">Comment<span class="text-danger">*</span>:</label><textarea class="form-control" name="communication[]"></textarea></div></div><div class="col-sm-2 mt-4"><div class="form-group"><button class="btn btn-warning remove_comments_button">Delete</button></div></div></div>';
    var y = 0; //Initial field counter is 0
    // Once add button is clicked
    $(addComment).click(function(){
        //Check maximum number of input fields
        if(y < maxcomments){ 
            y++;
           //Increase field counter
            $(comments_wrapper).append(body); //Add field html
        }else{
            alert('A maximum of ' + maxcomments + ' fields are allowed to be added. ');
        }
    });
    
    // Once remove button is clicked
    $(comments_wrapper).on('click', '.remove_comments_button', function(e){
        e.preventDefault();
        $(this).closest('.remove_comments').remove(); //Remove field html
        y--; //Decrease field counter
    });
  });
</script>
