@extends('dashboard_layouts.adminmaster')
<style>
.marT20{ margin-top:20px;}
.smalcards .card{height:100%; }
.smalcards .col-md-3{ margin-bottom: 10px;}
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}

   .text-700 {
    color: #5e6e82!important;
}
.mb-2, .my-2 {
    margin-bottom: 0.5rem!important;
}
.align-items-center {
    -webkit-box-align: center!important;
    -ms-flex-align: center!important;
    align-items: center!important;
}
.media {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start
}
.svg-inline--fa.fa-w-16 {
    width: 1em;
}
svg:not(:root).svg-inline--fa {
    overflow: visible;
}
.media-body {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
@media (min-width: 992px)
.pl-lg-3, .px-lg-3 {
    padding-left: 1rem!important;
}
.mr-2, .mx-2 {
    margin-right: 0.5rem!important;
}
.btn-falcon-primary:focus:not(.disabled):not(:disabled),.btn-falcon-primary:hover:not(.disabled):not(:disabled) {
    color: #1966cc!important;
    -webkit-box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08);
    box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08)
}
.pl-3, .px-3 {
    padding-left: 1rem!important;
}
.pr-3, .px-3 {
    padding-right: 1rem!important;
}
var avgMin
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}
.tabcontent_movie {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
@section('content')

<div class="card mb-3">
    <div class="card-header position-relative min-vh-25 mb-7">
        <?php 
            $coverPic = '/assets/img/generic/4.jpg';
            $profilepic = '/uploads/studio/1780210890575554.jpg'; 
            if ($img->coverPic) {
                $coverPic = $img->coverPic;
            }
            if ($img->profilePic) {
                $profilepic = $img->profilePic;
            }
            
        ?>
        <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image: url(../{{ $coverPic }})"></div>
        
        <div class="avatar avatar-5xl avatar-profile">
            <img class="rounded-circle img-thumbnail shadow-sm" src="../{{ $profilepic }}" width="200" alt="">
        </div>
    </div>
    
       
      <div class="card-body">
            <h4 class="mb-3">Contact Name: {{ $img->firstname }} ({{$img->contactType}})</h4>
        <div class="row">
            <div class="col-lg-8">
                <input type="hidden" value="{{ $data->id }}" id="contactId">
                 </div>
              
                
                
                
                
                <div class="row gx-4">
            
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Gender: {{ $img->gender }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Email: {{ $img->email }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Work Type: {{ $img->workType }}
                </h5>
                 
                 
                 
                </div>
                <div class="row gx-4">
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Mobile: {{ $img->mobile }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   DOB: {{ $img->dob }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Consultant Type: {{ $img->consultantType }}
                </h5>
                </div>
                  <div class="row gx-4">
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Payment Type: {{ $data->paymentType }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Created by : {{ $data->caseworker }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Location: {{ $img->location }}
                </h5>
                </div>
                 <div class="row gx-4">
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Technology: {{ $img->technology }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Skills: {{ $img->skill }}
                </h5>
               
               
            
            </div>
        </div>
    </div>
    <div class="card-body">
    <h4 class="mb-3">Payment Details</h4>
     <div class="row gx-4">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Payment Type: {{ $img->paymentType }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Account Type: {{ $img->accountType }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Bank: {{ $img->bankname }}
                </h5>
    </div>
    <div class="row gx-4">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Branch: {{ $img->branch }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   UPI ID: {{ $img->ZilleID }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Zelle ID: {{ $img->ZilleID }}
                </h5>
    </div>
    <div class="row gx-4">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Routing Number : {{ $img->RoutingNumber }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                 Description: {{ strip_tags(nl2br($img->description,ENT_NOQUOTES)) }}
                </h5>
                
    </div>
    </div>
       <hr class="border-dashed my-4 d-lg-none">
      
                <button class="btn btn-falcon-default btn-sm px-3 ml-2" type="button">
                        <a href="/contact/{{ $data->id }}/edit"> Edit Profile  </a>
                </button>
                <hr class="border-dashed my-4 d-lg-none">
     
    </div>
    
<div class="card-deck">
        <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
            <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
                <div class="card-body position-relative">
                    <h6>Total Amount<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                    </h6>
                    <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                      {{ $movie_amounts['total_amount'] }}
                    </div>
                </div>
        </div>
        <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
            <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
                  <div class="card-body position-relative">
                    <h6>Total Paid<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                    <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                      {{ $movie_amounts['total_paid_amount'] }}
                    </div>
            </div>
        </div>
        
       
        <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
            <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
                  <div class="card-body position-relative">
                    <h6>Total Un-paid Amount<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                    <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                      {{ $movie_amounts['total_unpaid_amount'] }}
                    </div>
            </div>
        </div>
</div>
 <div class="card mb-3">
    <div class="card-header">
        <div class="col-auto align-self-center">
              <h5 class="mb-0" data-anchor="data-anchor">SME PAYMENTS</h5>
              
            </div>
    
         
    </div>
    
    
  
    
    <div class="card-body pt-0">
    <table class="table table-bordered smedetailsview_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >ID</th>
                    <th >SME Name</th>
                    <th>Consultant Name</th>
                    <th >Transaction Date</th>
                    <th >Amount Paid ($)</th>
                     <th >Payment Status</th>
                    <th >Type</th>
                    <th >Employee</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        
    </div>
    </div>
     <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add New Payment</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('installment.store')}}" method="post" enctype="multipart/form-data">
            @csrf
                <div class="row gx-2">
        <div class="mb-3 col-md-6">
            
            
                    <label class="form-label" for="">Consultant Name</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="consultantName" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($data->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
        <div class="mb-3 col-sm-6"><label class="form-label" >Date</label><span style="color:red"> *</span>
           
              <input class="form-control" type="date" id="date" name="date" value="{{$data->date}}"  required />
            </div>
     </div>
     <div class="row gx-2">
        <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select organizerMultiple_director" id="organizerMultiple_director" style="width:100%" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" name="technology[]" multiple="multiple" disabled>
                         <option value="">Select Option</option>
                         <?php 
                          $technologies=explode(",", $data->technology);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
        <div class="mb-3 col-sm-6"><label class="form-label" >Amount</label><span style="color:red"> *</span>
           
              <input class="form-control" type="text" id="amount" name="amount" value="{{$data->amount}}"  required />
            </div>
     </div>
       <div class="row gx-2">
           
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee Name</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($data->caseworkerId==$cwvalue->id)?'selected':''}}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Payment Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="paymentType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($paymenttype)){
                            foreach ($paymenttype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}">{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
      <div class="form-check"><input class="form-check-input" type="checkbox" {{ ($data->paidStatus=='Paid')?'checked':''}} name="status" />
        <label class="form-label" >Amount paid</label>
      </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="installments-edit-modal" tabindex="-1" role="dialog" aria-labelledby="album-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Installment Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>


@endsection