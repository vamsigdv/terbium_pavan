<form action="{{ route('daily_work_track.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <input class="form-control" type="hidden" id="id" name="id" value="{{$track->id}}"  required />
    <input class="form-control" type="hidden" id="id" name="old_file" value="{{ $track->track_file }}"  required />
    <div class="mb-3"><label class="form-label" >Calls Count</label>
        <input class="form-control" type="number" id="name" name="calls_count" value="{{ $track->calls_count }}" required />
    </div>
    <div class="mb-3">
        <label class="form-label" >Upload File <?php 
            if ($track->track_file != '') { ?>
               <a href="download_report/{{ $track->id }}"><span class="fas fa-file"></a>
            <?php }
        ?></label> 
        <input class="form-control" type="file" name="track_file" required accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
    </div>
    <div class="mb-3"><label class="form-label" >Date</label>
        <input class="form-control" type="date" id="password" name="created_date" value="{{ $track->date }}" required />
    </div>
    <div class="mb-3">
      <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
    </div>
</form>