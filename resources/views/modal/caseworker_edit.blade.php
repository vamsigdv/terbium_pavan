<form action="{{ route('caseworker.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
      <div class="mb-3"><label class="form-label" >Batch Title</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="name" name="name" value="{{$data->name}}"  required />
        <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}"  required />
      </div>
      
      
           <div class="row gx-2">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Batch Start Date</label><span style="color:red"> *</span>
                    <input class="form-control" type="date" id="batch_end_date" name="batch_end_date" value="{{$data->batch_end_date}}"  required />
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Batch End Date</label><span style="color:red"> *</span>
                    <input class="form-control" type="date" id="batch_start_date" name="batch_start_date" value="{{$data->batch_start_date}}" required />
                </div>
            </div>
      
       <div class="row gx-2">
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">SME Name</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="consultantName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($data->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
      
        <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class=" form-select organizerMultiple_director"  style="width:100%" name="technology[]"  data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple" >
                         <option value="">Select Option</option>
                         <?php 
                          $technologies=explode(",", $data->technologyId);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
      
      
      </div>
      
        <div class="row gx-2">
             
            <div class="mb-3 col-md-6"><label class="form-label" >Amount </label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="amount" name="amount"  value="{{$data->amount}}" required />
            </div>
        <div class="mb-3 col-md-6"><label class="form-label" >Participants </label><span style="color:red"> *</span>
          <select class="form-select select2participents" name="participants[]" id="select2participents" style="width:100%" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple">
              <?php if ($contacts) { 
                $parts = explode(",", $data->participants);
                foreach ($contacts as $_c) { ?>
                <option value="{{ $_c->id }}" <?=(in_array($_c->id, $parts)) ? 'selected' : '' ?>>{{$_c->firstname}}</option>
              <?php }} ?>
          </select>
        </div>
      </div>
        <div class="row gx-2">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Created By</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($data->employeeId==$cwvalue->id)?'selected':''}}>{{$cwvalue->
firstname}}</option>
                        <?php    }} ?>
                    </select>
                     <input class="form-control" type="hidden" id="caseworker" name="caseworker" value={{$data->employeeId}} />
              
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Assigned To</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="assigned_to">
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($data->employeeId==$cwvalue->id)?'selected':''}}>{{$cwvalue->
firstname}}</option>
                        <?php    }} ?>
                    </select>
                     <input class="form-control" type="hidden" id="caseworker" name="assigned_to" value={{ $data->employeeId }} />
                </div>
                </divv>
      
      
     
      <div class="form-check"><input class="form-check-input" type="checkbox" {{ ($data->status==1)?'checked':''}} name="status" />
        <label class="form-label" >Status</label>
      </div>
      <?php if ($view!=1) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
</form>
    
     <script>

$(document).ready(function() {
    $(".organizerMultiple_director").select2({
    dropdownParent: $("#caseworker-edit-modal")
  });
  $(".select2participents").select2({
    dropdownParent: $("#caseworker-edit-modal")
  });
});

</script>