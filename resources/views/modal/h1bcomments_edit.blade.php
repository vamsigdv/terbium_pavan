<form action="{{ route('h1bcommunicationsupdate',$data->id)}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
      <div class="mb-3">
        <label for="email">Title<span class="text-danger">*</span>:</label>
        <input type="text" name="comm_title" class="form-control" value="{{$data->title}}">
        <input class="form-control" type="hidden" id="id" name="edit_id" value="{{$data->id}}"  required />
       
      </div>
      <div class="row gx-2 remove_comments"><div class="col-sm-10"><div class="form-group"><label for="email">Comment<span class="text-danger">*</span>:</label><textarea class="form-control" name="communication[]">{{$data->comments}}</textarea></div></div><div class="col-sm-2 mt-4"><div class="form-group"></div></div></div>
                          </div>
      
      <?php if ($view!=1) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form>