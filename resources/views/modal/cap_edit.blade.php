<form action="{{ route('cap.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
 
      <div class="mb-3"><label class="form-label" >Year</label><span style="color:red"> *</span>
        <input class="form-control" type="number" id="year" name="year" value="{{$data->year}}"  required />
        <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}"  required />
       
      </div>
      
      
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>

    </form>