<form action="{{ route('sme.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
  
     <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >TID</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}" required />
              <input class="form-control" type="text" id="transactionId" name="transactionId" value="{{$getTransactions->transactionId}}" readonly />
            </div>
              <div class="mb-3 col-md-6"><label class="form-label" >Payment Status</label><span style="color:red"> *</span>
              <select class="form-select" aria-label="Default select example" name="paymentStatus" required>
                    <option value="0">Select payment status</option>
                    <?php if ($payment_statuses) { 
                        foreach ($payment_statuses as $payment) {
                    ?>
                    <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                    <?php } 
                    }
                    ?>
                    </select>
            </div>
            <div class="mb-3 col-md-6"><label class="form-label" >Reason For SME Change</label>
              <input class="form-control" type="text" id="reasonSME" name="reasonSME"  value="{{$data->reasonSME}}"  />
            </div>
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Name Of Consultant</label><span style="color:red"> *</span>
              
               <select class="form-select" aria-label="Default select example" name="contactName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($getTransactions->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >SME Agreed Payment ($)</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="agreePayment" name="agreePayment" value="{{$data->agreePayment}}" required />
            </div>
            </div>
            
            <div class="row">
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="technology"  required>
                         <option value="">Select Option</option>
                         
                         <?php 
                         
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" {{($data->technologyId==$tvalue->id)?'selected':''}}>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comment</label>
              <input class="form-control" type="text" id="name" name="comment" value="{{$data->comment}}"  />
            </div>
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >No.of Interviews held</label> 
              <input class="form-control" type="text" id="name" name="NoofInterviews" value="{{$interviews}}"  readOnly/>
            </div>
            
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Work Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="workType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($worktype)){
                            foreach ($worktype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{($data->workTypeId==$pvalue->id)?'selected':''}}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
            
            
            <div class="row">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Resources/Trainer/SME</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="SMEName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($sme)){
                            foreach ($sme as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($data->smeid==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
           <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" >
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($getTransactions->caseworkerId==$cwvalue->id)?'selected':''}}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                    <input class="form-control" type="hidden" id="caseworker" name="caseworker" value={{$getTransactions->caseworkerId}} />
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Batch</label>
                    <select class="form-select" aria-label="Default select example" name="batch" >
                         <option value="">Select Option</option>
                         <?php if(!empty($batch)){
                            foreach ($batch as $bkey => $bvalue) { ?>
                                <option value="{{$bvalue->id}}" {{($data->batchId==$bvalue->id)?'selected':''}}>{{$bvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
            </div>
      <div class="form-check"><input class="form-check-input" type="hidden" {{ ($data->status==1)?'checked':''}} name="status" />
        
      </div>
      <?php if ($view!=1) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form>