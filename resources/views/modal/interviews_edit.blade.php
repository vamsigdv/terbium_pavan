<form action="{{ route('interviews.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
  
     <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >TID</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}" required />
             <input class="form-control" type="hidden" id="id" name="contactId" value="{{$getTransactions->contactId}}" required />
              <input class="form-control" type="text" id="transactionId" name="transactionId" value="{{$getTransactions->transactionId}}" readOnly />
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >No Of Rounds</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="Noofrounds" name="Noofrounds" value="{{$data->Noofrounds}}" required />
            </div>
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Name Of Consultant</label><span style="color:red"> *</span>
              
               <select class="form-select" aria-label="Default select example" name="contactName" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($getTransactions->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
              <div class="mb-3 col-md-6"><label class="form-label" >Mode Of Interview</label><span style="color:red"> *</span>
              <select class="form-select" aria-label="Default select example" name="modeofinterview" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($interviewtools)){
                            foreach ($interviewtools as $ikey => $ivalue) { ?>
                                <option value="{{$ivalue->id}}"  {{($data->modeofinterview==$ivalue->id)?'selected':''}}>{{$ivalue->name}}</option>
                        <?php    }} ?>
                </select>
            </div>
            
            
            </div>
            
            <div class="row">
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                     <select class="form-select organizerMultiple_director" id="organizerMultiple_director" style="width:100%" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" name="technology[]" multiple="multiple" required>
                         <option value="">Select Option</option>
                         
                         <?php 
                         $technologies=explode(",", $data->technologyId);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            <div class="mb-3 col-md-6"><label class="form-label" >SME Agreed Payment</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="SMEAgreeAmount" name="SMEAgreeAmount"  value="{{$data->SMEAgreeAmount}}" required />
            </div>
            
          
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Status Of Interview</label><span style="color:red"> *</span>
              <select class="form-select" aria-label="Default select example" name="Status" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($statustype)){
                            foreach ($statustype as $skey => $svalue) { ?>
                                <option value="{{$svalue->id}}" {{($data->Status == $svalue->id) ? 'selected' : '' }} >{{ $svalue->name }}</option>
                        <?php    }} ?>
                </select>
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comment</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="comment" name="comment"  value="{{$data->comment}}" required />
            </div>
            
           
           
            </div>
            
            
            <div class="row">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">SME</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="SMEName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($sme)){
                            foreach ($sme as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($data->smeid==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Payment Status</label><span style="color:red"> *</span>
                <?php if ($payment_statuses) { ?>
                <select class="form-select" name="paymentStatus" required>
                <?php foreach ($payment_statuses as $payment_status) { ?>
                    <option value="{{ $payment_status->id }}" <?php echo ($data->paymentStatus == $payment_status->id) ? 'selected': ''; ?>>{{ $payment_status->name }}</option>
              <?php } ?> 
              </select> 
              <?php } ?>
            </div>
            
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($data->caseWorkerId==$cwvalue->id)?'selected':''}}>{{$cwvalue->
firstname}}</option>
                        <?php    }} ?>
                    </select>
                     <input class="form-control" type="hidden" id="caseworker" name="caseworker" value={{$getTransactions->caseworkerId}} />
              
                </div>
                
                
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Recruter</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="recruter">
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($data->recruterId==$cwvalue->id)?'selected':''}}>{{$cwvalue->
firstname}}</option>
                        <?php    }} ?>
                    </select>
                    <!-- <input class="form-control" type="hidden" id="Recruter" name="recruter" value={{$getTransactions->recruterId}} />-->
              
                </div>
                
                 <div class="mb-3 col-md-6"><label class="form-label" >Interview Date</label><span style="color:red"> *</span>
              <input class="form-control" type="date" id="interviewdate" name="interviewdate" value={{$data->interviewdate}}  required />
            </div>
             <div class="mb-3 col-md-6">
                      
                    <?php if (!empty($data->resume)) { ?>
                        <a href="{{ url($data->resume) }}" target="_blank" rel="noopener noreferrer" > <img src="/assets/img/team/file.png" height="140" width="120"> </a>
                    <?php } ?>
                    <input class="form-control" type="file" accept="application/pdf" name="fileinput" onchange="upload(event, 'movie_poster')">
               <label class="form-label" for="">Replace Existing File</label><span style="color:red"> *</span>
            </div>
            </div>
      
      <?php if ($view!=1 && $data->paymentStatus !=1) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form>
    
     <script>

$(document).ready(function() {
    $(".organizerMultiple_director").select2({
    dropdownParent: $("#transactions-edit-modal")
  });
  
});

</script>