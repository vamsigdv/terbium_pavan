<form action="{{ route('installment.store')}}" method="post" enctype="multipart/form-data">
@csrf
    <div class="row gx-2">
        <div class="mb-3 col-md-6">
            <input class="form-control" type="hidden" id="id" name="transactionId" value="{{$data->id}}" required />
            <input class="form-control" type="hidden" id="id" name="contactId" value="{{$data->contactId}}" required />
            <input class="form-control" type="hidden" id="id" name="consultantName" value="{{ $data->consultantName }}" required />
            <label class="form-label" for="">Consultant Name</label><span style="color:red"> *</span>
            <input type="text" disabled value="{{ $data->consultantName }}" class="form-control" />
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" >Date</label><span style="color:red"> *</span>
            <input class="form-control" type="date" id="date" name="date" value="{{$data->date}}"  required />
        </div>
    </div>
    <div class="row gx-2">
        <div class="mb-3 col-md-6">
            <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
            <select class="form-select" id="select2insidemodal" style="width:100%" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" name="technology[]" multiple="multiple">
                 <option value="">Select Option</option>
                 <?php 
                  $technologies=explode(",", $data->technology);
                 if(!empty($technology)){
                    foreach ($technology as $tkey => $tvalue) { ?>
                        <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies)) ? 'selected':'' ?>>{{ $tvalue->name }}</option>
                <?php    }} ?>
            </select>
        </div>
        <div class="mb-3 col-sm-6">
            <label class="form-label" >Amount</label><span style="color:red"> *</span>
            <input class="form-control" type="text" id="amount" name="amount" value="{{$data->amount}}"  required />
        </div>
    </div>
    <div class="row gx-2">
        <div class="mb-3 col-md-6">
            <label class="form-label" for="">Employee Name</label><span style="color:red"> *</span>
            <select class="form-select" aria-label="Default select example" name="caseworker" disabled>
                 <option value="">Select Option</option>
                 <?php if(!empty($caseworker)){
                    foreach ($caseworker as $cwkey => $cwvalue) { ?>
                        <option value="{{$cwvalue->id}}" {{($data->caseworkerId==$cwvalue->id)?'selected':''}}>{{$cwvalue->firstname}}</option>
                <?php    }} ?>
            </select>
        </div>
        <div class="mb-3 col-md-6">
            <label class="form-label" for="">Mode of the payment</label><span style="color:red"> *</span>
            <select class="form-select" aria-label="Default select example" name="paymentType" required>
                 <option value="">Select Option</option>
                 <?php if(!empty($paymenttype)){
                    foreach ($paymenttype as $pkey => $pvalue) { ?>
                        <option value="{{$pvalue->id}}">{{$pvalue->name}}</option>
                <?php    }} ?>
            </select>
        </div>
    </div>
    <div class="row gx-2">
        <div class="mb-3 col-md-6">
            <label class="form-label" for="">Payment Status</label><span style="color:red"> *</span>
            <select class="form-select" aria-label="Default select example" name="status" required>
            <option value="0">Select payment status</option>
            <?php if ($payment_status) { 
                foreach ($payment_status as $payment) {
            ?>
            <option value="{{ $payment->id }}">{{ $payment->name }}</option>
            <?php } 
            }
            ?>
            </select>
        </div>
        <div class="mb-3 col-md-6">
            <label class="form-label" for="">Refund Amount</label><span style="color:red"> *</span>
            <input type="number" name="refund_amount" class="form-control">
        </div>
    </div>
    <div class="row gx-2">
        <div class="mb-3 col-md-12">
            <label class="form-label" for="">Comments</label><span style="color:red"> *</span>
            <textarea class="form-control" rows=5 cols=10 name="comments">{{ $data->comments }}</textarea>
        </div>
    </div>
    <div class="mb-3">
      <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
    </div>
</form>
<script>
$(document).ready(function() {
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
});
</script>