<form action="{{ url('update_city/' . $city->id)}}" method="post" enctype="multipart/form-data">
    {{ method_field('POST') }}
    @csrf
      <div class="mb-3">
        <label class="form-label" >City Name</label>
        <input class="form-control" type="text" id="country_name" name="name" value="{{ $city->name }}" />
      </div>
      <div class="mb-3"><label class="form-label" >State name</label><span style="color:red"> *</span>
        <select class="form-control" name="state_id">
            <?php foreach ($states as $state) {?>
                <option value="{{ $state->id }}" <?php echo ($state->id == $city->state_id) ? 'selected': '' ?> >{{ $state->name }}</option>
            <?php } ?>
        </select>
      </div>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
</form>