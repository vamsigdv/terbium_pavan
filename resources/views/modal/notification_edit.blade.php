<form action="{{ route('notifications.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
      <div class="mb-3">
        <label class="form-label" >Title</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="title" name="title" value="{{ $data->title }}" required />
      </div>
      <div class="mb-3"><label class="form-label" >Image</label><span style="color:red"> *</span>
        <?php if (!empty($data->image)) { ?>
          <a href="{{ url($data->image) }}" target="_blank" rel="noopener noreferrer"> <span class="nav-link-icon"> <span class="far fa-file-image"></span></span> </a>
        <?php } ?>
        <input class="form-control" type="file" accept="image/*" id="image" name="image"  />
        <input class="form-control" type="hidden" id="id" name="id" value="{{ $data->id }}"  required />
        <input class="form-control" type="hidden" id="photo" name="photo_old" value="{{ $data->image }}"  required />
      </div>
      <div class="mb-3">
        <label class="form-label" >Message</label>
        <textarea class="form-control description" name="description" id="" rows="1">{{ $data->message }}</textarea>
      </div>
      <div class="mb-3">
        <label class="form-label" >Type</label><span style="color:red"> *</span>
        <select class="form-control" name="type">
            <option value="Stories" <?php echo ($data->type == 'Stories') ? 'selected' : ''; ?>>Stories</option>
            <option value="Schemes" <?php echo ($data->type == 'Schemes') ? 'selected' : ''; ?>>Schemes</option> 
            <option value="Music" <?php echo ($data->type == 'Music') ? 'selected' : ''; ?>>Music</option>
            <option value="Short film" <?php echo ($data->type == 'Short film') ? 'selected' : ''; ?>>Short film</option>
            <option value="Movies" <?php echo ($data->type == 'Movies') ? 'selected' : ''; ?>>Movies</option>
            <option value="Video Bytes" <?php echo ($data->type == 'Video Bytes') ? 'selected' : ''; ?>>Video Bytes</option>
            <option value="Influencer" <?php echo ($data->type == 'Influencer') ? 'selected' : ''; ?>>Influencer</option>
        </select>
      </div> 
      <div class="mb-3">
        <label class="form-label" >Select Videos</label><span style="color:red"> *</span>
        <select class="form-control" name="movie_id" required>
            <option>Select Movie</option>
            <?php if (!empty($movies)) { 
                foreach ($movies as $movie)  { ?>
                <option value="{{ $movie->id }}" required <?php echo ($movie->id == $data->movie_id) ? 'selected' : ''; ?>>{{ $movie->name }}</option>
            <?php } } else { ?>
                <option>Select Movie</option>
            <?php }?>          
        </select>
      </div>
      <div class="mb-3"><label class="form-label" >Date</label><span style="color:red"> *</span>
        <input class="form-control" type="date" id="phone" name="date_time" value='{{ str_replace(" 00:00:00", "", $data->date_time) }}' required />
      </div>
      <div class="mb-3">
        <label class="form-label" >External Link</label>
        <textarea class="form-control" name="external_link">{{ $data->external_link }}</textarea>
      </div>    
      
      <div class="form-check"><input class="form-check-input" type="checkbox" {{ ($data->status==1)?'checked':'' }} name="status" />
        <label class="form-label" >Status</label>
      </div>
      <?php if ($view!=1) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form>