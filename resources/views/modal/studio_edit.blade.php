<form action="{{ route('studio.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    
    <?php 
  //  print_r();exit;
    if(!(Illuminate\Support\Str::contains(url()->previous(), 'payment'))){?>
      <div class="mb-3"><label class="form-label" >Studio Name / channel name / User name</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="studio_name" name="studio_name" value="{{$data->studio_name}}"  required />
        <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}"  required />
        <input class="form-control" type="hidden" id="id" name="admin_id" value="{{$data->admin_id}}"  required />
        <input class="form-control" type="hidden" id="photo" name="photo_old" value="{{$data->photo}}"  required />
      </div>
      <div class="row gx-2">
        <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
          <input class="form-control" type="text" id="first_name" name="first_name"  value="{{$data->first_name}}"  required />
        </div>
        <div class="mb-3 col-sm-4">
          <label class="form-label" >Middle Name</label>
          <input class="form-control" type="text" id="middle_name" name="middle_name"  value="{{$data->middle_name}}" />
        </div>
        <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
          <input class="form-control" type="text" id="last_name" name="last_name"  value="{{$data->last_name}}" required />
        </div>
      </div>
      <div class="row gx-2">
      <div class="mb-3 col-sm-6"><label class="form-label" >Mobile Number</label><span style="color:red"> *</span>
        <input class="form-control" type="number" id="contact" name="contact"  value="{{$data->contact}}" required />
      </div>
      <div class="mb-3 col-sm-6"><label class="form-label" >Email</label><span style="color:red"> *</span>
        <input class="form-control" type="email" id="email" name="email"  value="{{$data->email}}" required />
      </div>
    </div>
    
    
        <div class="row gx-2">
      <div class="mb-3 col-sm-6">
        <label class="form-label" >Photo/logo</label><span style="color:red"> *</span>
        <?php if (!empty($data->photo)) { ?>
          <a href="{{ url($data->photo) }}" target="_blank" rel="noopener noreferrer"> <span class="nav-link-icon"> <span class="far fa-file-image"></span></span> </a>
        <?php } ?>
          <input class="form-control" type="file" accept="image/*" id="photo" name="photo" />
      </div>

      <div class="mb-3 col-sm-6"><label class="form-label" >Password</label><span style="color:red"> *</span>
        <input class="form-control" type="password" id="password" name="password" value="{{$data->password}}"  required/>
      </div>
    </div>
    
    
    

    <div class="mb-3"><label class="form-label"  >Address </label>
      <textarea class="form-control" id="address" rows="1" name="address"> {{$data->address}}</textarea>
    </div>
    <div class="row gx-2">
      <div class="mb-3 col-sm-4"><label class="form-label" >Aadhaar number</label>
        <input class="form-control" type="text" id="Aadhaar" name="Aadhaar"  value="{{$data->vouchar}}" />
      </div>
      <div class="mb-3 col-sm-4"><label class="form-label" >Voter ID</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="voter" name="voter"  value="{{$data->voter}}" required />
      </div>
      
      <?php }
      
      if((Illuminate\Support\Str::contains(url()->previous(), 'payment')) || Auth::guard('adminiy')->user()->role == 'admin'){?>
       <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}"  required />
        <input class="form-control" type="hidden" id="id" name="admin_id" value="{{$data->admin_id}}"  required />
         <div class="mb-3 col-sm-4"><label class="form-label" >Bank Account Number</label><span style="color:red"> *</span>
        <input class="form-control" type="number" id="account_number" name="account_number"  value="{{$data->account_number}}" required />
      </div>
    </div>
    <div class="row gx-2">
   
  
    </div>
    
    
    <div class="row gx-2">
            <div class="mb-3 col-sm-4"><label class="form-label" >IFSC Code</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="ifsc" name="ifsc"  value="{{$data->ifsc}}"required />
      </div>
      
    <?php } if(!(Illuminate\Support\Str::contains(url()->previous(), 'payment'))){?>
      <div class="mb-3 col-sm-4"><label class="form-label" >City</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="district" name="district"  value="{{$data->district}}" required />
      </div>
      <div class="mb-3 col-sm-4"><label class="form-label" >Pin code</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="village" name="village"  value="{{$data->village}}" required />
      </div>
    </div>
    
    

     <div class="row gx-2">
      <div class="mb-3 col-sm-6"><label class="form-label" >Wallet</label>
        <input class="form-control" type="text" id="vouchar" name=""  value="{{$data->wallet}}" readonly />
      </div>
      <div class="mb-3 col-sm-6"><label class="form-label" >Earings</label>
        <input class="form-control" type="text" id="voter" name=""  value="{{$data->earings}}" readonly />
      </div>
      <div class="mb-3 col-sm-12"><label class="form-label" >DOB</label><span style="color:red"> *</span>
                                <input class="form-control" type="date"  id="dob" name="dob"  value="{{$data->dob }}" />
                            </div>
    </div>
      <div class="form-check"><input class="form-check-input" type="checkbox" {{ ($data->status==1)?'checked':''}} name="status" />
        <label class="form-label" >Status</label>
      </div>
      
      <?php } if ($view !=1 && ((Illuminate\Support\Str::contains(url()->previous(), 'payment')) || Auth::guard('adminiy')->user()->role == 'admin')){ ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form> 
    
    <script>


    $(function(){
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;    
    $('#dob').attr('max', maxDate);
});
</script>
