<form action="{{ url('update_sub_district/' . $subdistrict->id)}}" method="post" enctype="multipart/form-data">
    {{ method_field('POST') }}
    @csrf
      <div class="mb-3">
        <label class="form-label" >Sub District Name</label>
        <input class="form-control" type="text" id="country_name" name="district_name" value="{{ $subdistrict->district_name }}"/>
      </div>
      <div class="mb-3"><label class="form-label" >District name</label><span style="color:red"> *</span>
        <select class="form-control" name="district_id">
            <?php foreach ($dists as $dist) {?>
                <option value="{{ $dist->id }}" <?php echo ($subdistrict->district_id == $dist->id) ? 'selected': '' ?> >{{ $dist->district_name }}</option>
            <?php } ?>
        </select>
      </div>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
</form>
    