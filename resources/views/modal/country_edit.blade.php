<form action="{{ route('country.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
      <div class="mb-3">
        <label class="form-label" >Country Name</label>
        <input class="form-control" type="text" id="country_name" name="country_name" value="{{ $data->country_name }}"/>
      </div>
      <div class="mb-3"><label class="form-label" >Country Code</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="phone" name="country_code" value="{{$data->country_code}}" required />
      </div>
      
      <div class="mb-3"><label class="form-label" >Phone Code</label><span style="color:red"> *</span>
            <input class="form-control" type="text" id="phone" name="phone_code" value="{{$data->phonecode}}" required />
      </div>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
</form>
    