<form action="{{ route('termscondition.update')}}" method="post" enctype="multipart/form-data">

  @csrf
      <div class="mb-3">
       
        <input class="form-control" type="hidden"value="{{$data->id}}" name="id" />

      </div>
      <div class="mb-3">
        <label class="form-label" >Terms and Condition </label><span style="color:red">*</span>

         <textarea class="form-control description" rows='6' name="comments">{{$data->text}}</textarea>
      </div>
     
       
      <?php if ($view!=1 && (Auth::guard('adminiy')->user()->role == "admin" || Auth::guard('adminiy')->user()->role == "financial team")) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form>
    <script>
    //   tinymce.init({
    //             selector: '.description',
    //             plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
    //             toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
    //           });
    </script>