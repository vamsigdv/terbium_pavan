<form action="{{ route('states.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <div class="mb-3">
                    <label class="form-label" for="">Location</label><span style="color:red"> *</span>
                   <select class="form-select" aria-label="Default select example" name="locationName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($location)){
                            foreach ($location as $lkey => $lvalue) { ?>
                                <option value="{{$lvalue->id}}" {{ ($data->locationId==$lvalue->id)?'selected':'' }}>{{$lvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
      <div class="mb-3">
          <label class="form-label" >State Name</label><span style="color:red"> *</span>
        <input class="form-control" type="text" id="name" name="name" value="{{$data->name}}"  required />
        <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}"  required />
       
      </div>
     
      <div class="form-check"><input class="form-check-input" type="checkbox" {{ ($data->status==1)?'checked':''}} name="status" />
        <label class="form-label" >Status</label>
      </div>
      <?php if ($view!=1) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form>