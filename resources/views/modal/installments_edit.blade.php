<form action="{{ route('installment.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <div class="row gx-2">
        <div class="mb-3 col-md-6">
            <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}" required />
                    <label class="form-label" for="">Consultant Name</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="consultantName" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($data->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
        <div class="mb-3 col-sm-6"><label class="form-label" >Date</label><span style="color:red"> *</span>
           
              <input class="form-control" type="date" id="date" name="date" value="{{$data->date}}"  required />
            </div>
     </div>
     <div class="row gx-2">
        <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select organizerMultiple_director" id="organizerMultiple_director" style="width:100%" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" name="technology[]" multiple="multiple" disabled>
                         <option value="">Select Option</option>
                         <?php 
                          $technologies=explode(",", $tras->technology);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
        <div class="mb-3 col-sm-6"><label class="form-label" >Amount ($)</label><span style="color:red"> *</span>
           
              <input class="form-control" type="text" id="amount" name="amount" value="{{$data->amount}}" readOnly/>
            </div>
     </div>
       <div class="row gx-2">
           
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee Name</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($tras->caseworkerId==$cwvalue->id)?'selected':''}}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                 <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Payment Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="paymentType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($paymenttype)){
                            foreach ($paymenttype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{($data->paymentType==$pvalue->id)?'selected':''}}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
               
            </div>
            <div class="row gx-2">
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Payment Status</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="status" id="pstatus" required >
                    <option value="0">Select payment status</option>
                    <?php if ($payment_status) { 
                        foreach ($payment_status as $payment) {
                    ?>
                    <option value="{{ $payment->id }}" {{ ($data->paidStatus == $payment->id) ? 'selected' : '' }}>{{ $payment->name }}</option>
                    <?php } 
                    }
                    ?>
                    </select>
                </div>
                
                 <div class="mb-3 col-md-6 partial">
                    <label class="form-label" for="">Due($)</label><span style="color:red"> *</span>
                    <input type="number" name="due" value="{{$data->due}}" class="form-control">
                </div>
                
                 <div class="mb-3 col-md-6 partial">
                    <label class="form-label" for="">Next Commited Date </label><span style="color:red"> *</span>
                    <input type="date" name="nextcommiteddate" value="{{$data->nextcommiteddate}}" class="form-control">
                </div
                
                <?php if($type == 'sme'){ ?>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Work Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="workType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($worktype)){
                            foreach ($worktype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}"  {{ ($data->type == $pvalue->id) ? 'selected' : '' }}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                     <?php }else{ ?>
                     <input class="form-control" type="hidden" id="workType" name="workType" value=""/>
                     
                     <?php }?>
            </div>
            <div class="row gx-2">
                <div class="mb-3 col-md-12">
                    <label class="form-label" for="">Comments</label><span style="color:red"> *</span>
                    <textarea class="form-control" rows=5 cols=10 name="comments">{{ $data->comments }}</textarea>
                </div>
            </div>
      <?php if ($view!=1) { ?>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
      <?php } ?>
    </form>
    <script>

$(document).ready(function() {
    $(".organizerMultiple_director").select2({
    dropdownParent: $("#transactions-edit-modal")
  });
  var status = $('#pstatus').val();
  if(status == 4)
  {
        $('.partial').show();
  }
  else
  {
      $('.partial').hide();
  }
  $('#pstatus').on('change',function(){
       var status = $('#pstatus').val();
      // alert(status);
       if(status == 4)
       {
           $('.partial').show();
       }
       else
       {
           $('.partial').hide();
       }
  })
});

</script>