<form action="{{ url('studios/earning_update') }}/{{ $earning->id }}" method="post" enctype="multipart/form-data">
       @csrf
      <div class="mb-3"><label class="form-label" >Studio Name</label>
        <input class="form-control" type="text" id="number" name="uname" value="{{ $earning->studio->studio_name }}"  required disabled/>
      </div>
      <div class="mb-3"><label class="form-label" >Coins</label>
      <input class="form-control" type="number" id="number" name="coins" value="{{ $earning->coins }}"  required />
      </div>
      <div class="mb-3"><label class="form-label" >Type</label>
      <input class="form-control" type="text" id="number" name="type" value="{{ $earning->type }}"  required disabled/>
      </div>
    <div class="mb-3">
        <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
    </div>
    </form>