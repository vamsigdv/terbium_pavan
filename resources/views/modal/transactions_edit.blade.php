<form action="{{ route('transactions.update','1')}}" method="post" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
     <div class="row gx-2">
            <div class="mb-3 col-sm-6"><label class="form-label" >Date</label><span style="color:red"> *</span>
            <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}" required />
              <input class="form-control" type="date" id="date" name="date" value="{{$data->date}}"  required />
            </div>
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select organizerMultiple_director" id="organizerMultiple_director" style="width:100%" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" name="technology[]" multiple="multiple" required>
                         <option value="">Select Option</option>
                         <?php 
                          $technologies=explode(",", $data->technology);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
            <div class="row gx-2">
           
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Consultant Name</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="consultantName" disabled>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($data->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Payment Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="paymentType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($paymenttype)){
                            foreach ($paymenttype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{($data->paymentTypeId==$pvalue->id)?'selected':''}}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
            <div class="row gx-2">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Transaction ID</label><span style="color:red"> *</span>
                    <input class="form-control" type="text" id="transactionId" name="transactionId" value="{{$data->transactionId}}" required />
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Paying Now ($)</label><span style="color:red"> *</span>
                    <input class="form-control" type="text" id="payingNow" name="payingNow" value="{{$data->payingNow}}" required />
                </div>
            </div>
            <div class="row gx-2">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Due ($)</label><span style="color:red"> *</span>
                    <input class="form-control" type="text" id="Due" name="Due" value="{{$data->Due}}"  required />
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Next Commitment Date</label><span style="color:red"> *</span>
                    <input class="form-control" type="date" id="nextcomitdate" name="nextcomitdate" value="{{$data->nextcomitdate}}" required />
                </div>
            </div>
           
           <div class="row gx-2">
           
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Created By</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" {{ (Auth::guard('adminiy')->user()->role != 'Super Admin')? 'disabled':'' }}>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($data->caseworkerId==$cwvalue->id)?'selected':''}}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                    
                     <input class="form-control" type="hidden"  name="caseworker" value="{{$data->caseworkerId}}">
                </div>
               
               
               <div class="mb-3 col-md-6"><label class="form-label" >Payment Status</label><span style="color:red"> *</span>
                <?php if ($payment_statuses) { ?>
                <select class="form-select" name="paymentStatus" required>
                <?php foreach ($payment_statuses as $payment_status) { ?>
                    <option value="{{ $payment_status->id }}" <?php echo ($data->paymentStatus == $payment_status->id) ? 'selected': ''; ?>>{{ $payment_status->name }}</option>
              <?php } ?> 
              </select> 
              <?php } ?>
            </div>
            </div>
     
      <div class="form-check"><input class="form-check-input" type="hidden" {{ ($data->status==1)?'checked':''}} name="status" />
      
      </div>
      <?php if ($view!=1) { ?>
        <div class="mb-3">
           </div>
      <?php } ?>
    </form>
     <script>

$(document).ready(function() {
    $(".organizerMultiple_director").select2({
    dropdownParent: $("#transactions-edit-modal")
  });
  
});

</script>