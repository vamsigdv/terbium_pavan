@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card">
        <div class="modal-body py-4 px-5">
          <form action="{{ route('employee.update','1')}}" method="post" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            @csrf
            
              <div class="col-auto ms-auto" style="float:right">
            <div>
               <a href="{{ url('payment') }}" class="btn btn-falcon-success btn-sm" ><i class="fa fa-money-bill-alt" aria-hidden="true"></i> Payment Settings</a>
                </div>
            </div>
            
            <div class="mb-3 "><label class="form-label" >Email</label><span style="color:red"> *</span>
              <input class="form-control" type="email" id="edit_email" name="edit_email" value="{{$data->email}}"  required readonly="true" />
            </div>
            <div class="row gx-2">
              <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
              <input class="form-control" type="hidden" id="edit_id" name="edit_id" value="{{$data->id}}"   required />
              <input class="form-control" type="hidden" id="admin_id" name="admin_id"  value="{{$data->admin_id}}"   required />
                <input class="form-control" type="text" id="edit_first_name" name="edit_first_name"  value="{{$data->first_name}}" required />
              </div>
              <div class="mb-3 col-sm-4">
                <label class="form-label" >Middle Name</label>
                <input class="form-control" type="text" id="edit_middle_name" name="edit_middle_name" value="{{$data->middle_name}}"  />
              </div>
              <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
                <input class="form-control" type="text" id="edit_last_name" name="edit_last_name" value="{{$data->last_name}}" required />
              </div>
            </div>
       
       
           <div class="row gx-2">
            <div class="mb-3 col-sm-4"><label class="form-label" >Voter ID</label>
              <input class="form-control" type="text" id="edit_voter" name="edit_voter"  value="{{$data->voter}}"/>
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >Account Number</label>
              <input class="form-control" type="number" id="edit_account_number" name="edit_account_number" value="{{$data->account_number}}" />
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >IFSC Code</label>
              <input class="form-control" type="text" id="edit_ifsc" name="edit_ifsc" value="{{$data->ifsc}}" />
            </div>
               <div class="mb-3 col-sm-4"><label class="form-label" >Aadhar Number</label>
              <input class="form-control" type="number" id="edit_adhar" name="edit_adhar" value="{{$data->adhar}}" />
            </div>
                 <div class="mb-3 col-sm-4"><label class="form-label" >Mobile Number</label><span style="color:red"> *</span>
              <input class="form-control" type="number" id="edit_contact" name="edit_contact" value="{{$data->contact}}" required />
            </div>
           
             <div class="mb-3 col-sm-4"><label class="form-label" >Password</label><span style="color:red"> *</span>
        <input class="form-control" type="password" id="password" name="edit_password" required value="{{((Auth::guard('adminiy')->user()->showPassword1))}}"/>
      </div>
       <div class="mb-3 col-sm-4">
            <label class="form-label" ></label>
            <br>
            <button class="btn btn-primary" type="button" id="btn">Show</button>
        </div>
              </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
        </div>
   </div>
   <script>

    $('#btn').click(function(){

        var paswd= $('#password');
        if(paswd.attr("type")== "password"){

            paswd.attr("type","text");

            $('#btn').text("hide");

        }
        else{
                paswd.attr("type","password");

                $('#btn').text("show");
        }
    })
</script>
@endsection