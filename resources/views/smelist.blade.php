@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">SME List</h5>
          
        </div>
        
      
        
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      @if(session('success'))
      <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
      <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
      <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
      @if(session('failed'))
      <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
      <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
      <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
        <div id="consultant" class="tabcontent_contact">
          <table class="table table-bordered smelist_datatable table-striped fs--1 mb-0" style="width:100%">
            <thead class="bg-200 text-900">
              <tr>
                <th >#</th>
                <th >SME Name</th>
                <th >Mobile Number</th>
                <th >Gender</th>
                <th >Date Enrolled</th>
                <th >Commited Amount</th>
                <th >Paid Amount</th>
                <th >Due Amount</th>
                <th >State</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
       
    </div>
  </div>
   
@endsection