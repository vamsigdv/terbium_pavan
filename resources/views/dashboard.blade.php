
@extends('dashboard_layouts.adminmaster')
<style>
.marT20{ margin-top:20px;}
.smalcards .card{height:100%; }
.smalcards .col-md-3{ margin-bottom: 10px;}
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}

   .text-700 {
    color: #5e6e82!important;
}
.mb-2, .my-2 {
    margin-bottom: 0.5rem!important;
}
.align-items-center {
    -webkit-box-align: center!important;
    -ms-flex-align: center!important;
    align-items: center!important;
}
.media {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start
}
.svg-inline--fa.fa-w-16 {
    width: 1em;
}
svg:not(:root).svg-inline--fa {
    overflow: visible;
}
.media-body {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
@media (min-width: 992px)
.pl-lg-3, .px-lg-3 {
    padding-left: 1rem!important;
}
.mr-2, .mx-2 {
    margin-right: 0.5rem!important;
}
.btn-falcon-primary:focus:not(.disabled):not(:disabled),.btn-falcon-primary:hover:not(.disabled):not(:disabled) {
    color: #1966cc!important;
    -webkit-box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08);
    box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08)
}
.pl-3, .px-3 {
    padding-left: 1rem!important;
}
.pr-3, .px-3 {
    padding-right: 1rem!important;
}
var avgMin
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}


</style>

 @section('content')
 
 <?php if (Auth::guard('adminiy')->user()->role == "admin" || Auth::guard('adminiy')->user()->role == 'employee')
 {
     $smovie = "/movie";
     $pendingmovies = "/pendingmovies";
     $sshortfilms = "/shortfilms";
     $pendingshortfilms = "/pendingshortflims";
     $sinfluencer = "/influncer";
     $pendinginfluencer  = "/pendinginfluncer";
      $pendingmusicvideourl  = "/pendingmusicvideo";
     $smusicvideo = "/musicvideo";
     $smoviecreate = "/movie/create";
     $sshortfilmscreate = "/shortfilms/create";
     $sinfluencercreate = "/influncer/create";
     $smusicvideocreate = "/musicvideo/create";
 }
 else
 {
     $smovie = "/smovie";
     $pendingmovies = "/smovie";
     $sshortfilms = "/sshortfilms";
     $pendingshortfilms = "/sshortfilms";
     $pendinginfluencer  = "/sinfluncer";
     $pendingmusicvideourl  = "/smusicvideo";
     $sinfluencer = "/sinfluncer";
     $smusicvideo = "/smusicvideo";
     $smoviecreate = "/smovie/create";
     $sshortfilmscreate = "/studioshortfilms/create";
     $sinfluencercreate = "/studioinfluncercreate";
     $smusicvideocreate = "/smusicvideocreate";
 }
 ?>
 
 
 </br>
 
   <div class="card mb-3">
        <div class="card-deck">
            <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 On
Boarding<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$movie}}
                </div>
               
              </div>
            </div>
            <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>Consultants Cleared Interview<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                  {{$shortfilm}}
                </div>
                    </div>
            </div>
             <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 Interview Pending Candidates<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$musicvideo}}
                </div>
              
              </div>
            </div>
             
          
             <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                Amount Received From Consultants<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$schemes}}
                </div>
             
              </div>
            </div>
              <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                SME
Payments<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$stories}}
                </div>
                 </div>
            </div>
           
            
 </div>
        <div class="card-deck">
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 SME Payments
Yet to Pay<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$movie}}
                </div>
               
              </div>
            </div>
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>SME
Associated<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                  {{$shortfilm}}
                </div>
                    </div>
            </div>
             <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 Technologies
Covered<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$musicvideo}}
                </div>
              
              </div>
            </div>
             
          
             <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
               Current
Marketing Bench<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$schemes}}
                </div>
             
              </div>
            </div>
              <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                Consultants
Zero Interviews<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$stories}}
                </div>
                 </div>
            </div>
           
            
 </div>
        <div class="card-deck">
            <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 Average
No. of Interviews to close<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$movie}}
                </div>
               
              </div>
            </div>
            <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>Average Amount
to Clear the Interview<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                  {{$shortfilm}}
                </div>
                    </div>
            </div>
             <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 Average
days to close the Interview<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$musicvideo}}
                </div>
              
              </div>
            </div>
             
          
             <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                Consultant
Due Amount<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$schemes}}
                </div>
             
              </div>
            </div>
              <div class="card mb-4  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                Age of the Consultant
(On Boarded date to Till Date)<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$stories}}
                </div>
                 </div>
            </div>
           
            
 </div>
   </div>
@endsection
