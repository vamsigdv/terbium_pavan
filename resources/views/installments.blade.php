@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
      
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Transaction  Installments</h5>
          
        </div>
         <div class="col-auto ms-auto">
            <div>
                <?php if (Auth::guard('adminiy')->user()->role != "studio") {?>
                <a href="" rel="noopener noreferrer"><button data-id = "{{$transactionId}}" class="btn btn-falcon-success btn-sm complete"  type="button"><span class="ms-1">Complete</span></button></a>
                <a href="/transactions" rel="noopener noreferrer"><button class="btn btn-falcon-success btn-sm"  type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Transactions</span></button></a>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#installment-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Add New</span></button>
                 <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#upload-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Upload Excel</span></button>
                  <?php }else{?>
                   <a href="/transactions" rel="noopener noreferrer"><button class="btn btn-falcon-success btn-sm"  type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Transactions</span></button></a>
                  <?php }?>
            </div>
        </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
            <div class="row col-md-12 inputs">
                <form class="form-inline" action="">
                    <label for="email">From date:</label>
                    <input type="date" id="min" placeholder="Enter email" name="min">
                    <label for="pwd">To date:</label>
                    <input type="date" id="max" placeholder="Enter password" name="max">
                    <button type="button" class="buttoninstallment">Submit</button>
                </form>
            </div>
            <!--<table class="inputs">
        <tbody><tr>
            <td>From date:</td>
            <td><input type="date" id="min" name="min"></td>
            
        </tr>
        <tr>
            <td>T age:</td>
            <td><input type="date" id="max" name="max"></td>
            
        </tr>
        <tr>
            <td>
                <td><button type="button" class="buttoninstallment" >Submit</button></td>
            </td></tr>
    </tbody></table>-->
              <table class="table table-bordered installmentstransactions_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th>orderid</th>
                    <th>Name</th>
                    <th>Amount($) </th>
                    <th>Payment Month </th>
                    <th>Payment Status</th>
                    <th>Action</th>
                    <th>Comments</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
<div class="modal editmodal fade bd-modal-lg" id="installment-modal" tabindex="-1" role="dialog" aria-labelledby="pendingmovie-edit-modal-label" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content border-0">
    <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
      <div class="position-relative z-index-1 light">
        <h4 class="mb-0 text-white" id="authentication-modal-label">Installment Add</h4>
      </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <?php 
   
        $id = request()->route('id');
       
        $data = App\Models\transactioninstallments::select('transactioninstallments.id','transactioninstallments.installmentamount','transactioninstallments.duration','transactioninstallments.paymentmonth','movies.name','movies.id','transactioninstallments.payment_status', 'VID')
              ->leftjoin('movies', 'movies.id', '=', 'transactioninstallments.VID')
              ->where('orderId', request()->route('id'))
              ->first();
            if(empty($data))
            {
               $data =  App\Models\Transactions::leftjoin('movies', 'movies.id', '=', 'transactions.movie_id')->where('transactions.id', request()->route('id'))
              ->first();    
            }
    ?>
 
    <div class="modal-body py-4 px-5">
      <form action="{{ url('add_installment/' . $id) }}" method="POST" enctype="multipart/form-data">
        @csrf 
         
            <div class="mb-3">
                 <input class="form-control" type="hidden" value="{{ $data->VID }}" name="movie_id"/>
                <label class="form-label" >Name </label><span style="color:red">*</span>
                <input class="form-control" value="{{ $data->name }}" type="text"readonly />
            </div>
            <div class="mb-3"><label class="form-label" >Amount($)</label><span style="color:red">*</span>
                <input class="form-control" type="number" value="" name="installmentamount" required />
            </div>
            <div class="mb-3"><label class="form-label" >orderId</label><span style="color:red">*</span>
            <input class="form-control" type="text" value="{{ $id }}" name="orderId" readonly />
          </div>
          <div class="mb-3"><label class="form-label" >Duration</label><span style="color:red">*</span>
            <input class="form-control" type="number" value="" name="duration" readonly />
          </div>
          <div class="mb-3"><label class="form-label">Paymentmonth</label><span style="color:red">*</span>
            <input class="form-control" type="date" name="paymentmonth" id="release_date" required>
          </div>
          <div class="mb-3 ">
            <label class="form-label" for="">Transaction id</label><span style="color:red">*</span>
              <input class="form-control" type="text"  name="transactionId"  />
          </div>
          <div class="mb-3 ">
            <label class="form-label" >Status</label><span style="color:red">*</span>
            <select class="form-select" aria-label="Default select example" name="status">
                <option value="1">Paid</option>
                <option value="0">Unpaid</option>
                <option value="2">Rejected</option>
            </select>
        </div>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
     </form>
    </div>
   
   </div>
  </div>
</div>

<div class="modal editmodal fade bd-modal-lg" id="installment-edit-modal" tabindex="-1" role="dialog" aria-labelledby="pendingmovie-edit-modal-label" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content border-0">
    <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
      <div class="position-relative z-index-1 light">
        <h4 class="mb-0 text-white" id="authentication-modal-label">Installment Edit</h4>
      </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body py-4 px-5">
      <div id="edit_data"></div>
   </div>
  </div>
</div>


</div>
 <div class="modal editmodal fade bd-modal-lg" id="upload-modal" tabindex="-1" role="dialog" aria-labelledby="pendingmovie-edit-modal-label" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content border-0">
    <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
      <div class="position-relative z-index-1 light">
        <h4 class="mb-0 text-white" id="authentication-modal-label">Export data</h4>
      </div>
      <button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    
    <div class="modal-body py-4 px-5">
      <form action="" method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)" >
        @csrf 
         
            <div class="mb-3">
                 <input class="form-control" type="hidden" value="{{$transactionId}}" name="transaction_id"/>
                <label class="form-label" >Upload </label><span style="color:red">*</span>
                <input class="form-control" value="" type="file" name="exce_upload" />
            </div>
            <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit"  name="submit">Submit</button>
        </div>
       
     </form>
    </div>
   
   </div>
  </div>
</div>
<script type="text/javascript">
      
    $(document).ready(function (e) {
   
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
  
  
        $('#laravel-ajax-file-upload').submit(function(e) {
            e.preventDefault();
  
            var formData = new FormData(this);
  
            $.ajax({
                type:'POST',
                url: "{{ url('installmentsexcel')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    this.reset();
                    alert('Updated successfully');
                  /* location.reload(true);*/
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
        
        $('.complete').click(function(e) {
            e.preventDefault();
            var dataString = 'id='+ $(this).attr("data-id");
            $.ajax({
                type:'POST',
                url: "{{ route('updatetranstatus') }}",
                data: dataString,
                cache:false,
               
                success: (data) => {
                   window.location.reload();
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
    });
  
</script>

@endsection
