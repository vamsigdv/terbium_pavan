@include('dashboard_layouts.head')
@include('dashboard_layouts.sidebar')
<style>
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}

</style>
<div class="card-header bg-light">
              <h5 class="mb-0">Pending payment</h5>
            </div>
<div class="card-deck">
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 Pending MP3, Music Video, Lifestyle & Infotainment Transactions<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$pendingstudiotransaction}}
                </div>
               <a class="font-weight-semi-bold fs--1 text-nowrap" href="/studios/pendingtransactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>Pending Movies and Short Films Transactions<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                  {{$pendingrentaltrans}}
                </div>
              <a class="font-weight-semi-bold fs--1 text-nowrap" href="/pendingrentaltransactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
             <?php if (Auth::guard('adminiy')->user()->role == "admin")
 {?>
             <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 Pending Citizen Transactions<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$unpaidusertransaction}}
                </div>
               <a class="font-weight-semi-bold fs--1 text-nowrap" href="/user/pendingtransactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
            
            <?php } ?>
        
            
 </div>
<div class="card-header bg-light">
              <h5 class="mb-0">TRANSACTIONS </h5>
            </div>
<div class="card-deck">
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 Movies and Short Films Transactions<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$rentaltrans}}
                </div>
               <a class="font-weight-semi-bold fs--1 text-nowrap" href="/transactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>MP3, Music Video, Lifestyle & Infotainment Transactions<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                  {{$studiotransaction}}
                </div>
              <a class="font-weight-semi-bold fs--1 text-nowrap" href="/studios/transactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
             <?php if (Auth::guard('adminiy')->user()->role == "admin")
 {?>
             <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                Citizen Transactions<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$usertransaction}}
                </div>
               <a class="font-weight-semi-bold fs--1 text-nowrap" href="/studios/transactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
            
            <?php } ?>
 </div>
 
  <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Studio Logs</h5>
          
        </div>
      
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered studiologs_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >Id</th>
                    <th >Image</th>
                    <th >Movie</th>
                    <th >Description</th>
                    <th >Studio</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  </div>
@include('dashboard_layouts.footer')