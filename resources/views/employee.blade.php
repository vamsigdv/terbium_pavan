@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Employee Management</h5>
          
        </div>
        <div class="col-auto ms-auto">
            <div>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#employee-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New Employee</span></button></div>
            </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered employee_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >Id</th>
                    <th >First Name</th>
                    {{-- <th >Last Name</th> --}}
                    <th >Contact</th>
                    <th >Email</th>
                     <th >Password</th>
                    <th >Is Active</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-modal-lg" id="employee-modal" tabindex="-1" role="dialog" aria-labelledby="employee-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label"> Add Employee </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('employee.store')}}" method="post" enctype="multipart/form-data">
            @csrf 
            <div class="mb-3 "><label class="form-label" >Email</label><span style="color:red"> *</span>
            <input class="form-control" type="email" id="email" name="email" required />
            </div>
              
            <div class="row gx-2">
            <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="first_name" name="first_name"  required />
            </div>
            <div class="mb-3 col-sm-4">
              <label class="form-label" >Middle Name</label>
              <input class="form-control" type="text" id="middle_name" name="middle_name"  />
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="last_name" name="last_name" required />
            </div>
          </div>
              <div class="row gx-2">
      
          <div class="mb-3 col-sm-4"><label class="form-label" >Voter ID</label>
            <input class="form-control" type="text" id="voter" name="voter"  />
          </div>
          <div class="mb-3 col-sm-4"><label class="form-label" >Account Number</label>
            <input class="form-control" type="number" id="account_number" name="account_number"  />
              </div>
                 <div class="mb-3 col-sm-4"><label class="form-label" >IFSC Code</label>
            <input class="form-control" type="text" id="ifsc" name="ifsc"  />
          </div>
              <div class="mb-3 col-sm-4"><label class="form-label" >Aadhar Number</label>
            <input class="form-control" type="number" id="adhar" name="adhar"  />
          </div>
           <div class="mb-3 col-sm-4"><label class="form-label" >Mobile Number</label><span style="color:red"> *</span>
            <input class="form-control" type="number" id="contact" name="contact" required />
          </div>
             <div class="mb-3 col-sm-4"><label class="form-label" >Password</label><span style="color:red"> *</span>
            <input class="form-control" type="password" id="password" name="password" required />
          </div>
          </div>
          <div class="form-check"><input class="form-check-input" type="checkbox" checked name="status" />
              <label class="form-label" >Status</label>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="employee-edit-modal" tabindex="-1" role="dialog" aria-labelledby="employee-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Employee Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('employee.update','1')}}" method="post" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            @csrf
            <div class="mb-3 "><label class="form-label" >Email</label><span style="color:red"> *</span>
              <input class="form-control" type="email" id="edit_email" name="edit_email" required />
            </div>
            <div class="row gx-2">
              <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
              <input class="form-control" type="hidden" id="edit_id" name="edit_id"  required />
              <input class="form-control" type="hidden" id="admin_id" name="admin_id"  required />
                <input class="form-control" type="text" id="edit_first_name" name="edit_first_name"  required />
              </div>
              <div class="mb-3 col-sm-4">
                <label class="form-label" >Middle Name</label>
                <input class="form-control" type="text" id="edit_middle_name" name="edit_middle_name"  />
              </div>
              <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
                <input class="form-control" type="text" id="edit_last_name" name="edit_last_name" required />
              </div>
            </div>
       
       
           <div class="row gx-2">
            <div class="mb-3 col-sm-4"><label class="form-label" >Voter ID</label>
              <input class="form-control" type="text" id="edit_voter" name="edit_voter"  />
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >Account Number</label>
              <input class="form-control" type="number" id="edit_account_number" name="edit_account_number"  />
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >IFSC Code</label>
              <input class="form-control" type="text" id="edit_ifsc" name="edit_ifsc"  />
            </div>
               <div class="mb-3 col-sm-4"><label class="form-label" >Aadhar Number</label>
              <input class="form-control" type="number" id="edit_adhar" name="edit_adhar"  />
            </div>
                 <div class="mb-3 col-sm-4"><label class="form-label" >Mobile Number</label><span style="color:red"> *</span>
              <input class="form-control" type="number" id="edit_contact" name="edit_contact" required />
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >Password</label><span style="color:red"> *</span>
              <input class="form-control" type="password" id="edit_password" name="edit_password" required />
            </div>
              </div>
            <div class="form-check"><input class="form-check-input" type="checkbox" id="edit_status" name="edit_status" />
              <label class="form-label" >Status</label>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="employee-view-modal" tabindex="-1" role="dialog" aria-labelledby="employee-view-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Employee View </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
        
                 <form action="" method="post" enctype="multipart/form-data">
      <div class="mb-3 "><label class="form-label" >Email</label><span style="color:red"> *</span>
              <input class="form-control" type="email" id="view_email" name="view_email" readonly="true" />
            </div>
              
              
            <div class="row gx-2">
              <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
              <input class="form-control" type="hidden" id="view_id" name="view_id"  required />
                <input class="form-control" type="text" id="view_first_name" name="view_first_name"  readonly="true" />
              </div>
              <div class="mb-3 col-sm-4">
                <label class="form-label" >Middle Name</label>
                <input class="form-control" type="text" id="view_middle_name" name="view_middle_name" readonly="true"  />
              </div>
              <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
                <input class="form-control" type="text" id="view_last_name" name="view_last_name" readonly="true" />
              </div>
            </div>
            
            
            
               <div class="row gx-2">
         
       
            <div class="mb-3 col-sm-4"><label class="form-label" >Voter ID</label>
              <input class="form-control" type="text" id="view_voter" name="view_voter" readonly="true" />
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >Account Number</label>
              <input class="form-control" type="number" id="view_account_number" name="view_account_number" readonly="true" />
            </div>
            <div class="mb-3 col-sm-4"><label class="form-label" >IFSC Code</label>
              <input class="form-control" type="text" id="view_ifsc" name="view_ifsc" readonly="true" />
            </div>
                 <div class="mb-3 col-sm-4"><label class="form-label" >Aadhar Number</label>
              <input class="form-control" type="number" id="view_adhar" name="view_adhar" readonly="true" />
            </div>
               <div class="mb-3 col-sm-4"><label class="form-label" >Mobile Number</label><span style="color:red"> *</span>
              <input class="form-control" type="number" id="view_contact" name="view_contact" readonly="true" />
            </div>
         
            <div class="mb-3 col-sm-4"><label class="form-label" >Password</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="view_password" name="view_password" readonly="true" />
            </div>
                </div>
       
            
            <div class="form-check"><input class="form-check-input" type="checkbox" id="view_status" name="view_status" />
              <label class="form-label" >ACTIVE</label>     </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection