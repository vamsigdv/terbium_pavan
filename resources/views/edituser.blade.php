@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
        @endif
        
        
            <div class="col-auto ms-auto">
            
            
            </div>
             <div class="card-header position-relative min-vh-25 mb-7">
                 <?php $role= Auth::guard('adminiy')->user()->role ; 
              
              if(empty($data->cover_photo)){
                $cover_photo ="assets/img/generic/4.jpg";
              } 
              else 
              {
                $cover_photo=$data->cover_photo;
              }
              ?>

              <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image: url(/{{ $cover_photo }})"></div>
              <!--/.bg-holder-->
              <?php $role= Auth::guard('adminiy')->user()->role ; ?>
               <?php 
                    
                     if(empty($data->image))
                     {
                        $photo="assets/img/team/3-thumb.jpg";
                     }
                     else
                     {
                         $photo=$data->image;
                     }
                    ?>
              <div class="avatar avatar-5xl avatar-profile">
                <img class="rounded-circle img-thumbnail shadow-sm" src="/{{$photo}}" width="200" alt="">
              </div>
            </div>
            <div class="card mb-3">
        <div class="modal-body py-4 px-5">
          <form action="{{ route('users.update',$data->id)}}" method="post" enctype="multipart/form-data">
             {{ method_field('PATCH') }}

            @csrf
              
             
            <div class="row gx-2">
               
               <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="firstname" name="firstname" value="{{$data->firstname}}"  required  />
              <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}"  />
              
              </div>
              <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="lastname" name="lastname"  value="{{$data->lastname}}" required />
              
              </div>
              
            </div>
             <div class="row gx-2">
               
               <div class="mb-3 col-sm-4"><label class="form-label" >Address</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="Address" name="Address" value="{{$data->address}}"  required  />
              
              </div>
               <div class="mb-3 col-sm-4"><label class="form-label" >Email</label><span style="color:red"> *</span>
              <input class="form-control" type="email" id="email" name="email" value="{{$data->email}}"  readonly required  />
              
              </div>
              <div class="mb-3 col-sm-4"><!--<label class="form-label" >Account No</label><span style="color:red"> *</span>-->
              <input class="form-control" type="hidden" id="accountno" name="accountno"  value="{{$data->accountno}}" />
              
              </div>
              
              
            </div>
             <div class="row gx-2">
               
              
              <div class="mb-3 col-sm-4"><!--<label class="form-label" >Branch Code</label><span style="color:red"> *</span>-->
              <input class="form-control" type="hidden" id="branch" name="branch"  value="{{$data->branch}}" />
              
              </div>
              
            </div>
            <div class="row gx-2">
        <div class="mb-3 col-sm-4"><label class="form-label" >Contact</label><span style="color:red"> *</span>
        <input class="form-control" type="number" id="contact" name="contact"  value="{{$data->mobile}}" required />
      </div>
       <div class="mb-3 col-sm-4"><label class="form-label" >Password</label><span style="color:red"> *</span>
        <input class="form-control" type="password" id="password" name="password" required value="{{$data->showPassword}}"/>
      </div>
      
     
       <div class="mb-3 col-sm-4">
            <label class="form-label" ></label>
            </br>
          
            <button class="btn btn-primary" type="button" id="btn">Show</button>
        </div>
         <div class="mb-3 col-sm-8"><label class="form-label" >Role</label><span style="color:red"> *</span>
           
            <input type="radio" name="role" value="2" {{ ($data->role =='2')?'checked':'' }}> Accounts
            <input type="radio" name="role" value="3" {{ ($data->role =='3')?'checked':'' }}> Employee
            <input type="radio" name="role" value="6" {{ ($data->role =='6')?'checked':'' }}> immigration
            <input type="radio" name="role" value="1" {{ ($data->role =='1')?'checked':'' }}> Super Admin

      </div>
      
      <div class="row gx-4">
        <div class="mb-4 col-sm-4">
       
        <?php if (!empty($data->image)) { ?>
                        <a href="{{ url($data->image) }}" target="_blank" rel="noopener noreferrer"> <img src="{{ url($data->image) }}" height="250" width="180"  id="photo"/> </a>
                    <?php }else{ ?>
                    <img src="/assets/img/team/3-thumb.jpg" height="250" width="160" id="photo">
                    <?php } ?>  
          <input class="form-control" type="file" accept="image/*" id="photo" name="photo"   onchange="upload(event, 'photo')"/>
           <label class="form-label" >Photo/logo</label>
        </div>
  
           <div class="mb-4 col-sm-4">
         
                <?php if (!empty($data->cover_photo)) { ?>
                        <a href="{{ url($data->cover_photo) }}" target="_blank" rel="noopener noreferrer"> <img src="{{ url($data->cover_photo) }}" height="250" width="350"  id="cover_photo"/> </a>
                    <?php }else{ ?>
                        <img src="/assets/img/generic/4.jpg" height="250" width="350" id="cover_photo">
                    <?php } ?>
                  <input class="form-control" type="file" accept="image/*" id="photo" name="cover_photo" onchange="upload(event, 'cover_photo')"/>
                         <label class="form-label" >Cover Picture</label>
              </div>
      
       <div class="mb-3 col-sm-8"><label class="form-label" >Status</label><span style="color:red"> *</span>
           
            <input type="radio" name="is_delete" value="1" {{ ($data->is_delete =='1')?'checked':'' }}> Active
            <input type="radio" name="is_delete" value="0" {{ ($data->is_delete =='0')?'checked':'' }}> In-active
           

      </div>
    </div>
       
     
      
      
          
            <div class="mb-3 col-sm-4">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit"  onclick="return confirm('Do you want to submit Yes / No?')">Submit</button>
            </div>
          </form>
        </div>
   </div>
   </div>
@endsection

@section('js')

<script>

    $('#btn').click(function(){

        var paswd= $('#password');
        if(paswd.attr("type")== "password"){

            paswd.attr("type","text");

            $('#btn').text("hide");

        }
        else{
                paswd.attr("type","password");

                $('#btn').text("show");
        }
    })
</script>

@endsection
