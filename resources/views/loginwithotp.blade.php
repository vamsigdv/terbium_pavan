@include('dashboard_layouts.head')
<style>
    .loginwrapper{ display:flex; flex-wrap: flex-wrap; overflow: hidden;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;}
    .left-login{     width: 50%;
    background: #f8f8f8;
    display: flex;
    justify-content: center;
    align-items: center;}
    .right-login{ width:50%; overflow:auto}
    .logincontainer{ max-width: 400px;}
</style>
<div class="loginwrapper">
<div class="left-login">
    
    <div><a class="d-flex flex-center mb-4" href="#"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="153" /></a></div>
    
    </div>
    <div class="right-login">

<div class="container logincontainer" data-layout="container">
    <script>
      var isFluid = JSON.parse(localStorage.getItem('isFluid'));
      if (isFluid) {
        var container = document.querySelector('[data-layout]');
        container.classList.remove('container');
        container.classList.add('container-fluid');
      }
    </script>
        <div class="row flex-center min-vh-100 py-6">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body p-4 p-sm-5">
                            <div class="row flex-between-center mb-2">
                            <div class="col-auto">
                                <h5>Log in With OTP</h5>
                            </div>
                        
                            </div>
                            <form method="POST" action="{{ url('sendotp') }}" novalidate>
                                @csrf
                            <div class="mb-3">
                                <input class="form-control" name="contact" type="contact" value="{{ old('contact') }}" placeholder="Contact Number" />
                                <div class="invalid-feedback"  @error('contact') style="display:block" @enderror>Please Enter Valid Contact Number</div>
                                @if(session('error'))
                                <div class="invalid-feedback" style="display:block">{{ session('error')}}</div>
                                @endif
                            </div>
                            
                           
                            
                            <div class="mb-3"><button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Send Otp</button></div>
                            <div class="mb-3"><a href="{{ url('login') }}" class="btn btn-primary d-block w-100 mt-3">Log in with Password</a></div>
                            
                             
                            </form>
                            
                        </div>
                    </div>
                </div>
        </div>
</div>
</div></div>
    </main>
</body>
</html>