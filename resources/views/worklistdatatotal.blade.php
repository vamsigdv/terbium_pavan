
@extends('dashboard_layouts.adminmaster')
<style>
.marT20{ margin-top:20px;}
.smalcards .card{height:100%; }
.smalcards .col-md-3{ margin-bottom: 10px;}
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}

   .text-700 {
    color: #5e6e82!important;
}
.mb-2, .my-2 {
    margin-bottom: 0.5rem!important;
}
.align-items-center {
    -webkit-box-align: center!important;
    -ms-flex-align: center!important;
    align-items: center!important;
}
.media {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start
}
.svg-inline--fa.fa-w-16 {
    width: 1em;
}
svg:not(:root).svg-inline--fa {
    overflow: visible;
}
.media-body {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
@media (min-width: 992px)
.pl-lg-3, .px-lg-3 {
    padding-left: 1rem!important;
}
.mr-2, .mx-2 {
    margin-right: 0.5rem!important;
}
.btn-falcon-primary:focus:not(.disabled):not(:disabled),.btn-falcon-primary:hover:not(.disabled):not(:disabled) {
    color: #1966cc!important;
    -webkit-box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08);
    box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08)
}
.pl-3, .px-3 {
    padding-left: 1rem!important;
}
.pr-3, .px-3 {
    padding-right: 1rem!important;
}
var avgMin
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}


</style>

 @section('content')
 
 <?php 
     if (Auth::guard('adminiy')->user()->role == "admin" || Auth::guard('adminiy')->user()->role == 'employee') {
         $smovie = "/movie";
         $pendingmovies = "/pendingmovies";
         $sshortfilms = "/shortfilms";
         $pendingshortfilms = "/pendingshortflims";
         $sinfluencer = "/influncer";
         $pendinginfluencer  = "/pendinginfluncer";
          $pendingmusicvideourl  = "/pendingmusicvideo";
         $smusicvideo = "/musicvideo";
         $smoviecreate = "/movie/create";
         $sshortfilmscreate = "/shortfilms/create";
         $sinfluencercreate = "/influncer/create";
         $smusicvideocreate = "/musicvideo/create";
     } else {
         $smovie = "/smovie";
         $pendingmovies = "/smovie";
         $sshortfilms = "/sshortfilms";
         $pendingshortfilms = "/sshortfilms";
         $pendinginfluencer  = "/sinfluncer";
         $pendingmusicvideourl  = "/smusicvideo";
         $sinfluencer = "/sinfluncer";
         $smusicvideo = "/smusicvideo";
         $smoviecreate = "/smovie/create";
         $sshortfilmscreate = "/studioshortfilms/create";
         $sinfluencercreate = "/studioinfluncercreate";
         $smusicvideocreate = "/smusicvideocreate";
     }
 ?>
   <div class="card mb-3">
            <div class="card-header position-relative min-vh-25 mb-7">
              <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image: url(../assets/img/generic/4.jpg)"></div>
              <!--/.bg-holder-->
              <?php $role= Auth::guard('adminiy')->user()->role ; ?>
               <?php if($role == 'admin' || Auth::guard('adminiy')->user()->role == 'employee')
                     {
                        $photo=Auth::guard('adminiy')->user()->image;
                     }
                     elseif($role == 'studio')
                     {
                         
                         $studio =DB::table('studio')->where('admin_id', Auth::guard('adminiy')->user()->id)->first();
                         $photo=$studio->photo;
                     }
                     elseif($role == 'employee')
                     {
                         $employee=DB::table('employee')->where('admin_id', Auth::guard('adminiy')->user()->id)->first();
                         $photo=$employee->photo;
                     }
                     else
                     {
                         $photo="assets/img/team/3-thumb.png";
                     }
                    ?>
              <div class="avatar avatar-5xl avatar-profile">
                <img class="rounded-circle img-thumbnail shadow-sm" src="/{{$photo}}" width="200" alt="">
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-8">
                  <h4 class="mb-1">
                     {{$studio->studio_name}}<small class="text-primary ml-1" data-toggle="tooltip" data-placement="right" title="" data-fa-transform="shrink-4 down-2" data-original-title="Verified" data-fa-i2svg=""><svg class="svg-inline--fa fa-check-circle fa-w-16" data-toggle="tooltip" data-placement="right" title="Verified" data-fa-transform="shrink-4 down-2" aria-labelledby="svg-inline--fa-title-LzDypsteLaRj" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><title id="svg-inline--fa-title-LzDypsteLaRj">Verified</title><g transform="translate(256 256)"><g transform="translate(0, 64)  scale(0.75, 0.75)  rotate(0 0 0)"><path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z" transform="translate(-256 -256)"></path></g></g></svg></small>
                  </h4>
                  <h5 class="fs-0 font-weight-normal">
                   {{$studio->email}}
                  </h5>
                  <p class="text-500"><div class="col-md-6 col-xxl-3">
        <?php if (Auth::guard('adminiy')->user()->role == "studio") {
            $studio =DB::table('studio')->where('admin_id', Auth::guard('adminiy')->user()->id)->first();
            $count =DB::table('movies')->where('studio', $studio->id)->get()->count();
            $studio_transaction =DB::table('studio_transaction')->where('studio_id', $studio->id)->get()->count();
            ?>
        <div class="card ">
            <div class="card-header pb-0">
            <h6 class="mb-0 mt-2 d-flex align-items-center">Wallet Balence</h6>
            </div>
            <div class="card-body d-flex flex-column justify-content-end">
            <div class="row">
                <div class="col md-6">
                <p class="font-sans-serif lh-1 mb-1 fs-4">Rs.{{$studio->wallet}}</p>
            </div>
            <div class="col md-6">
                  <div>
                      <?php if($studio->wallet > 0){?>
                  <a href="{{url('studios/withdraw/'.$studio->id)}}" id="Withdraw">  <button class="btn btn-success"> Withdraw </button></a>
                  <?php }else{?>
                  <a href="#" id="Withdraw">  <button class="btn btn-success"> Withdraw </button></a>
                  <?php } ?>
                    </div>
                    <br>
                    <div>
                    <a href="{{url('studios/transactions/')}}">  <button class="btn btn-success"> Transactions </button></a>
                    </div>
                     <?php if (Auth::guard('adminiy')->user()->role == "studio") {?>
                    <div>
                    <a href="{{url('studios/earningsdata/0')}}">  <button class="btn btn-success"> Payment History </button></a>
                    </div>
                    
                     <?php } ?>
               
            </div>
            </div>
           
            </div>
        </div><br>
        
        
     
        <?php }?>
        </div></p>
                  
                  </button>
                  <hr class="border-dashed my-4 d-lg-none">
                </div>
                
                
                
              
              </div>
            </div>
          </div>
          
          <div class="card-header bg-light">
              <h5 class="mb-0">Pending Payment</h5>
            </div>
<div class="card-deck">
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 PENDING MUSIC VIDEO, LIFESTYLE & INFOTAINMENT TRANSACTIONS<span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$pendingstudiotransaction}}
                </div>
               <a class="font-weight-semi-bold fs--1 text-nowrap" href="/studios/pendingtransactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>PENDING MOVIES AND SHORTFILM TRANSACTIONS<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
                <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                  {{$pendingrentaltrans}}
                </div>
              <a class="font-weight-semi-bold fs--1 text-nowrap" href="/pendingrentaltransactions">See all<svg class="svg-inline--fa fa-angle-right fa-w-8 ml-1" data-fa-transform="down-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" data-fa-i2svg="" style="transform-origin: 0.25em 0.5625em;"><g transform="translate(128 256)"><g transform="translate(0, 32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" transform="translate(-128 -256)"></path></g></g></svg><!-- <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span> Font Awesome fontawesome.com --></a>
              </div>
            </div>
            
          
            
 </div>
 <div class="card-deck">
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-1.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>
                 MOVIE AND SHORTFILM EXCEL DOWNLOAD <span class="badge badge-soft-warning rounded-capsule ml-2"></span>
                </h6>
                <!--<div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-warning">
                  {{$pendingstudiotransaction}}
                </div>-->
               <a style="float:left;" class="font-weight-semi-bold fs--1 text-nowrap" href="/transactionreport/{{$studio->id}}">Download Excel</a>&nbsp;
               
              
               <button style="float:right;"  class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#upload-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Upload Excel</span></button>
              </div>
            </div>
            <div class="card mb-3  overflow-hidden" style="min-width: 10rem">
              <div class="bg-holder bg-card" style="background-image: url(assets/img/illustrations/corner-2.png)"></div>
              <!--/.bg-holder-->
              <div class="card-body position-relative">
                <h6>MUSIC VIDEO, LIFESTYLE & INFOTAINMENT  EXCEL DOWNLOAD{{$datacount}}<span class="badge badge-soft-info rounded-capsule ml-2"></span></h6>
               <!-- <div class="display-4 fs-4 mb-2 font-weight-normal text-sans-serif text-info">
                  {{$pendingrentaltrans}}
                </div>-->
                @if($datacount == 0)
                 <a class="font-weight-semi-bold fs--1 text-nowrap" href="javascript:alert('No data found')">Download Excel</a>
                @else
                <a class="font-weight-semi-bold fs--1 text-nowrap" href="/studios/earningsdataexport/{{$studio->id}}">Download Excel</a>
                @endif
              
              <button style="float:right;"  class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#upload-modal-movie" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Upload Excel</span></button>
              </div>
            </div>
            
          
            
 </div>
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Payments By Studio</h5>
          
        </div>
      
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered worklisttotal_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >Id</th>
                     <th >Movie Name</th>
                    <th >Studio</th>
                   <th>Paid Amount</th>
                    <th>Unpaid Amount</th>
                     <th>Approved Amount</th>
                     <th>Actions</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  </div>
   <div class="modal editmodal fade bd-modal-lg" id="upload-modal" tabindex="-1" role="dialog" aria-labelledby="pendingmovie-edit-modal-label" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content border-0">
    <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
      <div class="position-relative z-index-1 light">
        <h4 class="mb-0 text-white" id="authentication-modal-label">Export data</h4>
      </div>
      <button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    
    <div class="modal-body py-4 px-5">
      <form action="" method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)" >
        @csrf 
         
            <div class="mb-3">
                 <input class="form-control" type="hidden" name="sid" value="{{$studio->id}}"/>
                <label class="form-label" >Upload </label><span style="color:red">*</span>
                <input class="form-control" value="" type="file" name="exce_upload" />
            </div>
            <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit"  name="submit">Submit</button>
        </div>
       
     </form>
    </div>
   
   </div>
  </div>
</div>
<div class="modal editmodal fade bd-modal-lg" id="upload-modal-movie" tabindex="-1" role="dialog" aria-labelledby="pendingmovie-edit-modal-label" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content border-0">
    <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
      <div class="position-relative z-index-1 light">
        <h4 class="mb-0 text-white" id="authentication-modal-label">Export data</h4>
      </div>
      <button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    
    <div class="modal-body py-4 px-5">
      <form action="" method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload-movie" action="javascript:void(0)" >
        @csrf 
         
            <div class="mb-3">
                  <input class="form-control" type="hidden" name="sid" value="{{$studio->id}}"/>
                <label class="form-label" >Upload </label><span style="color:red">*</span>
                <input class="form-control" value="" type="file" name="exce_upload" />
            </div>
            <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit"  name="submit">Submit</button>
        </div>
       
     </form>
    </div>
   
   </div>
  </div>
</div>
<script type="text/javascript">
      
    $(document).ready(function (e) {
   
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
  
  
        $('#laravel-ajax-file-upload').submit(function(e) {
            e.preventDefault();
  
            var formData = new FormData(this);
  
            $.ajax({
                type:'POST',
                url: "{{ url('worklistinstallmentsexcel')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                   /* this.reset();*/
                    alert('Updated successfully');
                   /*location.reload(true);*/
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
        
        $('#laravel-ajax-file-upload-movie').submit(function(e) {
            e.preventDefault();
  
            var formData = new FormData(this);
  
            $.ajax({
                type:'POST',
                url: "{{ url('studioeearrningexcel')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    this.reset();
                    alert('Updated successfully');
                   location.reload(true);
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
        
        $('.complete').click(function(e) {
            e.preventDefault();
            var dataString = 'id='+ $(this).attr("data-id");
            $.ajax({
                type:'POST',
                url: "{{ route('updatetranstatus') }}",
                data: dataString,
                cache:false,
               
                success: (data) => {
                   window.location.reload();
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
    });
  
</script>
@endsection