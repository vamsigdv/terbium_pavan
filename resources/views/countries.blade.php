@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Country Management</h5>
          
        </div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
           <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="/country" role="tab" aria-controls="tab-home" aria-selected="true">Countries</a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" id="home-tab" data-toggle="tab" href="/get_states" role="tab" aria-controls="tab-home" aria-selected="true">States</a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" id="home-tab" data-toggle="tab" href="/get_cities" role="tab" aria-controls="tab-home" aria-selected="true">Cities</a>
            </li>
           
            <li class="nav-item">
              <a class="nav-link" id="home-tab" data-toggle="tab" href="/get_districts" role="tab" aria-controls="tab-home" aria-selected="true">Districts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="profile-tab" data-toggle="tab" href="/get_sub_districts" role="tab" aria-controls="tab-profile" aria-selected="false">Sub Districts</a>
            </li>
           <li class="nav-item">
              <a class="nav-link " id="profile-tab" data-toggle="tab" href="/get_villages" role="tab" aria-controls="tab-profile" aria-selected="false">Villages</a>
            </li>
                        
        </ul>
        <div class="col-auto ms-auto">
            <div>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#country-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></div>
            </div>
           
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered country_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >Id</th>
                    <th >Country Name</th>
                    <th >Country Code</th>
                    <th >Phone Code</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-modal-lg" id="country-modal" tabindex="-1" role="dialog" aria-labelledby="country-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add Country  </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('country.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3 "><label class="form-label" >Country Name </label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="studio_name" name="studio_name" required />
            </div>
            <div class="row gx-2">
            <div class="mb-3 col-sm-4"><label class="form-label" >Country Code</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="first_name" name="country_code"  required />
            </div>
            <div class="mb-3 col-sm-4">
              <label class="form-label" >Phone Code</label>
              <input class="form-control" type="text" id="middle_name" name="phone_code"  />
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="modal fade bd-modal-lg" id="country-edit-modal" tabindex="-1" role="dialog" aria-labelledby="country-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Edit Coutry Details </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>
@endsection