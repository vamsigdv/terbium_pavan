<html>
    <title> Terbium Tech Inovoice  </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/fav.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/fav.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/fav.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/fav.png')}}">
    <meta name="msapplication-TileImage" content="{{asset('images/logo.png')}}">
    <style>
    body{
        background-color: #000;
    }

    .padding{
    
    padding: 2rem !important;
    }
    
    .card {
      margin-bottom: 30px;
      border: none;
      -webkit-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
      -moz-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
      box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    }
    
    .card-header {
      background-color: #fff;
      border-bottom: 1px solid #e6e6f2;
    }
    
    h3 {
      font-size: 20px;
    }
    
    h5 {
      font-size: 15px;
      line-height: 26px;
      color: #3d405c;
      margin: 0px 0px 15px 0px;
      font-family: 'Circular Std Medium';
    }
    
    .text-dark {
      color: #3d405c !important;
    }
    </style>
    <body>
        <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 padding">
            <div class="card">
                <div class="card-header p-4">
                   <a class="pt-2 d-inline-block" href="index.html" data-abc="true"><img  src="{{asset('images/logo.png')}}" alt="" width="120">
                </a>
                <div class="float-right"> <h3 class="mb-0">Invoice #{{ $getTransaction->transaction['transactionId'] }}</h3>
                Date: <?php echo date('d M, Y'); ?></div>
                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-6">
                            <h5 class="mb-3">From:</h5>
                            <h3 class="text-dark mb-1">{{ $companyDetails->company_name }}</h3>
                            <div>Address: {{ $companyDetails->address }}</div>
                            <div>Email: {{ $companyDetails->company_email }}</div>
                            <div>Phone: Texas: {{ $companyDetails->phone_texas }}</div>
                            <div>Phone: India: {{ $companyDetails->phone_india }}</div>
                        </div>
                        <div class="col-sm-6 ">
                            <h5 class="mb-3">To:</h5>
                            <h3 class="text-dark mb-1">{{ $getTransaction->contact['firstname'] . ' ' .  $getTransaction->contact['lastname'] }}</h3>
                            <div> {{ $getTransaction->contact->location_details['name'] }}</div>
                            <div>Email: {{ $getTransaction->contact['email'] }}  </div>
                            <div>Phone: {{ $getTransaction->contact['mobile'] }} </div>
                        </div>
                    </div>
            <div class="table-responsive-sm">
            <table class="table table-striped">
            <thead>
      
            <tr>
            <th class="center">Order ID</th>
            <th class="right">Transaction Date</th>
            <th>Technology</th>
            <th class="center">Payment Status </th>
            <th class="right">Amount Paid</th>
            </tr>
    
            </thead>
            <tbody>
               <tr>
            <td class="center">{{ $getTransaction->transaction['transactionId'] }}</td>
         
            <td class="left">{{ date('d/m/Y', strtotime($getTransaction->date)) }}</td>
            <td class="right">{{ join(',', $techs) }}</td>
            <td class="center">{{ $getTransaction->pay_status['name'] }}</td>
             <td class="right">{{ $getTransaction->amount }}</td>
            </tr>
            </tbody>
            </table>
            </div>
            <div class="row">
            <div class="col-lg-4 col-sm-5">
            </div>
            <div class="col-lg-4 col-sm-5 ml-auto">
            <table class="table table-clear">
            <tbody>
            <tr>
            <td class="left">
            <strong class="text-dark">Subtotal</strong>
            </td>
            <td class="right">{{ $getTransaction->amount }}</td>
            </tr>
         
            <tr>
            <td class="left">
            <strong class="text-dark">VAT (10%)</strong>
            </td>
            <td class="right">{{ $getTransaction->amount * 10/100}}</td>
            </tr>
            <tr>
    
            <tr>
            <td class="left">
            <strong class="text-dark">Pending Payment</strong>
            </td>
            <td class="right">{{ $getTransaction->due }}</td>
            </tr>
            <td class="left">
            <strong class="text-dark">Total Paid</strong>
             </td>
            <td class="right">
            <strong class="text-dark">{{ ($getTransaction->amount)+$getTransaction->due+($getTransaction->amount * 10/100)}}</strong>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
            </div>
            </div>
            <div class="card-footer bg-white">
            <p class="mb-0">Terms and conditions apply</p>
            </div>
            </div>
        </div>
    </body>
</html>