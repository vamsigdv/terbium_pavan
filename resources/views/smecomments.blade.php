@extends('dashboard_layouts.adminmaster')
 @section('content')  
<div class="card overflow-hidden mb-3">
            <div class="card-header bg-light">
              <div class="row flex-between-center">
                     
                <div class="col-sm-auto">
                  <h5 class="mb-1 mb-md-0">Comments</h5>
                </div>
               
               <div class="col-auto ms-auto">
            <div>
                <a href="/details/{{$interview->contactId}}" class="btn btn-falcon-success btn-sm">Home</a>
                <a href="/smes/{{$interview->transactionId}}" class="btn btn-falcon-success btn-sm">Back</a>
                
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Change SME</span></button></div>
            </div>
               
              
            </div>
           
          </div> 
          <div class="card-body fs--1 p-0">
               
              <?php foreach($logs as $key => $log) {
              $transactiontechnology=DB::table('transactiontechnology')->where('id', $log->transactiontechnology)->first();
              $employee = DB::table('users')->where('id', $interview->caseWorkerId)->first();
              ?>
                 
              <a class="border-bottom-0 notification rounded-0 border-x-0 border-300" href="{{$log->link}}">
                <div class="notification-avatar">
                  <div class="avatar avatar-xl me-3">
                     
                      <img class="rounded-circle" src="{{ ($employee->image) != '' ? url($employee->image) : asset('assets/img/team/3-thumb.jpg')}}" alt="" />
                   
                  </div>
                </div>
                <div class="notification-body">
                  <p class="mb-1">OLD SME NAME:{{$transactiontechnology->SMEName}} </p>
                  <p class="mb-1">OLD SME AMOUNT($):{{$transactiontechnology->agreePayment}} </p>
                  <p class="mb-1">NEW SME NAME:{{$log->newSmeName}} </p>
                  <p class="mb-1">NEW SME AMOUNT($):{{$log->paymenttosme}} </p>
                  <br> 
                     <h6>Comments:</h6>
                  <p class="mb-1">{{strip_tags(str_replace("&nbsp;", "", $log->comments))}} </p>
                  <br> 
                  <h6>Commented Date:</h6>
                <p class="mb-1">{{$log->created_at}}</p>
                </div>
              </a>
              <?php } ?>
             </div>
          <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">SME Change</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('smecomments.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
            <input class="form-control" type="hidden" id="id" name="id" value="{{$data->id}}" required />
              <input class="form-control" type="hidden" id="transactionId" name="transactionId" value="{{$data->transactionId}}" readonly />
           
            <div class="mb-3 col-md-6"><label class="form-label" >Reason For SME Change</label>
              <input class="form-control" type="text" id="reasonSME" name="reasonSME"  value="{{$data->reasonSME}}"  />
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >Payment Status</label><span style="color:red"> *</span>
              <select class="form-select" aria-label="Default select example" name="paymentStatus" required>
                    <option value="">Select payment status</option>
                    <?php if ($payment_statuses) { 
                        foreach ($payment_statuses as $payment) {
                    ?>
                    <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                    <?php } 
                    }
                    ?>
                    </select>
            </div>
            
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >Name Of Consultant</label><span style="color:red"> *</span>
              
               <select class="form-select" aria-label="Default select example" name="contactName" disabled required>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($getTransactions->contactId==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                </select>
                <input class="form-control" type="hidden" id="agreePayment" name="contactId" value="{{$getTransactions->contactId}}" />
            </div>
            
            <div class="mb-3 col-md-6"><label class="form-label" >SME Agreed Payment</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="agreePayment" name="agreePayment" value="{{$data->agreePayment}}" required />
            </div>
            </div>
            
            <div class="row">
             <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="technology"  required>
                         <option value="">Select Option</option>
                         
                         <?php 
                         
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}" {{($data->technologyId==$tvalue->id)?'selected':''}}>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
            
            <div class="mb-3 col-md-6"><label class="form-label" >Comment</label>
              <input class="form-control" type="text" id="name" name="comment" value="{{$data->comment}}"  />
            </div>
            </div>
            
            <div class="row">
            <div class="mb-3 col-md-6"><label class="form-label" >No.of Interviews held</label> 
              <input class="form-control" type="text" id="name" name="NoofInterviews" value="{{$data->NoofInterviews}}"  />
            </div>
            
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Work Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="workType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($worktype)){
                            foreach ($worktype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{($data->workTypeId==$pvalue->id)?'selected':''}}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
            
            
            <div class="row">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Resources/Trainer/SME</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="SMEName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($sme)){
                            foreach ($sme as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" {{($data->smeid==$cvalue->id)?'selected':''}}>{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
           <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" >
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{($getTransactions->caseworkerId==$cwvalue->id)?'selected':''}}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                    <input class="form-control" type="hidden" id="caseworker" name="caseworker" value={{$getTransactions->caseworkerId}} />
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Batch</label>
                    <select class="form-select" aria-label="Default select example" name="batch">
                         <option value="">Select Option</option>
                         <?php if(!empty($batch)){
                            foreach ($batch as $bkey => $bvalue) { ?>
                                <option value="{{$bvalue->id}}" {{($data->batchId==$bvalue->id)?'selected':''}}>{{$bvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            
            </div>
      <div class="form-check"><input class="form-check-input" type="hidden" {{ ($data->status==1)?'checked':''}} name="status" />
        
      </div>
        <div class="mb-3">
          <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
        </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  
  
</div>
 <script>

$(document).ready(function() {
   
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
  
  
});


/*function disableFields(type) {
   //   alert(type);
    if (type === 'reshdule') {
       $('#status').attr("disabled", true);
       $('#status').val(3);
       $('#statusType').val(3);
    } else if (type == 'cleared') {
        $('#status').attr("disabled", true);
        $('#status').val(6);
        $('#statusType').val(6);
    }
}*/
</script>

@endsection
