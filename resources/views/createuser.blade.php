@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
        @endif
        
        
            <div class="col-auto ms-auto">
            
            
            </div>
             <div class="card-header position-relative min-vh-25 mb-7">
              <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image: url(../assets/img/generic/4.jpg)"></div>
              <!--/.bg-holder-->
              <?php $role= Auth::guard('adminiy')->user()->role ; ?>
               <?php if($role == 'admin')
                     {
                        $photo=Auth::guard('adminiy')->user()->image;
                     }
                     elseif($role == 'studio')
                     {
                         
                         $studio =DB::table('studio')->where('admin_id', Auth::guard('adminiy')->user()->id)->first();
                         $photo=$studio->photo;
                     }
                     elseif($role == 'employee')
                     {
                         $employee=DB::table('employee')->where('admin_id', Auth::guard('adminiy')->user()->id)->first();
                         $photo=$employee->photo;
                     }
                     else
                     {
                         $photo="assets/img/team/3-thumb.png";
                     }
                    ?>
              <div class="avatar avatar-5xl avatar-profile">
                <img class="rounded-circle img-thumbnail shadow-sm" src="/{{$photo}}" width="200" alt="">
              </div>
            </div>
        <div class="modal-body py-4 px-5">
            
            @if ($errors->any()) <div class="alert alert-danger">
                            <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul>
</div>@endif
          <form action="{{ route('users.store')}}" method="post" enctype="multipart/form-data">
            
            @csrf
              
             
            <div class="row gx-2">
               
               <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="firstname" name="firstname" value=""  required  />
              
              </div>
              <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="lastname" name="lastname"  value="" required />
              
              </div>
              
            </div>
             <div class="row gx-2">
               
               <div class="mb-3 col-sm-4"><label class="form-label" >Address</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="Address" name="Address" value=""  required  />
              
              </div>
               <div class="mb-3 col-sm-4"><label class="form-label" >Email</label><span style="color:red"> *</span>
              <input class="form-control" type="email" id="email" name="email" value=""  required  />
              
              </div>
              <div class="mb-3 col-sm-4"><!--<label class="form-label" >Account No</label><span style="color:red"> *</span>-->
              <input class="form-control" type="hidden" id="accountno" name="accountno"  value="" />
              
              </div>
              
            </div>
             <div class="row gx-2">
               
              
              <div class="mb-3 col-sm-4"><!--<label class="form-label" >Branch Code</label><span style="color:red"> *</span>-->
              <input class="form-control" type="hidden" id="branch" name="branch"  value="" />
              
              </div>
              
            </div>
            <div class="row gx-2">
        <div class="mb-3 col-sm-4"><label class="form-label" >Contact</label><span style="color:red"> *</span>
        <input class="form-control" type="number" id="contact" name="contact"  value="" required />
      </div>
       <div class="mb-3 col-sm-4"><label class="form-label" >Password</label><span style="color:red"> *</span>
        <input class="form-control" type="password" id="password" name="password" required value=""/>
      </div>
     
       <div class="mb-3 col-sm-4">
            <label class="form-label" ></label>
            </br>
          
            <button class="btn btn-primary" type="button" id="btn">Show</button>
        </div>
         <div class="mb-3 col-sm-8"><label class="form-label" >Role</label><span style="color:red"> *</span>
           
            <input type="radio" name="role" value="2"> Accounts
            <input type="radio" name="role" value="3" checked> Employee
            <input type="radio" name="role" value="6"> immigration
            <input type="radio" name="role" value="1"> Admin

      </div>
       <div class="row gx-4">
                      <div class="mb-3 col-md-4">
                <img src="/assets/img/team/3-thumb.jpg" height="250" width="160" id="photo">
               </div>
               <div class="mb-3 col-md-4">
                    <img src="/assets/img/generic/4.jpg" height="250" width="500" id="cover_photo">
                </div>
                
                
                
                
            </div>
            
        
     
        <div class="row gx-4">
            
                   
                    <div class="mb-3 col-sm-4">
        <label class="form-label" >Photo/logo</label>
       
          <input class="form-control" type="file" accept="image/*" id="photo" name="photo" onchange="upload(event, 'photo')"/>
      </div>
  
           <div class="mb-3 col-sm-4">
                <label class="form-label" >Cover Picture</label>
               
                  <input class="form-control" type="file" accept="image/*" id="photo" name="cover_photo" onchange="upload(event, 'cover_photo')"/>
              </div>
              
        </div>
        
              
       <div class="mb-3 col-sm-8"><label class="form-label" >Status</label><span style="color:red"> *</span>
           
            <input type="radio" name="is_delete" value="1" > Active
            <input type="radio" name="is_delete" value="0"> In-active
           

      </div>
        
        
        
        
    </div>
       
     
      
      
          
            <div class="mb-3 col-sm-4">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit"  onclick="return confirm('Do you want to submit Yes / No?')">Submit</button>
            </div>
          </form>
        </div>
   
@endsection

@section('js')

<script>

    $('#btn').click(function(){

        var paswd= $('#password');
        if(paswd.attr("type")== "password"){

            paswd.attr("type","text");

            $('#btn').text("hide");

        }
        else{
                paswd.attr("type","password");

                $('#btn').text("show");
        }
    })
</script>

@endsection
