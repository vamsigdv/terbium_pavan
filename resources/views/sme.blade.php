@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Assign SME</h5>
          
        </div>
        <div class="col-auto ms-auto">
            <div>
              {{-- target="_blank" --}}
                <div>
               <!-- <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>--></div>
            </div>
        </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
          <div class='row'>
                <div class="mb-3 col-md-4">
                    <label class="form-label" >Interview From date</label>
                    <input class="form-control" type="date" id="fromDate" name="nextcomitdate" onchange="return searchSME();">
                </div>
                <div class="mb-3 col-md-4">
                    <label class="form-label" >Interview To date</label>
                    <input class="form-control" type="date" id="toDate" name="nextcomitdate" onchange="return searchSME();">
                </div>
                <div class="mb-3 col-md-4">
                    <label class="form-label">Consultant</label>
                    <select class="form-select" aria-label="Default select example" name="contactName" id="contactName" onchange="return searchSME();">
                         <option value="">Select Option</option>
                         <?php if(!empty($consultants)){
                            foreach ($consultants as $ckey => $cvalue) { ?>
                                <option value="{{ $cvalue->id }}">{{ $cvalue->firstname }}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
            <div class='row'>
                <div class="mb-3 col-md-3">
                    <label class="form-label">SME</label>
                    <select class="form-select" aria-label="Default select example" name="SMEName" id="SMEName" onchange="return searchSME();">
                         <option value="">Select Option</option>
                         <?php if(!empty($smes)){
                            foreach ($smes as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" >{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label">Created By</label>
                    <select class="form-select" aria-label="Default select example" name="created_by" id="created_by" onchange="return searchSME();">
                         <option value="">Select Option</option>
                         <?php if(!empty($created_users)){
                            foreach ($created_users as $ckey => $created_user) { ?>
                                <option value="{{ $created_user->id }}" >{{ $created_user->name }}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label">Work Type</label>
                    <select class="form-select" aria-label="Default select example" name="work_type" id="work_type" onchange="return searchSME();">
                         <option value="">Select Option</option>
                         <?php if(!empty($work_types)){
                            foreach ($work_types as $ckey => $work_type) { ?>
                                <option value="{{ $work_type->name }}" >{{ $work_type->name }}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label"></label>
                    <button type="button" class="btn btn-warning form-control" name="Reset" onClick="return resetFields('sme');">Reset</button>
                </div>
            </div>
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              <table class="table table-bordered smeind_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >ID</th>
                    <th >Name Of The Consultant</th>
                    <th >Mobile Number</th>
                    <th >Transaction Date</th>
                    <th >Amount Paid</th>
                    <th >Technology</th>
                    <th >SME</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
    
   
    <div class="modal editmodal fade bd-modal-lg" id="sme-edit-modal" tabindex="-1" role="dialog" aria-labelledby="album-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Edit SME</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>
  </div>
   <script>

$(document).ready(function() {
   
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
  
  
});

</script>
@endsection