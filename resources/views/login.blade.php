@include('dashboard_layouts.head')
<style>
    .loginwrapper{ display:flex; flex-wrap: flex-wrap; overflow: hidden;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;}
    .left-login{     width: 50%;
    background: #f8f8f8;
    display: flex;
   }
    .right-login{ width:50%; overflow:auto}
    .logincontainer{ max-width: 400px;}
    .left-login img{ width:100%; height:100%; object-fit:cover}
</style>
<div class="loginwrapper">
<div class="left-login">
    
    <div><img class="" src="{{asset('images/banner9.jpg')}}" alt=""  height: 100%;
  width: 100% /></div>
    
    </div>
    <div class="right-login">

<div class="container logincontainer" data-layout="container">
    <script>
      var isFluid = JSON.parse(localStorage.getItem('isFluid'));
      if (isFluid) {
        var container = document.querySelector('[data-layout]');
        container.classList.remove('container');
        container.classList.add('container-fluid');
      }
    </script>
        <div class="row flex-center min-vh-100 py-6">
                <div class="col-sm-12"><a class="d-flex flex-center mb-4" href="#"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="153" /></a>
                    <div class="card">
                        <div class="card-body">
                            <div class="row flex-between-center mb-2">
                            <div class="col-auto">
                                <h5>Login</h5>
                            </div>
                        
                            </div>
                            <form method="POST" action="{{ route('adminiy.authenticate_email_with_otp') }}" novalidate>
                                @csrf
                            <div class="mb-3">
                                <input class="form-control" name="email" type="email" value="{{ old('email') }}" placeholder="Email address or Phone Number" />
                                <div class="invalid-feedback"  @error('email') style="display:block" @enderror>Invalid Email Address</div>
                                @if(session('error'))
                                <div class="invalid-feedback" style="display:block">{{ session('error')}}</div>
                                @endif
                            </div>
                            
                            <div class="mb-3">
                                <input class="form-control" name="password" value="{{ old('password') }}" id="passInput" type="password" placeholder="Password" />
                                   
                                <div class="invalid-feedback"  @error('password') style="display:block" @enderror>Invalid Password</div>
                                @if(session('password'))
                                <div class="invalid-feedback" style="display:block">{{ session('password')}}</div>
                                @endif
                            </div>
                            
                            
                             <div class="row flex-between-center">
                                <div class="col-auto">
                                <div class="form-check mb-2">
                                    <input class="form-check-input" name="spassword" type="checkbox"   id="showPass" />
                                    <label class="form-check-label mb-0" for="basic-checkbox">Show Password</label>
                                </div>
                                </div>
                                
                          
                                <div class="col-auto">
                                <div class="form-check mb-2">
                                    <input class="form-check-input" name="remember" type="checkbox"  checked {{ old('remember') ? 'checked' : '' }}  />
                                    <label class="form-check-label mb-0" for="basic-checkbox">Remember me</label>
                                </div>
                                
                                
                                
                                </div>
                                
                            </div>
                            <div class="mb-3"><button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Log in</button></div>
                             <div class="mb-3"><a href="{{ url('loginwithotp') }}" class="btn btn-primary d-block w-100 mt-3">Forget Password ? Log in With OTP</a></div>
                            
                          
                
                            
                            </form>
                            
                        </div>
                    </div>
                </div>
        </div>
</div>
</div>
</div>

    </main>
</body>
</html>
<script>
    
    $(document).ready(function(){
  
   $('#showPass').on('click', function(){
      var passInput=$("#passInput");
      if(passInput.attr('type')==='password')
        {
          passInput.attr('type','text');
      }else{
         passInput.attr('type','password');
      }
  })
})
</script>