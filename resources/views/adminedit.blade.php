@extends('dashboard_layouts.adminmaster')
 @section('content')  
 
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
            <div class="col-auto ms-auto">
             <div class="col-12">
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="home-tab" data-toggle="tab" href="/profileupdate" role="tab" aria-controls="tab-home" aria-selected="true">Account Settings</a>
                        </li>
                       
                       <li class="nav-item">
                          <a class="nav-link" id="profile-tab" data-toggle="tab" href="/generalsettings" role="tab" aria-controls="tab-profile" aria-selected="false">General Settings</a>
                        </li>
                       
                      </ul>
                     
                    </div>
            
            </div>
             <div class="card-header position-relative min-vh-25 mb-7">
                  <?php $role= Auth::guard('adminiy')->user()->role ; 
              
              if(Auth::guard('adminiy')->user()->cover_photo == ''){
                $cover_photo ="assets/img/generic/4.jpg";
              } 
              else {
                $cover_photo=Auth::guard('adminiy')->user()->cover_photo;
              }
             
              
              ?>
              <div class="bg-holder rounded-soft rounded-bottom-0" style="background-image: url(/{{$cover_photo}})"></div>
              <!--/.bg-holder-->
              <?php $role= Auth::guard('adminiy')->user()->role ; ?>
               <?php if(Auth::guard('adminiy')->user()->image != '')
                     {
                        $photo=Auth::guard('adminiy')->user()->image;
                     }
                     else
                     {
                         $photo="assets/img/team/3-thumb.jpg";
                     }
                    ?>
              <div class="avatar avatar-5xl avatar-profile">
                <img class="rounded-circle img-thumbnail shadow-sm" src="/{{$photo}}" width="200" alt="">
              </div>
            </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('adminupdate')}}" method="post" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            @csrf
              
              <div class="row gx-2">
               
               <div class="mb-3 col-sm-4"><label class="form-label" >First Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="first_name" name="firstname" value="{{$user->firstname}}"  required  />
              
              </div>
              <div class="mb-3 col-sm-4"><label class="form-label" >Last Name</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="lastname" name="lastname"  value="{{$user->lastname}}" required />
              
              </div>
              
            </div>
              
              
              <div class="row gx-2">
               
               <div class="mb-3 col-sm-4"><label class="form-label" >Address</label><span style="color:red"> *</span>
              <input class="form-control" type="text" id="Address" name="Address" value="{{$user->address}}"  required  />
              
              </div>
             <!-- <div class="mb-3 col-sm-4"><label class="form-label" >Account No</label><span style="color:red"> *</span>-->
              <input class="form-control" type="hidden" id="accountno" name="accountno"  value="{{$user->accountno}}"  />
             <!-- 
              </div>-->
            
              <div class="mb-3 col-sm-4"><label class="form-label" >Email</label><span style="color:red"> *</span>
              <input class="form-control" type="email" id="email" name="email" value="{{Auth::guard('adminiy')->user()->email}}"  required readonly="true" />
             
              </div>
            <div class="row gx-2">
               
              
              <div class="mb-3 col-sm-4"><!--<label class="form-label" >Branch Code</label><span style="color:red"> *</span>-->
              <input class="form-control" type="hidden" id="edit_id" name="edit_id" value="{{$user->id}}"   required />
              <input class="form-control" type="hidden" id="branch" name="branch"  value="{{$user->branch}}"  />
              <input class="form-control" type="hidden" id="old_password" name="old_password"  value="{{Auth::guard('adminiy')->user()->password}}" required />
              </div>
              
            </div>
            <div class="row gx-2">
      <div class="mb-3 col-sm-4"><label class="form-label" >Contact</label><span style="color:red"> *</span>
        <input class="form-control" type="number" id="contact" name="contact"  value="{{Auth::guard('adminiy')->user()->contact}}" required  readonly="true"/>
      </div>
       <div class="mb-3 col-sm-4"><label class="form-label" >Password</label><span style="color:red"> *</span>
        <input class="form-control" type="password" id="password" name="password" required value="{{((Auth::guard('adminiy')->user()->showPassword1))}}"/>
      </div>
      
     
       <div class="mb-3 col-sm-4">
            <label class="form-label" ></label>
            </br>
          
            <button class="btn btn-primary" type="button" id="btn">Show</button>
        </div>
    </div>
          
           <div class="mb-4 col-sm-4">
       
        <?php if (!empty($user->image)) { ?>
                        <a href="{{ url($user->image) }}" target="_blank" rel="noopener noreferrer"> <img src="{{ url($user->image) }}" height="250" width="180"  id="photo"/> </a>
                    <?php }else{ ?>
                    <img src="/assets/img/team/3-thumb.jpg" height="250" width="160" id="photo">
                    <?php } ?>  
          <input class="form-control" type="file" accept="image/*" id="photo" name="photo"   onchange="upload(event, 'photo')"/>
           <label class="form-label" >Photo/logo</label>
        </div>
  
           <div class="mb-4 col-sm-4">
         
                <?php if (!empty($user->cover_photo)) { ?>
                        <a href="{{ url($user->cover_photo) }}" target="_blank" rel="noopener noreferrer"> <img src="{{ url($user->cover_photo) }}" height="250" width="350"  id="cover_photo"/> </a>
                    <?php }else{ ?>
                        <img src="/assets/img/generic/4.jpg" height="250" width="350" id="cover_photo">
                    <?php } ?>
                  <input class="form-control" type="file" accept="image/*" id="cover_photo" name="cover_photo" onchange="upload(event, 'cover_photo')"/>
                         <label class="form-label" >Cover Picture</label>
              </div>
             <input type="hidden" name="role" value="{{ $user->role }}">
      <!--   <div class="mb-3 col-sm-8"><label class="form-label" >Role</label><span style="color:red"> *</span>-->
           
      <!--      <input type="radio" name="role" value="1" {{ ($user->role==1)?'checked':''}}>Super Admin-->
      <!--      <?php /* if($user->role > 1){?>-->
      <!--      <input type="radio" name="role" value="2" {{ ($user->role==2)?'checked':''}}> Accounts-->
      <!--      <input type="radio" name="role" value="3" {{ ($user->role==3)?'checked':''}}> Employee-->
      <!--      <input type="radio" name="role" value="4" {{ ($user->role==4)?'checked':''}}> Immigration-->
          
           
      <!--      <?php } */?>-->

      <!--</div>-->
    </div>
          
            <div class="mb-3 col-sm-4">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit"  onclick="return confirm('Do you want to submit Yes / No?')">Submit</button>
            </div>
          </form>
        </div>
   
@endsection

@section('js')

<script>

    $('#btn').click(function(){

        var paswd= $('#password');
        if(paswd.attr("type")== "password"){

            paswd.attr("type","text");

            $('#btn').text("hide");

        }
        else{
                paswd.attr("type","password");

                $('#btn').text("show");
        }
    })
</script>

@endsection
