@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
    <div class="card-header">
       
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">Transactions</h5>
          
        </div>
        <div class="col-auto ms-auto">
            <div>
                <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></div>
            </div>
      </div>
  
    </div>
 
    <div class="card-body pt-0">
      <div class="tab-content">
        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1" id="dom-c33e079e-6e1a-483b-8755-69c47bcdf1e1">
              @if(session('success'))
              <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
              <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              @if(session('failed'))
              <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
              <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
              <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              
              <table class="table table-bordered transactions_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >ID</th>
                    <th >Mobile</th>
                    <th >Transaction Date</th>
                    <th >Consultant Name</th>
                    <th >Intial Payment($)</th>
                    <th >Due($)</th>
                    <th >Technology</th>
                    <th >Work Type</th>
                    <th >Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bd-modal-lg" id="album-modal"  role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add New Transaction</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('transactions.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row gx-2">
            <div class="mb-3 col-sm-6"><label class="form-label" >Payment Date</label><span style="color:red"> *</span>
              <input class="form-control" type="date" id="date" name="date" value="<?= date('Y-m-d'); ?>" required />
            </div>
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                    <select class=" form-select" id="select2insidemodal" style="width:100%" name="technology[]"  data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple" >
                         <option value="">Select Option</option>
                         <?php if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}">{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
            <div class="row gx-2">
           
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Consultant Name</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="consultantName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($contact)){
                            foreach ($contact as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}">{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Payment Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="paymentType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($paymenttype)){
                            foreach ($paymenttype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}">{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
            </div>
             
            <div class="row gx-2">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Transaction ID </label><span style="color:red"> *</span>
                    <input class="form-control" type="text" id="transactionId" name="transactionId" required/>
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Paying Now($)</label><span style="color:red"> *</span>
                    <input class="form-control" type="text" id="payingNow" name="payingNow"  required />
                </div>
            </div>
            <div class="row gx-2">
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Due($)</label><span style="color:red"> *</span>
                    <input class="form-control" type="text" id="Due" name="Due"  required />
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Next Commitment Date</label>
                    <input class="form-control" type="date" id="nextcomitdate" name="nextcomitdate" min="<?php echo date("Y-m-d"); ?>"/>
                </div>
            </div>
           
           <div class="row gx-2">
           
            <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Employee</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" required {{ (Auth::guard('adminiy')->user()->role != 'Super Admin')? 'disabled':'' }}>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{ (Auth::guard('adminiy')->user()->user_id == $cwvalue->id)?'selected':'' }}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                    <?php  if(Auth::guard('adminiy')->user()->role != 'Super Admin'){?>
                    <input class="form-control" type="hidden"  name="caseworker" value="{{Auth::guard('adminiy')->user()->user_id}}">
                    <?php } ?>
                </div>
                <div class="mb-3 col-md-6"><label class="form-label" >Payment Status</label><span style="color:red"> *</span>
                <?php if ($payment_statuses) { ?>
                <select class="form-select" name="paymentStatus" required>
                <?php foreach ($payment_statuses as $payment_status) { ?>
                    <option value="{{ $payment_status->id }}">{{ $payment_status->name }}</option>
              <?php } ?> 
              </select> 
              <?php } ?>
            </div>
            </div>
           <div class="row gx-2">
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="">Work Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="work_type" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($work_types)){
                            foreach ($work_types as $work_type) { ?>
                                <option value="{{ $work_type->id }}">{{ $work_type->name }}</option>
                        <?php }} ?>
                    </select>
                </div>
            </div>
            <div class="form-check"><input class="form-check-input" type="hidden"  name="status" />
             
            </div>
            
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="transactions-edit-modal"  role="dialog" aria-labelledby="album-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Transaction Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal editmodal fade bd-modal-lg" id="transactions-view-modal" role="dialog" aria-labelledby="album-view-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Transaction View </h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
        
          <div id="view"></div>
        </div>
      </div>
    </div>
  </div>
  <script>

$(document).ready(function() {
   
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
  
  
});

</script>
@endsection