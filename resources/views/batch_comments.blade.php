@extends('dashboard_layouts.adminmaster')
 @section('content')  
<div class="card overflow-hidden mb-3">
    <div class="card-header bg-light">
        <div class="row flex-between-center">
            <div class="col-sm-auto">
              <h5 class="mb-1 mb-md-0">Batch Comments</h5>
            </div>
            <div class="col-auto ms-auto">
                <div>
                    <a href="/caseworker" class="btn btn-falcon-success btn-sm">Home</a>
                    
                    <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">Add Comments</span></button>
                </div>
            </div>
        </div>
    </div> 
    <div class="card-body fs--1 p-0">
      @if(session('success'))
      <div class="alert alert-success border-2 d-flex align-items-center" role="alert">
          <div class="bg-success me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
          <p class="mb-0 flex-1">{{ session('success')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
      @if(session('failed'))
      <div class="alert alert-danger border-2 d-flex align-items-center" role="alert">
          <div class="bg-danger me-3 icon-item"><span class="fas fa-check-circle text-white fs-3"></span></div>
          <p class="mb-0 flex-1">{{ session('failed')}}</p><button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
      <?php foreach($batch_comments as $log) { ?>
      <div class="border-bottom-0 notification rounded-0 border-x-0 border-300">
        <div class="notification-avatar">
          <div class="avatar avatar-xl me-3">
              <img class="rounded-circle" src="{{ asset('assets/img/team/3-thumb.jpg') }}" alt="" />
          </div>
        </div>
        <div class="notification-body">
            <?php $c_name = $contacts->where('id', $log->sme_id)->first()?>
          <p class="mb-1"><span style="font-weight:bold;">SME NAME: </span>{{ ($c_name) ? $c_name->firstname : '' }} </p>
          <p class="mb-1"><span style="font-weight:bold;">Date: </span>{{ $log->date }} </p>
          <p class="mb-1"><span style="font-weight:bold;">Start Time: </span>{{ $log->start_time }} </p>
          <p class="mb-1"><span style="font-weight:bold;">End Time: </span>{{ $log->end_time }} </p>
          <br> 
             <h6>Description:</h6>
          <p class="mb-1">{{strip_tags(str_replace("&nbsp;", "", $log->description))}} </p>
          <br> 
          <h6>Participants:</h6>
            <p class="mb-1">{{ $log->participants }}</p>
            <br>
          <h6>Attachment:</h6>
            <p class="mb-1">
                <a href="{{ url('case_worker/batch_comments/download_attachment/' . $log->id) }}"><i class="fa fa-file-invoice"></i></a>
            </p>
        </div>
        </div>
      <?php } ?>
    </div>
</div>
<div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content border-0">
            <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
                <div class="position-relative z-index-1 light">
                    <h4 class="mb-0 text-white" id="authentication-modal-label">Batch Comments</h4>
                </div>
                <button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body py-4 px-5">
               <form action="{{ url('case_worker/batch_comments/save')}}" method="post" enctype="multipart/form-data">
                   @csrf
                   <div class="row">
                       <input class="form-control" type="hidden" id="id" name="id" value="{{ $batch_id }}" required />
                       <div class="mb-3 col-md-6">
                           <label class="form-label">SME</label><span style="color:red"> *</span>
                           <select class="form-select" aria-label="Default select example" name="sme_id" required>
                                 <option value="">Select Option</option>
                                 <?php if(!empty($contacts)){
                                    foreach ($contacts as $ckey => $cvalue) { ?>
                                        <option value="{{$cvalue->id}}">{{$cvalue->firstname}}</option>
                                <?php    }} ?>
                            </select>
                       </div>
                       <div class="mb-3 col-md-6">
                           <label class="form-label">Date</label><span style="color:red"> *</span>
                           <input class="form-control" type="date" id="reasonSME" name="date" required/>
                       </div>
                   </div>
                   <div class="row">
                       <div class="mb-3 col-md-6">
                           <label class="form-label">Start Time</label><span style="color:red"> *</span>
                           <input class="form-control" type="time" id="reasonSME" name="start_time" required/>
                       </div>
                       <div class="mb-3 col-md-6">
                           <label class="form-label">End Time</label><span style="color:red"> *</span>
                           <input class="form-control" type="time" id="reasonSME" name="end_time" required/>
                       </div>
                   </div>
                   <div class="row">
                       <div class="mb-3 col-md-12">
                           <label class="form-label">Description</label><span style="color:red"> *</span>
                           <textarea rows="4" class="form-control" name="description" required></textarea>
                       </div>
                   </div>
                   <div class="row">
                       <div class="mb-3 col-md-6">
                           <label class="form-label">Attachment</label><span style="color:red"> *</span>
                           <input class="form-control" type="file" id="reasonSME" name="attachment" required/>
                       </div>
                       <div class="mb-3 col-md-6">
                           <label class="form-label">Participants</label><span style="color:red"> *</span>
                            <select class=" form-select" id="select2insidemodal" style="width:100%" name="participants[]"  data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" multiple="multiple" required>
                                 <option value="">Select Option</option>
                                 <?php if(!empty($contacts)){
                                    foreach ($contacts as $ckey => $cvalue) { ?>
                                        <option value="{{$cvalue->id}}">{{$cvalue->firstname}}</option>
                                <?php    }} ?>
                            </select>
                       </div>
                   </div>
                   <div class="mb-3">
                      <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
                    </div>
               </form> 
            </div>
        </div>
    </div>
</div>
<script>

$(document).ready(function() {
  $("#select2insidemodal").select2({
    dropdownParent: $("#album-modal")
  });
});
</script>
@endsection
