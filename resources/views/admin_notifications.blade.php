@include('dashboard_layouts.head')
@include('dashboard_layouts.sidebar')
<div class="card overflow-hidden mb-3">
            <div class="card-header bg-light">
              <div class="row flex-between-center">
                
                    <br>   
                <div class="col-sm-auto">
                  <h5 class="mb-1 mb-md-0">Your Logs</h5>
                </div>
                 <br>
                <br>   
            </div>
            <div class="card-body fs--1 p-0">
                  @foreach ( $logs as $key => $log )
              <a class="border-bottom-0 notification rounded-0 border-x-0 border-300" href="{{$log->link}}">
                <div class="notification-avatar">
                  
                </div>
                   <br>  
                <div class="notification-body">
                  <p class="mb-1">Comments: {{$log->log}} </p>
                     <p class="mb-1">Created at: {{$log->created_at}}</p>
                </div>
              </a>
               @endforeach
             </div>
          </div>
            <div class="card-footer bg-light d-flex justify-content-center">
              <div>
                   @if($_GET['page'] > '1') 
                <a class="btn btn-falcon-default btn-sm me-2" href="/adminnotifications?id={{$_GET['id']}}&page={{$_GET['page']-1}}&limit={{$_GET['limit']}}"><span class="fas fa-chevron-left"></span></a>
                @endif
                 @if($total_pages != 0)
                   @for($i = 1; $i <= $total_pages; $i++)
                    @if($_GET['page'] == $i)
                    <a class="btn btn-sm btn-falcon-default text-primary me-2 active" href="/adminnotifications?id={{$_GET['id']}}&page={{$i}}&limit={{$_GET['limit']}}">{{$i}}</a>
                    @else
                        <a class="btn btn-sm btn-falcon-default text-primary me-2" href="/adminnotifications?id={{$_GET['id']}}&page={{$i}}&limit={{$_GET['limit']}}">{{$i}}</a>
                    @endif
                    @endfor
                 @endif
                 @if($_GET['page'] < $total_pages) 
                 <a class="btn btn-falcon-default btn-sm me-2" href="/adminnotifications?id={{$_GET['id']}}&page={{$_GET['page']+1}}&limit={{$_GET['limit']}}"><span class="fas fa-chevron-right"></span></a>
                @endif
              </div>
            </div>
</div>

@include('dashboard_layouts.footer')