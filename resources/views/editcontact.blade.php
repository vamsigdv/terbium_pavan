
@extends('dashboard_layouts.adminmaster')
 @section('content')   
<!-- Stylesheet -->
<link href="{{asset('chips/tagsinput.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('chips/tagsinput.js')}}"></script>
<style>
    .bootstrap-tagsinput .badge [data-role="remove"]::after {
    content: "×";
    background-color: rgba(0, 0, 0, 0.1);
    font-size: 13px;
    padding: 0px 4px;
    border-radius: 50%;
}
</style>
 <div class="card mb-3">
    <div class="card-header">
      <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0">Edit Contact Details</h5>
        </div>
      </div>
    </div>
    <div class="card-body bg-light">
      <div class="tab-content">
        <form id="create_movie" action="{{ route('contact.update', $contact->id)}}" method="POST" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            @csrf
            

        
            <div class="row gx-2">
                <div class="mb-3 col-md-3">
                    <input class="form-control" type="hidden" id="id" name="id" value="{{$contact->id}}"  required />
                    <label class="form-label" for="">First Name</label><span style="color:red"> *</span>
                    <input class="form-control" type="text"  name="firstname" value="{{ $contact->firstname }}" required>
                </div>
                  <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Last Name</label><span style="color:red"> *</span>
                    <input class="form-control" type="text"  name="lastname" value="{{ $contact->lastname }}" required>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Middle Name</label>
                    <input class="form-control" type="text"  name="middlename" value="{{ $contact->middlename }}">
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Gender</label><span style="color:red"> *</span>
                    <br>
                    <input type="radio"  name="gender" value="Female" {{ ($contact->gender=="Female")?'checked':''}}> Female
                    <input type="radio"  name="gender" value="Male" {{ ($contact->gender=="Male")?'checked':''}}> Male
                </div>
            </div>
           
            <div class="row gx-2">
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Email</label><span style="color:red"> *</span>
                    <input class="form-control" type="email"  name="email" value="{{ $contact->email }}" required>
                </div>
                 <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Location</label><span style="color:red"> *</span>
                   <select class="form-select" aria-label="Default select example" name="location" id="location" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($location)){
                            foreach ($location as $lkey => $lvalue) { ?>
                                <option value="{{$lvalue->id}}" {{ ($contact->locationId==$lvalue->id)?'selected':'' }}>{{$lvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                  <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Mobile Number</label><span style="color:red"> *</span>
                    <input class="form-control" type="text"  name="mobile" value="{{ $contact->mobile }}" id="mobile" required>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Alternative Mobile Number</label>
                    <input class="form-control" type="text"  name="altmobile" value="{{ $contact->altmobile }}" id="altmobile">
                </div>
               
            </div>
            
            <div class="row gx-2">
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Contact Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="contactType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($contacttype)){
                            foreach ($contacttype as $key => $value) { ?>
                                <option value="{{$value->id}}" {{ ($contact->contactTypeId==$value->id)?'selected':'' }}>{{$value->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                  <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Work Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="workType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($worktype)){
                            foreach ($worktype as $wkey => $wvalue) { ?>
                                <option value="{{$wvalue->id}}" {{ ($contact->workTypeId==$wvalue->id)?'selected':'' }}>{{$wvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Consultant Type</label>
                    <select class="form-select" aria-label="Default select example" name="consultantType" >
                         <option value="">Select Option</option>
                         <?php if(!empty($consultanttype)){
                            foreach ($consultanttype as $ctkey => $ctvalue) { ?>
                                <option value="{{$ctvalue->id}}" {{ ($contact->consultantTypeId==$ctvalue->id)?'selected':'' }}>{{$ctvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Date Of Birth</label>
                    <input class="form-control" type="date"  name="dob" value="{{ $contact->dob }}">
                </div>
            </div>
            
            
            <div class="row gx-2">
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">State</label><span style="color:red"> *</span>
                   <select class="form-select" aria-label="Default select example" name="stateName" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($states)){
                            foreach ($states as $skey => $svalue) { ?>
                                <option value="{{$svalue->id}}" {{ ($contact->stateId==$svalue->id)?'selected':'' }}>{{$svalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-6 col-md-3">
                    <label class="form-label" for="">Technology</label><span style="color:red"> *</span>
                   <select class="form-select organizerMultiple_director" id="organizerMultiple_artist" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" name="technology[]" multiple="multiple" required>
                         <option value="">Select Option</option>
                         <?php 
                          $technologies=explode(",", $contact->technologyId);
                         if(!empty($technology)){
                            foreach ($technology as $tkey => $tvalue) { ?>
                                <option value="{{$tvalue->id}}"  <?=(in_array($tvalue->id,$technologies))?'selected':'' ?>>{{$tvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                  <div class="mb-6 col-md-3">
                    <label class="form-label" for="">Skill</label><span style="color:red"> *</span>
                    <select class="form-select organizerMultiple_writers" id="organizerMultiple_writers" multiple="multiple" data-options='{"removeItemButton":true,"placeholder":true}'  aria-label="Default select example" name="skill[]" required>
                         <option value="">Select Option</option>
                         <?php 
                         $skillsarray=explode(",", $contact->skillId);
                         if(!empty($skills)){
                            foreach ($skills as $skey => $svalue) { ?>
                                <option value="{{$svalue->id}}"  <?=(in_array($svalue->id,$skillsarray))?'selected':'' ?>>{{$svalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
         
            </div>

            
            <div class="mb-3 ">
                <label class="form-label" for="">Description</label>
                <textarea class="form-control description" name="description" id="" rows="2">{{ $contact->description }}</textarea>
            </div>
             </br>
            <div class="col-auto align-self-center">
          <h5 class="mb-0">Edit Payment Details</h5>
        </div>
        </br>
           <div class="row gx-2">
                       <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Payment Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="paymentType" required>
                         <option value="">Select Option</option>
                         <?php if(!empty($paymenttype)){
                            foreach ($paymenttype as $pkey => $pvalue) { ?>
                                <option value="{{$pvalue->id}}" {{ ($contact->paymentTypeId==$pvalue->id)?'selected':'' }}>{{$pvalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
               
                    <div class="mb-3 col-md-3">
                        <label class="form-label" for="">Zelle ID</label>
                        <input class="form-control" type="text"  name="ZilleID"  value="{{ $contact->ZilleID }}" >
                    </div>
                           <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Routing Number/ Bank Account Number</label>
                    <input class="form-control" type="text"  name="RoutingNumber"  value="{{ $contact->RoutingNumber }}" >
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Bank Name Zelle</label>
                    <input class="form-control" type="text"  name="BankNameZille" value="{{ $contact->BankNameZille }}" >
                </div>
 
        </div>
        

     
        
            
            <div class="row gx-2">
        
          <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Created by</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="caseworker" {{ (Auth::guard('adminiy')->user()->role != 'Super Admin')? 'disabled':'' }}>
                         <option value="">Select Option</option>
                         <?php if(!empty($caseworker)){
                            foreach ($caseworker as $cwkey => $cwvalue) { ?>
                                <option value="{{$cwvalue->id}}" {{ ($contact->caseworkerId==$cwvalue->id)?'selected':'' }}>{{$cwvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                    <input class="form-control" type="hidden"  name="caseworker" value="{{ $contact->caseworkerId }}" >
                </div>
        
        
                  <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Account Type</label><span style="color:red"> *</span>
                    <select class="form-select" aria-label="Default select example" name="accountType">
                         <option value="">Select Option</option>
                         <?php if(!empty($accounttype)){
                            foreach ($accounttype as $akey => $avalue) { ?>
                                <option value="{{$avalue->id}}" {{ ($contact->accountTypeId==$avalue->id)?'selected':'' }}>{{$avalue->name}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Bank Name</label>
                    <input class="form-control" type="text"  name="bankname" value="{{ $contact->bankname }}" >
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">Branch</label>
                    <input class="form-control" type="text"  name="branch" value="{{ $contact->branch }}" >
                </div>
            </div>
            
            
            <div class="row gx-2">
                <div class="mb-3 col-md-3">
                    <label class="form-label" for="">IFSC Code</label>
                    <input class="form-control" type="text"  name="ifsc" value="{{ $contact->ifsc }}" >
                </div>
                 <div class="mb-3 col-md-3">
                        <label class="form-label" for="">Contact Name</label>
                        <input class="form-control" type="text"  name="Contactname" value="{{ $contact->Contactname }}" >
                    </div>
                
                
       </div>
                   </br>
            <div class="col-auto align-self-center">
          <h5 class="mb-0">Edit Attachments</h5>
        </div>
        </br>     
            
    <div class="row gx-4">
        <div class="mb-4 col-sm-4">
       
        <?php if (!empty($contact->profilePic)) { ?>
                        <a href="{{ url($contact->profilePic) }}" target="_blank" rel="noopener noreferrer"> <img src="{{ url($contact->profilePic) }}" height="140" width="120" id="photo"/> </a>
                    <?php }else{ ?>
                    <img src="/assets/img/team/3-thumb.jpg" height="140" width="120" id="photo">
                    <?php } ?>
          <input class="form-control" type="file" accept="image/*" id="photo" name="photo" onchange="upload(event, 'photo')"/>
           <label class="form-label" >Photo/logo</label>
        </div>
  
           <div class="mb-4 col-sm-4">
         
                <?php if (!empty($contact->coverPic)) { ?>
                        <a href="{{ url($contact->coverPic) }}" target="_blank" rel="noopener noreferrer"> <img src="{{ url($contact->coverPic) }}" height="140" width="350"  id="cover_photo"/> </a>
                    <?php }else{ ?>
                        <img src="/assets/img/generic/4.jpg" height="140" width="350" id="cover_photo" onchange="upload(event, 'cover_photo')">
                    <?php } ?>
                  <input class="form-control" type="file" accept="image/*" id="photo" name="cover_photo"   onchange="upload(event, 'cover_photo')"/>
                         <label class="form-label" >Cover Picture</label>
              </div>
              
              
            <div class="mb-4 col-md-4">
                      
                    <?php if (!empty($contact->fileinput)) { ?>
                        <a href="{{ url($contact->fileinput) }}" target="_blank" rel="noopener noreferrer" > <img src="/assets/img/team/file.png" height="140" width="120"> </a>
                    <?php } ?>
                    <input class="form-control" type="file" accept="application/pdf" name="fileinput" onchange="upload(event, 'movie_poster')">
               <label class="form-label" for="">Replace Existing File</label>
            </div>
              
            </div>
            
            
            
            
            
            
            
            
            
            
            
            <div class="form-check"><input class="form-check-input" type="checkbox" {{ ($contact->status==1)?'checked':''}} name="status" />
                    <label class="form-label" >Active</label>
                  </div>
            
            
        <div class="col-12"><button class="btn btn-primary" type="submit">Submit</button></div>
     </form>
      </div>
    </div>
     
</div>

 <script>
  {
  tagClass: function(item) {
    return 'badge badge-info';
  },
  focusClass: 'focus',
  itemValue: function(item) {
    return item ? item.toString() : item;
  },
  itemText: function(item) {
    return this.itemValue(item);
  },
  itemTitle: function(item) {
    return null;
  },
  freeInput: true,
  addOnBlur: true,
  maxTags: undefined,
  maxChars: undefined,
  confirmKeys: [13, 44],
  delimiter: ',',
  delimiterRegex: null,
  cancelConfirmKeysOnEmpty: false,
  onTagExists: function(item, $tag) {
    $tag.hide().fadeIn();
  },
  trimValue: false,
  allowDuplicates: false,
  triggerChange: true
}
 </script>
  <script>
     $('.addactor').hide();
     $('.adddirector').hide();
     $('.addgener').hide();
     $('#organizerMultipleGener').on('change',function(){
        var geners = $(this).val();
        $('#geners').val(geners);
     })
      $('#release_date').on('change',function(){
          var release_date = $(this).val();
          $('#expiry_date').attr('min', release_date);
      });
      $('#expiry_date').on('change',function(){
          var expiry_date = $(this).val();
          var release_date = $('#release_date').val();
         //alert(release_date);
          if(expiry_date == release_date)
          {
            alert("Please Rental Period EndDate is greaterThan expiry Date");
            return false;
          }
          else if(release_date == '')
          {
              alert("Please Select Release Date");
              return false;
          }
          $('#expiry_date').attr('min', release_date);
      });
      $('#location').on('change',function()
      {
          var location = $(this).val();
         // alert(location);
          	  $.ajax({
                          url: "{{url('fetchlocation')}}",
                          type: "POST",  
                          data: {
                        location: location,
                        _token: '{{csrf_token()}}'
                        },
                    		dataType: 'json',                          
                            success: function (result) {
                                $('#mobile').val(result.countryCode);
                                $('#altmobile').val(result.countryCode);
                            }
                        });
      })
   
 </script>
   <script>

$(document).ready(function() {
   
   $('#organizerMultiple_artist').select2({
      selectOnClose: false
    });

   $('#organizerMultiple_writers').select2({
      selectOnClose: false
    });
  
});

</script>
@endsection