@extends('dashboard_layouts.adminmaster')
<style>
.marT20{ margin-top:20px;}
.smalcards .card{height:100%; }
.smalcards .col-md-3{ margin-bottom: 10px;}
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}

   .text-700 {
    color: #5e6e82!important;
}
.mb-2, .my-2 {
    margin-bottom: 0.5rem!important;
}
.align-items-center {
    -webkit-box-align: center!important;
    -ms-flex-align: center!important;
    align-items: center!important;
}
.media {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start
}
.svg-inline--fa.fa-w-16 {
    width: 1em;
}
svg:not(:root).svg-inline--fa {
    overflow: visible;
}
.media-body {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
@media (min-width: 992px)
.pl-lg-3, .px-lg-3 {
    padding-left: 1rem!important;
}
.mr-2, .mx-2 {
    margin-right: 0.5rem!important;
}
.btn-falcon-primary:focus:not(.disabled):not(:disabled),.btn-falcon-primary:hover:not(.disabled):not(:disabled) {
    color: #1966cc!important;
    -webkit-box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08);
    box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08)
}
.pl-3, .px-3 {
    padding-left: 1rem!important;
}
.pr-3, .px-3 {
    padding-right: 1rem!important;
}
var avgMin
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}
.tabcontent_movie {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>

@section('content')

<div class="card mb-3">
  
    <div class="card-body">
        <div class="row">
          
            <div class="col-lg-4">
                <h4 class="mb-3">H1B Pre-Registration</h4>
                  <input class="form-control" type="hidden" id="id" name="id" value="{{$h1b->id}}" required />
                </div>
           
           
                <div class="col-lg-4 col-auto ms-auto" style="float:right">
                <a href="/h1b/{{ $h1b->id }}/edit" rel="noopener noreferrer"><button class="btn btn-falcon-success btn-sm"  type="button">Edit Below  Details</button></a>
             </div>
             
              </div>
        </div>
        
  
             
    <div class="card-body">

            <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   First Name: {{ $h1b->first_name }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Last Name {{ $h1b->last_name }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Middle Name : {{ $h1b->middle_name }}
                </h5>
              </div>
         
            <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Email ID : {{ $h1b->email }}
                </h5>
                  <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Phone: {{ $h1b->phone }}
                  </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Qualification: {{ $h1b->qualification }}
                </h5>
             </div>
             <div class="row gx-3">
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Marital Status: 
                   @if($h1b->marital_status == 1)
                       Married
                    @elseif($h1b->marital_status == 2)
                        UnMarried
                    @elseif($h1b->marital_status == 3)
                        Divorced
                    @endif
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Years of Experiance: {{ $h1b->years_exp }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Technology: {{ $h1b->technolgy }}
                </h5>
             </div>
             <div class="row gx-3">
             
              <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
               H1B USCIS Cap: @if($cap)
    {{ $cap->year }}
@endif
              </h5>
              <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Reference By :{{ $h1b->ref_by }}
              </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Reference Contact: {{ $h1b->ref_contact }}
        </h5>
             
             
             </div>
        
            <div class="row gx-3">
                
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Pre-Registration Receipt#   {{$h1b->pre_recipt}}
              </h5>
                
       <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Gender: @if($h1b->gender == 1)
                        Male
                    @elseif($h1b->gender == 2)
                        Female
                    @elseif($h1b->gender == 3)
                        Others
                    @endif
        </h5>
        
                       
      <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Technical Skills :{{ $h1b->skills }}
                </h5>
            </div>
         
         
         
         
         
                   <div class="row">
                   
    
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Country Located:  @if($h1b->locations)
   {{$h1b->locations->name}}
@endif   
              </h5>
      <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Passport Details: {{ $h1b->passport }}
                </h5>
   
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Pre-Registration Status: 
           @if($h1b->pre_status == 1)
                        Picked
                    @elseif($h1b->pre_status == 2)
                        Not Picked
                    @endif
           
           
        </h5>
    </div> 
    
         
         
         
         
         
         
         
         
         
         
         
         
           </br>
           <div class="row">
          
            <div class="col-lg-4">
                <h4 class="mb-3">Post Selection Details</h4>
                  <input class="form-control" type="hidden" id="id" name="id" value="{{$h1b->id}}" required />
                </div>
              </div>

             <div class="row gx-3">
              
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  No of Dependents: {{ $h1b->dependents }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Case Worker : {{ $h1b->case_worker }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  RFE Response Status:
                      
                   @if($h1b->rfe_status == 1)
                       Preparing the Documents
                    @elseif($h1b->rfe_status ==2)
                        Submitted
                    @endif
                 </h5>
            </div>
           
          
             <div class="row gx-3">
              
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Petition Acknowledgement Email :
                  
                      
                   @if($h1b->ack_email == 1)
                      Yes
                    @elseif($h1b->ack_email == 2)
                        No
                    @endif
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  H1B Petition Documents Verfication :
                      
                   @if($h1b->h1b_petition_doc == 1)
                      Yes
                    @elseif($h1b->h1b_petition_doc == 2)
                        No
                    @endif
                  
                  
                </h5>
                   <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                 H1B Receipt# :{{ $h1b->h1b_recp }}
        </h5>
            </div>
           
             <div class="row gx-3">
                           <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           CoWorker Affidevit: 
           @if($h1b->co_affidevit == 1)
                Affidavit Not Needed
            @elseif($h1b->co_affidevit == 2)
                Affidavit Needed
            @elseif($h1b->co_affidevit == 3)
                Affidavit Completed
            @endif
        </h5>
            
                    <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           RFE: 
             @if($h1b->rfe == 1)
                      Received
                    @elseif($h1b->rfe == 2)
                        Not Received
                    @endif
                  
        </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  H1B Application Status : @if($h1b->application_status == 1)
                       Completed
                    @elseif($h1b->application_status == 2)
                       Pending
                  
                    @endif
                </h5>
                      
                </h5>
            </div>
        
        
        
        
            
    
    <div class="row gx-3">
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Shipping Tracker: {{ $h1b->ship_tracker }}
                
        </h5>
      <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Attorney Case Worker : {{ $h1b->attorney_emp }}
        </h5>
        
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Service Center : 
                   @if($h1b->service_center == 1)
                       California
                    @elseif($h1b->service_center == 2)
                    Texas
                     @elseif($h1b->service_center == 3)
                    Vermont
                       @elseif($h1b->service_center == 4)
                    Nebraska
                    @endif
                </h5>
        
    </div>
          <div class="row gx-3">
                
   
        
    </div>
  
        
            </br>
           <div class="row">
          
            <div class="col-lg-4">
                <h4 class="mb-3">Post Approval of H1B  Petition</h4>
                  <input class="form-control" type="hidden" id="id" name="id" value="{{$h1b->id}}" required />
                </div>
              </div>
        
        
        
        <div class="row gx-3">
 
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           USCIS Status: 
            @if($h1b->uscis_status == 1)
                       Approved
                    @elseif($h1b->uscis_status == 2)
                       RFE
                    @elseif($h1b->uscis_status == 3)
                       Declined
                    @endif
        </h5>
               
                       
              
        
    </div>
            <div class="row gx-3">
       <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           DS-160 Form: 
            @if($h1b->ds_form == 1)
                       Submitted
                    @elseif($h1b->ds_form == 2)
                       Not Submitted
                    @endif
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Visa Interview: {{ $h1b->visa_interview }}
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
          Vendor Letters : {{ $h1b->vendor_doc }}
        </h5>
    </div>
     <div class="row gx-3">
                  <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Visa Interview Slot : 
                    @if($h1b->Visa_int_slot == 1)
                       Values
                    @elseif($h1b->Visa_int_slot == 2)
                       Booked
                    @endif
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                 Bio Metric Details: {{ $h1b->bio_details }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                 Welcome Email: {{ $h1b->wel_email }}
                </h5>
               
          
    </div>
      
   
    
    
   

               <div class="row gx-3">
        
       
     
    </div>
  
   <div class="row gx-3">
       <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
       Visa Interview Required Docs : 
       
                    @if($h1b->visa_int_req_doc == 1)
                       Sent
                    @elseif($h1b->visa_int_req_doc == 2)
                       Not Yet Sent
                    @endif
      </h5>  
      <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
       Visa Slot Details: {{ $h1b->visa_slot_det }}
      </h5>  
       <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
       Supporting Document Verified :
            @if($h1b->doc_verified == 1)
                      Yes
                    @elseif($h1b->doc_verified == 2)
                        No
                    @endif
                  
      </h5>        
               
             
      
    </div>
    

        
        


     <div class="row gx-3">
                
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Education Evaluation :
                    @if($h1b->edu_evaluation == 1)
                       Complete
                    @elseif($h1b->edu_evaluation == 2)
                       Not Needed
                    @elseif($h1b->edu_evaluation == 3)
                       Pending
                    @endif
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  LCA: 
                  @if($h1b->lca == 1)
                       Certified
                    @elseif($h1b->lca == 2)
                       Pending
                    
                    @endif
                </h5>
    </div>




    <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Organization: {{ $h1b->organization }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  H1B Receipt :{{ $h1b->h1b_recp }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  H1B Documentation Status: 
                  @if($h1b->documentation == 1)
                       Complete
                    @elseif($h1b->documentation == 2)
                       Pending
                    @elseif($h1b->documentation == 3)
                       Not Needed
                    @endif
                </h5>
    </div>
</div>
<div class="card mb-3">
    <div class="card-header">
        <div class="row flex-between-end">
            <div class="col-auto align-self-center">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active"  data-bs-toggle="tab" href="#documents-tab" role="tab" aria-controls="tab-profile" aria-selected="true">Documents</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"  data-bs-toggle="tab" href="#communication-tab" role="tab" aria-controls="tab-profile" aria-selected="false">Communications</a>
                        
                        </li>
                        
                </ul>
            </div>
        </div>
    </div>
    
    </div>
    <div class="card-body pt-0">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="documents-tab" role="tabpanel" aria-labelledby="tab-documents-tab">
                <div class="col-auto ms-auto" style="float: right; margin-bottom: 3px;">
                    <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>
                </div>
                <table  class="table documents_datatable table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Document Name</th>
                            <th>Document Link</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="communication-tab" role="tabpanel" aria-labelledby="tab-communication-tab">
                <div class="col-auto ms-auto" style="float: right; margin-bottom: 3px;">
                    <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#h1bcomments-edit-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>
                </div>
            <table  class="table table-bordered communications_datatable table-striped fs--1 mb-0" style="width:100%">
            <thead class="bg-200 text-900">
              <tr>
                <th >ID</th>
                <th >Title</th>
                <th >Comments</th>
                <!--<th>Action</th>-->
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
         </table>
              
            </div>
        </div>
    </div>
    <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add Documents</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('addh1bdocuments', $h1b->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row gx-2">
                 <input class="form-control" type="hidden" id="id" name="edit_id" value="{{$h1b->id}}"  required />
                              <div class="col-sm-10">
                                  <div class="form-group">
                                        <label for="email">Document Type<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="document_type">
                                            <option value="">Select Option</option>
                                            <option value="1">Petitioner Document</option>
                                            <option value="2">Benefecary Document</option>
                                            <option value="3">Dependent Document</option>
                                            <option value="4">Recipt</option>
                                            <option value="5">Passport</option>
                                            <option value="6">Educational Details</option>
                                            <option value="7">Employment History</option>
                                            <option value="8">Educational Documents</option>
                                             <option value="9">Updated Resume</option>
                                             <option value="10">Experince Letters</option>
                                             <option value="11">Individual Profile</option>
                                             <option value="12">Job  duties mapping to the course work</option>
                                             <option value="13">Employment Offer Letter</option>
                                            <option value="14">MSA</option>
                                              <option value="15">G28</option>
                                                <option value="16">I129</option>
                                                  <option value="17">LOS</option>
                                                    <option value="18">LCA</option>
                                        </select>
                                    </div>
                              </div>
                              <div class="row gx-2 remove_comments">
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label for="email">Comment<span class="text-danger">*</span>:</label>
                                          <input type="file" name="document" class="form-control">
                                        </div>
                                    </div>
                                    </div>
                          </div>
                          <div class="comments_wrapper"></div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
       
      </div>
    </div>
  </div>
</div>




<script>

</script>
</div>
<div class="modal editmodal fade bd-modal-lg" id="h1bcomments-edit-modal" tabindex="-1" role="dialog" aria-labelledby="h1bcomments-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Comments Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <form action="{{ route('addh1bcomments', $h1b->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row gx-2">
                 <input class="form-control" type="hidden" id="id" name="edit_id" value="{{$h1b->id}}"  required />
                              <div class="col-sm-10">
                                  <div class="form-group">
                                        <label for="email">Title<span class="text-danger">*</span>:</label>
                                        <input type="text" name="comm_title" class="form-control">
                                    </div>
                              </div>
                             <!-- <div class="col-sm-2">
                                  <div class="form-group">
                                      <label>Action</label>
                                      <button class="btn btn-falcon-default btn-sm" type="button" id="add_comments">Add Comments
                                      </button>
                                 </div>
                              </div>-->
                              <div class="row gx-2 remove_comments"><div class="col-sm-10"><div class="form-group"><label for="email">Comment<span class="text-danger">*</span>:</label><textarea class="form-control" name="communication[]"></textarea></div></div><div class="col-sm-2 mt-4"><div class="form-group"></div></div></div>
                          </div>
                          <div class="comments_wrapper"></div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    var maxcomments = 10;
    var addComment = $('#add_comments');
    var comments_wrapper = $('.comments_wrapper'); //Input field wrapper
    var body = '<div class="row gx-2 remove_comments"><div class="col-sm-10"><div class="form-group"><label for="email">Comment<span class="text-danger">*</span>:</label><textarea class="form-control" name="communication[]"></textarea></div></div><div class="col-sm-2 mt-4"><div class="form-group"><button class="btn btn-warning remove_comments_button">Delete</button></div></div></div>';
    var y = 0; //Initial field counter is 0
    // Once add button is clicked
    $(addComment).click(function(){
        //Check maximum number of input fields
        if(y < maxcomments){ 
            y++;
           //Increase field counter
            $(comments_wrapper).append(body); //Add field html
        }else{
            alert('A maximum of ' + maxcomments + ' fields are allowed to be added. ');
        }
    });
    
    // Once remove button is clicked
    $(comments_wrapper).on('click', '.remove_comments_button', function(e){
        e.preventDefault();
        $(this).closest('.remove_comments').remove(); //Remove field html
        y--; //Decrease field counter
    });
  });
</script>
 
@endsection