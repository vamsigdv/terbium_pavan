@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card mb-3">
     <div class="card-header">
         <div class="row flex-between-end">
        <div class="col-auto align-self-center">
          <h5 class="mb-0" data-anchor="data-anchor">SME Payments</h5>
          
        </div>
        <div class="col-auto ms-auto">
            <div>
              {{-- target="_blank" --}}
                <div>
               <!-- <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button>--></div>
            </div>
        </div>
      </div>
     </div>

    <div class="card-body pt-0">
        <div class="tab-content">
            <div class='row'>
                <div class="mb-3 col-md-4">
                    <label class="form-label" >From Date</label>
                    <input class="form-control" type="date" id="fromDate" name="nextcomitdate" onchange="return searchSMPayments();">
                </div>
                <div class="mb-3 col-md-4">
                    <label class="form-label" >To Date</label>
                    <input class="form-control" type="date" id="toDate" name="nextcomitdate" onchange="return searchSMPayments();">
                </div>
                <div class="mb-3 col-md-4">
                    <label class="form-label">Type</label>
                    <select class="form-select" aria-label="Default select example" name="type" id="type" onchange="return searchSMPayments();">
                        <option value="">Select Option</option>
                        <option value="Interview">Interview</option>
                        <option value="Training">Training</option>
                        <option value="Batch">Batch</option>
                        <option value="Support">Support</option>
                    </select>
                </div>
            </div>
            <div class='row'>
                <div class="mb-3 col-md-3">
                    <label class="form-label">Consultant</label>
                    <select class="form-select" aria-label="Default select example" name="contactName" id="contactName" onchange="return searchSMPayments();">
                         <option value="">Select Option</option>
                         <?php if(!empty($consultants)){
                            foreach ($consultants as $ckey => $cvalue) { ?>
                                <option value="{{ $cvalue->id }}">{{ $cvalue->firstname }}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label">Employee</label>
                    <select class="form-select" aria-label="Default select example" name="SMEName" id="SMEName" onchange="return searchSMPayments();">
                         <option value="">Select Option</option>
                         <?php if(!empty($employees)){
                            foreach ($employees as $ckey => $cvalue) { ?>
                                <option value="{{$cvalue->id}}" >{{$cvalue->firstname}}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label">Created By</label>
                    <select class="form-select" aria-label="Default select example" name="created_by" id="created_by" onchange="return searchSMPayments();">
                         <option value="">Select Option</option>
                         <?php if(!empty($created_users)){
                            foreach ($created_users as $ckey => $created_user) { ?>
                                <option value="{{ $created_user->id }}" >{{ $created_user->name }}</option>
                        <?php    }} ?>
                    </select>
                </div>
                <div class="mb-3 col-md-3">
                    <label class="form-label">Payment Status</label>
                    <select class="form-select" aria-label="Default select example" name="payment_status" id="payment_status" onchange="return searchSMPayments();">
                         <option value="">Select Option</option>
                         <?php if(!empty($payment_statuses)){
                            foreach ($payment_statuses as $ckey => $payment_status) { ?>
                                <option value="{{ $payment_status->id }}" >{{ $payment_status->name }}</option>
                        <?php    }} ?>
                    </select>
                </div>
                
            </div>
            <div class="row">
                <div class="mb-3">
                    <label class="form-label"></label>
                    <button type="button" class="btn btn-warning form-control" name="Reset" onClick="return resetFields('smpayments');">Reset</button>
                </div>
            </div>
            <table class="table table-bordered smepayments_datatable table-striped fs--1 mb-0" style="width:100%">
                <thead class="bg-200 text-900">
                  <tr>
                    <th >ID</th>
                    <th >Name of the Consultant</th>
                    <th >Transaction Date</th>
                    <th >Amount Paid</th>
                     <th >Payment Status</th>
                    <th >Type</th>
                    <th >Employee</th>
                    <th>SME NAME</th>
                      <th>SME Number</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
   <div class="modal editmodal fade bd-modal-lg" id="installments-edit-modal" tabindex="-1" role="dialog" aria-labelledby="album-edit-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Installment Edit</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
           <div id="edit_data"></div>
        </div>
      </div>
    </div>
  </div>


@endsection