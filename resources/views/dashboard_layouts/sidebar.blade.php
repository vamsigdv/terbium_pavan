
<style>
    .wid100p{ width:100% !important;}
    .widauto{ width:auto !important;}
</style>
 <?php $role= Auth::guard('adminiy')->user()->role ; 
        $roleid = (DB::table('role')->where('name', $role)->first());
?>
 <div class="container-fluid" data-layout="container">
   <script>
     var isFluid = JSON.parse(localStorage.getItem('isFluid'));
     if (isFluid) {
       var container = document.querySelector('[data-layout]');
       container.classList.remove('container');
       container.classList.add('container-fluid');
     }
   </script>
   <nav class="navbar navbar-light navbar-vertical navbar-expand-xl navbar-card" style="display: none;">
     <script>
       var navbarStyle = localStorage.getItem("navbarStyle");
       if (navbarStyle && navbarStyle !== 'transparent') {
         document.querySelector('.navbar-vertical').classList.add(`navbar-${navbarStyle}`);
       }
     </script>
     <div class="d-flex align-items-center">
       <div class="toggle-icon-wrapper">
         <button class="btn navbar-toggler-humburger-icon navbar-vertical-toggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
       </div><a class="navbar-brand" href="">
         <div class="d-flex align-items-left py-3">
   <!-- <a href="/" > <img  src="{{asset('images/logo.png')}}" alt="" width="120"></a>-->
    <div class="d-flex align-items-center"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="80" /><span class="font-sans-serif"></span></div>

         </div>
       </a>
     </div>
     <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
       <div class="navbar-vertical-content scrollbar">

       

        <ul class="navbar-nav flex-column mb-3" id="navbarVerticalNav">
  
            <?php  $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 1)->first();
                   if(!empty($rolepermissions)){
            ?>         
            <li class="nav-item">
                <a class="nav-link {{ request()->routeIs('index') ? 'active' : '' }}" href="/" aria-expanded="false">
                    <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-chart-pie"></span></span><span class="nav-link-text ps-1">Dashboard</span>
                    </div>
                  </a>
          </li>
        <?php }  ?>
<hr>
        <li class="nav-item">
                @php
                        $login = 'show';
                        $setting = 'show';
                        $contact = 'show';
                         $general = 'show';
                         $transactions = 'show';
                @endphp
                 <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 2)->first();
                   if(!empty($rolepermissions)){
            ?> 
                     <ul class="nav collapse {{ $contact }}" id="$contact">
               
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('contact.index') ? 'active' : '' }}" href="{{ route('contact.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-podcast"></span></span><span class="nav-link-text ps-1">Contacts</span></div>
                      </a>
                  </li>

                </ul>
                <?php }  ?>
                <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 3)->first();
                   if(!empty($rolepermissions)){
            ?> 
                     <ul class="nav collapse {{ $transactions }}" id="transactions">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('transactions.index') ? 'active' : '' }}" href="{{ route('transactions.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-rupee-sign"></span></span><span class="nav-link-text ps-1">Transactions</span></div>
                      </a>
                  </li>
                </ul>
                 <?php }  ?>
                 <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 4)->first();
                   if(!empty($rolepermissions)){
            ?> 
                     <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('sme.index') ? 'active' : '' }}" href="{{ route('sme.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-wallet"></span></span><span class="nav-link-text ps-1">SME Payments</span></div>
                      </a>
                  </li>

                </ul>
                 <?php }  ?>
                 <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 5)->first();
                   if(!empty($rolepermissions)){
            ?> 
                     <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('interviews.index') ? 'active' : '' }}" href="{{ route('interviews.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-door-open"></span></span><span class="nav-link-text ps-1">Interviews</span></div>
                      </a>
                  </li>

                </ul>
                <?php }  ?>
                <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 6)->first();
                   if(!empty($rolepermissions)){
            ?> 
                <ul class="nav collapse {{ $general }}" id="general">
                    <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('smepaymentsdata') ? 'active' : '' }}" href="{{ route('smepaymentsdata')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-people-arrows"></span></span><span class="nav-link-text ps-1">SME Payments</span></div>
                      </a>
                  </li>

                </ul>
                
                <?php }  ?>
                 <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 20)->first();
                   if(!empty($rolepermissions)){
            ?> 
                <ul class="nav collapse {{ $general }}" id="general">
                    <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('daily_work_track.index') ? 'active' : '' }}" href="{{ route('daily_work_track.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-history"></span></span><span class="nav-link-text ps-1">EMP Daily Logs</span></div>
                      </a>
                  </li>

                </ul>
                   <?php }  ?>
                   <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 19)->first();
                   if(!empty($rolepermissions)){
                ?>
                <ul class="nav collapse {{ $general }}" id="general">
                    <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('notifications') ? 'active' : '' }}" href="/notifications?page=1&limit=10" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-people-arrows"></span></span><span class="nav-link-text ps-1">Notifications</span></div>
                      </a>
                  </li>

                </ul>
                 <?php }  ?>
                <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 7)->first();
                   if(!empty($rolepermissions)){
                ?> 
                     <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('roles.index') ? 'active' : '' }}" href="{{ route('roles.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-smile"></span></span><span class="nav-link-text ps-1">User Privileges</span></div>
                      </a>
                  </li>

                </ul>
                 <?php }  ?>
                <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 8)->first();
                   if(!empty($rolepermissions)){
            ?> 
                     <ul class="nav collapse {{ $login }}" id="login">
               
                    <li class="nav-item">
                        <a class="nav-link  {{ request()->routeIs('users.index') ? 'active' : '' }}" href="{{ route('users.index')}}" >
                            <div class="d-flex align-items-center"><span class="nav-link-icon">
                              <span class="fas fa-skating"></span></span><span class="nav-link-text ps-1">Logins</span></div>
                          </a>
                    </li>

              
       
                
                </ul>
                <?php } ?>
                   <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 18)->first();
                   if(!empty($rolepermissions)){
            ?> 
                <ul class="nav collapse {{ $login }}" id="login">
                    <li class="nav-item">
                        <a class="nav-link  {{ request()->routeIs('h1b.index') ? 'active' : '' }}" href="{{ route('h1b.index')}}" >
                            <div class="d-flex align-items-center"><span class="nav-link-icon">
                              <span class="far fa-paper-plane"></span></span><span class="nav-link-text ps-1">H1B Pre Reg</span></div>
                        </a>
                    </li>
                </ul>
                 <?php } ?>
                  <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 17)->first();
                   if(!empty($rolepermissions)){
            ?> 
                 <ul class="nav collapse {{ $login }}" id="login">
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('caseworker.index') ? 'active' : '' }}" href="{{ route('caseworker.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-swatchbook"></span></span><span class="nav-link-text ps-1">Batches</span></div>
                      </a>
                  </li>
                </ul>
                <?php } ?>
                     <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('generalsettings') ? 'active' : '' }}" href="{{ route('generalsettings')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-toggle-on"></span></span><span class="nav-link-text ps-1">Settings</span></div>
                      </a>
                  </li>

                </ul>
                
                    </li>
                    
                <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 10)->first();
                   if(!empty($rolepermissions)){
            ?> 
                    <li class="nav-item">
    <a class="nav-link dropdown-indicator" href="#reports" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="dashboard">
        <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-copy"></span></span><span class="nav-link-text ps-1">Reports</span>
        </div>
    </a>
     @php
                    $setting = '';
                    if( request()->routeIs('smelist') || request()->routeIs('contactlist')|| request()->routeIs('batchinfo')  )
                    $setting = 'show';
                @endphp
                <ul class="nav collapse {{ $setting }}" id="reports">
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('smelist') ? 'active' : '' }}" href="{{ route('smelist')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-users-cog"></span></span><span class="nav-link-text ps-1">SME List</span></div>
                      </a>
                  </li>
                  
                  <li class="nav-item">
                <a class="nav-link {{  request()->routeIs('contactlist') ? 'active' : '' }}" href="{{ route('contactlist')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-mask"></span></span><span class="nav-link-text ps-1">Consultant List</span>
                    </div>
                  </a>
            </li>
                     <li class="nav-item">
                <a class="nav-link {{  request()->routeIs('batchinfo') ? 'active' : '' }}" href="{{ route('batchinfo')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-mask"></span></span><span class="nav-link-text ps-1">SME Info at Glance</span>
                    </div>
                  </a>
            </li>
                
                </ul>
 </li>
            <?php  } ?>
<hr>
                
                 <?php 
                 $rolepermissions = '';
                 $rolepermissions = DB::table('role_permission')->where('role_id', $roleid->id)->where('permission_id', 9)->first();
                   if(!empty($rolepermissions)){
            ?> 
                <li class="nav-item">
                <a class="nav-link dropdown-indicator" href="#setting" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="dashboard">
                  <div class="d-flex align-items-center">


                    <span class="nav-link-icon"><span class="fas fa-road"></span></span><span class="nav-link-text ps-1">Masters</span>


                  </div>
                </a>
                @php
                    $setting = '';
                    if( request()->routeIs('skills.index') || request()->routeIs('contacttype.index') || request()->routeIs('technology.index') || request()->routeIs('paymenttype.index')|| request()->routeIs('consultanttype.index') || request()->routeIs('location.index') || request()->routeIs('statustype.index') || request()->routeIs('worktype.index')|| request()->routeIs('interviewtools.index') || request()->routeIs('caseworker.index') || request()->routeIs('accounttype.index'))
                    $setting = 'show';
                @endphp
                <ul class="nav collapse {{ $setting }}" id="setting">
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('skills.index') ? 'active' : '' }}" href="{{ route('skills.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-users-cog"></span></span><span class="nav-link-text ps-1">Skills</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('cap.index') ? 'active' : '' }}" href="{{ route('cap.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-users-cog"></span></span><span class="nav-link-text ps-1">Cap</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                <a class="nav-link {{  request()->routeIs('contacttype.index') ? 'active' : '' }}" href="{{ route('contacttype.index')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-mask"></span></span><span class="nav-link-text ps-1">Contact Type</span>
                    </div>
                  </a>
            </li>
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('technology.index') ? 'active' : '' }}" href="{{ route('technology.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-poo-storm"></span></span><span class="nav-link-text ps-1">Technology</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('paymenttype.index') ? 'active' : '' }}" href="{{ route('paymenttype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-money-bill-alt"></span></span><span class="nav-link-text ps-1">Payment Type</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('consultanttype.index') ? 'active' : '' }}" href="{{ route('consultanttype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-weight"></span></span><span class="nav-link-text ps-1">Consultant Type</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link {{  request()->routeIs('location.index') ? 'active' : '' }}" href="{{ route('location.index')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-route"></span></span><span class="nav-link-text ps-1">Location</span>
                    </div>
                    </a>
            </li>
            <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('states.index') ? 'active' : '' }}" href="{{ route('states.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-window-restore"></span></span><span class="nav-link-text ps-1">States</span></div>
                      </a>
                  </li>
                  
                  
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('statustype.index') ? 'active' : '' }}" href="{{ route('statustype.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-window-restore"></span></span><span class="nav-link-text ps-1">Status Type</span></div>
                      </a>
                  </li>
                  
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('payment_status.index') ? 'active' : '' }}" href="{{ route('payment_status.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-window-restore"></span></span><span class="nav-link-text ps-1">Payment Status</span></div>
                      </a>
                  </li>
                  
                  
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('worktype.index') ? 'active' : '' }}" href="{{ route('worktype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-people-carry"></span></span><span class="nav-link-text ps-1">Work Type</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('interviewtools.index') ? 'active' : '' }}" href="{{ route('interviewtools.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-university"></span></span><span class="nav-link-text ps-1">Interview tools</span></div>
                      </a>
                  </li>
                
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('accounttype.index') ? 'active' : '' }}" href="{{ route('accounttype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-swatchbook"></span></span><span class="nav-link-text ps-1">Account Type</span></div>
                      </a>
                  </li>
                
                </ul>
              </li>

               
              </li>

<?php  } ?>

            
             

    



             

        </ul>

       </div>
     </div>
   </nav>


   {{-- sidebar end --}}
   <nav class="navbar navbar-light navbar-glass navbar-top navbar-expand-xl" style="display: none;">


   </nav>
   <div class="content">
 <nav class="navbar navbar-light navbar-glass navbar-top navbar-expand" style="display: none;">
          


         <button class="btn navbar-toggler-humburger-icon navbar-toggler me-1 me-sm-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
         <a class="navbar-brand me-1 me-sm-3" href="index.html">
           <!--<div class="d-flex align-items-center"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="40" /><span class="font-sans-serif"></span></div>-->
         </a>
         <ul class="navbar-nav navbar-nav-icons ms-auto flex-row align-items-center">
           {{-- <li class="nav-item">
             <div class="theme-control-toggle fa-icon-wait px-2"><input class="form-check-input ms-0 theme-control-toggle-input" id="themeControlToggle" type="checkbox" data-theme-control="theme" value="dark" /><label class="mb-0 theme-control-toggle-label theme-control-toggle-light" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch to light theme"><span class="fas fa-sun fs-0"></span></label><label class="mb-0 theme-control-toggle-label theme-control-toggle-dark" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch to dark theme"><span class="fas fa-moon fs-0"></span></label></div>
           </li> --}}
           <li class="nav-item dropdown">
         <a class="nav-link notification-indicator" href="/webprivacy">
        Legal Terms
        </a>

       </li>


       <li class="nav-item dropdown">
         <a class="nav-link notification-indicator notification-indicator-primary px-0 fa-icon-wait" href="/notifications?page=1&limit=10">
        <span class="fas fa-bell" data-fa-transform="shrink-6" style="font-size: 33px;"></span>
       <!-- <?php $admin= DB::table('adminiy')->where('id', Auth::guard('adminiy')->user()->id)->first() ?>
        {{$admin->notification}}-->

        </a>

       </li>

           <li class="nav-item dropdown"><a class="nav-link pe-0 ps-2" id="navbarDropdownUser" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php if(Auth::guard('adminiy')->user()->image)
                {
                   $photo=Auth::guard('adminiy')->user()->image;
                } else
                {
                    $photo="assets/img/team/3-thumb.jpg";
                }
               ?>
               <div class="avatar avatar-xl">
                 <img class="rounded-circle" src="/{{$photo}}" alt="" />
               </div>


             </a>
             <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser">
               <div class="bg-white dark__bg-1000 rounded-2 py-2">
                 {{-- <a class="dropdown-item fw-bold text-warning" href="index.html#!"><span class="fas fa-crown me-1"></span><span>Go Pro</span></a>
                 <div class="dropdown-divider"></div> --}}
                 <div class="dropdown-item">Name-> <b> {{ Auth::guard('adminiy')->user()->name}} </b></div>
                 <div class="dropdown-item">Role-> <b>{{ $role}} </b></div>
                 <a class="dropdown-item" href="/profileupdate">Settings</a>
                 <!--<a class="dropdown-item" href="index.html#!">Feedback</a>
                 <div class="dropdown-divider"></div>-->
              <!--   <a class="dropdown-item" href="/payment"> Payment Settings</a>-->
                 <a class="dropdown-item" href="{{ route('logout')}}">Sign Out</a>
               </div>
             </div>
           </li>
         </ul>
       </nav>

       <script>
         var navbarPosition = localStorage.getItem('navbarPosition');
         var navbarVertical = document.querySelector('.navbar-vertical');
         var navbarTopVertical = document.querySelector('.content .navbar-top');
         var navbarTop = document.querySelector('[data-layout] .navbar-top');
         var navbarTopCombo = document.querySelector('.content [data-navbar-top="combo"]');
         if (navbarPosition === 'top') {
           navbarTop.removeAttribute('style');
           navbarTopVertical.remove(navbarTopVertical);
           navbarVertical.remove(navbarVertical);
           navbarTopCombo.remove(navbarTopCombo);
         } else if (navbarPosition === 'combo') {
           navbarVertical.removeAttribute('style');
           navbarTopCombo.removeAttribute('style');
           navbarTop.remove(navbarTop);
           navbarTopVertical.remove(navbarTopVertical);
         } else {
           navbarVertical.removeAttribute('style');
           navbarTopVertical.removeAttribute('style');
           navbarTop.remove(navbarTop);
           // navbarTopCombo.remove(navbarTopCombo);
         }

       </script>
       <script>
 $('#search').on('click',function(){
     var data = document.getElementById("input").value;
     if(data != ''){
          window.location = "search?page=1&limit=12&data="+data;
     }
     else
     {
       alert("Please Enter Search Data");
     }

 });
</script>
