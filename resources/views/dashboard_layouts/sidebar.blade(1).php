
<style>
    .wid100p{ width:100% !important;}
    .widauto{ width:auto !important;}
</style>
 <div class="container-fluid" data-layout="container">
   <script>
     var isFluid = JSON.parse(localStorage.getItem('isFluid'));
     if (isFluid) {
       var container = document.querySelector('[data-layout]');
       container.classList.remove('container');
       container.classList.add('container-fluid');
     }
   </script>
   <nav class="navbar navbar-light navbar-vertical navbar-expand-xl navbar-card" style="display: none;">
     <script>
       var navbarStyle = localStorage.getItem("navbarStyle");
       if (navbarStyle && navbarStyle !== 'transparent') {
         document.querySelector('.navbar-vertical').classList.add(`navbar-${navbarStyle}`);
       }
     </script>
     <div class="d-flex align-items-center">
       <div class="toggle-icon-wrapper">
         <button class="btn navbar-toggler-humburger-icon navbar-vertical-toggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
       </div><a class="navbar-brand" href="">
         <div class="d-flex align-items-left py-3">
    <a href="http://prabhu.yoursoftwaredemo.com/dashboard" > <img  src="{{asset('images/logo.png')}}" alt="" width="120"></a>

         </div>
       </a>
     </div>
     <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
       <div class="navbar-vertical-content scrollbar">

        <?php $role= Auth::guard('adminiy')->user()->role ; ?>

        <ul class="navbar-nav flex-column mb-3" id="navbarVerticalNav">

             {{--  dashboard  --}}
             <?php if($role == "admin" || $role == "employee" || $role == "studio"){?>
            <li class="nav-item">
                <a class="nav-link {{ request()->routeIs('index') ? 'active' : '' }}" href="/" aria-expanded="false">
                    <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-chart-pie"></span></span><span class="nav-link-text ps-1">Dashboard</span>
                    </div>
                  </a>
          
<?php  }?>
  
               


<hr>






             <?php if ($role == "admin" || $role == "employee" || $role == "vemployee") { ?>

              {{--  Login  --}}
              
              
                        
              <li class="nav-item">
               
                   <?php if ( $role != "vemployee") { ?>
          
                @php
                   
                        $login = 'show';
                        $setting = 'show';
                        $contact = 'show';
                         $general = 'show';
                         $transactions = 'show';


                @endphp
                
                
                
                     <ul class="nav collapse {{ $contact }}" id="$contact">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('contact.index') ? 'active' : '' }}" href="{{ route('contact.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-podcast"></span></span><span class="nav-link-text ps-1">Contacts</span></div>
                      </a>
                  </li>

                </ul>
                
                <ul class="nav collapse {{ $transactions }}" id="transactions">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('transactions.index') ? 'active' : '' }}" href="{{ route('transactions.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-rupee-sign"></span></span><span class="nav-link-text ps-1">Transactions</span></div>
                      </a>
                  </li>

              
       
                
                </ul>
      
                
                 
                
                          <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('sme.index') ? 'active' : '' }}" href="{{ route('sme.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-wallet"></span></span><span class="nav-link-text ps-1">Assign SME</span></div>
                      </a>
                  </li>

                </ul>
                
         
                
                    <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('interviews.index') ? 'active' : '' }}" href="{{ route('interviews.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-door-open"></span></span><span class="nav-link-text ps-1">Interviews</span></div>
                      </a>
                  </li>

                </ul>
                
       
                
             
             
            <ul class="nav collapse {{ $general }}" id="general">
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('smepaymentsdata') ? 'active' : '' }}" href="{{ route('smepaymentsdata')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-people-arrows"></span></span><span class="nav-link-text ps-1">SME Payments</span></div>
                      </a>
                  </li>

                </ul>
             
                
                
              <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('roles.index') ? 'active' : '' }}" href="{{ route('roles.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-smile"></span></span><span class="nav-link-text ps-1">User Privileges</span></div>
                      </a>
                  </li>

                </ul>
                
                 
            
                
                    <ul class="nav collapse {{ $login }}" id="login">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('users.index') ? 'active' : '' }}" href="{{ route('users.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-skating"></span></span><span class="nav-link-text ps-1">Logins</span></div>
                      </a>
                  </li>

              
       
                
                </ul>
                
                
                
                     <ul class="nav collapse {{ $general }}" id="general">
               
              <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('generalsettings') ? 'active' : '' }}" href="{{ route('generalsettings')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-key"></span></span><span class="nav-link-text ps-1">Settings</span></div>
                      </a>
                  </li>

                </ul>
                

<hr>

 <li class="nav-item">
    <a class="nav-link dropdown-indicator" href="#reports" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="dashboard">
        <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-copy"></span></span><span class="nav-link-text ps-1">Reports</span>
        </div>
    </a>
     @php
                    $setting = '';
                    if( request()->routeIs('smelist') || request()->routeIs('contactlist')|| request()->routeIs('batchinfo')  )
                    $setting = 'show';
                @endphp
                <ul class="nav collapse {{ $setting }}" id="reports">
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('smelist') ? 'active' : '' }}" href="{{ route('smelist')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-users-cog"></span></span><span class="nav-link-text ps-1">SME LIST</span></div>
                      </a>
                  </li>
                  
                  <li class="nav-item">
                <a class="nav-link {{  request()->routeIs('contactlist') ? 'active' : '' }}" href="{{ route('contactlist')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-mask"></span></span><span class="nav-link-text ps-1">Consultant List</span>
                    </div>
                  </a>
            </li>
                     <li class="nav-item">
                <a class="nav-link {{  request()->routeIs('batchinfo') ? 'active' : '' }}" href="{{ route('batchinfo')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-mask"></span></span><span class="nav-link-text ps-1">SME Info at Glance</span>
                    </div>
                  </a>
            </li>
                
                </ul>
 </li>
<hr>
                
                
                <li class="nav-item">
                <a class="nav-link dropdown-indicator" href="#setting" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="dashboard">
                  <div class="d-flex align-items-center">


                    <span class="nav-link-icon"><span class="fas fa-road"></span></span><span class="nav-link-text ps-1">Masters</span>


                  </div>
                </a>
                @php
                    $setting = '';
                    if( request()->routeIs('skills.index') || request()->routeIs('contacttype.index') || request()->routeIs('technology.index') || request()->routeIs('paymenttype.index')|| request()->routeIs('consultanttype.index') || request()->routeIs('location.index') || request()->routeIs('statustype.index') || request()->routeIs('worktype.index')|| request()->routeIs('interviewtools.index') || request()->routeIs('caseworker.index') || request()->routeIs('accounttype.index'))
                    $setting = 'show';
                @endphp
                <ul class="nav collapse {{ $setting }}" id="setting">
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('skills.index') ? 'active' : '' }}" href="{{ route('skills.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-users-cog"></span></span><span class="nav-link-text ps-1">Skills</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                <a class="nav-link {{  request()->routeIs('contacttype.index') ? 'active' : '' }}" href="{{ route('contacttype.index')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-mask"></span></span><span class="nav-link-text ps-1">Contact Type</span>
                    </div>
                  </a>
            </li>
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('technology.index') ? 'active' : '' }}" href="{{ route('technology.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-poo-storm"></span></span><span class="nav-link-text ps-1">Technology</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('paymenttype.index') ? 'active' : '' }}" href="{{ route('paymenttype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-money-bill-alt"></span></span><span class="nav-link-text ps-1">Payment Type</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('consultanttype.index') ? 'active' : '' }}" href="{{ route('consultanttype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-weight"></span></span><span class="nav-link-text ps-1">Consultant Type</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link {{  request()->routeIs('location.index') ? 'active' : '' }}" href="{{ route('location.index')}}">
                    <div class="d-flex align-items-center">
                        <span class="nav-link-icon"><span class="fas fa-route"></span></span><span class="nav-link-text ps-1">Location</span>
                    </div>
                    </a>
            </li>
            <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('states.index') ? 'active' : '' }}" href="{{ route('states.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-window-restore"></span></span><span class="nav-link-text ps-1">States</span></div>
                      </a>
                  </li>
                  
                  
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('statustype.index') ? 'active' : '' }}" href="{{ route('statustype.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-window-restore"></span></span><span class="nav-link-text ps-1">Status Type</span></div>
                      </a>
                  </li>
                  
                  <li class="nav-item">
                    <a class="nav-link  {{ request()->routeIs('payment_status.index') ? 'active' : '' }}" href="{{ route('payment_status.index') }}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-window-restore"></span></span><span class="nav-link-text ps-1">Payment Status</span></div>
                      </a>
                  </li>
                  
                  
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('worktype.index') ? 'active' : '' }}" href="{{ route('worktype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-people-carry"></span></span><span class="nav-link-text ps-1">Work Type</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('interviewtools.index') ? 'active' : '' }}" href="{{ route('interviewtools.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-university"></span></span><span class="nav-link-text ps-1">Interview tools</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('caseworker.index') ? 'active' : '' }}" href="{{ route('caseworker.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-paw"></span></span><span class="nav-link-text ps-1">Batches</span></div>
                      </a>
                  </li>
                  <li class="nav-item">
                   
                    <a class="nav-link  {{ request()->routeIs('accounttype.index') ? 'active' : '' }}" href="{{ route('accounttype.index')}}" >
                        <div class="d-flex align-items-center"><span class="nav-link-icon">
                          <span class="fas fa-swatchbook"></span></span><span class="nav-link-text ps-1">Account Type</span></div>
                      </a>
                  </li>
                
                </ul>
              </li>

                <?php } ?>
              </li>



            
              <?php } ?>
              
 

    



             

        </ul>

       </div>
     </div>
   </nav>


   {{-- sidebar end --}}
   <nav class="navbar navbar-light navbar-glass navbar-top navbar-expand-xl" style="display: none;">


   </nav>
   <div class="content">

       <nav class="navbar navbar-light navbar-glass navbar-top navbar-expand" style="display: none;">
          <div class="col-md-9">
           <div class="search-box wid100p" data-list='{"valueNames":["title"]}'>
                <form action="/search" method="get">
                   <div class="row">
                     <div class="col-lg-8">
                          <input class="form-control search-input fuzzy-search" name="data" type="text" id="input" placeholder="Search..." aria-label="Search" />
                          <input type="hidden" name="page" value="1" /> 
                          <input type="hidden" name="limit" value="12" /> 
                           <span class="fas fa-search search-box-icon"></span>
                     </div>
                     <div class="col-lg-4">
                         <input class="form-control search-input fuzzy-search widauto" type="submit" value="search" id="search"/>
                     </div>
                 </div> 
                </form>
</div>
</div>


         <button class="btn navbar-toggler-humburger-icon navbar-toggler me-1 me-sm-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
         <a class="navbar-brand me-1 me-sm-3" href="index.html">
           <div class="d-flex align-items-center"><img class="me-2" src="{{asset('images/logo.png')}}" alt="" width="40" /><span class="font-sans-serif"></span></div>
         </a>
         <ul class="navbar-nav navbar-nav-icons ms-auto flex-row align-items-center">
           {{-- <li class="nav-item">
             <div class="theme-control-toggle fa-icon-wait px-2"><input class="form-check-input ms-0 theme-control-toggle-input" id="themeControlToggle" type="checkbox" data-theme-control="theme" value="dark" /><label class="mb-0 theme-control-toggle-label theme-control-toggle-light" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch to light theme"><span class="fas fa-sun fs-0"></span></label><label class="mb-0 theme-control-toggle-label theme-control-toggle-dark" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch to dark theme"><span class="fas fa-moon fs-0"></span></label></div>
           </li> --}}
           <li class="nav-item dropdown">
         <a class="nav-link notification-indicator" href="/webprivacy">
        Legal Terms
        </a>

       </li>


       <li class="nav-item dropdown">
         <a class="nav-link notification-indicator notification-indicator-primary px-0 fa-icon-wait" href="/adminnotifications?page=1&limit=10">
        <span class="fas fa-bell" data-fa-transform="shrink-6" style="font-size: 33px;"></span>
       <!-- <?php $admin= DB::table('adminiy')->where('id', Auth::guard('adminiy')->user()->id)->first() ?>
        {{$admin->notification}}-->

        </a>

       </li>

           <li class="nav-item dropdown"><a class="nav-link pe-0 ps-2" id="navbarDropdownUser" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php if(Auth::guard('adminiy')->user()->image)
                {
                   $photo=Auth::guard('adminiy')->user()->image;
                } else
                {
                    $photo="assets/img/team/3-thumb.png";
                }
               ?>
               <div class="avatar avatar-xl">
                 <img class="rounded-circle" src="/{{$photo}}" alt="" />
               </div>


             </a>
             <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser">
               <div class="bg-white dark__bg-1000 rounded-2 py-2">
                 {{-- <a class="dropdown-item fw-bold text-warning" href="index.html#!"><span class="fas fa-crown me-1"></span><span>Go Pro</span></a>
                 <div class="dropdown-divider"></div> --}}
                 <div class="dropdown-item">Name-> <b> {{ Auth::guard('adminiy')->user()->name}} </b></div>
                 <div class="dropdown-item">Role-> <b>{{ $role}} </b></div>
                 <a class="dropdown-item" href="/profileupdate">Settings</a>
                 <!--<a class="dropdown-item" href="index.html#!">Feedback</a>
                 <div class="dropdown-divider"></div>-->
              <!--   <a class="dropdown-item" href="/payment"> Payment Settings</a>-->
                 <a class="dropdown-item" href="{{ route('logout')}}">Sign Out</a>
               </div>
             </div>
           </li>
         </ul>
       </nav>

       <script>
         var navbarPosition = localStorage.getItem('navbarPosition');
         var navbarVertical = document.querySelector('.navbar-vertical');
         var navbarTopVertical = document.querySelector('.content .navbar-top');
         var navbarTop = document.querySelector('[data-layout] .navbar-top');
         var navbarTopCombo = document.querySelector('.content [data-navbar-top="combo"]');
         if (navbarPosition === 'top') {
           navbarTop.removeAttribute('style');
           navbarTopVertical.remove(navbarTopVertical);
           navbarVertical.remove(navbarVertical);
           navbarTopCombo.remove(navbarTopCombo);
         } else if (navbarPosition === 'combo') {
           navbarVertical.removeAttribute('style');
           navbarTopCombo.removeAttribute('style');
           navbarTop.remove(navbarTop);
           navbarTopVertical.remove(navbarTopVertical);
         } else {
           navbarVertical.removeAttribute('style');
           navbarTopVertical.removeAttribute('style');
           navbarTop.remove(navbarTop);
           // navbarTopCombo.remove(navbarTopCombo);
         }

       </script>
       <script>
 $('#search').on('click',function(){
     var data = document.getElementById("input").value;
     if(data != ''){
          window.location = "search?page=1&limit=12&data="+data;
     }
     else
     {
       alert("Please Enter Search Data");
     }

 });
</script>
