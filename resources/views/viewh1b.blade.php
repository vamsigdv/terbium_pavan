@extends('dashboard_layouts.adminmaster')
<style>
.marT20{ margin-top:20px;}
.smalcards .card{height:100%; }
.smalcards .col-md-3{ margin-bottom: 10px;}
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}

   .text-700 {
    color: #5e6e82!important;
}
.mb-2, .my-2 {
    margin-bottom: 0.5rem!important;
}
.align-items-center {
    -webkit-box-align: center!important;
    -ms-flex-align: center!important;
    align-items: center!important;
}
.media {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start
}
.svg-inline--fa.fa-w-16 {
    width: 1em;
}
svg:not(:root).svg-inline--fa {
    overflow: visible;
}
.media-body {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
@media (min-width: 992px)
.pl-lg-3, .px-lg-3 {
    padding-left: 1rem!important;
}
.mr-2, .mx-2 {
    margin-right: 0.5rem!important;
}
.btn-falcon-primary:focus:not(.disabled):not(:disabled),.btn-falcon-primary:hover:not(.disabled):not(:disabled) {
    color: #1966cc!important;
    -webkit-box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08);
    box-shadow: 0 0 0 1px rgba(43,45,80,.1),0 2px 5px 0 rgba(43,45,80,.1),0 3px 9px 0 rgba(43,45,80,.08),0 1px 1.5px 0 rgba(0,0,0,.08),0 1px 2px 0 rgba(0,0,0,.08)
}
.pl-3, .px-3 {
    padding-left: 1rem!important;
}
.pr-3, .px-3 {
    padding-right: 1rem!important;
}
var avgMin
  @media (min-width: 576px) {
    .card-deck {
        display:-webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        margin-right: -.5rem;
        margin-left: -.5rem
    }

    .card-deck .card {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 0%;
        flex: 1 0 0%;
        margin-right: .5rem;
        margin-bottom: 0;
        margin-left: .5rem
    }
}
.tabcontent_movie {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>

@section('content')

<div class="card mb-3">
  
    <div class="card-body">
        
        <div class="row">
        
             
            <div class="col-lg-4">
                <h4 class="mb-3">H1B Pre- Registration </h4>
                  <input class="form-control" type="hidden" id="id" name="id" value="{{$h1b->id}}" required />
                </div>
                  </div>
                  <div class="col-lg-8 col-auto ms-auto" style="float:left">
                <a href="/h1b/{{ $h1b->id }}/edit" rel="noopener noreferrer"><button class="btn btn-falcon-success btn-sm"  type="button">Edit Below Details</button></a>
            
             </div>   
            
          
            
        </div>
        
    <div class="card-body">
    <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Email: {{ $h1b->email }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Passport {{ $h1b->passport }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Application Status: {{ $h1b->application_status }}
                </h5>
    </div>
    <div class="row gx-3">
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Employee: {{ $h1b->case_worker }}
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Experience: {{ $h1b->years_exp }}
        </h5>
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Gender: {{ $h1b->gender }}
        </h5>
    </div>
    <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Marital Status: {{ $h1b->marital_status }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Reference By :{{ $h1b->ref_by }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Documentation Verified: {{ $h1b->doc_verified }}
                </h5>
    </div>
    <div class="row gx-3">
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Co Affidevit: {{ $h1b->co_affidevit }}
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           USCIS Status: {{ $h1b->uscis_status }}
        </h5>
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           USCIS Cap: {{ $h1b->uscis_cap }}
        </h5>
    </div>
     <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                   Pre Recipt: {{ $h1b->pre_recipt }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Education Evaluation {{ $h1b->edu_evaluation }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  LCA: {{ $h1b->lca }}
                </h5>
    </div>
    <div class="row gx-3">
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           DS-160 Form: {{ $h1b->ds_form }}
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           RFE: {{ $h1b->rfe }}
        </h5>
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Phone: {{ $h1b->phone }}
        </h5>
    </div>
    <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Qualification: {{ $h1b->qualification }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Skills :{{ $h1b->skills }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Country Located: {{ $h1b->country_loc }}
                </h5>
    </div>
    <div class="row gx-3">
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           # of Dependents: {{ $h1b->dependents }}
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Attorney Employee: {{ $h1b->attorney_emp }}
        </h5>
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Reference By: {{ $h1b->ref_contact }}
        </h5>
    </div>
    <div class="row gx-3">
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Acknowledgement Email Sent: {{ $h1b->ack_email }}
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Pre-Reg Status: {{ $h1b->pre_status }}
        </h5>
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Bio Metric Details: {{ $h1b->bio_details }}
        </h5>
    </div>
    <div class="row gx-3">
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Organization: {{ $h1b->organization }}
                </h5>
                <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  H1B Receipt :{{ $h1b->h1b_recp }}
                </h5>
                 <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
                  Documentation: {{ $h1b->documentation }}
                </h5>
    </div>
    <div class="row gx-3">
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Shipping Tracker: {{ $h1b->ship_tracker }}
        </h5>
        <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           Visa Interview: {{ $h1b->visa_interview }}
        </h5>
         <h5 class="fs-0 font-weight-normal mb-3 col-md-4">
           RFE Response Status: {{ $h1b->rfe_status }}
        </h5>
    </div>
</div>
<div class="card mb-3">
    <div class="card-header">
        <div class="row flex-between-end">
            <div class="col-auto align-self-center">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
              <a class="nav-link active" href="/h1b/{{$h1b->id}}"  data-toggle="tab" href="javascript:void(0);" aria-controls="tab-profile" aria-selected="false">Documents</a>
            </li>
                    <li class="nav-item">
                         <a class="nav-link" href="/h1bcommunications/{{$h1b->id}}"  data-toggle="tab" href="javascript:void(0);" aria-controls="tab-profile" aria-selected="false">Communications</a>
                    </li>
                </ul>
            </div>
          
        </div>
    </div>
    <div class="col-auto ms-auto">
        <button class="btn btn-falcon-success btn-sm" data-bs-toggle="modal" data-bs-target="#album-modal" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span><span class="ms-1">New</span></button></div>
    </div>
    <div class="card-body pt-0">
        <table class="table table-bordered documents_datatable table-striped fs--1 mb-0" style="width:100%">
            <thead class="bg-200 text-900">
              <tr>
                <th >ID</th>
                <th >Document Name</th>
                <th >Document Link</th>
               <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
         </table>
    </div>
     <div class="modal fade bd-modal-lg" id="album-modal" tabindex="-1" role="dialog" aria-labelledby="album-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content border-0">
        <div class="modal-header px-5 position-relative modal-shape-header bg-shape">
          <div class="position-relative z-index-1 light">
            <h4 class="mb-0 text-white" id="authentication-modal-label">Add Documents</h4>
          </div><button class="btn-close btn-close-white position-absolute top-0 end-0 mt-2 me-2" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-4 px-5">
          <form action="{{ route('addh1bdocuments', $h1b->id)}}" method="post" enctype="multipart/form-data">
            @csrf
             <div class="row gx-2">
                  <input class="form-control" type="hidden" id="id" name="edit_id" value="{{$h1b->id}}"  required />
                              <div class="col-sm-5">
                                  <div class="form-group">
                                        <label for="email">Document Type<span class="text-danger">*</span>:</label>
                                        <select class="form-select" name="uploaded_document[1][name]">
                                            <option value="">Select Option</option>
                                            <option value="1">Petitioner Document</option>
                                            <option value="2">Benefecary Document</option>
                                            <option value="3">Dependent Document</option>
                                            <option value="4">Recipt</option>
                                        </select>
                                    </div>
                              </div>
                              <div class="col-sm-5">
                                  <div class="form-group">
                                        <label for="email">Attach Document<span class="text-danger">*</span>:</label>
                                        <input type="file" name="uploaded_document[1][doc]" class="form-control">
                                    </div>
                              </div>
                              <div class="col-sm-2">
                                  <div class="form-group">
                                      <label>Action</label>
                                      <button class="btn btn-falcon-success btn-sm" type="button" id="add_more_docs">Add More Docs
                                      </button>
                                 </div>
                              </div>
                          </div>
            <div class="field_wrapper"></div>
            <div class="mb-3">
              <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Submit</button>
            </div>
          </form>
         
        </div>
      </div>
    </div>
  </div>
</div>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
  $(document).ready(function() {
    
    var maxField = 4; //Input fields increment limitation
    var addButton = $('#add_more_docs'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    
    var x = 1; //Initial field counter is 1
    
    // Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++;
            var fieldHTML = '<div class="row gx-2 remove_element"><div class="col-sm-5"><div class="form-group"><label for="email">Document Type<span class="text-danger">*</span>:</label><select class="form-select" name="uploaded_document[' + x + '][name]"><option value="">Select Option</option><option value="1">Petitioner Document</option><option value="2">Benefecary Document</option><option value="3">Department Document</option><option value="4">Recipt</option></select></div></div>'; //New input field html
            fieldHTML +='<div class="col-sm-5"><div class="form-group"><label for="email">Attch Document<span class="text-danger">*</span>:</label><input type="file" name="uploaded_document[' + x + '][doc]" class="form-control"></div></div>';
            fieldHTML +='<div class="col-sm-2"><div class="form-group mt-4"><button class="btn btn-danger remove_button">Delete</button></div></div>';
             //Increase field counter
            $(wrapper).append(fieldHTML); //Add field html
        }else{
            alert('A maximum of ' + maxField + ' fields are allowed to be added. ');
        }
    });
    
    // Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).closest('.remove_element').remove(); //Remove field html
        x--; //Decrease field counter
    });
    
    
    var maxcomments = 10;
    var addComment = $('#add_comments');
    var comments_wrapper = $('.comments_wrapper'); //Input field wrapper
    var body = '<div class="row gx-2 remove_comments"><div class="col-sm-10"><div class="form-group"><label for="email">Comment<span class="text-danger">*</span>:</label><textarea class="form-control" name="communication[]"></textarea></div></div><div class="col-sm-2 mt-4"><div class="form-group"><button class="btn btn-warning remove_comments_button">Delete</button></div></div></div>';
    var y = 0; //Initial field counter is 0
    // Once add button is clicked
    $(addComment).click(function(){
        //Check maximum number of input fields
        if(y < maxcomments){ 
            y++;
           //Increase field counter
            $(comments_wrapper).append(body); //Add field html
        }else{
            alert('A maximum of ' + maxcomments + ' fields are allowed to be added. ');
        }
    });
    
    // Once remove button is clicked
    $(comments_wrapper).on('click', '.remove_comments_button', function(e){
        e.preventDefault();
        $(this).closest('.remove_comments').remove(); //Remove field html
        y--; //Decrease field counter
    });
  });
</script>  
 
@endsection