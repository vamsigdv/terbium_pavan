@extends('dashboard_layouts.adminmaster')
 @section('content')  
 <div class="card-body bg-light">
                  <div class="row">
                    <div class="col-12">
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                       
                     
                        <li class="nav-item">
                          <a class="nav-link" id="home-tab" data-toggle="tab" href="/webprivacy" role="tab" aria-controls="tab-home" aria-selected="true">Privacy</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="profile-tab" data-toggle="tab" href="/webtermsofuse" role="tab" aria-controls="tab-profile" aria-selected="false">Terms & condition</a>
                        </li>
                         
                       
                         <li class="nav-item">
                          <a class="nav-link" id="home-tab" data-toggle="tab" href="/webcookiespreference" role="tab" aria-controls="tab-home" aria-selected="true">Cookies Preference</a>
                        </li>
                        <li class="nav-item ">
                          <a class="nav-link" id="profile-tab" data-toggle="tab" href="webopensourcelicence" role="tab" aria-controls="tab-profile" aria-selected="false">Open Source Licence</a>
                        </li>
                        
                        
                        
                            <li class="nav-item">
                          <a class="nav-link active" id="profile-tab" data-toggle="tab" href="/helptab" role="tab" aria-controls="tab-profile" aria-selected="false">Help Centre</a>
                        </li>
                      
                      </ul>
                    </div>
                  </div>
</div>
 <div class="card mb-3">
           
          <div class="row g-0">
            <div class="col-lg-8 pe-lg-2">
              <div class="card mb-3">
                <div class="card-header bg-light">
                  <h5 class="mb-0" id="account">Help Center</h5>
                </div>
                <div class="card-body">
                  <h6 class="text-primary">Contact Us </h6>
                  <p class="fs-0 mb-0 fw-semi-bold">+1 5128663866</p>
                   <p class="fs-0 mb-0 fw-semi-bold">info@terbiumtech.com</p>
                  
                     <h6 class="text-primary">Address</h6>
                  <ol type="1">
                    <li>106 Taylor St, Unit B, Hutto, TX 78634</li>
                  </ol>
                  <hr class="my-4" />
       
                </div>
              </div>
            </div>
            <div class="col-lg-4 ps-lg-2">
              <div class="sticky-sidebar">
                <div class="card sticky-top">
                  <div class="card-header border-bottom">
                
                  </div>
                  <div class="card-body">
          
                  </div>
                </div>
              </div>
            </div>
          </div>
         
            </div>
  @endsection