  $(document).ready(function () {
        //customer edit
        $('body').on('click', '#customer_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/customer/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#customer-edit-modal').modal('show');
          })
        });
        $('#customer-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //customer view
        $('body').on('click', '#customer_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/customer/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#customer-view-modal').modal('show');
          })
        });
        $('#customer-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });
        //genre edit
        $('body').on('click', '#genre_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/genre/${id}/edit`;
          $.get(url, function (data) {
              $('#genre-edit-modal').modal('show');
              $('#edit_id').val(data.data.id);
              $('#edit_title').val(data.data.title);
              $('#edit_icon').val(data.data.icon);
              $('#edit_publication').val(data.data.publication);
              if (data.data.description) {
                $("#edit_description").val($("#edit_description").val() + data.data.description);
              }
              if (data.data.status==1) {
                $("#edit_status").prop("checked", true);
              }
          })
        });
        //genre view
        $('body').on('click', '#genre_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/genre/${id}/edit`;
          $.get(url, function (data) {
              $('#genre-view-modal').modal('show');
              $('#view_id').val(data.data.id);
              $('#view_title').val(data.data.title);
              $('#view_icon').val(data.data.icon);
              $('#view_publication').val(data.data.publication);
              if (data.data.description) {
                $("#view_description").val($("#view_description").val() + data.data.description);
              }
              if (data.data.status==1) {
                $("#view_status").prop("checked", true);
              }
          })
        });
        //artist edit
        $('body').on('click', '#artist_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/artist/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#artist-edit-modal').modal('show');
          })
        });
        $('#artist-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //artist view
        $('body').on('click', '#artist_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/artist/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#artist-view-modal').modal('show');
          })
        });
        $('#artist-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });

         //employee edit
         $('body').on('click', '#employee_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/employee/${id}/edit`;
          $.get(url, function (data) {
              $('#employee-edit-modal').modal('show');
              $('#edit_id').val(data.data.id);
              $('#admin_id').val(data.data.admin_id);
              $('#edit_first_name').val(data.data.first_name);
              $('#edit_middle_name').val(data.data.middle_name);
              $('#edit_last_name').val(data.data.last_name);
              $('#edit_contact').val(data.data.contact);
              $('#edit_email').val(data.data.email);
              $('#edit_adhar').val(data.data.adhar);
              $('#edit_voter').val(data.data.voter);
              $('#edit_account_number').val(data.data.account_number);
              $('#edit_ifsc').val(data.data.ifsc);
              $('#edit_password').val(data.data.password);
              if (data.data.status==1) {
                $("#edit_status").prop("checked", true);
              }
          })
        });
           //employee edit
           $('body').on('click', '#employee_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/employee/${id}/edit`;
            $.get(url, function (data) {
                $('#employee-view-modal').modal('show');
                $('#view_id').val(data.data.id);
                $('#view_first_name').val(data.data.first_name);
                $('#view_middle_name').val(data.data.middle_name);
                $('#view_last_name').val(data.data.last_name);
                $('#view_contact').val(data.data.contact);
                $('#view_email').val(data.data.email);
                $('#view_adhar').val(data.data.adhar);
                $('#view_voter').val(data.data.voter);
                $('#view_account_number').val(data.data.account_number);
                $('#view_ifsc').val(data.data.ifsc);
                $('#view_password').val(data.data.password);
                if (data.data.status==1) {
                  $("#view_status").prop("checked", true);
                }
            })
          });
          //contentapprover edit
         $('body').on('click', '#contentapprover_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/contentapprover/${id}/edit`;
          $.get(url, function (data) {
              $('#contentapprover-edit-modal').modal('show');
              $('#edit_id').val(data.data.id);
              $('#admin_id').val(data.data.admin_id);
              $('#edit_first_name').val(data.data.first_name);
              $('#edit_middle_name').val(data.data.middle_name);
              $('#edit_last_name').val(data.data.last_name);
              $('#edit_contact').val(data.data.contact);
              $('#edit_email').val(data.data.email);
              $('#edit_adhar').val(data.data.adhar);
              $('#edit_voter').val(data.data.voter);
              $('#edit_account_number').val(data.data.account_number);
              $('#edit_ifsc').val(data.data.ifsc);
              $('#edit_password').val(data.data.password);
              if (data.data.status==1) {
                $("#edit_status").prop("checked", true);
              }
          })
        });
           //contentapprover view
           $('body').on('click', '#contentapprover_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/contentapprover/${id}/edit`;
            $.get(url, function (data) {
                $('#contentapprover-view-modal').modal('show');
                $('#view_id').val(data.data.id);
                $('#view_first_name').val(data.data.first_name);
                $('#view_middle_name').val(data.data.middle_name);
                $('#view_last_name').val(data.data.last_name);
                $('#view_contact').val(data.data.contact);
                $('#view_email').val(data.data.email);
                $('#view_adhar').val(data.data.adhar);
                $('#view_voter').val(data.data.voter);
                $('#view_account_number').val(data.data.account_number);
                $('#view_ifsc').val(data.data.ifsc);
                $('#view_password').val(data.data.password);
                if (data.data.status==1) {
                  $("#view_status").prop("checked", true);
                }
            })
          });//financialteam edit
          $('body').on('click', '#financialteam_edit', function (event) {
           event.preventDefault();
           var id = $(this).data('id');
           var url = `/financialteam/${id}/edit`;
           $.get(url, function (data) {
               $('#financialteam-edit-modal').modal('show');
               $('#edit_id').val(data.data.id);
               $('#admin_id').val(data.data.admin_id);
               $('#edit_first_name').val(data.data.first_name);
               $('#edit_middle_name').val(data.data.middle_name);
               $('#edit_last_name').val(data.data.last_name);
               $('#edit_contact').val(data.data.contact);
               $('#edit_email').val(data.data.email);
               $('#edit_adhar').val(data.data.adhar);
               $('#edit_voter').val(data.data.voter);
               $('#edit_account_number').val(data.data.account_number);
               $('#edit_ifsc').val(data.data.ifsc);
               $('#edit_password').val(data.data.password);
               if (data.data.status==1) {
                 $("#edit_status").prop("checked", true);
               }
           })
         });
            //financialteam view
            $('body').on('click', '#financialteam_view', function (event) {
             event.preventDefault();
             var id = $(this).data('id');
             var url = `/financialteam/${id}/edit`;
             $.get(url, function (data) {
                 $('#financialteam-view-modal').modal('show');
                 $('#view_id').val(data.data.id);
                 $('#view_first_name').val(data.data.first_name);
                 $('#view_middle_name').val(data.data.middle_name);
                 $('#view_last_name').val(data.data.last_name);
                 $('#view_contact').val(data.data.contact);
                 $('#view_email').val(data.data.email);
                 $('#view_adhar').val(data.data.adhar);
                 $('#view_voter').val(data.data.voter);
                 $('#view_account_number').val(data.data.account_number);
                 $('#view_ifsc').val(data.data.ifsc);
                 $('#view_password').val(data.data.password);
                 if (data.data.status==1) {
                   $("#view_status").prop("checked", true);
                 }
             })
           });

           //studio edit
        $('body').on('click', '#studio_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/studio/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#studio-edit-modal').modal('show');
          })
        });
        $('#studio-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //studio view
        $('body').on('click', '#studio_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/studio/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#studio-view-modal').modal('show');
          })
        });
        $('#studio-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });


        //album edit
        $('body').on('click', '#album_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/album/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#album-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#album-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //album view
        $('body').on('click', '#album_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/album/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#album-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#album-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });

        //musicgenre edit
        $('body').on('click', '#musicgenre_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/musicgenre/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#musicgenre-edit-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#musicgenre-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });
        //musicgenre view
        $('body').on('click', '#musicgenre_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/musicgenre/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#view").append(data);
              $('#musicgenre-view-modal').modal('show');
              tinymce.init({
                selector: '.description',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
              });
          })
        });
        $('#musicgenre-view-modal').on('hidden.bs.modal', function (e) {
          $("#view").empty();
        });

          //musicartist edit
          $('body').on('click', '#musicartist_edit', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/musicartist/${id}/edit`;
            $.get(url, function (data) {
                var data = JSON.parse((data));
                $("#edit_data").append(data);
                $('#musicartist-edit-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
            })
          });
          $('#musicartist-edit-modal').on('hidden.bs.modal', function (e) {
            $("#edit_data").empty();
          });
          //musicartist view
          $('body').on('click', '#musicartist_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/musicartist/${id}`;
            $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#view").append(data);
                $('#musicartist-view-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
            })
          });
          $('#musicartist-view-modal').on('hidden.bs.modal', function (e) {
            $("#view").empty();
          });

           //mp3 edit
           $('body').on('click', '#mp3_edit', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/mp3/${id}/edit`;
            $.get(url, function (data) {
                var data = JSON.parse((data));
                $("#edit_data").append(data);
                $('#mp3-edit-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
                new Choices('#album', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#genre', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#artist', {
                  removeItemButton: true,
                  placeholder:true
                });
            })
          });
          $('#mp3-edit-modal').on('hidden.bs.modal', function (e) {
            $("#edit_data").empty();
          });
          //mp3 view
          $('body').on('click', '#mp3_view', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/mp3/${id}`;
            $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#view").append(data);
                $('#mp3-view-modal').modal('show');
                tinymce.init({
                  selector: '.description',
                  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                  toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                });
                new Choices('#album', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#genre', {
                  removeItemButton: true,
                  placeholder:true
                });
                new Choices('#artist', {
                  removeItemButton: true,
                  placeholder:true
                });
            })
          });
          $('#mp3-view-modal').on('hidden.bs.modal', function (e) {
            $("#view").empty();
          });

           //pending movie edit
         $('body').on('click', '#pendingmovie_edit_new', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/pendingmovies/${id}/edit`;
          $.get(url, function (data) {
              $('#pendingmovie-edit-modal').modal('show');
              $('#id').val(data.data.id);
              $('#name').val(data.data.name);
              $('#amount_required').val(data.data.amount_required);
              $('#amount_given').val(data.data.amount_given);
          })
        });
           //pendingmovie view
           $('body').on('click', '#pendingmovie_view_new', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = `/pendingmovies/${id}/edit`;
            $.get(url, function (data) {
              $('#pendingmovie-view-modal').modal('show');
              $('#view_name').val(data.data.name);
              $('#view_amount_required').val(data.data.amount_required);
              $('#view_amount_given').val(data.data.amount_given);
            })
          })
           //transaction movie edit
         $('body').on('click', '#transaction_edit', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}/edit`;
          $.get(url, function (data) {
              var data = JSON.parse((data));
              $("#edit_data").append(data);
              $('#transactions-edit-modal').modal('show');

          })
        });
        $('body').on('click', '#transaction_view', function (event) {
          event.preventDefault();
          var id = $(this).data('id');
          var url = `/transactions/${id}`;
          $.get(url, function (data) {
            var data = JSON.parse((data));
            $("#edit_data").append(data);
            $('#transactions-edit-modal').modal('show');
          })
        })
        $('#transactions-edit-modal').on('hidden.bs.modal', function (e) {
          $("#edit_data").empty();
        });

        //common delete
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('body').on('click', '.delete-confirm', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        const redirect = $(this).data("url");
        swal({
            title: 'Are you sure?',
            text: 'This record and it`s details will be permanantly deleted!',
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
          if (value) {
            $.ajax({
              url: url,
              type: 'DELETE',
              success: function(result) {
                window.location.href = redirect;
              },
              error: function (jqXHR, textStatus, errorThrown) {
               console.log(errorThrown);
              }
          });
          }

        });
      });
      //remove modal data on hide
      $('.editmodal').on('hidden.bs.modal', function (e) {
        $(this)
          .find("input,textarea,select")
            .val('')
            .end()
          .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
        });
      //creating slug
      $('body').on('keyup change', '#title', function (event) {
      var Text = $(this).val();
      Text = Text.toLowerCase();
      Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
      $("#slug").val(Text);
      });
      $('body').on('keyup change', '#edit_title', function (event) {
      var Text = $(this).val();
      Text = Text.toLowerCase();
      Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
      $("#edit_slug").val(Text);
      });

      $(function () {
        //customers table
        var table = $('.customer_datatable').DataTable({
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/customer",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'status', name: 'status',
                  render: function( data) {
                    if (data=='1')
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                            else
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                            endif
                        }
              },
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
         //genre table
         var table = $('.genre_datatable').DataTable({
          "order": [[ 0, "desc" ]] ,
            processing: true,
            serverSide: true,
            ajax: "/genre",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'icon', name: 'icon'},
                {data: 'status', name: 'status',
                  render: function( data) {
                    if (data=='1')
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                            else
                              return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                            endif
                        }
              },
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
          //genre table
          var table = $('.artist_datatable').DataTable({
            "order": [[ 0, "desc" ]] ,
              processing: true,
              serverSide: true,
              ajax: "/artist",
              columns: [
                  {data: 'id', name: 'id'},
                  {data: 'title', name: 'title'},
                  {data: 'number', name: 'number'},
                  {data: 'type', name: 'type'},
                  {data: 'status', name: 'status',
                    render: function( data) {
                      if (data=='1')
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                              else
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                              endif
                          }
                },
                  {data: 'action', name: 'action', orderable: false, searchable: false},
              ]
          });
          //employee table
          var table = $('.employee_datatable').DataTable({
            "order": [[ 0, "desc" ]] ,
              processing: true,
              serverSide: true,
              ajax: "/employee",
              columns: [
                  {data: 'id', name: 'id'},
                  {data: 'first_name', name: 'first_name'},
                  // {data: 'last_name', name: 'last_name'},
                  {data: 'contact', name: 'contact'},
                  {data: 'email', name: 'email'},
                  {data: 'status', name: 'status',
                    render: function( data) {
                      if (data=='1')
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                              else
                                return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                              endif
                          }
                },
                  {data: 'action', name: 'action', orderable: false, searchable: false},
              ]
          });
             //contentapprover table
             var table = $('.contentapprover_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/contentapprover",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'first_name', name: 'first_name'},
                    // {data: 'last_name', name: 'last_name'},
                    {data: 'contact', name: 'contact'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            //financialteam table
            var table = $('.financialteam_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/financialteam",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'first_name', name: 'first_name'},
                    // {data: 'last_name', name: 'last_name'},
                    {data: 'contact', name: 'contact'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
             //studio table
             var table = $('.studio_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/studio",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'studio_name', name: 'studio_name'},
                    // {data: 'last_name', name: 'last_name'},
                    {data: 'contact', name: 'contact'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
              //movie table
              var table = $('.movie_datatable_new').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/megha/public/movie",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                      // {data: 'actors', name: 'actors'},
                      // {data: 'directors', name: 'directors'},
                      {data: 'release_date', name: 'release_date'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
                //album table
             var table = $('.album_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/album",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            //musicgenre table
            var table = $('.musicgenre_datatable').DataTable({
              "order": [[ 0, "desc" ]] ,
                processing: true,
                serverSide: true,
                ajax: "/musicgenre",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'status', name: 'status',
                      render: function( data) {
                        if (data=='1')
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                else
                                  return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                endif
                            }
                  },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
              //musicartist table
              var table = $('.musicartist_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/musicartist",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'title', name: 'title'},
                      {data: 'status', name: 'status',
                        render: function( data) {
                          if (data=='1')
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                  else
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                  endif
                              }
                    },
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              //mp3 table
              var table = $('.mp3_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/mp3",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'title', name: 'title'},
                      {data: 'duration', name: 'duration'},
                      {data: 'status', name: 'status',
                        render: function( data) {
                          if (data=='1')
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>';
                                  else
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                  endif
                              }
                    },
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
               //pending movie table
               var table = $('.pendingmovie_datatable_new').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/pendingmovies",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'name', name: 'name'},
                      {data: 'amount_required', name: 'amount_required'},
                      // {data: 'amount_given', name: 'amount_given'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
              // movie table
              var table = $('.transactions_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax: "/transactions",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'amount_required', name: 'amount_required'},
                      {data: 'amount_given', name: 'amount_given'},
                      {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
                //stories table
                var table = $('.stories_datatable').DataTable({
                  "order": [[ 0, "desc" ]] ,
                    processing: true,
                    serverSide: true,
                    ajax: "/stories",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        // {data: 'actors', name: 'actors'},
                        // {data: 'directors', name: 'directors'},
                        {data: 'release_date', name: 'release_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                 //shortfilms table
                 var table = $('.shortfilms_datatable').DataTable({
                  "order": [[ 0, "desc" ]] ,
                    processing: true,
                    serverSide: true,
                    ajax: "/shortfilms",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        // {data: 'actors', name: 'actors'},
                        // {data: 'directors', name: 'directors'},
                        {data: 'release_date', name: 'release_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                //schemes table
                var table = $('.schemes_datatable').DataTable({
                  "order": [[ 0, "desc" ]] ,
                    processing: true,
                    serverSide: true,
                    ajax: "/schemes",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        // {data: 'actors', name: 'actors'},
                        // {data: 'directors', name: 'directors'},
                        {data: 'release_date', name: 'release_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                //influncer table
                var table = $('.influncer_datatable').DataTable({
                  "order": [[ 0, "desc" ]] ,
                    processing: true,
                    serverSide: true,
                    ajax: "/influncer",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        // {data: 'actors', name: 'actors'},
                        // {data: 'directors', name: 'directors'},
                        {data: 'release_date', name: 'release_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                //music video table
                var table = $('.musicvideo_datatable').DataTable({
                  "order": [[ 0, "desc" ]] ,
                    processing: true,
                    serverSide: true,
                    ajax: "/musicvideo",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        // {data: 'actors', name: 'actors'},
                        // {data: 'directors', name: 'directors'},
                        {data: 'release_date', name: 'release_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                        // studio transactions
              $('.studio_transaction_datatable').DataTable({
                "order": [[ 0, "desc" ]] ,
                  processing: true,
                  serverSide: true,
                  ajax:  "/studios/transactions",
                  columns: [
                      {data: 'id', name: 'id'},
                      {data: 'order_id', name: 'order_id'},
                      {data: 'sname', name: 'sname'},
                      {data: 'finance_name', name: 'finance_name'},
                      {data: 'amount', name: 'amount'},
                      // {data: 'transaction_id', name: 'transaction_id'},
                      {data: 'status', name: 'status',
                        render: function( data) {
                          if (data=='1')
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-success">Active<span class="ms-1 fas fa-check" data-fa-transform="shrink-2"></span></span>'; 
                                  else
                                    return '<span class="badge badge rounded-pill d-block p-2 badge-soft-danger">In Active<span class="ms-1 fas fa-ban" data-fa-transform="shrink-2"></span></span>';
                                  endif
                              }
                    },
                      // {data: 'action', name: 'action', orderable: false, searchable: false},
                  ]
              });
      });
  });
